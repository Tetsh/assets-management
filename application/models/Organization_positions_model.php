<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Organization_positions_model extends CRM_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add new contact position
     * @param array $data $_POST data
     */
    public function add($data)
    {
        $this->db->insert('tblcontactpositions', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            logActivity('New Contact Position Created [ID:' . $insert_id . ', Name:' . $data['name'] . ']');

            return $insert_id;
        }

        return false;
    }

    /**
    * Get contact positions where contact belongs
    * @param  mixed $id contact id
    * @return array
    */
    public function get_contact_positions($id)
    {
        $this->db->where('contact_id', $id);

        return $this->db->get('tblcontactpositions_in')->result_array();
    }

    /**
     * Get all contact positions
     * @param  string $id
     * @return mixed
     */
    public function get_positions($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get('tblcontactpositions')->row();
        }
        $this->db->order_by('name', 'asc');

        return $this->db->get('tblcontactpositions')->result_array();
    }

    /**
     * Edit contact position
     * @param  array $data $_POST data
     * @return boolean
     */
    public function edit($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tblcontactspositions', array(
            'name' => $data['name'],
        ));
        if ($this->db->affected_rows() > 0) {
            logActivity('Contact Position Updated [ID:' . $data['id'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete contact position
     * @param  mixed $id position id
     * @return boolean
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tblcontactpositions');
        if ($this->db->affected_rows() > 0) {
            $this->db->where('positionid', $id);
            $this->db->delete('tblcontactpositions_in');
            logActivity('Contact Position Deleted [ID:' . $id . ']');

            return true;
        }

        return false;
    }

    /**
    * Update/sync contact positions where belongs
    * @param  mixed $id        contact id
    * @param  mixed $positions_in
    * @return boolean
    */
    public function sync_contact_positions($id, $positions_in)
    {
        if ($positions_in == false) {
            unset($positions_in);
        }
        $affectedRows    = 0;
        $contact_positions = $this->get_contact_positions($id);
        if (sizeof($contact_positions) > 0) {
            foreach ($contact_positions as $contact_position) {
                if (isset($positions_in)) {
                    if (!in_array($contact_position['positionid'], $positions_in)) {
                        $this->db->where('contact_id', $id);
                        $this->db->where('id', $contact_position['id']);
                        $this->db->delete('tblcontactpositions_in');
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                } else {
                    $this->db->where('contact_id', $id);
                    $this->db->delete('tblcontactpositions_in');
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
            if (isset($positions_in)) {
                foreach ($positions_in as $position) {
                    $this->db->where('contact_id', $id);
                    $this->db->where('positionid', $position);
                    $_exists = $this->db->get('tblcontactpositions_in')->row();
                    if (!$_exists) {
                        if (empty($position)) {
                            continue;
                        }
                        $this->db->insert('tblcontactpositions_in', array(
                            'contact_id' => $id,
                            'positionid' => $position,
                        ));
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                }
            }
        } else {
            if (isset($positions_in)) {
                foreach ($positions_in as $position) {
                    if (empty($position)) {
                        continue;
                    }
                    $this->db->insert('tblcontactpositions_in', array(
                        'contact_id' => $id,
                        'positionid' => $position,
                    ));
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
        }

        if ($affectedRows > 0) {
            return true;
        }

        return false;
    }
}
