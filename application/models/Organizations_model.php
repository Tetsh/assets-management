<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Organizations_model extends CRM_Model
{
    private $contact_columns;

    public function __construct()
    {
        parent::__construct();

        $this->contact_columns = do_action('contact_columns', array('firstname', 'lastname', 'email', 'phonenumber', 'title', 'password', 'send_set_password_email', 'donotsendwelcomeemail', 'permissions', 'direction', 'invoice_emails', 'transfer_emails', 'credit_note_emails', 'contract_emails', 'task_emails', 'asset_emails', 'is_primary'));

        $this->load->model(array('organization_vault_entries_model', 'organization_groups_model', 'organization_positions_model', 'statement_model'));
    }

    /**
     * Get organization object based on passed organizationid if not passed organizationid return array of all organizations
     * @param  mixed $id    organization id
     * @param  array  $where
     * @return mixed
     */
    public function get($id = '', $where = array())
    {
        $this->db->select(implode(',', prefixed_table_fields_array('tblorganizations')) . ','.get_sql_select_organization_company());

        $this->db->join('tblcountries', 'tblcountries.country_id = tblorganizations.country', 'left');
        $this->db->join('tblcontacts', 'tblcontacts.userid = tblorganizations.userid AND is_primary = 1', 'left');
        $this->db->where($where);

        if (is_numeric($id)) {
            $this->db->where('tblorganizations.userid', $id);
            $organization = $this->db->get('tblorganizations')->row();

            if (get_option('company_requires_vat_number_field') == 0) {
                $organization->vat = null;
            }

            return $organization;
        }

        $this->db->order_by('company', 'asc');

        return $this->db->get('tblorganizations')->result_array();
    }

    /**
     * Get organizations contacts
     * @param  mixed $organization_id
     * @param  array  $where       perform where in query
     * @return array
     */
    public function get_contacts($organization_id = '', $where = array('active' => 1))
    {
        $this->db->where($where);
        if ($organization_id != '') {
            $this->db->where('userid', $organization_id);
        }
        $this->db->order_by('is_primary', 'DESC');

        return $this->db->get('tblcontacts')->result_array();
    }

    /**
     * Get single contacts
     * @param  mixed $id contact id
     * @return object
     */
    public function get_contact($id)
    {
        $this->db->where('id', $id);

        return $this->db->get('tblcontacts')->row();
    }

    /**
     * @param array $_POST data
     * @param organization_request is this request from the organization area
     * @return integer Insert ID
     * Add new organization to database
     */
    public function add($data, $organization_or_lead_convert_request = false)
    {
        $contact_data = array();
        foreach ($this->contact_columns as $field) {
            if (isset($data[$field])) {
                $contact_data[$field] = $data[$field];
                // Phonenumber is also used for the company profile
                if ($field != 'phonenumber') {
                    unset($data[$field]);
                }
            }
        }

        // From organization profile register
        if (isset($data['contact_phonenumber'])) {
            $contact_data['phonenumber'] = $data['contact_phonenumber'];
            unset($data['contact_phonenumber']);
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        if (isset($data['groups_in'])) {
            $groups_in = $data['groups_in'];
            unset($data['groups_in']);
        }

        $data = $this->check_zero_columns($data);

        $data['datecreated'] = date('Y-m-d H:i:s');

        if (is_staff_logged_in()) {
            $data['addedfrom'] = get_staff_user_id();
        }

        $hook_data                = do_action('before_organization_added', array('data'=>$data));
        $data = $hook_data['data'];

        $this->db->insert('tblorganizations', $data);

        $userid = $this->db->insert_id();
        if ($userid) {
            if (isset($custom_fields)) {
                $_custom_fields = $custom_fields;
                // Possible request from the register area with 2 types of custom fields for contact and for comapny/organization
                if (count($custom_fields) == 2) {
                    unset($custom_fields);
                    $custom_fields['organizations']                = $_custom_fields['organizations'];
                    $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                } elseif (count($custom_fields) == 1) {
                    if (isset($_custom_fields['contacts'])) {
                        $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                        unset($custom_fields);
                    }
                }
                handle_custom_fields_post($userid, $custom_fields);
            }
            /**
             * Used in Import, Lead Convert, Register
             */
            if ($organization_or_lead_convert_request == true) {
                $contact_id = $this->add_contact($contact_data, $userid, $organization_or_lead_convert_request);
            }
            if (isset($groups_in)) {
                foreach ($groups_in as $group) {
                    $this->db->insert('tblorganizationgroups_in', array(
                        'organization_id' => $userid,
                        'groupid' => $group,
                    ));
                }
            }
            do_action('after_organization_added', $userid);
            $log = $data['company'];

            if ($log == '' && isset($contact_id)) {
                $log = get_contact_full_name($contact_id);
            }

            $isStaff = null;
            if (!is_organization_logged_in() && is_staff_logged_in()) {
                $log .= ' From Staff: ' . get_staff_user_id();
                $isStaff = get_staff_user_id();
            }

            logActivity('New Organization Created [' . $log . ']', $isStaff);
        }

        return $userid;
    }

    /**
     * @param  array $_POST data
     * @param  integer ID
     * @return boolean
     * Update organization informations
     */
    public function update($data, $id, $organization_request = false)
    {
        if (isset($data['update_all_other_transactions'])) {
            $update_all_other_transactions = true;
            unset($data['update_all_other_transactions']);
        }

        if (isset($data['update_credit_notes'])) {
            $update_credit_notes = true;
            unset($data['update_credit_notes']);
        }

        $affectedRows = 0;
        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        if (isset($data['groups_in'])) {
            $groups_in = $data['groups_in'];
            unset($data['groups_in']);
        }

        $data = $this->check_zero_columns($data);

        $_data = do_action('before_organization_updated', array(
            'userid' => $id,
            'data' => $data,
        ));

        $data  = $_data['data'];
        $this->db->where('userid', $id);
        $this->db->update('tblorganizations', $data);

        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }

        if (isset($update_all_other_transactions) || isset($update_credit_notes)) {
            $transactions_update = array(
                    'billing_street' => $data['billing_street'],
                    'billing_city' => $data['billing_city'],
                    'billing_state' => $data['billing_state'],
                    'billing_zip' => $data['billing_zip'],
                    'billing_country' => $data['billing_country'],
                    'shipping_street' => $data['shipping_street'],
                    'shipping_city' => $data['shipping_city'],
                    'shipping_state' => $data['shipping_state'],
                    'shipping_zip' => $data['shipping_zip'],
                    'shipping_country' => $data['shipping_country'],
                );
            if (isset($update_all_other_transactions)) {

                // Update all invoices except paid ones.
                $this->db->where('organizationid', $id);
                $this->db->where('status !=', 2);
                $this->db->update('tblinvoices', $transactions_update);
                if ($this->db->affected_rows() > 0) {
                    $affectedRows++;
                }

                // Update all transfers
                $this->db->where('organizationid', $id);
                $this->db->update('tbltransfers', $transactions_update);
                if ($this->db->affected_rows() > 0) {
                    $affectedRows++;
                }
            }
            if (isset($update_credit_notes)) {
                $this->db->where('organizationid', $id);
                $this->db->where('status !=', 2);
                $this->db->update('tblcreditnotes', $transactions_update);
                if ($this->db->affected_rows() > 0) {
                    $affectedRows++;
                }
            }
        }

        if (!isset($groups_in)) {
            $groups_in = false;
        }

        if ($this->organization_groups_model->sync_organization_groups($id, $groups_in)) {
            $affectedRows++;
        }

        if ($affectedRows > 0) {
            do_action('after_organization_updated', $id);
            logActivity('Customer Info Updated [' . $data['company'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Update contact data
     * @param  array  $data           $_POST data
     * @param  mixed  $id             contact id
     * @param  boolean $organization_request is request from organizations area
     * @return mixed
     */
    public function update_contact($data, $id, $organization_request = false)
    {
        $affectedRows = 0;
        $contact = $this->get_contact($id);
        if (empty($data['password'])) {
            unset($data['password']);
        } else {
            $this->load->helper('phpass');
            $hasher                       = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $data['password']             = $hasher->HashPassword($data['password']);
            $data['last_password_change'] = date('Y-m-d H:i:s');
        }

        $send_set_password_email = isset($data['send_set_password_email']) ? true : false;
        $set_password_email_sent = false;

        $permissions = isset($data['permissions']) ? $data['permissions'] : array();
        $data['is_primary'] = isset($data['is_primary']) ? 1 : 0;

        // Contact cant change if is primary or not
        if ($organization_request == true) {
            unset($data['is_primary']);
            if (isset($data['email'])) {
                unset($data['email']);
            }
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        if (isset($data['positions_in'])) {
            $positions_in = $data['positions_in'];
            unset($data['positions_in']);
        }

        if ($organization_request == false) {
            $data['invoice_emails'] = isset($data['invoice_emails']) ? 1 :0;
            $data['transfer_emails'] = isset($data['transfer_emails']) ? 1 :0;
            $data['credit_note_emails'] = isset($data['credit_note_emails']) ? 1 :0;
            $data['contract_emails'] = isset($data['contract_emails']) ? 1 :0;
            $data['task_emails'] = isset($data['task_emails']) ? 1 :0;
            $data['asset_emails'] = isset($data['asset_emails']) ? 1 :0;
        }

        $hook_data = do_action('before_update_contact', array('data'=>$data, 'id'=>$id));
        $data = $hook_data['data'];

        $this->db->where('id', $id);
        $this->db->update('tblcontacts', $data);

        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
            if (isset($data['is_primary']) && $data['is_primary'] == 1) {
                $this->db->where('userid', $contact->userid);
                $this->db->where('id !=', $id);
                $this->db->update('tblcontacts', array(
                    'is_primary' => 0,
                ));
            }
        }

        if ($organization_request == false) {
            $organization_permissions = $this->roles_model->get_contact_permissions($id);
            if (sizeof($organization_permissions) > 0) {
                foreach ($organization_permissions as $organization_permission) {
                    if (!in_array($organization_permission['permission_id'], $permissions)) {
                        $this->db->where('userid', $id);
                        $this->db->where('permission_id', $organization_permission['permission_id']);
                        $this->db->delete('tblcontactpermissions');
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                }
                foreach ($permissions as $permission) {
                    $this->db->where('userid', $id);
                    $this->db->where('permission_id', $permission);
                    $_exists = $this->db->get('tblcontactpermissions')->row();
                    if (!$_exists) {
                        $this->db->insert('tblcontactpermissions', array(
                            'userid' => $id,
                            'permission_id' => $permission,
                        ));
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                }
            } else {
                foreach ($permissions as $permission) {
                    $this->db->insert('tblcontactpermissions', array(
                        'userid' => $id,
                        'permission_id' => $permission,
                    ));
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
            if ($send_set_password_email) {
                $set_password_email_sent = $this->authentication_model->set_password_email($data['email'], 0);
            }
        }

        if (!isset($positions_in)) {
            $positions_in = false;
        }

        if ($this->organization_positions_model->sync_contact_positions($id, $positions_in)) {
            $affectedRows++;
        }

        if ($affectedRows > 0 && !$set_password_email_sent) {
            logActivity('Contact Updated [' . $data['firstname'] . ' ' . $data['lastname'] . ']');

            return true;
        } elseif ($affectedRows > 0 && $set_password_email_sent) {
            return array(
                'set_password_email_sent_and_profile_updated' => true,
            );
        } elseif ($affectedRows == 0 && $set_password_email_sent) {
            return array(
                'set_password_email_sent' => true,
            );
        }

        return false;
    }

    /**
     * Add new contact
     * @param array  $data               $_POST data
     * @param mixed  $organization_id        organization id
     * @param boolean $not_manual_request is manual from admin area organization profile or register, convert to lead
     */
    public function add_contact($data, $organization_id, $not_manual_request = false)
    {
        $send_set_password_email = isset($data['send_set_password_email']) ? true : false;

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        if (isset($data['permissions'])) {
            $permissions = $data['permissions'];
            unset($data['permissions']);
        }

        $send_welcome_email = true;
        if (isset($data['donotsendwelcomeemail'])) {
            $send_welcome_email = false;
        } elseif (strpos($_SERVER['HTTP_REFERER'], 'register') !== false) {
            $send_welcome_email = true;
            // If organization register set this auto contact as primary
            $data['is_primary'] = 1;
        }

        if (isset($data['is_primary'])) {
            $data['is_primary'] = 1;
            $this->db->where('userid', $organization_id);
            $this->db->update('tblcontacts', array(
                'is_primary' => 0,
            ));
        } else {
            $data['is_primary'] = 0;
        }

        $password_before_hash  = '';
        $data['userid'] = $organization_id;
        if (isset($data['password'])) {
            $password_before_hash = $data['password'];
            $this->load->helper('phpass');
            $hasher              = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $data['password']    = $hasher->HashPassword($data['password']);
        }

        $data['datecreated'] = date('Y-m-d H:i:s');

        if (!$not_manual_request) {
            $data['invoice_emails'] = isset($data['invoice_emails']) ? 1 :0;
            $data['transfer_emails'] = isset($data['transfer_emails']) ? 1 :0;
            $data['credit_note_emails'] = isset($data['credit_note_emails']) ? 1 :0;
            $data['contract_emails'] = isset($data['contract_emails']) ? 1 :0;
            $data['task_emails'] = isset($data['task_emails']) ? 1 :0;
            $data['asset_emails'] = isset($data['asset_emails']) ? 1 :0;
        }

        $hook_data = array(
            'data' => $data,
            'not_manual_request' => $not_manual_request,
        );

        $hook_data = do_action('before_create_contact', $hook_data);
        $data  = $hook_data['data'];

        $data['email'] = trim($data['email']);

        $this->db->insert('tblcontacts', $data);
        $contact_id = $this->db->insert_id();

        if ($contact_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($contact_id, $custom_fields);
            }
            // request from admin area
            if (!isset($permissions) && $not_manual_request == false) {
                $permissions = array();
            } elseif ($not_manual_request == true) {
                $permissions         = array();
                $_permissions        = get_contact_permissions();
                $default_permissions = @unserialize(get_option('default_contact_permissions'));
                if (is_array($default_permissions)) {
                    foreach ($_permissions as $permission) {
                        if (in_array($permission['id'], $default_permissions)) {
                            array_push($permissions, $permission['id']);
                        }
                    }
                }
            }

            if ($not_manual_request == true) {
                // update all email notifications to 0
                $this->db->where('id', $contact_id);
                $this->db->update('tblcontacts', array(
                    'invoice_emails'=>0,
                    'transfer_emails'=>0,
                    'credit_note_emails'=>0,
                    'contract_emails'=>0,
                    'task_emails'=>0,
                    'asset_emails'=>0,
                ));
            }
            foreach ($permissions as $permission) {
                $this->db->insert('tblcontactpermissions', array(
                    'userid' => $contact_id,
                    'permission_id' => $permission,
                ));

                // Auto set email notifications based on permissions
                if ($not_manual_request == true) {
                    if ($permission == 6) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', array('asset_emails'=>1, 'task_emails'=>1));
                    } elseif ($permission == 3) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', array('contract_emails'=>1));
                    } elseif ($permission == 2) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', array('transfer_emails'=>1));
                    } elseif ($permission == 1) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', array('invoice_emails'=>1, 'credit_note_emails'=>1));
                    }
                }
            }

            $lastAnnouncement = $this->db->query("SELECT announcementid FROM tblannouncements WHERE showtousers = 1 AND announcementid = (SELECT MAX(announcementid) FROM tblannouncements)")->row();
            if ($lastAnnouncement) {
                // Get all announcements and set it to read.
                $this->db->select('announcementid')
                ->from('tblannouncements')
                ->where('showtousers', 1)
                ->where('announcementid !=', $lastAnnouncement->announcementid);

                $announcements = $this->db->get()->result_array();
                foreach ($announcements as $announcement) {
                    $this->db->insert('tbldismissedannouncements', array(
                        'announcementid' => $announcement['announcementid'],
                        'staff' => 0,
                        'userid' => $contact_id,
                    ));
                }
            }
            if ($send_welcome_email == true) {
                $this->load->model('emails_model');
                $merge_fields = array();
                $merge_fields = array_merge($merge_fields, get_organization_contact_merge_fields($data['userid'], $contact_id, $password_before_hash));
                $this->emails_model->send_email_template('new-organization-created', $data['email'], $merge_fields);
            }

            if ($send_set_password_email) {
                $this->authentication_model->set_password_email($data['email'], 0);
            }

            logActivity('Contact Created [' . $data['firstname'] . ' ' . $data['lastname'] . ']');
            do_action('contact_created', $contact_id);

            return $contact_id;
        }

        return false;
    }

    /**
     * Used to update company details from organizations area
     * @param  array $data $_POST data
     * @param  mixed $id
     * @return boolean
     */
    public function update_company_details($data, $id)
    {
        $affectedRows = 0;
        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }
        if (isset($data['country']) && $data['country'] == '' || !isset($data['country'])) {
            $data['country'] = 0;
        }
        if (isset($data['billing_country']) && $data['billing_country'] == '') {
            $data['billing_country'] = 0;
        }
        if (isset($data['shipping_country']) && $data['shipping_country'] == '') {
            $data['shipping_country'] = 0;
        }

        // From v.1.9.4 these fields are textareas
        $data['address'] = trim($data['address']);
        $data['address'] = nl2br($data['address']);
        if (isset($data['billing_street'])) {
            $data['billing_street'] = trim($data['billing_street']);
            $data['billing_street'] = nl2br($data['billing_street']);
        }
        if (isset($data['shipping_street'])) {
            $data['shipping_street'] = trim($data['shipping_street']);
            $data['shipping_street'] = nl2br($data['shipping_street']);
        }

        $this->db->where('userid', $id);
        $this->db->update('tblorganizations', $data);
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }
        if ($affectedRows > 0) {
            do_action('organization_updated_company_info', $id);
            logActivity('Customer Info Updated From Organizations Area [' . $data['company'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Get organization staff members that are added as organization admins
     * @param  mixed $id organization id
     * @return array
     */
    public function get_admins($id)
    {
        $this->db->where('organization_id', $id);

        return $this->db->get('tblorganizationadmins')->result_array();
    }

    /**
     * Get unique staff id's of organization admins
     * @return array
     */
    public function get_organizations_admin_unique_ids()
    {
        return $this->db->query('SELECT DISTINCT(staff_id) FROM tblorganizationadmins')->result_array();
    }

    /**
     * Assign staff members as admin to organizations
     * @param  array $data $_POST data
     * @param  mixed $id   organization id
     * @return boolean
     */
    public function assign_admins($data, $id)
    {
        $affectedRows = 0;

        if (count($data) == 0) {
            $this->db->where('organization_id', $id);
            $this->db->delete('tblorganizationadmins');
            if ($this->db->affected_rows() > 0) {
                $affectedRows++;
            }
        } else {
            $current_admins     = $this->get_admins($id);
            $current_admins_ids = array();
            foreach ($current_admins as $c_admin) {
                array_push($current_admins_ids, $c_admin['staff_id']);
            }
            foreach ($current_admins_ids as $c_admin_id) {
                if (!in_array($c_admin_id, $data['organization_admins'])) {
                    $this->db->where('staff_id', $c_admin_id);
                    $this->db->where('organization_id', $id);
                    $this->db->delete('tblorganizationadmins');
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
            foreach ($data['organization_admins'] as $n_admin_id) {
                if (total_rows('tblorganizationadmins', array(
                    'organization_id' => $id,
                    'staff_id' => $n_admin_id,
                )) == 0) {
                    $this->db->insert('tblorganizationadmins', array(
                        'organization_id' => $id,
                        'staff_id' => $n_admin_id,
                        'date_assigned' => date('Y-m-d H:i:s'),
                    ));
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
        }
        if ($affectedRows > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param  integer ID
     * @return boolean
     * Delete organization, also deleting rows from, dismissed organization announcements, ticket replies, tickets, autologin, user notes
     */
    public function delete($id)
    {
        $affectedRows = 0;

        if (is_reference_in_table('organizationid', 'tblinvoices', $id)
            || is_reference_in_table('organizationid', 'tbltransfers', $id)
            || is_reference_in_table('organizationid', 'tblcreditnotes', $id)) {
            return array(
                'referenced' => true,
            );
        }

        do_action('before_organization_deleted', $id);

        $this->db->where('userid', $id);
        $this->db->delete('tblorganizations');
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;

            // Delete all tickets start here
            $this->db->where('userid', $id);
            $tickets = $this->db->get('tbltickets')->result_array();
            $this->load->model('tickets_model');
            foreach ($tickets as $ticket) {
                $this->tickets_model->delete($ticket['ticketid']);
            }

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'organization');
            $this->db->delete('tblnotes');

            // Delete all user contacts
            $this->db->where('userid', $id);
            $contacts = $this->db->get('tblcontacts')->result_array();
            foreach ($contacts as $contact) {
                $this->delete_contact($contact['id']);
            }
            // Get all organization contracts
            $this->load->model('contracts_model');
            $this->db->where('organization', $id);
            $contracts = $this->db->get('tblcontracts')->result_array();
            foreach ($contracts as $contract) {
                $this->contracts_model->delete($contract['id']);
            }
            // Delete the custom field values
            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'organizations');
            $this->db->delete('tblcustomfieldsvalues');

            // Get organization related tasks
            $this->db->where('rel_type', 'organization');
            $this->db->where('rel_id', $id);
            $tasks = $this->db->get('tblstafftasks')->result_array();

            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id']);
            }
            $this->db->where('rel_type', 'organization');
            $this->db->where('rel_id', $id);
            $this->db->delete('tblreminders');

            $this->db->where('organization_id', $id);
            $this->db->delete('tblorganizationadmins');

            $this->db->where('organization_id', $id);
            $this->db->delete('tblvault');

            $this->db->where('organization_id', $id);
            $this->db->delete('tblorganizationgroups_in');

            // Delete all assets
            $this->load->model('assets_model');
            $this->db->where('organizationid', $id);
            $assets = $this->db->get('tblassets')->result_array();
            foreach ($assets as $asset) {
                $this->assets_model->delete($asset['id']);
            }
            $this->load->model('proposals_model');
            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'organization');
            $proposals = $this->db->get('tblproposals')->result_array();
            foreach ($proposals as $proposal) {
                $this->proposals_model->delete($proposal['id']);
            }
            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'organization');
            $attachments = $this->db->get('tblfiles')->result_array();
            foreach ($attachments as $attachment) {
                $this->delete_attachment($attachment['id']);
            }

            $this->db->where('organizationid', $id);
            $expenses = $this->db->get('tblexpenses')->result_array();

            $this->load->model('expenses_model');
            foreach ($expenses as $expense) {
                $this->expenses_model->delete($expense['id']);
            }
        }
        if ($affectedRows > 0) {
            do_action('after_organization_deleted', $id);
            logActivity('Organization Deleted [' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete organization contact
     * @param  mixed $id contact id
     * @return boolean
     */
    public function delete_contact($id)
    {
        $this->db->select('userid');
        $this->db->where('id', $id);
        $result      = $this->db->get('tblcontacts')->row();
        $organization_id = $result->userid;
        do_action('before_delete_contact', $id);
        $this->db->where('id', $id);
        $this->db->delete('tblcontacts');
        if ($this->db->affected_rows() > 0) {
            if (is_dir(get_upload_path_by_type('contact_profile_images') . $id)) {
                delete_dir(get_upload_path_by_type('contact_profile_images') . $id);
            }

            $this->db->where('contact_id', $id);
            $this->db->delete('tblorganizationfiles_shares');

            $this->db->where('userid', $id);
            $this->db->where('staff', 0);
            $this->db->delete('tbldismissedannouncements');

            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'contacts');
            $this->db->delete('tblcustomfieldsvalues');

            $this->db->where('userid', $id);
            $this->db->delete('tblcontactpermissions');

            // Delete autologin if found
            $this->db->where('user_id', $id);
            $this->db->where('staff', 0);
            $this->db->delete('tbluserautologin');

            $this->db->select('ticketid');
            $this->db->where('contactid', $id);
            $this->db->where('userid', $organization_id);
            $tickets = $this->db->get('tbltickets')->result_array();

            $this->load->model('tickets_model');
            foreach ($tickets as $ticket) {
                $this->tickets_model->delete($ticket['ticketid']);
            }

            $this->db->where('contactid', $id);
            $this->db->where('userid', $organization_id);
            $this->db->delete('tblticketreplies');

            return true;
        }

        return false;
    }

    /**
     * Get organization default currency
     * @param  mixed $id organization id
     * @return mixed
     */
    public function get_organization_default_currency($id)
    {
        $this->db->select('default_currency');
        $this->db->where('userid', $id);
        $result = $this->db->get('tblorganizations')->row();
        if ($result) {
            return $result->default_currency;
        }

        return false;
    }

    /**
     *  Get organization billing details
     * @param   mixed $id   organization id
     * @return  array
     */
    public function get_organization_billing_and_shipping_details($id)
    {
        $this->db->select('billing_street,billing_city,billing_state,billing_zip,billing_country,shipping_street,shipping_city,shipping_state,shipping_zip,shipping_country');
        $this->db->from('tblorganizations');
        $this->db->where('userid', $id);

        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            $result[0]['billing_street'] = clear_textarea_breaks($result[0]['billing_street']);
            $result[0]['shipping_street'] = clear_textarea_breaks($result[0]['shipping_street']);
        }

        return $result;
    }

    /**
     * Get organization files uploaded in the organization profile
     * @param  mixed $id    organization id
     * @param  array  $where perform where
     * @return array
     */
    public function get_organization_files($id, $where = array())
    {
        $this->db->where($where);
        $this->db->where('rel_id', $id);
        $this->db->where('rel_type', 'organization');
        $this->db->order_by('dateadded', 'desc');

        return $this->db->get('tblfiles')->result_array();
    }

    /**
     * Delete organization attachment uploaded from the organization profile
     * @param  mixed $id attachment id
     * @return boolean
     */
    public function delete_attachment($id)
    {
        $this->db->where('id', $id);
        $attachment = $this->db->get('tblfiles')->row();
        $deleted    = false;
        if ($attachment) {
            if (empty($attachment->external)) {
                $relPath = get_upload_path_by_type('organization') . $attachment->rel_id . '/';
                $fullPath =$relPath.$attachment->file_name;
                unlink($fullPath);
                $fname = pathinfo($fullPath, PATHINFO_FILENAME);
                $fext = pathinfo($fullPath, PATHINFO_EXTENSION);
                $thumbPath = $relPath.$fname.'_thumb.'.$fext;
                if (file_exists($thumbPath)) {
                    unlink($thumbPath);
                }
            }

            $this->db->where('id', $id);
            $this->db->delete('tblfiles');
            if ($this->db->affected_rows() > 0) {
                $deleted = true;
                $this->db->where('file_id', $id);
                $this->db->delete('tblorganizationfiles_shares');
                logActivity('Customer Attachment Deleted [CustomerID: ' . $attachment->rel_id . ']');
            }

            if (is_dir(get_upload_path_by_type('organization') . $attachment->rel_id)) {
                // Check if no attachments left, so we can delete the folder also
                $other_attachments = list_files(get_upload_path_by_type('organization') . $attachment->rel_id);
                if (count($other_attachments) == 0) {
                    delete_dir(get_upload_path_by_type('organization') . $attachment->rel_id);
                }
            }
        }

        return $deleted;
    }

    /**
     * @param  integer ID
     * @param  integer Status ID
     * @return boolean
     * Update contact status Active/Inactive
     */
    public function change_contact_status($id, $status)
    {
        $hook_data['id']     = $id;
        $hook_data['status'] = $status;
        $hook_data           = do_action('change_contact_status', $hook_data);
        $status              = $hook_data['status'];
        $id                  = $hook_data['id'];
        $this->db->where('id', $id);
        $this->db->update('tblcontacts', array(
            'active' => $status,
        ));
        if ($this->db->affected_rows() > 0) {
            logActivity('Contact Status Changed [ContactID: ' . $id . ' Status(Active/Inactive): ' . $status . ']');

            return true;
        }

        return false;
    }

    /**
     * @param  integer ID
     * @param  integer Status ID
     * @return boolean
     * Update organization status Active/Inactive
     */
    public function change_organization_status($id, $status)
    {
        $this->db->where('userid', $id);
        $this->db->update('tblorganizations', array(
            'active' => $status,
        ));

        if ($this->db->affected_rows() > 0) {
            logActivity('Customer Status Changed [CustomerID: ' . $id . ' Status(Active/Inactive): ' . $status . ']');

            return true;
        }

        return false;
    }

    /**
     * @param  mixed $_POST data
     * @return mixed
     * Change contact password, used from organization area
     */
    public function change_contact_password($data)
    {
        $hook_data['data'] = $data;
        $hook_data         = do_action('before_contact_change_password', $hook_data);
        $data              = $hook_data['data'];

        // Get current password
        $this->db->where('id', get_contact_user_id());
        $organization = $this->db->get('tblcontacts')->row();
        $this->load->helper('phpass');
        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        if (!$hasher->CheckPassword($data['oldpassword'], $organization->password)) {
            return array(
                'old_password_not_match' => true,
            );
        }
        $update_data['password']             = $hasher->HashPassword($data['newpasswordr']);
        $update_data['last_password_change'] = date('Y-m-d H:i:s');
        $this->db->where('id', get_contact_user_id());
        $this->db->update('tblcontacts', $update_data);
        if ($this->db->affected_rows() > 0) {
            logActivity('Contact Password Changed [ContactID: ' . get_contact_user_id() . ']');

            return true;
        }

        return false;
    }

    /**
     * Get organization groups where organization belongs
     * @param  mixed $id organization id
     * @return array
     */
    public function get_organization_groups($id)
    {
        return $this->organization_groups_model->get_organization_groups($id);
    }

    /**
     * Get all organization groups
     * @param  string $id
     * @return mixed
     */
    public function get_groups($id = '')
    {
        return $this->organization_groups_model->get_groups($id);
    }

    /**
     * Delete organization groups
     * @param  mixed $id group id
     * @return boolean
     */
    public function delete_group($id)
    {
        return $this->organization_groups_model->delete($id);
    }

    /**
     * Add new organization groups
     * @param array $data $_POST data
     */
    public function add_group($data)
    {
        return $this->organization_groups_model->add($data);
    }

    /**
     * Edit organization group
     * @param  array $data $_POST data
     * @return boolean
     */
    public function edit_group($data)
    {
        return $this->organization_groups_model->edit($data);
    }

    /**
    * Create new vault entry
    * @param  array $data        $_POST data
    * @param  mixed $organization_id organization id
    * @return boolean
    */
    public function vault_entry_create($data, $organization_id)
    {
        return $this->organization_vault_entries_model->create($data, $organization_id);
    }

    /**
     * Update vault entry
     * @param  mixed $id   vault entry id
     * @param  array $data $_POST data
     * @return boolean
     */
    public function vault_entry_update($id, $data)
    {
        return $this->organization_vault_entries_model->update($id, $data);
    }

    /**
     * Delete vault entry
     * @param  mixed $id entry id
     * @return boolean
     */
    public function vault_entry_delete($id)
    {
        return $this->organization_vault_entries_model->delete($id);
    }

    /**
     * Get organization vault entries
     * @param  mixed $organization_id
     * @param  array  $where       additional wher
     * @return array
     */
    public function get_vault_entries($organization_id, $where = array())
    {
        return $this->organization_vault_entries_model->get_by_organization_id($organization_id, $where);
    }

    /**
     * Get single vault entry
     * @param  mixed $id vault entry id
     * @return object
     */
    public function get_vault_entry($id)
    {
        return $this->organization_vault_entries_model->get($id);
    }

    /**
    * Get organization statement formatted
    * @param  mixed $organization_id organization id
    * @param  string $from        date from
    * @param  string $to          date to
    * @return array
    */
    public function get_statement($organization_id, $from, $to)
    {
        return $this->statement_model->get_statement($organization_id, $from, $to);
    }

    /**
    * Send organization statement to email
    * @param  mixed $organization_id organization id
    * @param  array $send_to     array of contact emails to send
    * @param  string $from        date from
    * @param  string $to          date to
    * @param  string $cc          email CC
    * @return boolean
    */
    public function send_statement_to_email($organization_id, $send_to, $from, $to, $cc = '')
    {
        return $this->statement_model->send_statement_to_email($organization_id, $send_to, $from, $to, $cc);
    }

    public function get_organizations_distinct_countries()
    {
        return $this->db->query('SELECT DISTINCT(country_id), short_name FROM tblorganizations JOIN tblcountries ON tblcountries.country_id=tblorganizations.country')->result_array();
    }

    private function check_zero_columns($data)
    {
        if (!isset($data['show_primary_contact'])) {
            $data['show_primary_contact'] = 0;
        }

        if (isset($data['default_currency']) && $data['default_currency'] == '' || !isset($data['default_currency'])) {
            $data['default_currency'] = 0;
        }

        if (isset($data['country']) && $data['country'] == '' || !isset($data['country'])) {
            $data['country'] = 0;
        }

        if (isset($data['billing_country']) && $data['billing_country'] == '' || !isset($data['billing_country'])) {
            $data['billing_country'] = 0;
        }

        if (isset($data['shipping_country']) && $data['shipping_country'] == '' || !isset($data['shipping_country'])) {
            $data['shipping_country'] = 0;
        }

        return $data;
    }

    /**
     * Get contact positions where contact belongs
     * @param  mixed $id contact id
     * @return array
     */
    public function get_contact_positions($id)
    {
        return $this->organization_positions_model->get_contact_positions($id);
    }

    /**
     * Get all contact positions
     * @param  string $id
     * @return mixed
     */
    public function get_positions($id = '')
    {
        return $this->organization_positions_model->get_positions($id);
    }

    /**
     * Delete contact positions
     * @param  mixed $id group id
     * @return boolean
     */
    public function delete_position($id)
    {
        return $this->organization_positions_model->delete($id);
    }

    /**
     * Add new contact positions
     * @param array $data $_POST data
     */
    public function add_position($data)
    {
        return $this->organization_positions_model->add($data);
    }

    /**
     * Edit contact position
     * @param  array $data $_POST data
     * @return boolean
     */
    public function edit_position($data)
    {
        return $this->organization_positions_model->edit($data);
    }



}
