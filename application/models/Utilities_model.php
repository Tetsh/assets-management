<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Utilities_model extends CRM_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add new event
     * @param array $data event $_POST data
     */
    public function event($data)
    {
        $data['userid'] = get_staff_user_id();
        $data['start']  = to_sql_date($data['start'], true);
        if ($data['end'] == '') {
            unset($data['end']);
        } else {
            $data['end'] = to_sql_date($data['end'], true);
        }
        if (isset($data['public'])) {
            $data['public'] = 1;
        } else {
            $data['public'] = 0;
        }
        $data['description'] = nl2br($data['description']);
        if (isset($data['eventid'])) {
            unset($data['userid']);
            $this->db->where('eventid', $data['eventid']);
            $event = $this->db->get('tblevents')->row();
            if (!$event) {
                return false;
            }
            if ($event->isstartnotified == 1) {
                if ($data['start'] > $event->start) {
                    $data['isstartnotified'] = 0;
                }
            }

            $this->db->where('eventid', $data['eventid']);
            $this->db->update('tblevents', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }

            return false;
        } else {
            $this->db->insert('tblevents', $data);
            $insert_id = $this->db->insert_id();
        }
        if ($insert_id) {
            return true;
        }

        return false;
    }

    /**
     * Get event by passed id
     * @param  mixed $id eventid
     * @return object
     */
    public function get_event_by_id($id)
    {
        $this->db->where('eventid', $id);

        return $this->db->get('tblevents')->row();
    }

    /**
     * Get all user events
     * @return array
     */
    public function get_all_events($start, $end)
    {
        $is_staff_member = is_staff_member();
        $this->db->select('title,start,end,eventid,userid,color,public');
        // Check if is passed start and end date
        $this->db->where('(start BETWEEN "' . $start . '" AND "' . $end . '")');
        $this->db->where('userid', get_staff_user_id());
        if ($is_staff_member) {
            $this->db->or_where('public', 1);
        }

        return $this->db->get('tblevents')->result_array();
    }

    public function get_event($id)
    {
        $this->db->where('eventid', $id);

        return $this->db->get('tblevents')->row();
    }

    public function get_calendar_data($start, $end, $organization_id = '', $contact_id = '', $filters = false)
    {
        $is_admin                     = is_admin();
        $has_permission_tasks_view      = has_permission('tasks', '', 'view');
        $has_permission_assets_view      = has_permission('assets', '', 'view');
        $has_permission_invoices      = has_permission('invoices', '', 'view');
        $has_permission_invoices_own  = has_permission('invoices', '', 'view_own');
        $has_permission_transfers     = has_permission('transfers', '', 'view');
        $has_permission_transfers_own = has_permission('transfers', '', 'view_own');
        $has_permission_contracts     = has_permission('contracts', '', 'view');
        $has_permission_contracts_own = has_permission('contracts', '', 'view_own');
        $has_permission_proposals     = has_permission('proposals', '', 'view');
        $has_permission_proposals_own = has_permission('proposals', '', 'view_own');
        $data                         = array();

        $organization_data = false;
        if (is_numeric($organization_id) && is_numeric($contact_id)) {
            $organization_data                      = true;
            $has_contact_permission_invoices  = has_contact_permission('invoices', $contact_id);
            $has_contact_permission_transfers = has_contact_permission('transfers', $contact_id);
            $has_contact_permission_proposals = has_contact_permission('proposals', $contact_id);
            $has_contact_permission_contracts = has_contact_permission('contracts', $contact_id);
            $has_contact_permission_assets  = has_contact_permission('assets', $contact_id);
        }

        $hook_data = array(
            'data' => $data,
            'organization_data' => $organization_data,
        );

        if ($organization_data == true) {
            $hook_data['organization_id']  = $organization_id;
            $hook_data['contact_id'] = $contact_id;
        }

        $hook_data = do_action('before_fetch_events', $hook_data);
        $data      = $hook_data['data'];

        // excluded calendar_filters from post
        $ff = (count($filters) > 1 && isset($filters['calendar_filters']) ? true : false);

        if (get_option('show_invoices_on_calendar') == 1 && !$ff || $ff && array_key_exists('invoices', $filters)) {
            $this->db->select('duedate as date,number,id,organizationid,hash,'.get_sql_select_organization_company());
            $this->db->from('tblinvoices');
            $this->db->join('tblorganizations','tblorganizations.userid=tblinvoices.organizationid');
            $this->db->where_not_in('status', array(
                2,
                5,
            ));

            $this->db->where('(duedate BETWEEN "' . $start . '" AND "' . $end . '")');

            if ($organization_data) {
                $this->db->where('organizationid', $organization_id);

                if (get_option('exclude_invoice_from_organization_area_with_draft_status') == 1) {
                    $this->db->where('status !=', 6);
                }
            } else {
                if (!$has_permission_invoices) {
                    $this->db->where('tblinvoices.addedfrom', get_staff_user_id());
                }
            }
            $invoices = $this->db->get()->result_array();
            foreach ($invoices as $invoice) {
                if (!$has_permission_invoices && !$has_permission_invoices_own && !$organization_data) {
                    continue;
                } elseif ($organization_data && !$has_contact_permission_invoices) {
                    continue;
                }

                $rel_showcase = '';

                /**
                 * Show company name on calendar tooltip for admins
                 */
                if (!$organization_data) {
                    $rel_showcase = ' (' . $invoice['company'] . ')';
                }

                $number              = format_invoice_number($invoice['id']);

                $invoice['_tooltip'] = _l('calendar_invoice') . ' - ' . $number . $rel_showcase;
                $invoice['title']    = $number;
                $invoice['color']    = get_option('calendar_invoice_color');

                if (!$organization_data) {
                    $invoice['url'] = admin_url('invoices/list_invoices/' . $invoice['id']);
                } else {
                    $invoice['url'] = site_url('viewinvoice/' . $invoice['id'] . '/' . $invoice['hash']);
                }

                array_push($data, $invoice);
            }
        }
        if (get_option('show_transfers_on_calendar') == 1  && !$ff || $ff && array_key_exists('transfers', $filters)) {
            $this->db->select('number,id,organizationid,hash,CASE WHEN expirydate IS NULL THEN date ELSE expirydate END as date,'.get_sql_select_organization_company(), false);
            $this->db->from('tbltransfers');
            $this->db->join('tblorganizations','tblorganizations.userid=tbltransfers.organizationid');
            $this->db->where('status !=', 3, false);
            $this->db->where('status !=', 4, false);
            // $this->db->where('expirydate IS NOT NULL');

            $this->db->where("CASE WHEN expirydate IS NULL THEN (date BETWEEN '$start' AND '$end') ELSE (expirydate BETWEEN '$start' AND '$end') END", null, false);

            if ($organization_data) {
                $this->db->where('organizationid', $organization_id, false);

                if (get_option('exclude_transfer_from_organization_area_with_draft_status') == 1) {
                    $this->db->where('status !=', 1, false);
                }
            } else {
                if (!$has_permission_transfers) {
                    $this->db->where('tbltransfers.addedfrom', get_staff_user_id(), false);
                }
            }

            $transfers = $this->db->get()->result_array();

            foreach ($transfers as $transfer) {
                if (!$has_permission_transfers && !$has_permission_transfers_own && !$organization_data) {
                    continue;
                } elseif ($organization_data && !$has_contact_permission_transfers) {
                    continue;
                }

                $rel_showcase = '';
                if (!$organization_data) {
                    $rel_showcase = ' (' . $transfer['company'] . ')';
                }

                $number               = format_transfer_number($transfer['id']);
                $transfer['_tooltip'] = _l('calendar_transfer') . ' - ' . $number . $rel_showcase;
                $transfer['title']    = $number;
                $transfer['color']    = get_option('calendar_transfer_color');
                if (!$organization_data) {
                    $transfer['url'] = admin_url('transfers/list_transfers/' . $transfer['id']);
                } else {
                    $transfer['url'] = site_url('viewtransfer/' . $transfer['id'] . '/' . $transfer['hash']);
                }
                array_push($data, $transfer);
            }
        }
        if (get_option('show_proposals_on_calendar') == 1 && !$ff || $ff && array_key_exists('proposals', $filters)) {
            $this->db->select('subject,id,hash,CASE WHEN open_till IS NULL THEN date ELSE open_till END as date', false);
            $this->db->from('tblproposals');
            $this->db->where('status !=', 2, false);
            $this->db->where('status !=', 3, false);


            $this->db->where("CASE WHEN open_till IS NULL THEN (date BETWEEN '$start' AND '$end') ELSE (open_till BETWEEN '$start' AND '$end') END", null, false);

            if ($organization_data) {
                $this->db->where('rel_type', 'organization');
                $this->db->where('rel_id', $organization_id, false);

                if (get_option('exclude_proposal_from_organization_area_with_draft_status')) {
                    $this->db->where('status !=', 6, false);
                }
            } else {
                if (!$has_permission_proposals) {
                    $this->db->where('addedfrom', get_staff_user_id(), false);
                }
            }

            $proposals = $this->db->get()->result_array();
            foreach ($proposals as $proposal) {
                if (!$has_permission_proposals && !$has_permission_proposals_own && !$organization_data) {
                    continue;
                } elseif ($organization_data && !$has_contact_permission_proposals) {
                    continue;
                }

                $proposal['_tooltip'] = _l('proposal');
                $proposal['title']    = $proposal['subject'];
                $proposal['color']    = get_option('calendar_proposal_color');
                if (!$organization_data) {
                    $proposal['url'] = admin_url('proposals/list_proposals/' . $proposal['id']);
                } else {
                    $proposal['url'] = site_url('viewproposal/' . $proposal['id'] . '/' . $proposal['hash']);
                }
                array_push($data, $proposal);
            }
        }

        if (get_option('show_tasks_on_calendar') == 1 && !$ff || $ff && array_key_exists('tasks', $filters)) {
            $this->db->select('name as title,id,'.tasks_rel_name_select_query() . ' as rel_name,rel_id,status,CASE WHEN duedate IS NULL THEN startdate ELSE duedate END as date', false);
            $this->db->from('tblstafftasks');
            $this->db->where('status !=', 5);

            $this->db->where("CASE WHEN duedate IS NULL THEN (startdate BETWEEN '$start' AND '$end') ELSE (duedate BETWEEN '$start' AND '$end') END", null, false);

            if ($organization_data) {
                $this->db->where('rel_type', 'asset');
                $this->db->where('rel_id IN (SELECT id FROM tblassets WHERE organizationid='.$organization_id.')');
                $this->db->where('rel_id IN (SELECT asset_id FROM tblassetsettings WHERE name="view_tasks" AND value=1)');
                $this->db->where('visible_to_organization', 1);
            }

            if (!$has_permission_tasks_view && !$organization_data) {
                $this->db->where('(id IN (SELECT taskid FROM tblstafftaskassignees WHERE staffid = ' . get_staff_user_id() . '))');
            }

            $tasks = $this->db->get()->result_array();

            foreach ($tasks as $task) {
                $rel_showcase = '';

                if (!empty($task['rel_id']) && !$organization_data) {
                    $rel_showcase   = ' (' . $task['rel_name'] . ')';
                }

                $task['date'] = $task['date'];

                $name             = mb_substr($task['title'], 0, 60) . '...';
                $task['_tooltip'] = _l('calendar_task') . ' - ' . $name . $rel_showcase;
                $task['title']    = $name;
                $status = get_task_status_by_id($task['status']);
                $task['color']    = $status['color'];

                if (!$organization_data) {
                    $task['onclick'] = 'init_task_modal(' . $task['id'] . '); return false';
                    $task['url']     = '#';
                } else {
                    $task['url'] = site_url('organizations/asset/' . $task['rel_id'] . '?group=asset_tasks&taskid=' . $task['id']);
                }
                array_push($data, $task);
            }
        }

        if (!$organization_data) {
            $available_reminders = $this->app->get_available_reminders_keys();
            $hideNotifiedReminders = get_option('hide_notified_reminders_from_calendar');
            foreach ($available_reminders as $key) {
                if (get_option('show_' . $key . '_reminders_on_calendar') == 1  && !$ff || $ff && array_key_exists($key.'_reminders', $filters)) {
                    $this->db->select('date,description,firstname,lastname,creator,staff,rel_id')
                    ->from('tblreminders')
                    ->where('(date BETWEEN "' . $start . '" AND "' . $end . '")')
                    ->where('rel_type', $key)
                    ->join('tblstaff', 'tblstaff.staffid = tblreminders.staff');
                    if ($hideNotifiedReminders == '1') {
                        $this->db->where('isnotified', 0);
                    }
                    $reminders = $this->db->get()->result_array();
                    foreach ($reminders as $reminder) {
                        if ((get_staff_user_id() == $reminder['creator'] || get_staff_user_id() == $reminder['staff']) || $is_admin) {
                            $_reminder['title'] = '';

                            if (get_staff_user_id() != $reminder['staff']) {
                                $_reminder['title'] .= '(' . $reminder['firstname'] . ' ' . $reminder['lastname'] . ') ';
                            }

                            $name                  = mb_substr($reminder['description'], 0, 60) . '...';

                            $_reminder['_tooltip'] = _l('calendar_' . $key . '_reminder') . ' - ' . $name;
                            $_reminder['title'] .= $name;
                            $_reminder['date']  = $reminder['date'];
                            $_reminder['color'] = get_option('calendar_reminder_color');

                            if ($key == 'organization') {
                                $url = admin_url('organizations/organization/' . $reminder['rel_id']);
                            } elseif ($key == 'invoice') {
                                $url = admin_url('invoices/list_invoices/' . $reminder['rel_id']);
                            } elseif ($key == 'transfer') {
                                $url = admin_url('transfers/list_transfers/' . $reminder['rel_id']);
                            } elseif ($key == 'lead') {
                                $url = '#';
                                $_reminder['onclick'] = 'init_lead('.$reminder['rel_id'].'); return false;';
                            } elseif ($key == 'proposal') {
                                $url = admin_url('proposals/list_proposals/' . $reminder['rel_id']);
                            } elseif ($key == 'expense') {
                                $url = 'expenses/list_expenses/' . $reminder['rel_id'];
                            } elseif ($key == 'credit_note') {
                                $url = 'credit_notes/list_credit_notes/' . $reminder['rel_id'];
                            }

                            $_reminder['url'] = $url;
                            array_push($data, $_reminder);
                        }
                    }
                }
            }
        }

        if (get_option('show_contracts_on_calendar') == 1 && !$ff || $ff && array_key_exists('contracts', $filters)) {
            $this->db->select('subject as title,dateend,datestart,id,organization,content,'.get_sql_select_organization_company());
            $this->db->from('tblcontracts');
            $this->db->join('tblorganizations','tblorganizations.userid=tblcontracts.organization');
            $this->db->where('trash', 0);

            if ($organization_data) {
                $this->db->where('organization', $organization_id);
                $this->db->where('not_visible_to_organization', 0);
            } else {
                if (!$has_permission_contracts) {
                    $this->db->where('tblcontracts.addedfrom', get_staff_user_id());
                }
            }

            $this->db->where('(dateend > "' . date('Y-m-d') . '" AND dateend IS NOT NULL AND dateend BETWEEN "' . $start . '" AND "' . $end . '" OR datestart >"' . date('Y-m-d') . '")');


            $contracts = $this->db->get()->result_array();

            foreach ($contracts as $contract) {
                if (!$has_permission_contracts && !$has_permission_contracts_own && !$organization_data) {
                    continue;
                } elseif ($organization_data && !$has_contact_permission_contracts) {
                    continue;
                }

                $rel_showcase = '';
                if (!$organization_data) {
                    $rel_showcase = ' (' . $contract['company'] . ')';
                }

                $name                  = $contract['title'];
                $_contract['title']    = $name;
                $_contract['color']    = get_option('calendar_contract_color');
                $_contract['_tooltip'] = _l('calendar_contract') . ' - ' . $name . $rel_showcase;
                if (!$organization_data) {
                    $_contract['url'] = admin_url('contracts/contract/' . $contract['id']);
                } else {
                    if (empty($contract['content'])) {
                        // No url for contracts
                        $_contract['url'] = '#';
                    } else {
                        $_contract['url'] = site_url('organizations/contract_pdf/' . $contract['id']);
                    }
                }
                if (!empty($contract['dateend'])) {
                    $_contract['date'] = $contract['dateend'];
                } else {
                    $_contract['date'] = $contract['datestart'];
                }
                array_push($data, $_contract);
            }
        }
        //calendar_asset
        if (get_option('show_assets_on_calendar') == 1 && !$ff || $ff && array_key_exists('assets', $filters)) {
            $this->load->model('assets_model');
            $this->db->select('name as title,id,organizationid, CASE WHEN deadline IS NULL THEN start_date ELSE deadline END as date,'.get_sql_select_organization_company(), false);

            $this->db->from('tblassets');

            // Exclude cancelled and finished
            $this->db->where('status !=', 4);
            $this->db->where('status !=', 5);
            $this->db->where("CASE WHEN deadline IS NULL THEN (start_date BETWEEN '$start' AND '$end') ELSE (deadline BETWEEN '$start' AND '$end') END", null, false);

            $this->db->join('tblorganizations','tblorganizations.userid=tblassets.organizationid');

            if (!$organization_data && !$has_permission_assets_view) {
                $this->db->where('id IN (SELECT asset_id FROM tblassetmembers WHERE staff_id='.get_staff_user_id().')');
            } else if($organization_data) {
                $this->db->where('organizationid', $organization_id);
            }

            $assets = $this->db->get()->result_array();
            foreach ($assets as $asset) {
                $rel_showcase = '';

                if (!$organization_data) {
                    $rel_showcase = ' (' . $asset['company'] . ')';
                } else {
                    if (!$has_contact_permission_assets) {
                        continue;
                    }
                }

                $name                 = $asset['title'];
                $_asset['title']    = $name;
                $_asset['color']    = get_option('calendar_asset_color');
                $_asset['_tooltip'] = _l('calendar_asset') . ' - ' . $name . $rel_showcase;
                if (!$organization_data) {
                    $_asset['url'] = admin_url('assets/view/' . $asset['id']);
                } else {
                    $_asset['url'] = site_url('organizations/asset/' . $asset['id']);
                }

                $_asset['date'] = $asset['date'];

                array_push($data, $_asset);
            }
        }
        if (!$organization_data && !$ff || (!$organization_data && $ff && array_key_exists('events', $filters))) {
            $events = $this->get_all_events($start, $end);
            foreach ($events as $event) {
                if ($event['userid'] != get_staff_user_id() && !$is_admin) {
                    $event['is_not_creator'] = true;
                    $event['onclick']        = true;
                }
                $event['_tooltip'] = _l('calendar_event') . ' - ' . $event['title'];
                $event['color']    = $event['color'];
                array_push($data, $event);
            }
        }

        return $data;
    }

    /**
     * Delete user event
     * @param  mixed $id event id
     * @return boolean
     */
    public function delete_event($id)
    {
        $this->db->where('eventid', $id);
        $this->db->delete('tblevents');
        if ($this->db->affected_rows() > 0) {
            logActivity('Event Deleted [' . $id . ']');

            return true;
        }

        return false;
    }
}
