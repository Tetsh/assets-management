<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Organization_groups_model extends CRM_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add new organization group
     * @param array $data $_POST data
     */
    public function add($data)
    {
        $this->db->insert('tblorganizationsgroups', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            logActivity('New Organization Group Created [ID:' . $insert_id . ', Name:' . $data['name'] . ']');

            return $insert_id;
        }

        return false;
    }

    /**
    * Get organization groups where organization belongs
    * @param  mixed $id organization id
    * @return array
    */
    public function get_organization_groups($id)
    {
        $this->db->where('organization_id', $id);

        return $this->db->get('tblorganizationgroups_in')->result_array();
    }

    /**
     * Get all organization groups
     * @param  string $id
     * @return mixed
     */
    public function get_groups($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get('tblorganizationsgroups')->row();
        }
        $this->db->order_by('name', 'asc');

        return $this->db->get('tblorganizationsgroups')->result_array();
    }

    /**
     * Edit organization group
     * @param  array $data $_POST data
     * @return boolean
     */
    public function edit($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tblorganizationsgroups', array(
            'name' => $data['name'],
        ));
        if ($this->db->affected_rows() > 0) {
            logActivity('Organization Group Updated [ID:' . $data['id'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete organization group
     * @param  mixed $id group id
     * @return boolean
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tblorganizationsgroups');
        if ($this->db->affected_rows() > 0) {
            $this->db->where('groupid', $id);
            $this->db->delete('tblorganizationgroups_in');
            logActivity('Organization Group Deleted [ID:' . $id . ']');

            return true;
        }

        return false;
    }

    /**
    * Update/sync organization groups where belongs
    * @param  mixed $id        organization id
    * @param  mixed $groups_in
    * @return boolean
    */
    public function sync_organization_groups($id, $groups_in)
    {
        if ($groups_in == false) {
            unset($groups_in);
        }
        $affectedRows    = 0;
        $organization_groups = $this->get_organization_groups($id);
        if (sizeof($organization_groups) > 0) {
            foreach ($organization_groups as $organization_group) {
                if (isset($groups_in)) {
                    if (!in_array($organization_group['groupid'], $groups_in)) {
                        $this->db->where('organization_id', $id);
                        $this->db->where('id', $organization_group['id']);
                        $this->db->delete('tblorganizationgroups_in');
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                } else {
                    $this->db->where('organization_id', $id);
                    $this->db->delete('tblorganizationgroups_in');
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
            if (isset($groups_in)) {
                foreach ($groups_in as $group) {
                    $this->db->where('organization_id', $id);
                    $this->db->where('groupid', $group);
                    $_exists = $this->db->get('tblorganizationgroups_in')->row();
                    if (!$_exists) {
                        if (empty($group)) {
                            continue;
                        }
                        $this->db->insert('tblorganizationgroups_in', array(
                            'organization_id' => $id,
                            'groupid' => $group,
                        ));
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                }
            }
        } else {
            if (isset($groups_in)) {
                foreach ($groups_in as $group) {
                    if (empty($group)) {
                        continue;
                    }
                    $this->db->insert('tblorganizationgroups_in', array(
                        'organization_id' => $id,
                        'groupid' => $group,
                    ));
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
        }

        if ($affectedRows > 0) {
            return true;
        }

        return false;
    }
}
