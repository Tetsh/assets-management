<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transfers_model extends CRM_Model
{
    private $statuses;
    private $shipping_fields = array('shipping_street', 'shipping_city', 'shipping_city', 'shipping_state', 'shipping_zip', 'shipping_country');

    public function __construct()
    {
        parent::__construct();
        $this->statuses = do_action('before_set_transfer_statuses', array(
            1,
            2,
            5,
            3,
            4,
        ));
    }

    /**
     * Get unique sale agent for transfers / Used for filters
     * @return array
     */
    public function get_sale_agents()
    {
        return $this->db->query("SELECT DISTINCT(sale_agent) as sale_agent, CONCAT(firstname, ' ', lastname) as full_name FROM tbltransfers JOIN tblstaff on tblstaff.staffid=tbltransfers.sale_agent WHERE sale_agent != 0")->result_array();
    }

    /**
     * Get transfer/s
     * @param  mixed $id    transfer id
     * @param  array  $where perform where
     * @return mixed
     */
    public function get($id = '', $where = array())
    {
        $this->db->select('*,tbltransfers.id as id');
        $this->db->from('tbltransfers');
        $this->db->where($where);
        if (is_numeric($id)) {
            $this->db->where('tbltransfers.id', $id);
            $transfer = $this->db->get()->row();
            if ($transfer) {
                $transfer->attachments                           = $this->get_attachments($id);
                $transfer->visible_attachments_to_organization_found = false;
                foreach ($transfer->attachments as $attachment) {
                    if ($attachment['visible_to_organization'] == 1) {
                        $transfer->visible_attachments_to_organization_found = true;
                        break;
                    }
                }
                $transfer->assets = get_assets_by_type('transfer', $id);

                $transfer->organization = $this->organizations_model->get($transfer->organizationid);
            }

            return $transfer;
        }
        $this->db->order_by('number,YEAR(date)', 'desc');

        return $this->db->get()->result_array();
    }

    /**
     * Get transfer statuses
     * @return array
     */
    public function get_statuses()
    {
        return $this->statuses;
    }

    /**
     * Function that will perform transfers pipeline query
     * @param  mixed  $status
     * @param  string  $search
     * @param  integer $page
     * @param  array   $sort
     * @param  boolean $count
     * @return array
     */
    public function do_kanban_query($status, $search = '', $page = 1, $sort = array(), $count = false)
    {
        $default_pipeline_order      = get_option('default_transfers_pipeline_sort');
        $default_pipeline_order_type = get_option('default_transfers_pipeline_sort_type');
        $limit                       = get_option('transfers_pipeline_limit');

        $fields_organization    = $this->db->list_fields('tblorganizations');
        $fields_transfers = $this->db->list_fields('tbltransfers');

        $has_permission_view = has_permission('transfers', '', 'view');

        $this->db->select('tbltransfers.id,status,invoiceid,'.get_sql_select_organization_company().',total,date,expirydate,organizationid');
        $this->db->from('tbltransfers');
        $this->db->join('tblorganizations', 'tblorganizations.userid = tbltransfers.organizationid');
        $this->db->where('status', $status);
        if (!$has_permission_view) {
            $this->db->where('addedfrom', get_staff_user_id());
        }

        if ($search != '') {
            if (!_startsWith($search, '#')) {
                $where = '(';
                $i     = 0;
                foreach ($fields_organization as $f) {
                    $where .= 'tblorganizations.' . $f . ' LIKE "%' . $search . '%"';
                    $where .= ' OR ';
                    $i++;
                }
                $i = 0;
                foreach ($fields_transfers as $f) {
                    $where .= 'tbltransfers.' . $f . ' LIKE "%' . $search . '%"';
                    $where .= ' OR ';

                    $i++;
                }
                $where = substr($where, 0, -4);
                $where .= ')';
                $this->db->where($where);
            } else {
                $this->db->where('tbltransfers.id IN
                (SELECT rel_id FROM tbltags_in WHERE tag_id IN
                (SELECT id FROM tbltags WHERE name="' . strafter($search, '#') . '")
                AND tbltags_in.rel_type=\'transfer\' GROUP BY rel_id HAVING COUNT(tag_id) = 1)
                ');
            }
        }

        if (isset($sort['sort_by']) && $sort['sort_by'] && isset($sort['sort']) && $sort['sort']) {
            $this->db->order_by('tbltransfers.' . $sort['sort_by'], $sort['sort']);
        } else {
            $this->db->order_by('tbltransfers.' . $default_pipeline_order, $default_pipeline_order_type);
        }

        if ($count == false) {
            if ($page > 1) {
                $page--;
                $position = ($page * $limit);
                $this->db->limit($limit, $position);
            } else {
                $this->db->limit($limit);
            }
        }

        if ($count == false) {
            return $this->db->get()->result_array();
        } else {
            return $this->db->count_all_results();
        }
    }

    /**
     * Convert transfer to invoice
     * @param  mixed $id transfer id
     * @return mixed     New invoice ID
     */
    public function convert_to_invoice($id, $organization = false, $draft_invoice = false)
    {
        // Recurring invoice date is okey lets convert it to new invoice
        $_transfer = $this->get($id);

        $new_invoice_data = array();
        if ($draft_invoice == true) {
            $new_invoice_data['save_as_draft'] = true;
        }
        $new_invoice_data['organizationid']   = $_transfer->organizationid;
        $new_invoice_data['asset_id'] = $_transfer->asset_id;
        $new_invoice_data['number']     = get_option('next_invoice_number');
        $new_invoice_data['date']       = _d(date('Y-m-d'));
        $new_invoice_data['duedate']    = _d(date('Y-m-d'));
        if (get_option('invoice_due_after') != 0) {
            $new_invoice_data['duedate'] = _d(date('Y-m-d', strtotime('+' . get_option('invoice_due_after') . ' DAY', strtotime(date('Y-m-d')))));
        }
        $new_invoice_data['show_quantity_as'] = $_transfer->show_quantity_as;
        $new_invoice_data['currency']         = $_transfer->currency;
        $new_invoice_data['subtotal']         = $_transfer->subtotal;
        $new_invoice_data['total']            = $_transfer->total;
        $new_invoice_data['adjustment']       = $_transfer->adjustment;
        $new_invoice_data['discount_percent'] = $_transfer->discount_percent;
        $new_invoice_data['discount_total']   = $_transfer->discount_total;
        $new_invoice_data['discount_type']    = $_transfer->discount_type;
        $new_invoice_data['sale_agent']       = $_transfer->sale_agent;
        // Since version 1.0.6
        $new_invoice_data['billing_street']   = clear_textarea_breaks($_transfer->billing_street);
        $new_invoice_data['billing_city']     = $_transfer->billing_city;
        $new_invoice_data['billing_state']    = $_transfer->billing_state;
        $new_invoice_data['billing_zip']      = $_transfer->billing_zip;
        $new_invoice_data['billing_country']  = $_transfer->billing_country;
        $new_invoice_data['shipping_street']  = clear_textarea_breaks($_transfer->shipping_street);
        $new_invoice_data['shipping_city']    = $_transfer->shipping_city;
        $new_invoice_data['shipping_state']   = $_transfer->shipping_state;
        $new_invoice_data['shipping_zip']     = $_transfer->shipping_zip;
        $new_invoice_data['shipping_country'] = $_transfer->shipping_country;

        if ($_transfer->include_shipping == 1) {
            $new_invoice_data['include_shipping'] = 1;
        }

        $new_invoice_data['show_shipping_on_invoice'] = $_transfer->show_shipping_on_transfer;
        $new_invoice_data['terms']                    = get_option('predefined_terms_invoice');
        $new_invoice_data['organizationnote']               = get_option('predefined_organizationnote_invoice');
        // Set to unpaid status automatically
        $new_invoice_data['status']                   = 1;
        $new_invoice_data['adminnote']                = '';

        $this->load->model('payment_modes_model');
        $modes      = $this->payment_modes_model->get('', array(
            'expenses_only !=' => 1,
        ));
        $temp_modes = array();
        foreach ($modes as $mode) {
            if ($mode['selected_by_default'] == 0) {
                continue;
            }
            $temp_modes[] = $mode['id'];
        }
        $new_invoice_data['allowed_payment_modes'] = $temp_modes;
        $new_invoice_data['newitems']              = array();
        $custom_fields_items = get_custom_fields('items');
        $key                                       = 1;
        foreach ($_transfer->items as $item) {
            $new_invoice_data['newitems'][$key]['description']      = $item['description'];
            $new_invoice_data['newitems'][$key]['long_description'] = clear_textarea_breaks($item['long_description']);
            $new_invoice_data['newitems'][$key]['qty']              = $item['qty'];
            $new_invoice_data['newitems'][$key]['unit']             = $item['unit'];
            $new_invoice_data['newitems'][$key]['taxname']          = array();
            $taxes                                                  = get_transfer_item_taxes($item['id']);
            foreach ($taxes as $tax) {
                // tax name is in format TAX1|10.00
                array_push($new_invoice_data['newitems'][$key]['taxname'], $tax['taxname']);
            }
            $new_invoice_data['newitems'][$key]['rate']  = $item['rate'];
            $new_invoice_data['newitems'][$key]['order'] = $item['item_order'];
            foreach ($custom_fields_items as $cf) {
                $new_invoice_data['newitems'][$key]['custom_fields']['items'][$cf['id']] = get_custom_field_value($item['id'], $cf['id'], 'items', false);

                if (!defined('COPY_CUSTOM_FIELDS_LIKE_HANDLE_POST')) {
                    define('COPY_CUSTOM_FIELDS_LIKE_HANDLE_POST', true);
                }
            }
            $key++;
        }
        $this->load->model('invoices_model');
        $id = $this->invoices_model->add($new_invoice_data);
        if ($id) {
            // Customer accepted the transfer and is auto converted to invoice
            if (!is_staff_logged_in()) {
                $this->db->where('rel_type', 'invoice');
                $this->db->where('rel_id', $id);
                $this->db->delete('tblsalesactivity');
                $this->invoices_model->log_invoice_activity($id, 'invoice_activity_auto_converted_from_transfer', true, serialize(array(
                    '<a href="' . admin_url('transfers/list_transfers/' . $_transfer->id) . '">' . format_transfer_number($_transfer->id) . '</a>',
                )));
            }
            // For all cases update addefrom and sale agent from the invoice
            // May happen staff is not logged in and these values to be 0
            $this->db->where('id', $id);
            $this->db->update('tblinvoices', array(
                'addedfrom' => $_transfer->addedfrom,
                'sale_agent' => $_transfer->sale_agent,
            ));

            // Update transfer with the new invoice data and set to status accepted
            $this->db->where('id', $_transfer->id);
            $this->db->update('tbltransfers', array(
                'invoiced_date' => date('Y-m-d H:i:s'),
                'invoiceid' => $id,
                'status' => 4,
            ));
            if ($organization == false) {
                $this->log_transfer_activity($_transfer->id, 'transfer_activity_converted', false, serialize(array(
                    '<a href="' . admin_url('invoices/list_invoices/' . $id) . '">' . format_invoice_number($id) . '</a>',
                )));
            }

            do_action('transfer_converted_to_invoice', array('invoice_id'=>$id, 'transfer_id'=>$_transfer->id));
        }

        return $id;
    }

    /**
     * Copy transfer
     * @param  mixed $id transfer id to copy
     * @return mixed
     */
    public function copy($id)
    {
        $_transfer                             = $this->get($id);
        $new_transfer_data                     = array();
        $new_transfer_data['organizationid']         = $_transfer->organizationid;
        $new_transfer_data['asset_id']       = $_transfer->asset_id;
        $new_transfer_data['number']           = get_option('next_transfer_number');
        $new_transfer_data['date']             = _d(date('Y-m-d'));
        $new_transfer_data['expirydate'] = null;

        if ($_transfer->expirydate && get_option('transfer_due_after') != 0) {
            $new_transfer_data['expirydate']       = _d(date('Y-m-d', strtotime('+' . get_option('transfer_due_after') . ' DAY', strtotime(date('Y-m-d')))));
        }

        $new_transfer_data['show_quantity_as'] = $_transfer->show_quantity_as;
        $new_transfer_data['currency']         = $_transfer->currency;
        $new_transfer_data['subtotal']         = $_transfer->subtotal;
        $new_transfer_data['total']            = $_transfer->total;
        $new_transfer_data['adminnote']        = $_transfer->adminnote;
        $new_transfer_data['adjustment']       = $_transfer->adjustment;
        $new_transfer_data['discount_percent'] = $_transfer->discount_percent;
        $new_transfer_data['discount_total']   = $_transfer->discount_total;
        $new_transfer_data['discount_type']    = $_transfer->discount_type;
        $new_transfer_data['terms']            = $_transfer->terms;
        $new_transfer_data['sale_agent']       = $_transfer->sale_agent;
        $new_transfer_data['reference_no']     = $_transfer->reference_no;
        // Since version 1.0.6
        $new_transfer_data['billing_street']   = clear_textarea_breaks($_transfer->billing_street);
        $new_transfer_data['billing_city']     = $_transfer->billing_city;
        $new_transfer_data['billing_state']    = $_transfer->billing_state;
        $new_transfer_data['billing_zip']      = $_transfer->billing_zip;
        $new_transfer_data['billing_country']  = $_transfer->billing_country;
        $new_transfer_data['shipping_street']  = clear_textarea_breaks($_transfer->shipping_street);
        $new_transfer_data['shipping_city']    = $_transfer->shipping_city;
        $new_transfer_data['shipping_state']   = $_transfer->shipping_state;
        $new_transfer_data['shipping_zip']     = $_transfer->shipping_zip;
        $new_transfer_data['shipping_country'] = $_transfer->shipping_country;
        if ($_transfer->include_shipping == 1) {
            $new_transfer_data['include_shipping'] = $_transfer->include_shipping;
        }
        $new_transfer_data['show_shipping_on_transfer'] = $_transfer->show_shipping_on_transfer;
        // Set to unpaid status automatically
        $new_transfer_data['status']                    = 1;
        $new_transfer_data['organizationnote']                = $_transfer->organizationnote;
        $new_transfer_data['adminnote']                 = '';
        $new_transfer_data['newitems']                  = array();
        $custom_fields_items = get_custom_fields('items');
        $key                                            = 1;
        foreach ($_transfer->items as $item) {
            $new_transfer_data['newitems'][$key]['description']      = $item['description'];
            $new_transfer_data['newitems'][$key]['long_description'] = clear_textarea_breaks($item['long_description']);
            $new_transfer_data['newitems'][$key]['qty']              = $item['qty'];
            $new_transfer_data['newitems'][$key]['unit']             = $item['unit'];
            $new_transfer_data['newitems'][$key]['taxname']          = array();
            $taxes                                                   = get_transfer_item_taxes($item['id']);
            foreach ($taxes as $tax) {
                // tax name is in format TAX1|10.00
                array_push($new_transfer_data['newitems'][$key]['taxname'], $tax['taxname']);
            }
            $new_transfer_data['newitems'][$key]['rate']  = $item['rate'];
            $new_transfer_data['newitems'][$key]['order'] = $item['item_order'];
            foreach($custom_fields_items as $cf) {

                $new_transfer_data['newitems'][$key]['custom_fields']['items'][$cf['id']] = get_custom_field_value($item['id'],$cf['id'],'items',false);

                if(!defined('COPY_CUSTOM_FIELDS_LIKE_HANDLE_POST')) {
                    define('COPY_CUSTOM_FIELDS_LIKE_HANDLE_POST',true);
                }

            }
            $key++;
        }
        $id = $this->add($new_transfer_data);
        if ($id) {
            $custom_fields = get_custom_fields('transfer');
            foreach ($custom_fields as $field) {
                $value = get_custom_field_value($_transfer->id, $field['id'], 'transfer');
                if ($value == '') {
                    continue;
                }

                $this->db->insert('tblcustomfieldsvalues', array(
                    'relid' => $id,
                    'fieldid' => $field['id'],
                    'fieldto' => 'transfer',
                    'value' => $value,
                ));
            }

            $tags = get_tags_in($_transfer->id, 'transfer');
            handle_tags_save($tags, $id, 'transfer');

            logActivity('Copied Estimate ' . format_transfer_number($_transfer->id));

            return $id;
        }

        return false;
    }

    /**
     * Performs transfers totals status
     * @param  array $data
     * @return array
     */
    public function get_transfers_total($data)
    {
        $statuses = $this->get_statuses();
        $this->load->model('currencies_model');
        if (isset($data['currency'])) {
            $currencyid = $data['currency'];
        } elseif (isset($data['organization_id']) && $data['organization_id'] != '') {
            $currencyid = $this->organizations_model->get_organization_default_currency($data['organization_id']);
            if ($currencyid == 0) {
                $currencyid = $this->currencies_model->get_base_currency()->id;
            }
        } elseif (isset($data['asset_id']) && $data['asset_id'] != '') {
            $this->load->model('assets_model');
            $currencyid = $this->assets_model->get_currency($data['asset_id'])->id;
        } else {
            $currencyid = $this->currencies_model->get_base_currency()->id;
        }

        $symbol = $this->currencies_model->get_currency_symbol($currencyid);
        $where  = '';
        if (isset($data['organization_id']) && $data['organization_id'] != '') {
            $where = ' AND organizationid=' . $data['organization_id'];
        }

        if (isset($data['asset_id']) && $data['asset_id'] != '') {
            $where .= ' AND asset_id=' . $data['asset_id'];
        }

        if (!has_permission('transfers', '', 'view')) {
            $where .= ' AND addedfrom=' . get_staff_user_id();
        }
        $sql = 'SELECT';
        foreach ($statuses as $transfer_status) {
            $sql .= '(SELECT SUM(total) FROM tbltransfers WHERE status=' . $transfer_status;
            $sql .= ' AND currency =' . $currencyid;
            if (isset($data['years']) && count($data['years']) > 0) {
                $sql .= ' AND YEAR(date) IN (' . implode(', ', $data['years']) . ')';
            } else {
                $sql .= ' AND YEAR(date) = '.date('Y');
            }
            $sql .= $where;
            $sql .= ') as "' . $transfer_status . '",';
        }

        $sql     = substr($sql, 0, -1);
        $result  = $this->db->query($sql)->result_array();
        $_result = array();
        $i       = 1;
        foreach ($result as $key => $val) {
            foreach ($val as $status => $total) {
                $_result[$i]['total']  = $total;
                $_result[$i]['symbol'] = $symbol;
                $_result[$i]['status'] = $status;
                $i++;
            }
        }
        $_result['currencyid'] = $currencyid;

        return $_result;
    }

    /**
     * Insert new transfer to database
     * @param array $data invoiec data
     * @return mixed - false if not insert, transfer ID if succes
     */
    public function add($data)
    {
        $data['datecreated'] = date('Y-m-d H:i:s');

        $data['addedfrom']   = get_staff_user_id();

        $data['prefix']        = get_option('transfer_prefix');

        $data['number_format'] = get_option('transfer_number_format');

        $save_and_send = isset($data['save_and_send']);

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        $data['hash'] = app_generate_hash();
        $tags = isset($data['tags']) ? $data['tags'] : '';

        $assets               = array();
        if (isset($data['newassets'])) {
            $assets = $data['newassets'];
            unset($data['newassets']);
        }

        $data = $this->map_shipping_columns($data);

        $data['billing_street'] = trim($data['billing_street']);
        $data['billing_street'] = nl2br($data['billing_street']);

        if (isset($data['shipping_street'])) {
            $data['shipping_street'] = trim($data['shipping_street']);
            $data['shipping_street'] = nl2br($data['shipping_street']);
        }

        $hook_data = do_action('before_transfer_added', array(
            'data' => $data,
            'assets' => $assets,
        ));

        $data  = $hook_data['data'];
        $assets = $hook_data['assets'];



        $this->db->insert('tbltransfers', $data);
        $insert_id = $this->db->insert_id();

        if ($insert_id) {
            // Update next transfer number in settings
            $this->db->where('name', 'next_transfer_number');
            $this->db->set('value', 'value+1', false);
            $this->db->update('tbloptions');

            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }

            handle_tags_save($tags, $insert_id, 'transfer');

            foreach ($assets as $key => $asset) {
                if ($assetid = add_new_transfer_asset_post($asset, $insert_id, 'transfer')) {
                    // die(print_r($assetid));
                    _maybe_insert_post_item_tax($assetid, $asset, $insert_id, 'transfer');
                }
            }

            update_sales_total_tax_column($insert_id, 'transfer', 'tbltransfers');
            $this->log_transfer_activity($insert_id, 'transfer_activity_created');

            do_action('after_transfer_added', $insert_id);

            if ($save_and_send === true) {
                $this->send_transfer_to_organization($insert_id, '', true, '', true);
            }

            return $insert_id;
        }

        return false;
    }

    /**
     * Get item by id
     * @param  mixed $id item id
     * @return object
     */
    public function get_transfer_item($id)
    {
        $this->db->where('id', $id);

        return $this->db->get('tblitems_in')->row();
    }

    /**
     * Update transfer data
     * @param  array $data transfer data
     * @param  mixed $id   transferid
     * @return boolean
     */
    public function update($data, $id)
    {
        $affectedRows             = 0;

        $data['number']           = trim($data['number']);

        $original_transfer        = $this->get($id);

        $original_status          = $original_transfer->status;

        $original_number          = $original_transfer->number;

        $original_number_formatted = format_transfer_number($id);

        $save_and_send = isset($data['save_and_send']);

        $items = array();
        if (isset($data['items'])) {
            $items = $data['items'];
            unset($data['items']);
        }

        $newitems = array();
        if (isset($data['newitems'])) {
            $newitems = $data['newitems'];
            unset($data['newitems']);
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        if (isset($data['tags'])) {
            if (handle_tags_save($data['tags'], $id, 'transfer')) {
                $affectedRows++;
            }
        }

        $data['billing_street'] = trim($data['billing_street']);
        $data['billing_street'] = nl2br($data['billing_street']);

        $data['shipping_street'] = trim($data['shipping_street']);
        $data['shipping_street'] = nl2br($data['shipping_street']);

        $data = $this->map_shipping_columns($data);

        $hook_data  = do_action('before_transfer_updated', array(
            'data'=>$data,
            'id'=>$id,
            'items'=>$items,
            'newitems'=>$newitems,
            'removed_items'=>isset($data['removed_items']) ? $data['removed_items'] : array(),
        ));

        $data                  = $hook_data['data'];
        $data['removed_items'] = $hook_data['removed_items'];
        $items                 = $hook_data['items'];
        $newitems              = $hook_data['newitems'];

        // Delete items checked to be removed from database
        foreach ($data['removed_items'] as $remove_item_id) {
            $original_item = $this->get_transfer_item($remove_item_id);
            if (handle_removed_sales_item_post($remove_item_id, 'transfer')) {
                $affectedRows++;
                $this->log_transfer_activity($id, 'invoice_transfer_activity_removed_item', false, serialize(array(
                        $original_item->description,
                    )));
            }
        }

        unset($data['removed_items']);

        $this->db->where('id', $id);
        $this->db->update('tbltransfers', $data);

        if ($this->db->affected_rows() > 0) {
            // Check for status change
            if ($original_status != $data['status']) {
                $this->log_transfer_activity($original_transfer->id, 'not_transfer_status_updated', false, serialize(array(
                    '<original_status>' . $original_status . '</original_status>',
                    '<new_status>' . $data['status'] . '</new_status>',
                )));
                if ($data['status'] == 2) {
                    $this->db->where('id', $id);
                    $this->db->update('tbltransfers', array('sent'=>1, array('datesend'=>date('Y-m-d H:i:s'))));
                }
            }
            if ($original_number != $data['number']) {
                $this->log_transfer_activity($original_transfer->id, 'transfer_activity_number_changed', false, serialize(array(
                    $original_number_formatted,
                    format_transfer_number($original_transfer->id),
                )));
            }
            $affectedRows++;
        }

        foreach ($items as $key => $item) {
            $original_item    = $this->get_transfer_item($item['itemid']);

            if (update_sales_item_post($item['itemid'], $item, 'item_order')) {
                $affectedRows++;
            }

            if (update_sales_item_post($item['itemid'], $item, 'unit')) {
                $affectedRows++;
            }

            if (update_sales_item_post($item['itemid'], $item, 'rate')) {
                $this->log_transfer_activity($id, 'invoice_transfer_activity_updated_item_rate', false, serialize(array(
                        $original_item->rate,
                        $item['rate'],
                    )));
                $affectedRows++;
            }

            if (update_sales_item_post($item['itemid'], $item, 'qty')) {
                $this->log_transfer_activity($id, 'invoice_transfer_activity_updated_qty_item', false, serialize(array(
                        $item['description'],
                        $original_item->qty,
                        $item['qty'],
                    )));
                $affectedRows++;
            }

            if (update_sales_item_post($item['itemid'], $item, 'description')) {
                $this->log_transfer_activity($id, 'invoice_transfer_activity_updated_item_short_description', false, serialize(array(
                        $original_item->description,
                        $item['description'],
                    )));
                $affectedRows++;
            }

            if (update_sales_item_post($item['itemid'], $item, 'long_description')) {
                $this->log_transfer_activity($id, 'invoice_transfer_activity_updated_item_long_description', false, serialize(array(
                        $original_item->long_description,
                        $item['long_description'],
                    )));
                $affectedRows++;
            }

            if (isset($item['custom_fields'])) {
                if (handle_custom_fields_post($item['itemid'], $item['custom_fields'])) {
                    $affectedRows++;
                }
            }

            if (!isset($item['taxname']) || (isset($item['taxname']) && count($item['taxname']) == 0)) {
                if (delete_taxes_from_item($item['itemid'], 'transfer')) {
                    $affectedRows++;
                }
            } else {
                $item_taxes        = get_transfer_item_taxes($item['itemid']);
                $_item_taxes_names = array();
                foreach ($item_taxes as $_item_tax) {
                    array_push($_item_taxes_names, $_item_tax['taxname']);
                }

                $i = 0;
                foreach ($_item_taxes_names as $_item_tax) {
                    if (!in_array($_item_tax, $item['taxname'])) {
                        $this->db->where('id', $item_taxes[$i]['id'])
                            ->delete('tblitemstax');
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                    $i++;
                }
                if (_maybe_insert_post_item_tax($item['itemid'], $item, $id, 'transfer')) {
                    $affectedRows++;
                }
            }
        }

        foreach ($newitems as $key => $item) {
            if ($new_item_added = add_new_sales_item_post($item, $id, 'transfer')) {
                _maybe_insert_post_item_tax($new_item_added, $item, $id, 'transfer');
                $this->log_transfer_activity($id, 'invoice_transfer_activity_added_item', false, serialize(array(
                    $item['description'],
                )));
                $affectedRows++;
            }
        }

        if ($affectedRows > 0) {
            update_sales_total_tax_column($id, 'transfer', 'tbltransfers');
        }

        if ($save_and_send === true) {
            $this->send_transfer_to_organization($id, '', true, '', true);
        }

        if ($affectedRows > 0) {
            do_action('after_transfer_updated', $id);

            return true;
        }

        return false;
    }

    public function mark_action_status($action, $id, $organization = false)
    {
        $this->db->where('id', $id);
        $this->db->update('tbltransfers', array(
            'status' => $action,
        ));

        $notifiedUsers = array();

        if ($this->db->affected_rows() > 0) {
            $transfer = $this->get($id);
            if ($organization == true) {
                $this->db->where('staffid', $transfer->addedfrom);
                $this->db->or_where('staffid', $transfer->sale_agent);
                $staff_transfer = $this->db->get('tblstaff')->result_array();
                $invoiceid      = false;
                $invoiced       = false;

                $this->load->model('emails_model');

                $this->emails_model->set_rel_id($id);
                $this->emails_model->set_rel_type('transfer');

                $merge_fields_for_staff_email = array();
                if (!is_organization_logged_in()) {
                    $contact_id = get_primary_contact_user_id($transfer->organizationid);
                } else {
                    $contact_id = get_contact_user_id();
                }
                $merge_fields_for_staff_email = array_merge($merge_fields_for_staff_email, get_organization_contact_merge_fields($transfer->organizationid, $contact_id));
                $merge_fields_for_staff_email = array_merge($merge_fields_for_staff_email, get_transfer_merge_fields($transfer->id));


                if ($action == 4) {
                    if (get_option('transfer_auto_convert_to_invoice_on_organization_accept') == 1) {
                        $invoiceid = $this->convert_to_invoice($id, true);
                        $this->load->model('invoices_model');
                        if ($invoiceid) {
                            $invoiced = true;
                            $invoice  = $this->invoices_model->get($invoiceid);
                            $this->log_transfer_activity($id, 'transfer_activity_organization_accepted_and_converted', true, serialize(array(
                                '<a href="' . admin_url('invoices/list_invoices/' . $invoiceid) . '">' . format_invoice_number($invoice->id) . '</a>',
                            )));
                        }
                    } else {
                        $this->log_transfer_activity($id, 'transfer_activity_organization_accepted', true);
                    }

                    // Send thank you email to all contacts with permission transfers
                    $contacts = $this->organizations_model->get_contacts($transfer->organizationid, array('active'=>1, 'transfer_emails'=>1));
                    foreach ($contacts as $contact) {
                        $merge_fields = array();
                        $merge_fields = array_merge($merge_fields, get_organization_contact_merge_fields($transfer->organizationid, $contact['id']));
                        $merge_fields = array_merge($merge_fields, get_transfer_merge_fields($transfer->id));
                        $this->emails_model->send_email_template('transfer-thank-you-to-organization', $contact['email'], $merge_fields);
                    }
                    foreach ($staff_transfer as $member) {
                        $notified = add_notification(array(
                            'fromcompany' => true,
                            'touserid' => $member['staffid'],
                            'description' => 'not_transfer_organization_accepted',
                            'link' => 'transfers/list_transfers/' . $id,
                            'additional_data' => serialize(array(
                                format_transfer_number($transfer->id),
                            )),
                        ));
                        if ($notified) {
                            array_push($notifiedUsers, $member['staffid']);
                        }
                        // Send staff email notification that organization accepted transfer
                        $this->emails_model->send_email_template('transfer-accepted-to-staff', $member['email'], $merge_fields_for_staff_email);
                    }

                    pusher_trigger_notification($notifiedUsers);
                    do_action('transfer_accepted', $id);

                    return array(
                        'invoiced' => $invoiced,
                        'invoiceid' => $invoiceid,
                    );
                } elseif ($action == 3) {
                    foreach ($staff_transfer as $member) {
                        $notified = add_notification(array(
                            'fromcompany' => true,
                            'touserid' => $member['staffid'],
                            'description' => 'not_transfer_organization_declined',
                            'link' => 'transfers/list_transfers/' . $id,
                            'additional_data' => serialize(array(
                                format_transfer_number($transfer->id),
                            )),
                        ));

                        if ($notified) {
                            array_push($notifiedUsers, $member['staffid']);
                        }
                        // Send staff email notification that organization declined transfer
                        $this->emails_model->send_email_template('transfer-declined-to-staff', $member['email'], $merge_fields_for_staff_email);
                    }

                    pusher_trigger_notification($notifiedUsers);
                    $this->log_transfer_activity($id, 'transfer_activity_organization_declined', true);
                    do_action('transfer_declined', $id);

                    return array(
                        'invoiced' => $invoiced,
                        'invoiceid' => $invoiceid,
                    );
                }
            } else {
                if ($action == 2) {
                    $this->db->where('id', $id);
                    $this->db->update('tbltransfers', array('sent'=>1, 'datesend'=>date('Y-m-d H:i:s')));
                }
                // Admin marked transfer
                $this->log_transfer_activity($id, 'transfer_activity_marked', false, serialize(array(
                    '<status>' . $action . '</status>',
                )));

                return true;
            }
        }

        return false;
    }

    /**
     * Get transfer attachments
     * @param  mixed $transfer_id
     * @param  string $id          attachment id
     * @return mixed
     */
    public function get_attachments($transfer_id, $id = '')
    {
        // If is passed id get return only 1 attachment
        if (is_numeric($id)) {
            $this->db->where('id', $id);
        } else {
            $this->db->where('rel_id', $transfer_id);
        }
        $this->db->where('rel_type', 'transfer');
        $result = $this->db->get('tblfiles');
        if (is_numeric($id)) {
            return $result->row();
        } else {
            return $result->result_array();
        }
    }

    /**
     *  Delete transfer attachment
     * @param   mixed $id  attachmentid
     * @return  boolean
     */
    public function delete_attachment($id)
    {
        $attachment = $this->get_attachments('', $id);
        $deleted    = false;
        if ($attachment) {
            if (empty($attachment->external)) {
                unlink(get_upload_path_by_type('transfer') . $attachment->rel_id . '/' . $attachment->file_name);
            }
            $this->db->where('id', $attachment->id);
            $this->db->delete('tblfiles');
            if ($this->db->affected_rows() > 0) {
                $deleted = true;
                logActivity('Estimate Attachment Deleted [EstimateID: ' . $attachment->rel_id . ']');
            }

            if (is_dir(get_upload_path_by_type('transfer') . $attachment->rel_id)) {
                // Check if no attachments left, so we can delete the folder also
                $other_attachments = list_files(get_upload_path_by_type('transfer') . $attachment->rel_id);
                if (count($other_attachments) == 0) {
                    // okey only index.html so we can delete the folder also
                    delete_dir(get_upload_path_by_type('transfer') . $attachment->rel_id);
                }
            }
        }

        return $deleted;
    }

    /**
     * Delete transfer items and all connections
     * @param  mixed $id transferid
     * @return boolean
     */
    public function delete($id)
    {
        if (get_option('delete_only_on_last_transfer') == 1) {
            if (!is_last_transfer($id)) {
                return false;
            }
        }
        $transfer = $this->get($id);
        if (!is_null($transfer->invoiceid)) {
            return array(
                'is_invoiced_transfer_delete_error' => true,
            );
        }
        do_action('before_transfer_deleted', $id);
        $this->db->where('id', $id);
        $this->db->delete('tbltransfers');
        if ($this->db->affected_rows() > 0) {
            if (get_option('transfer_number_decrement_on_delete') == 1) {
                $current_next_transfer_number = get_option('next_transfer_number');
                if ($current_next_transfer_number > 1) {
                    // Decrement next transfer number to
                    $this->db->where('name', 'next_transfer_number');
                    $this->db->set('value', 'value-1', false);
                    $this->db->update('tbloptions');
                }
            }
            if (total_rows('tblproposals', array(
                'transfer_id' => $id,
            )) > 0) {
                $this->db->where('transfer_id', $id);
                $transfer = $this->db->get('tblproposals')->row();
                $this->db->where('id', $transfer->id);
                $this->db->update('tblproposals', array(
                    'transfer_id' => null,
                    'date_converted' => null,
                ));
            }

            $this->db->where('relid IN (SELECT id from tblitems_in WHERE rel_type="transfer" AND rel_id="'.$id.'")');
            $this->db->where('fieldto', 'items');
            $this->db->delete('tblcustomfieldsvalues');

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'transfer');
            $this->db->delete('tblnotes');

            $this->db->where('rel_type', 'transfer');
            $this->db->where('rel_id', $id);
            $this->db->delete('tblviewstracking');

            $this->db->where('rel_type', 'transfer');
            $this->db->where('rel_id', $id);
            $this->db->delete('tbltags_in');

            $this->db->where('rel_type', 'transfer');
            $this->db->where('rel_id', $id);
            $this->db->delete('tblreminders');

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'transfer');
            $this->db->delete('tblitems_in');


            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'transfer');
            $this->db->delete('tblitemstax');


            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'transfer');
            $this->db->delete('tblsalesactivity');


            // Delete the custom field values
            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'transfer');
            $this->db->delete('tblcustomfieldsvalues');

            $attachments = $this->get_attachments($id);
            foreach ($attachments as $attachment) {
                $this->delete_attachment($attachment['id']);
            }

            // Get related tasks
            $this->db->where('rel_type', 'transfer');
            $this->db->where('rel_id', $id);
            $tasks = $this->db->get('tblstafftasks')->result_array();
            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id']);
            }

            return true;
        }

        return false;
    }

    /**
     * Set transfer to sent when email is successfuly sended to organization
     * @param mixed $id transferid
     */
    public function set_transfer_sent($id, $emails_sent = array())
    {
        $this->db->where('id', $id);
        $this->db->update('tbltransfers', array(
            'sent' => 1,
            'datesend' => date('Y-m-d H:i:s'),
        ));
        $this->log_transfer_activity($id, 'invoice_transfer_activity_sent_to_organization', false, serialize(array(
            '<custom_data>' . implode(', ', $emails_sent) . '</custom_data>',
        )));
        // Update transfer status to sent
        $this->db->where('id', $id);
        $this->db->update('tbltransfers', array(
            'status' => 2,
        ));
    }

    /**
     * Send expiration reminder to organization
     * @param  mixed $id transfer id
     * @return boolean
     */
    public function send_expiry_reminder($id)
    {
        $transfer        = $this->get($id);
        $transfer_number = format_transfer_number($transfer->id);
        $pdf             = transfer_pdf($transfer);
        $attach          = $pdf->Output($transfer_number . '.pdf', 'S');
        $emails_sent     = array();
        $sms_sent = false;
        $sms_reminder_log = array();

         // For all cases update this to prevent sending multiple reminders eq on fail
        $this->db->where('id', $id);
        $this->db->update('tbltransfers', array(
            'is_expiry_notified' => 1,
        ));

        $contacts        = $this->organizations_model->get_contacts($transfer->organizationid, array('active'=>1, 'transfer_emails'=>1));
        $this->load->model('emails_model');

        $this->emails_model->set_rel_id($id);
        $this->emails_model->set_rel_type('transfer');

        foreach ($contacts as $contact) {
            $this->emails_model->add_attachment(array(
                    'attachment' => $attach,
                    'filename' => $transfer_number . '.pdf',
                    'type' => 'application/pdf',
                ));
            $merge_fields = array();
            $merge_fields = array_merge($merge_fields, get_organization_contact_merge_fields($transfer->organizationid, $contact['id']));
            $merge_fields = array_merge($merge_fields, get_transfer_merge_fields($transfer->id));

            if ($this->emails_model->send_email_template('transfer-expiry-reminder', $contact['email'], $merge_fields)) {
                array_push($emails_sent, $contact['email']);
            }

            if(can_send_sms_based_on_creation_date($transfer->datecreated)
                && $this->sms->trigger(SMS_TRIGGER_ESTIMATE_EXP_REMINDER, $contact['phonenumber'], $merge_fields)) {
                $sms_sent = true;
                array_push($sms_reminder_log,$contact['firstname'] . ' ('.$contact['phonenumber'].')');
            }
        }

        if (count($emails_sent) > 0 || $sms_sent) {

            if(count($emails_sent) > 0){
                $this->log_transfer_activity($id, 'not_expiry_reminder_sent', false, serialize(array(
                    '<custom_data>' . implode(', ', $emails_sent) . '</custom_data>',
                )));
            }

            if($sms_sent) {
                $this->log_transfer_activity($id, 'sms_reminder_sent_to', false, serialize(array(
                   implode(', ', $sms_reminder_log)
                )));
            }

            return true;
        }

        return false;
    }

    /**
     * Send transfer to organization
     * @param  mixed  $id        transferid
     * @param  string  $template  email template to sent
     * @param  boolean $attachpdf attach transfer pdf or not
     * @return boolean
     */
    public function send_transfer_to_organization($id, $template = '', $attachpdf = true, $cc = '', $manually = false)
    {
        $this->load->model('emails_model');

        $this->emails_model->set_rel_id($id);
        $this->emails_model->set_rel_type('transfer');

        $transfer = $this->get($id);
        if ($template == '') {
            if ($transfer->sent == 0) {
                $template = 'transfer-send-to-organization';
            } else {
                $template = 'transfer-already-send';
            }
        }
        $transfer_number = format_transfer_number($transfer->id);


        $emails_sent         = array();
        $sent                = false;
        $sent_to             = $this->input->post('sent_to');
        if ($manually === true) {
            $sent_to  = array();
            $contacts = $this->organizations_model->get_contacts($transfer->organizationid, array('active'=>1, 'transfer_emails'=>1));
            foreach ($contacts as $contact) {
                array_push($sent_to, $contact['id']);
            }
        }

        $status_now          = $transfer->status;
        $status_auto_updated = false;
        if (is_array($sent_to) && count($sent_to) > 0) {
            $i = 0;
            // Auto update status to sent in case when user sends the transfer is with status draft
            if ($status_now == 1) {
                $this->db->where('id', $transfer->id);
                $this->db->update('tbltransfers', array(
                    'status' => 2,
                ));
                $status_auto_updated = true;
            }

            if ($attachpdf) {
                $_pdf_transfer = $this->get($transfer->id);
                $pdf    = transfer_pdf($_pdf_transfer);
                $attach = $pdf->Output($transfer_number . '.pdf', 'S');
            }

            foreach ($sent_to as $contact_id) {
                if ($contact_id != '') {
                    if ($attachpdf) {
                        $this->emails_model->add_attachment(array(
                            'attachment' => $attach,
                            'filename' => $transfer_number . '.pdf',
                            'type' => 'application/pdf',
                        ));
                    }

                    if ($this->input->post('email_attachments')) {
                        $_other_attachments = $this->input->post('email_attachments');

                        foreach ($_other_attachments as $attachment) {
                            $_attachment = $this->get_attachments($id, $attachment);

                            $this->emails_model->add_attachment(array(
                                'attachment' => get_upload_path_by_type('transfer') . $id . '/' . $_attachment->file_name,
                                'filename' => $_attachment->file_name,
                                'type' => $_attachment->filetype,
                                'read' => true,
                            ));
                        }
                    }

                    $contact = $this->organizations_model->get_contact($contact_id);

                    $merge_fields = array();
                    $merge_fields = array_merge($merge_fields, get_organization_contact_merge_fields($transfer->organizationid, $contact_id));
                    $merge_fields = array_merge($merge_fields, get_transfer_merge_fields($transfer->id));
                    // Send cc only for the first contact
                    if (!empty($cc) && $i > 0) {
                        $cc = '';
                    }
                    if ($this->emails_model->send_email_template($template, $contact->email, $merge_fields, '', $cc)) {
                        $sent = true;
                        array_push($emails_sent, $contact->email);
                    }
                }
                $i++;
            }
        } else {
            return false;
        }
        if ($sent) {
            $this->set_transfer_sent($id, $emails_sent);
            do_action('transfer_sent', $id);

            return true;
        } else {
            if ($status_auto_updated) {
                // Estimate not send to organization but the status was previously updated to sent now we need to revert back to draft
                $this->db->where('id', $transfer->id);
                $this->db->update('tbltransfers', array(
                    'status' => 1,
                ));
            }
        }

        return false;
    }

    /**
     * All transfer activity
     * @param  mixed $id transferid
     * @return array
     */
    public function get_transfer_activity($id)
    {
        $this->db->where('rel_id', $id);
        $this->db->where('rel_type', 'transfer');
        $this->db->order_by('date', 'asc');

        return $this->db->get('tblsalesactivity')->result_array();
    }

    /**
     * Log transfer activity to database
     * @param  mixed $id   transferid
     * @param  string $description activity description
     */
    public function log_transfer_activity($id, $description = '', $organization = false, $additional_data = '')
    {
        $staffid   = get_staff_user_id();
        $full_name = get_staff_full_name(get_staff_user_id());
        if (DEFINED('CRON')) {
            $staffid   = '[CRON]';
            $full_name = '[CRON]';
        } elseif ($organization == true) {
            $staffid   = null;
            $full_name = '';
        }

        $this->db->insert('tblsalesactivity', array(
            'description' => $description,
            'date' => date('Y-m-d H:i:s'),
            'rel_id' => $id,
            'rel_type' => 'transfer',
            'staffid' => $staffid,
            'full_name' => $full_name,
            'additional_data' => $additional_data,
        ));
    }

    /**
     * Updates pipeline order when drag and drop
     * @param  mixe $data $_POST data
     * @return void
     */
    public function update_pipeline($data)
    {
        $this->mark_action_status($data['status'], $data['transferid']);
        foreach ($data['order'] as $order_data) {
            $this->db->where('id', $order_data[0]);
            $this->db->update('tbltransfers', array(
                'pipeline_order' => $order_data[1],
            ));
        }
    }

    /**
     * Get transfer unique year for filtering
     * @return array
     */
    public function get_transfers_years()
    {
        return $this->db->query('SELECT DISTINCT(YEAR(date)) as year FROM tbltransfers ORDER BY year DESC')->result_array();
    }

    private function map_shipping_columns($data)
    {
        if (!isset($data['include_shipping'])) {
            foreach ($this->shipping_fields as $_s_field) {
                if (isset($data[$_s_field])) {
                    $data[$_s_field] = null;
                }
            }
            $data['show_shipping_on_transfer'] = 1;
            $data['include_shipping']          = 0;
        } else {
            $data['include_shipping'] = 1;
            // set by default for the next time to be checked
            if (isset($data['show_shipping_on_transfer']) && ($data['show_shipping_on_transfer'] == 1 || $data['show_shipping_on_transfer'] == 'on')) {
                $data['show_shipping_on_transfer'] = 1;
            } else {
                $data['show_shipping_on_transfer'] = 0;
            }
        }

        return $data;
    }
}
