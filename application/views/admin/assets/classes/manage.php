<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_asset_class(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('new_asset_class'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($classes) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('asset_class_dt_code'); ?></th>
								<th><?php echo _l('asset_class_dt_name'); ?></th>
								<th><?php echo _l('asset_class_dt_depreciation'); ?></th>
								<th><?php echo _l('options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($classes as $class){ ?>
								<tr>
									<td><?php echo $class['id']; ?></td>
									<td><a href="#" onclick="edit_asset_class(this,<?php echo $class['id']; ?>);return false;" data-name="<?php echo $class['name']; ?>" data-code="<?php echo $class['code']; ?>" data-code="<?php echo $class['depreciation']; ?>" ><?php echo $class['code']; ?></a></td>
									<td><a href="#" onclick="edit_asset_class(this,<?php echo $class['id']; ?>);return false;" data-name="<?php echo $class['name']; ?>" data-code="<?php echo $class['code']; ?>" data-code="<?php echo $class['depreciation']; ?>" ><?php echo $class['name']; ?></a></td>
									<td><a href="#" onclick="edit_asset_class(this,<?php echo $class['id']; ?>);return false;" data-name="<?php echo $class['name']; ?>" data-code="<?php echo $class['code']; ?>" data-code="<?php echo $class['depreciation']; ?>"><?php echo $class['depreciation']; ?></a></td>
									<td>
										<a href="#" onclick="edit_asset_class(this,<?php echo $class['id']; ?>); return false" data-name="<?php echo $class['name']; ?>" data-code="<?php echo $class['code']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<a href="<?php echo admin_url('assets/delete_asset_class/'.$class['id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('no_asset_classes_found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="asset_class" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('assets/asset_class')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('asset_class_edit'); ?></span>
					<span class="add-title"><?php echo _l('asset_class_add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('code','asset_class_add_edit_code'); ?>
					</div>
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('name','asset_class_add_edit_name'); ?>
					</div>					
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('depreciation','asset_class_add_edit_depreciation'); ?>
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		_validate_form($('form'),{code:'required'},{name:'required'},manage_asset_classes);
		$('#asset_class').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#asset_class input[name="code"]').val('');
			$('#asset_class input[name="name"]').val('');
			$('#asset_class input[name="depreciation"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_asset_classes(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_asset_class(){
		$('#asset_class').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_asset_class(invoker,id){
		var code = $(invoker).data('code');
		var name = $(invoker).data('name');
		var depreciation = $(invoker).data('depreciation');
		$('#additional').append(hidden_input('id',id));
		$('#asset_class input[name="code"]').val(code);
		$('#asset_class input[name="name"]').val(name);
		$('#asset_class input[name="depreciation"]').val(depreciation);
		$('#asset_class').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
