<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <?php echo form_open($this->uri->uri_string(),array('id'=>'asset_form')); ?>
            <div class="col-md-7">
                <div class="panel_s">
                    <div class="panel-body">
                        <h4 class="no-margin">
                            <?php echo $title; ?>
                        </h4>
                        <hr class="hr-panel-heading" />
                        <?php
                        $disable_type_edit = '';
                        if(isset($asset)){
                            if($asset->billing_type != 1){
                                if(total_rows('tblstafftasks',array('rel_id'=>$asset->id,'rel_type'=>'asset','billable'=>1,'billed'=>1)) > 0){
                                    $disable_type_edit = 'disabled';
                                }
                            }
                        }
                        ?>
                        <?php $value = (isset($asset) ? $asset->name : ''); ?>
                        <?php echo render_input('name','asset_name',$value); ?>
                        <div class="form-group select-placeholder">
                            <label for="organizationid" class="control-label"><?php echo _l('asset_organization'); ?></label>
                            <select id="organizationid" name="organizationid" data-live-search="true" data-width="100%" class="ajax-search" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                               <?php $selected = (isset($asset) ? $asset->organizationid : '');
                               if($selected == ''){
                                   $selected = (isset($organization_id) ? $organization_id: '');
                               }
                               if($selected != ''){
                                $rel_data = get_relation_data('organization',$selected);
                                $rel_val = get_relation_values($rel_data,'organization');
                                echo '<option value="'.$rel_val['id'].'" selected>'.$rel_val['name'].'</option>';
                            } ?>
                        </select>
                    </div>
                    <?php
                    if(isset($asset)){
                        $value = $this->assets_model->calc_progress_by_tasks($asset->id);
                    } else if(isset($asset)){
                        $value = $asset->progress;
                    } else {
                        $value = 0;
                    }
                    ?>
                    <label for=""><?php echo _l('asset_progress'); ?> <span class="label_progress"><?php echo $value; ?>%</span></label>
                    <?php echo form_hidden('progress',$value); ?>
                    <div class="asset_progress_slider asset_progress_slider_horizontal mbot15"></div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group select-placeholder">
                                <label for="status"><?php echo _l('asset_class'); ?></label>
                                <div class="clearfix"></div>
                                <select name="status" id="status" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                    <?php foreach($assets_classes as $classes){ ?>
                                    <option value="<?php echo $classes['id']; ?>" <?php if(!isset($asset) && $classes['id'] == 2 || (isset($asset) && $asset->classes == $classes['id'])){echo 'selected';} ?>><?php echo $classes['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group select-placeholder">
                                <label for="status"><?php echo _l('asset_category'); ?></label>
                                <div class="clearfix"></div>
                                <select name="status" id="status" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                    <?php foreach($assets_categories as $categories){ ?>
                                    <option value="<?php echo $categories['id']; ?>" <?php if(!isset($asset) && $categories['id'] == 2 || (isset($asset) && $asset->categories == $categories['id'])){echo 'selected';} ?>><?php echo $categories['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <?php echo render_select('department_id',$departments,array('departmentid','name'),'ticket_settings_departments',(count($departments) == 1) ? $departments[0]['departmentid'] : '',array('required'=>'true')); ?>
                        </div>                        
                        <div class="col-md-6">
                            <div class="form-group select-placeholder">
                                <label for="status"><?php echo _l('asset_status'); ?></label>
                                <div class="clearfix"></div>
                                <select name="status" id="status" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                    <?php foreach($statuses as $status){ ?>
                                    <option value="<?php echo $status['id']; ?>" <?php if(!isset($asset) && $status['id'] == 2 || (isset($asset) && $asset->status == $status['id'])){echo 'selected';} ?>><?php echo $status['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php if(total_rows('tblemailtemplates',array('slug'=>'asset-finished-to-organization','active'=>0)) == 0){ ?>
                    <div class="form-group asset_marked_as_finished hide">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox" name="asset_marked_as_finished_email_to_contacts" id="asset_marked_as_finished_email_to_contacts">
                            <label for="asset_marked_as_finished_email_to_contacts"><?php echo _l('asset_marked_as_finished_to_contacts'); ?></label>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($asset)){ ?>
                    <div class="form-group mark_all_tasks_as_completed hide">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox" name="mark_all_tasks_as_completed" id="mark_all_tasks_as_completed">
                            <label for="mark_all_tasks_as_completed"><?php echo _l('asset_mark_all_tasks_as_completed'); ?></label>
                        </div>
                    </div>
                    <div class="notify_asset_members_status_change hide">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox" name="notify_asset_members_status_change" id="notify_asset_members_status_change">
                            <label for="notify_asset_members_status_change"><?php echo _l('notify_asset_members_status_change'); ?></label>
                        </div>
                        <hr />
                    </div>
                    <?php } ?>
                    <?php
                    $input_field_hide_class_total_cost = '';
                    if(!isset($asset)){
                        if($auto_select_billing_type && $auto_select_billing_type->billing_type != 1 || !$auto_select_billing_type){
                            $input_field_hide_class_total_cost = 'hide';
                        }
                    } else if(isset($asset) && $asset->billing_type != 1){
                        $input_field_hide_class_total_cost = 'hide';
                    }
                    ?>
                    <div id="asset_cost" class="<?php echo $input_field_hide_class_total_cost; ?>">
                        <?php $value = (isset($asset) ? $asset->asset_cost : ''); ?>
                        <?php echo render_input('asset_cost','asset_total_cost',$value,'number'); ?>
                    </div>
                    <?php
                    $input_field_hide_class_rate_per_hour = '';
                    if(!isset($asset)){
                        if($auto_select_billing_type && $auto_select_billing_type->billing_type != 2 || !$auto_select_billing_type){
                            $input_field_hide_class_rate_per_hour = 'hide';
                        }
                    } else if(isset($asset) && $asset->billing_type != 2){
                        $input_field_hide_class_rate_per_hour = 'hide';
                    }
                    ?>

                    <div class="row">
                        <div class="col-md-6" id="asset_cost">
                            <?php $value = (isset($asset) ? $asset->asset_cost : ''); ?>
                            <?php echo render_input('asset_cost','asset_total_cost',$value,'number'); ?>
                        </div>
                        <div class="col-md-6">
                           <?php
                           $selected = array();
                           if(isset($asset_members)){
                            foreach($asset_members as $member){
                                array_push($selected,$member['staff_id']);
                            }
                        } else {
                            array_push($selected,get_staff_user_id());
                        }
                        echo render_select('asset_members[]',$staff,array('staffid',array('firstname','lastname')),'asset_members',$selected,array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?php $value = (isset($asset) ? _d($asset->start_date) : _d(date('Y-m-d'))); ?>
                        <?php echo render_date_input('start_date','asset_start_date',$value); ?>
                    </div>
                    <div class="col-md-6">
                        <?php $value = (isset($asset) ? _d($asset->deadline) : ''); ?>
                        <?php echo render_date_input('deadline','asset_deadline',$value); ?>
                    </div>
                </div>
                <?php if(isset($asset) && $asset->date_finished != null && $asset->status == 4) { ?>
                    <?php echo render_datetime_input('date_finished','asset_completed_date',_dt($asset->date_finished)); ?>
                <?php } ?>
                <div class="form-group">
                    <label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
                    <input type="text" class="tagsinput" id="tags" name="tags" value="<?php echo (isset($asset) ? prep_tags_input(get_tags_in($asset->id,'asset')) : ''); ?>" data-role="tagsinput">
                </div>
                <?php $rel_id_custom_field = (isset($asset) ? $asset->id : false); ?>
                <?php echo render_custom_fields('assets',$rel_id_custom_field); ?>
                <p class="bold"><?php echo _l('asset_description'); ?></p>
                <?php $contents = ''; if(isset($asset)){$contents = $asset->description;} ?>
                <?php echo render_textarea('description','',$contents,array(),array(),'','tinymce'); ?>
                <?php if(total_rows('tblemailtemplates',array('slug'=>'assigned-to-asset','active'=>0)) == 0){ ?>
                <div class="checkbox checkbox-primary">
                   <input type="checkbox" name="send_created_email" id="send_created_email">
                   <label for="send_created_email"><?php echo _l('asset_send_created_email'); ?></label>
               </div>
               <?php } ?>
               <div class="btn-bottom-toolbar text-right">
                   <button type="submit" data-form="#asset_form" class="btn btn-info" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('submit'); ?></button>
               </div>
           </div>
       </div>
   </div>
   <div class="col-md-5">
    <div class="panel_s">
        <div class="panel-body" id="asset-settings-area">
         <h4 class="no-margin">
             <?php echo _l('asset_settings'); ?>
         </h4>
         <hr class="hr-panel-heading" />
         <?php foreach($settings as $setting){ ?>

            <?php
            $checked = ' checked';
            if(isset($asset)){
                if($asset->settings->{$setting} == 0){
                    $checked = '';
                }
            } else {
                foreach($last_asset_settings as $_l_setting) {
                    if($setting == $_l_setting['name']){
                        // hide_tasks_on_main_tasks_table is not applied on most used settings to prevent confusions
                        if($_l_setting['value'] == 0 || $_l_setting['name'] == 'hide_tasks_on_main_tasks_table'){
                            $checked = '';
                        }
                    }
                }
                if(count($last_asset_settings) == 0 && $setting == 'hide_tasks_on_main_tasks_table') {
                    $checked = '';
                }
            } ?>
            <?php if($setting != 'available_features'){ ?>
            <div class="checkbox">
            <input type="checkbox" name="settings[<?php echo $setting; ?>]" <?php echo $checked; ?> id="<?php echo $setting; ?>">
            <label for="<?php echo $setting; ?>">
                <?php if($setting == 'hide_tasks_on_main_tasks_table'){ ?>
                <?php echo _l('hide_tasks_on_main_tasks_table'); ?>
                <?php } else{ ?>
                <?php echo _l('asset_allow_organization_to',_l('asset_setting_'.$setting)); ?>
                <?php } ?>
            </label>
        </div>
        <?php } else { ?>
        <div class="form-group mtop15 select-placeholder">
            <label for="available_features"><?php echo _l('visible_tabs'); ?></label>
            <select name="settings[<?php echo $setting; ?>][]" id="<?php echo $setting; ?>" multiple="true" class="selectpicker" id="available_features" data-width="100%" data-actions-box="true">
            <?php $tabs = get_asset_tabs_admin(null); ?>
            <?php foreach($tabs as $tab) {
                $selected = '';
             ?>
            <?php if(isset($tab['dropdown'])){ ?>
            <optgroup label="<?php echo $tab['lang']; ?>">
                <?php foreach($tab['dropdown'] as $tab_dropdown) {
                    $selected = '';
                    if(isset($asset) && $asset->settings->available_features[$tab_dropdown['name']] == 1) {
                        $selected = ' selected';
                    } else if(!isset($asset) && count($last_asset_settings) > 0) {
                        foreach($last_asset_settings as $last_asset_setting) {
                            if($last_asset_setting['name'] == $setting) {
                                if($last_asset_setting['value'][$tab_dropdown['name']] == 1) {
                                    $selected = ' selected';
                                }
                            }
                        }
                    } else if(!isset($asset)) {
                        $selected = ' selected';
                    }
                 ?>
                <option value="<?php echo $tab_dropdown['name']; ?>"<?php echo $selected; ?><?php if(isset($tab_dropdown['linked_to_organization_option']) && is_array($tab_dropdown['linked_to_organization_option']) && count($tab_dropdown['linked_to_organization_option']) > 0){ ?> data-linked-organization-option="<?php echo implode(',',$tab_dropdown['linked_to_organization_option']); ?>"<?php } ?>><?php echo $tab_dropdown['lang']; ?></option>
                <?php } ?>
            </optgroup>
            <?php } else {
                if(isset($asset) && $asset->settings->available_features[$tab['name']] == 1) {
                    $selected = ' selected';
                } else if(!isset($asset) && count($last_asset_settings) > 0) {
                        foreach($last_asset_settings as $last_asset_setting) {
                            if($last_asset_setting['name'] == $setting) {
                                if($last_asset_setting['value'][$tab['name']] == 1) {
                                    $selected = ' selected';
                                }
                            }
                        }
                    } else if(!isset($asset)) {
                        $selected = ' selected';
                    }
                ?>
            <option value="<?php echo $tab['name']; ?>"<?php if($tab['name'] =='asset_overview'){echo ' disabled selected';} ?>
                <?php echo $selected; ?><?php if(isset($tab['linked_to_organization_option']) && is_array($tab['linked_to_organization_option']) && count($tab['linked_to_organization_option']) > 0){ ?> data-linked-organization-option="<?php echo implode(',',$tab['linked_to_organization_option']); ?>"<?php } ?>><?php echo $tab['lang']; ?>
            </option>
            <?php } ?>
            <?php } ?>
        </select>
        </div>
        <?php } ?>
        <hr class="no-margin" />
        <?php } ?>
    </div>
</div>
</div>
<?php echo form_close(); ?>
</div>
<div class="btn-bottom-pusher"></div>
</div>
</div>
<?php init_tail(); ?>
<script>
    <?php if(isset($asset)){ ?>
        var original_asset_status = '<?php echo $asset->status; ?>';
        <?php } ?>
        $(function(){

            $('select[name="billing_type"]').on('change',function(){
                var type = $(this).val();
                if(type == 1){
                    $('#asset_cost').removeClass('hide');
                    $('#asset_rate_per_hour').addClass('hide');
                } else if(type == 2){
                    $('#asset_cost').addClass('hide');
                    $('#asset_rate_per_hour').removeClass('hide');
                } else {
                    $('#asset_cost').addClass('hide');
                    $('#asset_rate_per_hour').addClass('hide');
                }
            });

            _validate_form($('form'),{name:'required',organizationid:'required',start_date:'required',billing_type:'required'});

            $('select[name="status"]').on('change',function(){
                var status = $(this).val();
                var mark_all_tasks_completed = $('.mark_all_tasks_as_completed');
                var notify_asset_members_status_change = $('.notify_asset_members_status_change');
                mark_all_tasks_completed.removeClass('hide');
                if(typeof(original_asset_status) != 'undefined'){
                    if(original_asset_status != status){
                        mark_all_tasks_completed.removeClass('hide');
                        mark_all_tasks_completed.find('input').prop('checked',true);
                        notify_asset_members_status_change.removeClass('hide');
                    } else {
                        mark_all_tasks_completed.addClass('hide');
                        mark_all_tasks_completed.find('input').prop('checked',false);
                        notify_asset_members_status_change.addClass('hide');
                    }
                }
                if(status == 4){
                    $('.asset_marked_as_finished').removeClass('hide');
                } else {
                    $('.asset_marked_as_finished').addClass('hide');
                    $('.asset_marked_as_finished').prop('checked',false);
                }
            });

            $('form').on('submit',function(){
                $('select[name="billing_type"]').prop('disabled',false);
                $('#available_features,#available_features option').prop('disabled',false);
                $('input[name="asset_rate_per_hour"]').prop('disabled',false);
            });


            $('#asset-settings-area input').on('change',function(){
                if($(this).attr('id') == 'view_tasks' && $(this).prop('checked') == false){
                    $('#create_tasks').prop('checked',false).prop('disabled',true);
                    $('#edit_tasks').prop('checked',false).prop('disabled',true);
                    $('#view_task_comments').prop('checked',false).prop('disabled',true);
                    $('#comment_on_tasks').prop('checked',false).prop('disabled',true);
                    $('#view_task_attachments').prop('checked',false).prop('disabled',true);
                    $('#view_task_checklist_items').prop('checked',false).prop('disabled',true);
                    $('#upload_on_tasks').prop('checked',false).prop('disabled',true);
                    $('#view_task_total_logged_time').prop('checked',false).prop('disabled',true);
                } else if($(this).attr('id') == 'view_tasks' && $(this).prop('checked') == true){
                    $('#create_tasks').prop('disabled',false);
                    $('#edit_tasks').prop('disabled',false);
                    $('#view_task_comments').prop('disabled',false);
                    $('#comment_on_tasks').prop('disabled',false);
                    $('#view_task_attachments').prop('disabled',false);
                    $('#view_task_checklist_items').prop('disabled',false);
                    $('#upload_on_tasks').prop('disabled',false);
                    $('#view_task_total_logged_time').prop('disabled',false);
                }
            });

            // Auto adjust organization permissions based on selected asset visible tabs
            // Eq Asset creator disable TASKS tab, then this function will auto turn off organization asset option Allow organization to view tasks

            $('#available_features').on('change',function(){
                $("#available_features option").each(function(){
                   if($(this).data('linked-organization-option') && !$(this).is(':selected')) {
                        var opts = $(this).data('linked-organization-option').split(',');
                        for(var i = 0; i<opts.length;i++) {
                            var asset_option = $('#'+opts[i]);
                            asset_option.prop('checked',false);
                            if(opts[i] == 'view_tasks') {
                                asset_option.trigger('change');
                            }
                        }
                   }
               });
            });
            $("#view_tasks").trigger('change');
            <?php if(!isset($asset)) { ?>
                $('#available_features').trigger('change');
            <?php } ?>
        });
    </script>
</body>
</html>
