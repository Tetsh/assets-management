<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_asset_category(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('new_asset_category'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($categories) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('asset_category_dt_code'); ?></th>
								<th><?php echo _l('asset_category_dt_name'); ?></th>
								<th><?php echo _l('asset_category_dt_arabic_name'); ?></th>
								<th><?php echo _l('options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($categories as $category){ ?>
								<tr>
									<td><?php echo $category['id']; ?></td>
									<td><a href="#" onclick="edit_asset_category(this,<?php echo $category['id']; ?>);return false;" data-name="<?php echo $category['name']; ?>" data-arabic_name="<?php echo $category['arabic_name']; ?>" data-code="<?php echo $category['code']; ?>" ><?php echo $category['code']; ?></a></td>
									<td><a href="#" onclick="edit_asset_category(this,<?php echo $category['id']; ?>);return false;" data-name="<?php echo $category['name']; ?>" data-arabic_name="<?php echo $category['arabic_name']; ?>" data-code="<?php echo $category['code']; ?>" ><?php echo $category['name']; ?></a></td>
									<td><a href="#" onclick="edit_asset_category(this,<?php echo $category['id']; ?>);return false;" data-name="<?php echo $category['name']; ?>" data-arabic_name="<?php echo $category['arabic_name']; ?>" data-code="<?php echo $category['code']; ?>" ><?php echo $category['arabic_name']; ?></a></td>
									<td>
										<a href="#" onclick="edit_asset_category(this,<?php echo $category['id']; ?>); return false" data-name="<?php echo $category['name']; ?>" data-arabic_name="<?php echo $category['arabic_name']; ?>" data-code="<?php echo $category['code']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<a href="<?php echo admin_url('assets/delete_asset_category/'.$category['id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('no_asset_categories_found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="asset_category" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('assets/asset_category')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('asset_category_edit'); ?></span>
					<span class="add-title"><?php echo _l('asset_category_add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('code','asset_category_add_edit_code'); ?>
					</div>
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('name','asset_category_add_edit_name'); ?>
					</div>					
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('arabic_name','asset_category_add_edit_arabic_name'); ?>
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		_validate_form($('form'),{code:'required'},{name:'required'},{arabic_name:'required'},manage_asset_categories);
		$('#asset_category').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#asset_category input[name="code"]').val('');
			$('#asset_category input[name="name"]').val('');
			$('#asset_category input[name="arabic_name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_asset_categories(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_asset_category(){
		$('#asset_category').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_asset_category(invoker,id){
		var code = $(invoker).data('code');
		var name = $(invoker).data('name');
		var arabic_name = $(invoker).data('arabic_name');
		$('#additional').append(hidden_input('id',id));
		$('#asset_category input[name="code"]').val(code);
		$('#asset_category input[name="name"]').val(name);
		$('#asset_category input[name="arabic_name"]').val(arabic_name);
		$('#asset_category').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
