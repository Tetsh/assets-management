<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
            <div class="panel_s">
              <div class="panel-body">
              <div class="_buttons">
                  <?php if(has_permission('assets','','create')){ ?>
              <a href="<?php echo admin_url('assets/asset'); ?>" class="btn btn-info mright5 test pull-left display-block">
                <?php echo _l('new_asset'); ?>
              </a>

              <a href="<?php echo admin_url('assets/import'); ?>" class="btn btn-info pull-left display-block mright5 hidden-xs">
                <?php echo _l('import_asset'); ?>
              </a>

              <?php } ?>
              <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-filter" aria-hidden="true"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right width300">
                  <li>
                    <a href="#" data-cview="all" onclick="dt_custom_view('','.table-assets',''); return false;">
                      <?php echo _l('expenses_list_all'); ?>
                    </a>
                  </li>
                  <?php
                  // Only show this filter if user has permission for assets view otherwisde wont need this becuase by default this filter will be applied
                  if(has_permission('assets','','view')){ ?>
                  <li>
                    <a href="#" data-cview="my_assets" onclick="dt_custom_view('my_assets','.table-assets','my_assets'); return false;">
                      <?php echo _l('home_my_assets'); ?>
                    </a>
                  </li>
                  <?php } ?>
                  <li class="divider"></li>
                  <?php foreach($statuses as $status){ ?>
                    <li class="<?php if($status['filter_default'] == true && !$this->input->get('status') || $this->input->get('status') == $status['id']){echo 'active';} ?>">
                      <a href="#" data-cview="<?php echo 'asset_status_'.$status['id']; ?>" onclick="dt_custom_view('asset_status_<?php echo $status['id']; ?>','.table-assets','asset_status_<?php echo $status['id']; ?>'); return false;">
                        <?php echo $status['name']; ?>
                      </a>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
                <div class="clearfix"></div>
                <hr class="hr-panel-heading" />
              </div>
               <div class="row mbot15">
                <div class="col-md-12">
                  <h4 class="no-margin"><?php echo _l('assets_summary'); ?></h4>
                  <?php
                  $_where = '';
                  ?>
                </div>
                <div class="_filters _hidden_inputs">
                  <?php
                  echo form_hidden('my_assets');
                  foreach($statuses as $status){
                   $value = $status['id'];
                     if($status['filter_default'] == false && !$this->input->get('status')){
                        $value = '';
                     } else if($this->input->get('status')) {
                        $value = ($this->input->get('status') == $status['id'] ? $status['id'] : "");
                     }
                     echo form_hidden('asset_status_'.$status['id'],$value);
                    ?>
                   <div class="col-md-2 col-xs-6 border-right">
                    <?php $where = ($_where == '' ? '' : $_where.' AND ').'status = '.$status['id']; ?>
                    <a href="#" onclick="dt_custom_view('asset_status_<?php echo $status['id']; ?>','.table-assets','asset_status_<?php echo $status['id']; ?>',true); return false;">
                     <h3 class="bold"><?php echo total_rows('tblassets',$where); ?></h3>
                     <span style="color:<?php echo $status['color']; ?>" asset-status-<?php echo $status['id']; ?>">
                     <?php echo $status['name']; ?>
                     </span>
                   </a>
                 </div>
                 <?php } ?>
               </div>
             </div>
             <div class="clearfix"></div>
              <hr class="hr-panel-heading" />
             <?php echo form_hidden('custom_view'); ?>
             <?php
             $table_data = array(
              '#',
              _l('asset_name'),
              _l('asset_location'),
              _l('asset_organization'),
              _l('Class'),
              _l('Category'),
              _l('asset_start_date'),
              _l('Departmenet'),
              _l('asset_cost'),
              _l('asset_status'),
              );

              $custom_fields = get_custom_fields('assets',array('show_on_table'=>1));
               foreach($custom_fields as $field){
                  array_push($table_data,$field['name']);
              }

              $table_data = do_action('assets_table_columns',$table_data);
              array_push($table_data, _l('options'));

            render_datatable($table_data,'assets'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/assets/copy_settings'); ?>
<?php init_tail(); ?>
<script>
$(function(){
     var ProjectsServerParams = {};
    $.each($('._hidden_inputs._filters input'),function(){
      ProjectsServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
    });
     var assets_not_sortable = $('.table-assets').find('th').length - 1;
     initDataTable('.table-assets', admin_url+'assets/table', [assets_not_sortable], [assets_not_sortable], ProjectsServerParams, <?php echo do_action('assets_table_default_order',json_encode(array(5,'ASC'))); ?>);
     init_ajax_search('organization', '#organizationid_copy_asset.ajax-search');
});
</script>
</body>
</html>
