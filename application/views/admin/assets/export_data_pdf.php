<?php

$pdf = new Pdf('L', 'mm', 'landscape', true, 'UTF-8', false);
$pdf->SetTitle($asset->name);
$this->pdf->SetMargins(PDF_MARGIN_LEFT, 26, PDF_MARGIN_RIGHT);
$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->pdf->SetAutoPageBreak(TRUE, 30);
$pdf->SetAuthor(get_option('company'));
$pdf->SetFont(get_option('pdf_font'), '', get_option('pdf_font_size'));
$pdf->AddPage();
$dimensions = $pdf->getPageDimensions();
$custom_fields = get_custom_fields('assets');

$divide_document_overview = 3;
// If custom fields found divide the overview in 4 parts not in 3 to include the custom fields too
if(count($custom_fields) > 0){
    $divide_document_overview = 4;
}

// Like heading asset name
$html = '<h1>'._l('asset_name').': '.$asset->name.'</h1>';
// asset overview heading
$html .= '<h3>'.ucwords(_l('asset_overview')).'</h3>';
if(!empty($asset->description)){
    // Project description
    $html .= '<p><b style="background-color:#f0f0f0;">' . _l('asset_description') . '</b><br /><br /> ' . $asset->description . '</p>';
}

$pdf->writeHTML($html, true, false, false, false, '');
$pdf->Ln(10);
$html = '';
// Project overview
// Billing type
if($asset->billing_type == 1){
  $type_name = 'asset_billing_type_fixed_cost';
} else if($asset->billing_type == 2){
  $type_name = 'asset_billing_type_asset_hours';
} else {
  $type_name = 'asset_billing_type_asset_task_hours';
}
$html .= '<b style="background-color:#f0f0f0;">'._l('asset_overview').'</b><br /><br />';

$html .= '<b>'._l('asset_billing_type').': </b>' . _l($type_name) . '<br />';

if($asset->billing_type == 1 || $asset->billing_type == 2){
  if($asset->billing_type == 1){
      $html .= '<b>'._l('asset_total_cost').': </b>' . format_money($asset->asset_cost,$asset->currency_data->symbol) . '<br />';
  } else {
    $html .= '<b>'._l('asset_rate_per_hour').': </b>' . format_money($asset->asset_rate_per_hour,$asset->currency_data->symbol) . '<br />';
  }
}
$status = get_asset_status_by_id($asset->status);
// Project status
$html .= '<b>' . _l('asset_status') . ': </b>' . $status['name'] . '<br />';
// Date created
$html .= '<b>' . _l('asset_datecreated') . ': </b>' . _d($asset->asset_created) . '<br />';
// Start date
$html .= '<b>' . _l('asset_start_date') . ': </b>' . _d($asset->start_date) . '<br />';
// Deadline
$d = $asset->deadline ? _d($asset->deadline) : '/';
$html .= '<b>' . _l('asset_deadline') . ': </b>' . $d  . '<br /><br />';
// Total Days
$html .= '<b>' . _l('total_asset_worked_days') . ': </b>' . $total_days . '<br />';
// Total logged hours for this asset
$html .= '<b>' . _l('asset_overview_total_logged_hours') . ': </b>' . $total_logged_time . '<br />';
// Total members
$html .= '<b>' . _l('total_asset_members') . ': </b>' . $total_members . '<br />';
// Total files
$html .= '<b>' . _l('total_asset_files') . ': </b>' . $total_files_attached . '<br />';
// Total Discussions
$html .= '<b>' . _l('total_asset_discussions_created') . ': </b>' . $total_files_attached . '<br />';
// Total Milestones
$html .= '<b>' . _l('total_milestones') . ': </b>' . $total_milestones . '<br />';
// Total Tickets
$html .= '<b>' . _l('total_tickets_related_to_asset') . ': </b>' . $total_tickets . '<br />';
// Write asset overview data
$pdf->MultiCell(($dimensions['wk'] / $divide_document_overview) - $dimensions['lm'], 0, $html, 0, 'L', 0, 0, '', '', true, 0, true);

$html = '';
$html .= '<b style="background-color:#f0f0f0;">'.ucwords(_l('finance_overview')) . '</b><br /><br />';
$html .= '<b>' . _l('assets_total_invoices_created') . ' </b>' . $total_invoices .'<br />';
// Not paid invoices total
$html .= '<b>' . _l('outstanding_invoices') . ' </b>' . format_money($invoices_total_data['due'],$asset->currency_data->symbol) .'<br />';
// Due invoices total
$html .= '<b>' . _l('past_due_invoices') . ' </b>' . format_money($invoices_total_data['overdue'],$asset->currency_data->symbol) .'<br />';
// Paid invoices
$html .= '<b>' . _l('paid_invoices') . ' </b>' . format_money($invoices_total_data['paid'],$asset->currency_data->symbol) .'<br /><br />';

// Finance Overview
if($asset->billing_type == 2 || $asset->billing_type == 3) {
    // Total logged time + money
    $logged_time_data = $this->assets_model->total_logged_time_by_billing_type($asset->id);
    $html .= '<b>' . _l('asset_overview_logged_hours') . ' </b>' . $logged_time_data['logged_time'] . ' - ' . format_money($logged_time_data['total_money'],$asset->currency_data->symbol) .'<br />';
    // Total billable time + money
    $logged_time_data = $this->assets_model->data_billable_time($asset->id);
    $html .= '<b>' . _l('asset_overview_billable_hours') . ' </b>' . $logged_time_data['logged_time'] . ' - ' . format_money($logged_time_data['total_money'],$asset->currency_data->symbol) .'<br />';
    // Total billed time + money
    $logged_time_data = $this->assets_model->data_billed_time($asset->id);
    $html .= '<b>' . _l('asset_overview_billed_hours') . ' </b>' . $logged_time_data['logged_time'] . ' - ' . format_money($logged_time_data['total_money'],$asset->currency_data->symbol) .'<br />';
    // Total unbilled time + money
    $logged_time_data = $this->assets_model->data_unbilled_time($asset->id);
    $html .= '<b>' . _l('asset_overview_unbilled_hours') . ' </b>' . $logged_time_data['logged_time'] . ' - ' . format_money($logged_time_data['total_money'],$asset->currency_data->symbol) .'<br /><br/>';
}

// Total expenses + money
$html .= '<b>' . _l('asset_overview_expenses') . ': </b>' . format_money(sum_from_table('tblexpenses',array('where'=>array('asset_id'=>$asset->id),'field'=>'amount')),$asset->currency_data->symbol) . '<br />';
// Billable expenses + money
$html .= '<b>' . _l('asset_overview_expenses_billable') . ': </b>' . format_money(sum_from_table('tblexpenses',array('where'=>array('asset_id'=>$asset->id,'billable'=>1),'field'=>'amount')),$asset->currency_data->symbol) . '<br />';
// Billed expenses + money
$html .= '<b>' . _l('asset_overview_expenses_billed') . ': </b>' . format_money(sum_from_table('tblexpenses',array('where'=>array('asset_id'=>$asset->id,'invoiceid !='=>'NULL','billable'=>1),'field'=>'amount')),$asset->currency_data->symbol) . '<br />';
// Unbilled expenses + money
$html .= '<b>' . _l('asset_overview_expenses_unbilled') . ': </b>' . format_money(sum_from_table('tblexpenses',array('where'=>array('asset_id'=>$asset->id,'invoiceid IS NULL','billable'=>1),'field'=>'amount')),$asset->currency_data->symbol) . '<br />';
// Write finance overview
$pdf->MultiCell(($dimensions['wk'] / $divide_document_overview) - $dimensions['lm'], 0, $html, 0, 'L', 0, 0, '', '', true, 0, true);

// Custom fields
// Check for custom fields
if(count($custom_fields) > 0) {
$html = '';
$html .= '<b style="background-color:#f0f0f0;">'.ucwords(_l('asset_custom_fields')) . '</b><br /><br />';

foreach($custom_fields as $field){
    $value = get_custom_field_value($asset->id,$field['id'],'assets');
    $value = $value === '' ? '/' : $value;
    $html .= '<b>' . ucfirst($field['name']) . ': </b>' . $value . '<br />';
}

// Write custom fields
$pdf->MultiCell(($dimensions['wk'] / $divide_document_overview) - $dimensions['lm'], 0, $html, 0, 'L', 0, 0, '', '', true, 0, true);
}

$html = '';
// Customer Info
$html .= '<b style="background-color:#f0f0f0;">'.ucwords(_l('asset_organization')) . '</b><br /><br /><b>'.$asset->organization_data->company . '</b><br />';
$html .= $asset->organization_data->address . '<br />';

if(!empty($asset->organization_data->city)){
    $html .= $asset->organization_data->city;
}
if(!empty($asset->organization_data->state)){
    $html .=', '.$asset->organization_data->state;
}
$country = get_country_short_name($asset->organization_data->country);
if(!empty($country)){
    $html .= '<br />'.$country;
}
if(!empty($asset->organization_data->zip)){
    $html .= ', ' .$asset->organization_data->zip;
}
if(!empty($asset->organization_data->phonenumber)){
    $html .= '<br />' .$asset->organization_data->phonenumber;
}
if (!empty($asset->organization_data->vat)) {
    $html .= '<br />'._l('organization_vat_number') . ': ' . $asset->organization_data->vat;
}

// Write custom info
$pdf->MultiCell(($dimensions['wk'] / $divide_document_overview) - $dimensions['lm'], 0, $html, 0, 'L', 0, 0, '', '', true, 0, true);

// Set new lines to prevent overlaping the content
$pdf->Ln(80);
// $pdf->setY(140);
// Project members overview
$html = '';
// Heading
$html .= '<p><b style="background-color:#f0f0f0;">'.ucwords(_l('asset_members_overview')).'</b></p>';
$html .= '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="5" border="1">';
$html .= '<thead>';
$html .= '<tr bgcolor="#323a45" style="color:#ffffff;">';
$html .= '<th width="12.5%"><b>'._l('asset_member').'</b></th>';
$html .= '<th width="12.5%"><b>'._l('staff_total_task_assigned').'</b></th>';
$html .= '<th width="12.5%"><b>'._l('staff_total_comments_on_tasks').'</b></th>';
$html .= '<th width="12.5%"><b>'._l('total_asset_discussions_created').'</b></th>';
$html .= '<th width="12.5%"><b>'._l('total_asset_discussions_comments').'</b></th>';
$html .= '<th width="12.5%"><b>'._l('total_asset_files').'</b></th>';
$html .= '<th width="12.5%"><b>'._l('time_h').'</b></th>';
$html .= '<th width="12.5%"><b>'._l('time_decimal').'</b></th>';
$html .= '</tr>';
$html .= '</thead>';
$html .= '<tbody>';
foreach($members as $member){
    $html .= '<tr style="color:#4a4a4a;">';
        $html .= '<td>'.get_staff_full_name($member['staff_id']).'</td>';
        $html .= '<td>'.total_rows('tblstafftasks','rel_type="asset" AND rel_id="'.$asset->id.'" AND id IN (SELECT taskid FROM tblstafftaskassignees WHERE staffid="'.$member['staff_id'].'")').'</td>';
        $html .= '<td>'.total_rows('tblstafftaskcomments','staffid = '.$member['staff_id']. ' AND taskid IN (SELECT id FROM tblstafftasks WHERE rel_type="asset" AND rel_id="'.$asset->id.'")').'</td>';
        $html .= '<td>'.total_rows('tblassetdiscussions',array('staff_id'=>$member['staff_id'],'asset_id'=>$asset->id)).'</td>';
        $html .= '<td>'.total_rows('tblassetdiscussioncomments','staff_id='.$member['staff_id'] . ' AND discussion_id IN (SELECT id FROM tblassetdiscussions WHERE asset_id='.$asset->id.')').'</td>';
        $html .= '<td>'.total_rows('tblassetfiles',array('staffid'=>$member['staff_id'],'asset_id'=>$asset->id)).'</td>';
        $member_tasks_assigned = $this->tasks_model->get_tasks_by_staff_id($member['staff_id'],array('rel_id'=>$asset->id,'rel_type'=>'asset'));
        $seconds = 0;
        foreach($member_tasks_assigned as $member_task){
            $seconds += $this->tasks_model->calc_task_total_time($member_task['id'],' AND staff_id='.$member['staff_id']);
        }
        $html .= '<td>'.seconds_to_time_format($seconds).'</td>';
        $html .= '<td>'.sec2qty($seconds).'</td>';
    $html .= '</tr>';
}
$html .= '</tbody>';
$html .= '</table>';
// Write asset members table data
$pdf->writeHTML($html, true, false, false, false, '');

// Tasks overview
$pdf->Ln(5);
$html = '';
$html .= '<p><b style="background-color:#f0f0f0;">'.ucwords(_l('detailed_overview')).'</b></p>';
$html .= '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="5" border="1">';
$html .= '<thead>';
$html .= '<tr bgcolor="#323a45" style="color:#ffffff;">';
$html .= '<th width="26.12%"><b>'._l('tasks_dt_name').'</b></th>';
$html .= '<th width="12%"><b>'._l('total_task_members_assigned').'</b></th>';
$html .= '<th width="12%"><b>'._l('total_task_members_followers').'</b></th>';
$html .= '<th width="9.28%"><b>'._l('task_single_start_date').'</b></th>';
$html .= '<th width="9.28%"><b>'._l('task_single_due_date').'</b></th>';
$html .= '<th width="7%"><b>'._l('task_status').'</b></th>';
$html .= '<th width="14.28%"><b>'._l('time_h').'</b></th>';
$html .= '<th width="10%"><b>'._l('time_decimal').'</b></th>';
$html .= '</tr>';
$html .= '</thead>';
$html .= '<tbody>';
foreach($tasks as $task){
    $html .= '<tr style="color:#4a4a4a;">';
        $html .= '<td width="26.12%">'.$task['name'].'</td>';
        $html .= '<td width="12%">'.total_rows('tblstafftaskassignees',array('taskid'=>$task['id'])).'</td>';
        $html .= '<td width="12%">'.total_rows('tblstafftasksfollowers',array('taskid'=>$task['id'])).'</td>';
        $html .= '<td width="9.28%">'._d($task['startdate']).'</td>';
        $html .= '<td width="9.28%">'.(is_date($task['duedate']) ? _d($task['duedate']): '').'</td>';
        $html .= '<td width="7%">'.format_task_status($task['status'],true,true).'</td>';
        $html .= '<td width="14.28%">'.seconds_to_time_format($task['total_logged_time']).'</td>';
        $html .= '<td width="10%">'.sec2qty($task['total_logged_time']).'</td>';

    $html .= '</tr>';
}
$html .= '</tbody>';
$html .= '</table>';
// Write tasks data
$pdf->writeHTML($html, true, false, false, false, '');

// Timesheets overview
$pdf->Ln(5);
$html = '';
$html .= '<p><b style="background-color:#f0f0f0;">'.ucwords(_l('timesheets_overview')).'</b></p>';
$html .= '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="5" border="1">';
$html .= '<thead>';
$html .= '<tr bgcolor="#323a45" style="color:#ffffff;">';
$html .= '<th width="16.66%"><b>'._l('asset_timesheet_user').'</b></th>';
$html .= '<th width="16.66%"><b>'._l('asset_timesheet_task').'</b></th>';
$html .= '<th width="16.66%"><b>'._l('asset_timesheet_start_time').'</b></th>';
$html .= '<th width="16.66%"><b>'._l('asset_timesheet_end_time').'</b></th>';
$html .= '<th width="16.66%"><b>'._l('time_h').'</b></th>';
$html .= '<th width="16.66%"><b>'._l('time_decimal').'</b></th>';
$html .= '</tr>';
$html .= '</thead>';
$html .= '<tbody>';
foreach($timesheets as $timesheet){
    $html .= '<tr style="color:#4a4a4a;">';
        $html .= '<td>'.get_staff_full_name($timesheet['staff_id']).'</td>';
        $html .= '<td>' . $timesheet['task_data']->name . '</td>';
        $html .= '<td>'._dt($timesheet['start_time'],true).'</td>';
        $html .= '<td>'.(!is_null($timesheet['end_time']) ? _dt($timesheet['end_time'],true) : '').'</td>';
        $html .= '<td>'.seconds_to_time_format($timesheet['total_spent']).'</td>';
        $html .= '<td>'.sec2qty($timesheet['total_spent']).'</td>';

    $html .= '</tr>';
}
$html .= '</tbody>';
$html .= '</table>';
// Write timesheets data
$pdf->writeHTML($html, true, false, false, false, '');

// Milestones overview
$pdf->Ln(5);
$html = '';
$html .= '<p><b style="background-color:#f0f0f0;">'.ucwords(_l('asset_milestones_overview')).'</b></p>';
$html .= '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="5" border="1">';
$html .= '<thead>';
$html .= '<tr bgcolor="#323a45" style="color:#ffffff;">';
$html .= '<th width="20%"><b>'._l('milestone_name').'</b></th>';
$html .= '<th width="30%"><b>'._l('milestone_description').'</b></th>';
$html .= '<th width="15%"><b>'._l('milestone_due_date').'</b></th>';
$html .= '<th width="15%"><b>'._l('total_tasks_in_milestones').'</b></th>';
$html .= '<th width="20%"><b>'._l('milestone_total_logged_time').'</b></th>';
$html .= '</tr>';
$html .= '</thead>';
$html .= '<tbody>';
foreach($milestones as $milestone){
    $html .= '<tr style="color:#4a4a4a;">';
        $html .= '<td width="20%">'.$milestone['name'].'</td>';
        $html .= '<td width="30%">'.$milestone['description'].'</td>';
        $html .= '<td width="15%">'._d($milestone['due_date']).'</td>';
        $html .= '<td width="15%">'.total_rows('tblstafftasks',array('milestone'=>$milestone['id'],'rel_id'=>$asset->id,'rel_type'=>'asset')).'</td>';
        $html .= '<td width="20%">'.seconds_to_time_format($milestone['total_logged_time']).'</td>';
    $html .= '</tr>';
}
$html .= '</tbody>';
$html .= '</table>';
// Write milestones table data
$pdf->writeHTML($html, true, false, false, false, '');

if (ob_get_length() > 0 && ENVIRONMENT == 'production') {
    ob_end_clean();
}

// Output PDF to user
$pdf->output('#'.$asset->id.'_' . $asset->name.'_'._d(date('Y-m-d')).'.pdf','I');
