<?php init_head(); ?>
<style>
#ribbon_asset_<?php echo $asset->id; ?> span::before {
  border-top: 3px solid <?php echo $asset_status['color']; ?>;
  border-left: 3px solid <?php echo $asset_status['color']; ?>;
}
</style>
<div id="wrapper">
  <?php echo form_hidden('asset_id',$asset->id) ?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s asset-top-panel panel-full">
          <div class="panel-body _buttons">
            <div class="row">
              <div class="col-md-8 asset-heading">
                <h3 class="hide asset-name"><?php echo $asset->name; ?></h3>
                <div id="asset_view_name">
                 <select class="selectpicker" id="asset_top" data-width="fit"<?php if(count($other_assets) > 4){ ?> data-live-search="true" <?php } ?>>
                   <option value="<?php echo $asset->id; ?>" selected><?php echo $asset->name; ?></option>
                   <?php foreach($other_assets as $op){ ?>
                   <option value="<?php echo $op['id']; ?>" data-subtext="<?php echo $op['company']; ?>">#<?php echo $op['id']; ?> - <?php echo $op['name']; ?></option>
                   <?php } ?>
                 </select>
               </div>
             </div>
             <div class="col-md-4 text-right">
              <?php if(has_permission('tasks','','create')){ ?>
              <a href="#" onclick="new_task_from_relation(undefined,'asset',<?php echo $asset->id; ?>); return false;" class="btn btn-info"><?php echo _l('new_task'); ?></a>
              <?php } ?>
              <?php
              $invoice_func = 'pre_invoice_asset';
              ?>
              <?php if(has_permission('invoices','','create')){ ?>
              <a href="#" onclick="<?php echo $invoice_func; ?>(<?php echo $asset->id; ?>); return false;" class="invoice-asset btn btn-info<?php if($asset->organization_data->active == 0){echo ' disabled';} ?>"><?php echo _l('invoice_asset'); ?></a>
              <?php } ?>
              <?php
              $asset_pin_tooltip = _l('pin_asset');
              if(total_rows('tblpinnedassets',array('staff_id'=>get_staff_user_id(),'asset_id'=>$asset->id)) > 0){
                $asset_pin_tooltip = _l('unpin_asset');
              }
              ?>
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo _l('more'); ?> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right width200 asset-actions">
                  <li>
                   <a href="<?php echo admin_url('assets/pin_action/'.$asset->id); ?>">
                    <?php echo $asset_pin_tooltip; ?>
                  </a>
                </li>
                <?php if(has_permission('assets','','edit')){ ?>
                <li>
                  <a href="<?php echo admin_url('assets/asset/'.$asset->id); ?>">
                    <?php echo _l('edit_asset'); ?>
                  </a>
                </li>
                <?php } ?>
                <?php if(has_permission('assets','','create')){ ?>
                <li>
                  <a href="#" onclick="copy_asset(); return false;">
                    <?php echo _l('copy_asset'); ?>
                  </a>
                </li>
                <?php } ?>
                <?php if(has_permission('assets','','create') || has_permission('assets','','edit')){ ?>
                <li class="divider"></li>
                <?php foreach($statuses as $status){
                  if($status['id'] == $asset->status){continue;}
                  ?>
                  <li>
                    <a href="#" onclick="asset_mark_as_modal(<?php echo $status['id']; ?>,<?php echo $asset->id; ?>); return false;"><?php echo _l('asset_mark_as',$status['name']); ?></a>
                  </li>
                  <?php } ?>
                  <?php } ?>
                  <li class="divider"></li>
                  <?php if(has_permission('assets','','create')){ ?>
                  <li>
                   <a href="<?php echo admin_url('assets/export_asset_data/'.$asset->id); ?>" target="_blank"><?php echo _l('export_asset_data'); ?></a>
                 </li>
                 <?php } ?>
                 <?php if(is_admin()){ ?>
                 <li>
                  <a href="<?php echo admin_url('assets/view_asset_as_organization/'.$asset->id .'/'.$asset->organizationid); ?>" target="_blank"><?php echo _l('asset_view_as_organization'); ?></a>
                </li>
                <?php } ?>
                <?php if(has_permission('assets','','delete')){ ?>
                <li>
                  <a href="<?php echo admin_url('assets/delete/'.$asset->id); ?>" class="_delete">
                    <span class="text-danger"><?php echo _l('delete_asset'); ?></span>
                  </a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="panel_s asset-menu-panel">
      <div class="panel-body">
        <?php do_action('before_render_asset_view',$asset->id); ?>
        <?php echo '<div class="ribbon asset-status-ribbon-'.$asset->status.'" id="ribbon_asset_'.$asset->id.'"><span style="background:'.$asset_status['color'].'">'.$asset_status['name'].'</span></div>'; ?>
        <?php $this->load->view('admin/assets/asset_tabs'); ?>
      </div>
    </div>
    <?php if($view == 'asset_milestones') { ?>
    <a href="#" class="asset-tabs-and-opts-toggler screen-options-btn bold"><?php echo _l('show_tabs_and_options'); ?></a>
    <?php } else { ?>
    <?php if((has_permission('assets','','create') || has_permission('assets','','edit')) && $asset->status == 1 && $this->assets_model->timers_started_for_asset($asset->id)){ ?>
    <div class="alert alert-warning asset-no-started-timers-found mbot15">
      <?php echo _l('asset_not_started_status_tasks_timers_found'); ?>
    </div>
    <?php } ?>
    <?php if($asset->deadline && date('Y-m-d') > $asset->deadline && $asset->status == 2){ ?>
    <div class="alert alert-warning bold asset-due-notice mbot15">
      <?php echo _l('asset_due_notice',floor((abs(time() - strtotime($asset->deadline)))/(60*60*24))); ?>
    </div>
    <?php } ?>
    <?php if(!has_contact_permission('assets',get_primary_contact_user_id($asset->organizationid)) && total_rows('tblcontacts',array('userid'=>$asset->organizationid)) > 0){ ?>
    <div class="alert alert-warning asset-permissions-warning mbot15">
      <?php echo _l('asset_organization_permission_warning'); ?>
    </div>
    <?php } ?>
    <?php } ?>
    <div class="panel_s">
      <div class="panel-body">
        <?php echo $group_view; ?>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<?php if(isset($discussion)){
  echo form_hidden('discussion_id',$discussion->id);
  echo form_hidden('discussion_user_profile_image_url',$discussion_user_profile_image_url);
  echo form_hidden('current_user_is_admin',$current_user_is_admin);
}
echo form_hidden('asset_percent',$percent);
?>
<div id="invoice_asset"></div>
<div id="pre_invoice_asset"></div>
<?php $this->load->view('admin/assets/milestone'); ?>
<?php $this->load->view('admin/assets/copy_settings'); ?>
<?php $this->load->view('admin/assets/_mark_tasks_finished'); ?>
<?php init_tail(); ?>
<?php $discussion_lang = get_asset_discussions_language_array(); ?>
<?php echo app_script('assets/js','assets.js'); ?>
<!-- For invoices table -->
<script>
  taskid = '<?php echo $this->input->get('taskid'); ?>';
</script>
<script>
  var gantt_data = {};
  <?php if(isset($gantt_data)){ ?>
    gantt_data = <?php echo json_encode($gantt_data); ?>;
    <?php } ?>
    var discussion_id = $('input[name="discussion_id"]').val();
    var discussion_user_profile_image_url = $('input[name="discussion_user_profile_image_url"]').val();
    var current_user_is_admin = $('input[name="current_user_is_admin"]').val();
    var asset_id = $('input[name="asset_id"]').val();
    if(typeof(discussion_id) != 'undefined'){
      discussion_comments('#discussion-comments',discussion_id,'regular');
    }
    $(function(){
     var asset_progress_color = '<?php echo do_action('admin_asset_progress_color','#84c529'); ?>';
     var circle = $('.asset-progress').circleProgress({fill: {
      gradient: [asset_progress_color, asset_progress_color]
    }}).on('circle-animation-progress', function(event, progress, stepValue) {
      $(this).find('strong.asset-percent').html(parseInt(100 * stepValue) + '<i>%</i>');
    });
  });

    function discussion_comments(selector,discussion_id,discussion_type){
     $(selector).comments({
       roundProfilePictures: true,
       textareaRows: 4,
       textareaRowsOnFocus: 6,
       profilePictureURL:discussion_user_profile_image_url,
       enableUpvoting: false,
       enableAttachments:true,
       popularText:'',
       enableDeletingCommentWithReplies:false,
       textareaPlaceholderText:"<?php echo $discussion_lang['discussion_add_comment']; ?>",
       newestText:"<?php echo $discussion_lang['discussion_newest']; ?>",
       oldestText:"<?php echo $discussion_lang['discussion_oldest']; ?>",
       attachmentsText:"<?php echo $discussion_lang['discussion_attachments']; ?>",
       sendText:"<?php echo $discussion_lang['discussion_send']; ?>",
       replyText:"<?php echo $discussion_lang['discussion_reply']; ?>",
       editText:"<?php echo $discussion_lang['discussion_edit']; ?>",
       editedText:"<?php echo $discussion_lang['discussion_edited']; ?>",
       youText:"<?php echo $discussion_lang['discussion_you']; ?>",
       saveText:"<?php echo $discussion_lang['discussion_save']; ?>",
       deleteText:"<?php echo $discussion_lang['discussion_delete']; ?>",
       viewAllRepliesText:"<?php echo $discussion_lang['discussion_view_all_replies'] . ' (__replyCount__)'; ?>",
       hideRepliesText:"<?php echo $discussion_lang['discussion_hide_replies']; ?>",
       noCommentsText:"<?php echo $discussion_lang['discussion_no_comments']; ?>",
       noAttachmentsText:"<?php echo $discussion_lang['discussion_no_attachments']; ?>",
       attachmentDropText:"<?php echo $discussion_lang['discussion_attachments_drop']; ?>",
       currentUserIsAdmin:current_user_is_admin,
       getComments: function(success, error) {
         $.get(admin_url + 'assets/get_discussion_comments/'+discussion_id+'/'+discussion_type,function(response){
           success(response);
         },'json');
       },
       postComment: function(commentJSON, success, error) {
         $.ajax({
           type: 'post',
           url: admin_url + 'assets/add_discussion_comment/'+discussion_id+'/'+discussion_type,
           data: commentJSON,
           success: function(comment) {
             comment = JSON.parse(comment);
             success(comment)
           },
           error: error
         });
       },
       putComment: function(commentJSON, success, error) {
         $.ajax({
           type: 'post',
           url: admin_url + 'assets/update_discussion_comment',
           data: commentJSON,
           success: function(comment) {
             comment = JSON.parse(comment);
             success(comment)
           },
           error: error
         });
       },
       deleteComment: function(commentJSON, success, error) {
         $.ajax({
           type: 'post',
           url: admin_url + 'assets/delete_discussion_comment/'+commentJSON.id,
           success: success,
           error: error
         });
       },
       timeFormatter: function(time) {
         return moment(time).fromNow();
       },
       uploadAttachments: function(commentArray, success, error) {
         var responses = 0;
         var successfulUploads = [];
         var serverResponded = function() {
           responses++;
             // Check if all requests have finished
             if(responses == commentArray.length) {
                 // Case: all failed
                 if(successfulUploads.length == 0) {
                   error();
                 // Case: some succeeded
               } else {
                 successfulUploads = JSON.parse(successfulUploads);
                 success(successfulUploads)
               }
             }
           }
           $(commentArray).each(function(index, commentJSON) {
             // Create form data
             var formData = new FormData();
             if(commentJSON.file.size && commentJSON.file.size > max_php_ini_upload_size_bytes){
              alert_float('danger',"<?php echo _l("file_exceeds_max_filesize"); ?>");
              serverResponded();
            } else {
             $(Object.keys(commentJSON)).each(function(index, key) {
               var value = commentJSON[key];
               if(value) formData.append(key, value);
             });

             if (typeof(csrfData) !== 'undefined') {
                formData.append(csrfData['token_name'], csrfData['hash']);
             }
             $.ajax({
               url: admin_url + 'assets/add_discussion_comment/'+discussion_id+'/'+discussion_type,
               type: 'POST',
               data: formData,
               cache: false,
               contentType: false,
               processData: false,
               success: function(commentJSON) {
                 successfulUploads.push(commentJSON);
                 serverResponded();
               },
               error: function(data) {
                var error = JSON.parse(data.responseText);
                alert_float('danger',error.message);
                serverResponded();
              },
            });
           }
         });
         }
       });
}
</script>
</body>
</html>
