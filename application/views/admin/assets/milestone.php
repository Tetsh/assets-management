<!-- Miles Stones -->
<div class="modal fade" id="milestone" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('assets/milestone'),array('id'=>'milestone_form')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('edit_milestone'); ?></span>
                    <span class="add-title"><?php echo _l('new_milestone'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_hidden('asset_id',$asset->id); ?>
                        <div id="additional_milestone"></div>
                        <?php echo render_input('name','milestone_name'); ?>
                        <?php echo render_date_input('due_date','milestone_due_date','',($asset->deadline) ? array('data-date-end-date'=>$asset->deadline) : array()); ?>
                        <?php echo render_textarea('description','milestone_description'); ?>
                        <div class="checkbox">
                            <input type="checkbox" id="description_visible_to_organization" name="description_visible_to_organization">
                            <label for="description_visible_to_organization"><?php echo _l('description_visible_to_organization'); ?></label>
                        </div>
                        <?php echo render_input('milestone_order','asset_milestone_order',total_rows('tblmilestones',array('asset_id'=>$asset->id)) + 1,'number'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Mile stones end -->
