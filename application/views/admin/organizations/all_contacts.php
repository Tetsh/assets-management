<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body">
            <div class="clearfix"></div>
            <?php
            $table_data = array(
              _l('organization_firstname'),
              _l('organization_lastname'),
              _l('organization_email'),
              _l('organizations_list_company'),
              _l('organization_phonenumber'),
              _l('contact_position'),
              _l('organizations_list_last_login'),
              _l('contact_active'),
              );
            $custom_fields = get_custom_fields('contacts',array('show_on_table'=>1));
            foreach($custom_fields as $field){
              array_push($table_data,$field['name']);
            }
            array_push($table_data,_l('options'));
            render_datatable($table_data,'all-contacts');
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<?php $this->load->view('admin/organizations/organization_js'); ?>
<div id="contact_data"></div>
<script>
 $(function(){
  var optionsHeading = $('.table-all-contacts').find('th').length - 1;
  initDataTable('.table-all-contacts', window.location.href, [optionsHeading], [optionsHeading],'undefined',[0,'ASC']);
});
</script>
</body>
</html>
