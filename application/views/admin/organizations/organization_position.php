<div class="modal fade" id="organization_position_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('organization_position_edit_heading'); ?></span>
                    <span class="add-title"><?php echo _l('organization_position_add_heading'); ?></span>
                </h4>
            </div>
            <?php echo form_open('admin/organizations/position',array('id'=>'organization-position-modal')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('name','organization_position_name'); ?>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load',function(){
       _validate_form($('#organization-position-modal'), {
        name: 'required'
    }, manage_organization_positions);

       $('#organization_position_modal').on('show.bs.modal', function(e) {
        var invoker = $(e.relatedTarget);
        var position_id = $(invoker).data('id');
        $('#organization_position_modal .add-title').removeClass('hide');
        $('#organization_position_modal .edit-title').addClass('hide');
        $('#organization_position_modal input[name="id"]').val('');
        $('#organization_position_modal input[name="name"]').val('');
        // is from the edit button
        if (typeof(position_id) !== 'undefined') {
            $('#organization_position_modal input[name="id"]').val(position_id);
            $('#organization_position_modal .add-title').addClass('hide');
            $('#organization_position_modal .edit-title').removeClass('hide');
            $('#organization_position_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
        }
    });
   });
    function manage_organization_positions(form) {
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-organization-positions')){
                    $('.table-organization-positions').DataTable().ajax.reload();
                }
                if($('body').hasClass('dynamic-create-positions') && typeof(response.id) != 'undefined') {
                    var positions = $('select[name="positions_in[]"]');
                    positions.prepend('<option value="'+response.id+'">'+response.name+'</option>');
                    positions.selectpicker('refresh');
                }
                alert_float('success', response.message);
            }
            $('#organization_position_modal').modal('hide');
        });
        return false;
    }

</script>
