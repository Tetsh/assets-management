<h4 class="organization-profile-group-heading"><?php echo _l('organization_add_edit_profile'); ?></h4>
<div class="row">
   <?php echo form_open($this->uri->uri_string(),array('class'=>'organization-form','autocomplete'=>'off')); ?>
   <div class="additional"></div>
   <div class="col-md-12">
      <ul class="nav nav-tabs profile-tabs row organization-profile-tabs" role="tablist">
         <li role="presentation" class="<?php if(!$this->input->get('tab')){echo 'active';}; ?>">
            <a href="#contact_info" aria-controls="contact_info" role="tab" data-toggle="tab">
               <?php echo _l( 'organization_profile_details'); ?>
            </a>
         </li>
         <?php
         $organization_custom_fields = false;
         if(total_rows('tblcustomfields',array('fieldto'=>'organizations','active'=>1)) > 0 ){
              $organization_custom_fields = true;
          ?>
          <li role="presentation" class="<?php if($this->input->get('tab') == 'custom_fields'){echo 'active';}; ?>">
            <a href="#custom_fields" aria-controls="custom_fields" role="tab" data-toggle="tab">
               <?php echo do_action('organization_profile_tab_custom_fields_text',_l( 'custom_fields')); ?>
            </a>
         </li>
         <?php } ?>
         <li role="presentation">
            <a href="#billing_and_shipping" aria-controls="billing_and_shipping" role="tab" data-toggle="tab">
               <?php echo _l( 'billing_shipping'); ?>
            </a>
         </li>
         <?php do_action('after_organization_billing_and_shipping_tab',isset($organization) ? $organization : false); ?>
         <?php if(isset($organization)){ ?>
         <li role="presentation<?php if($this->input->get('tab') && $this->input->get('tab') == 'contacts'){echo ' active';}; ?>">
            <a href="#contacts" aria-controls="contacts" role="tab" data-toggle="tab">
                <?php if(is_empty_organization_company($organization->userid)) {
                  echo _l('contact');
                } else {
                  echo _l( 'organization_contacts');
                }
                ?>
            </a>
         </li>
         <li role="presentation">
            <a href="#organization_admins" aria-controls="organization_admins" role="tab" data-toggle="tab">
               <?php echo _l( 'organization_admins'); ?>
            </a>
         </li>
         <?php do_action('after_organization_admins_tab',$organization); ?>
         <?php } ?>
      </ul>
      <div class="tab-content">
         <?php do_action('after_custom_profile_tab_content',isset($organization) ? $organization : false); ?>
         <?php if($organization_custom_fields) { ?>
         <div role="tabpanel" class="tab-pane <?php if($this->input->get('tab') == 'custom_fields'){echo ' active';}; ?>" id="custom_fields">
               <?php $rel_id=( isset($organization) ? $organization->userid : false); ?>
               <?php echo render_custom_fields( 'organizations',$rel_id); ?>
         </div>
         <?php } ?>
         <div role="tabpanel" class="tab-pane<?php if(!$this->input->get('tab')){echo ' active';}; ?>" id="contact_info">
            <div class="row">
               <div class="col-md-12<?php if(isset($organization) && (!is_empty_organization_company($organization->userid) && total_rows('tblcontacts',array('userid'=>$organization->userid,'is_primary'=>1)) > 0)) { echo ''; } else {echo ' hide';} ?>" id="organization-show-primary-contact-wrapper">
                  <div class="checkbox checkbox-info mbot20 no-mtop">
                     <input type="checkbox" name="show_primary_contact"<?php if(isset($organization) && $organization->show_primary_contact == 1){echo ' checked';}?> value="1" id="show_primary_contact">
                     <label for="show_primary_contact"><?php echo _l('show_primary_contact',_l('invoices').', '._l('transfers').', '._l('payments').', '._l('credit_notes')); ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <?php $value=( isset($organization) ? $organization->company : ''); ?>
                  <?php $attrs = (isset($organization) ? array() : array('autofocus'=>true)); ?>
                  <?php echo render_input( 'company', 'organization_company',$value,'text',$attrs); ?>

                  <?php $value=( isset($organization) ? $organization->code : ''); ?>
                  <?php echo render_input( 'code', 'organization_code',$value,'text',$attrs); ?>

                   <?php 
                   $selected = array();
                   if(isset($organization_groups)){
                     foreach($organization_groups as $group){
                        array_push($selected,$group['groupid']);
                     }
                  }
                  if(is_admin() || get_option('staff_members_create_inline_organization_groups') == '1'){
                    echo render_select_with_input_group('groups_in[]',$groups,array('id','name'),'organization_groups',$selected,'<a href="#" data-toggle="modal" data-target="#organization_group_modal"><i class="fa fa-plus"></i></a>',array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
                    } else {
                      echo render_select('groups_in[]',$groups,array('id','name'),'organization_groups',$selected,array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
                    }
                  ?>
                  <?php if(!isset($organization)){ ?>
                  <i class="fa fa-question-circle pull-left" data-toggle="tooltip" data-title="<?php echo _l('organization_currency_change_notice'); ?>"></i>
                  <?php }
                  $s_attrs = array('data-none-selected-text'=>_l('system_default_string'));
                  $selected = '';
                  if(isset($organization) && organization_have_transactions($organization->userid)){
                     $s_attrs['disabled'] = true;
                  }
                  foreach($currencies as $currency){
                     if(isset($organization)){
                       if($currency['id'] == $organization->default_currency){
                         $selected = $currency['id'];
                      }
                   }
                }
                         // Do not remove the currency field from the organization profile!
                echo render_select('default_currency',$currencies,array('id','name','symbol'),'invoice_add_edit_currency',$selected,$s_attrs); ?>
                <?php if(get_option('disable_language') == 0){ ?>
                <div class="form-group select-placeholder">
                   <label for="default_language" class="control-label"><?php echo _l('localization_default_language'); ?>
                   </label>
                   <select name="default_language" id="default_language" class="form-control selectpicker" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                      <option value=""><?php echo _l('system_default_string'); ?></option>
                      <?php foreach(list_folders(APPPATH .'language') as $language){
                         $selected = '';
                         if(isset($organization)){
                            if($organization->default_language == $language){
                               $selected = 'selected';
                            }
                         }
                         ?>
                         <option value="<?php echo $language; ?>" <?php echo $selected; ?>><?php echo ucfirst($language); ?></option>
                         <?php } ?>
                      </select>
                   </div>
                   <?php } ?>

            </div>
            <div class="col-md-6">
               <?php $value=( isset($organization) ? $organization->address : ''); ?>
               <?php echo render_textarea( 'address', 'organization_address',$value); ?>
               <?php $value=( isset($organization) ? $organization->city : ''); ?>
               <?php echo render_input( 'city', 'organization_city',$value); ?>
               <?php $value=( isset($organization) ? $organization->state : ''); ?>
               <?php echo render_input( 'state', 'organization_state',$value); ?>
               <?php $value=( isset($organization) ? $organization->zip : ''); ?>
               <?php echo render_input( 'zip', 'organization_postal_code',$value); ?>
               <?php $countries= get_all_countries();
               $organization_default_country = get_option('organization_default_country');
               $selected =( isset($organization) ? $organization->country : $organization_default_country);
               echo render_select( 'country',$countries,array( 'country_id',array( 'short_name')), 'organizations_country',$selected,array('data-none-selected-text'=>_l('dropdown_non_selected_tex')));
               ?>
            </div>
         </div>
      </div>
      <?php if(isset($organization)){ ?>
      <div role="tabpanel" class="tab-pane<?php if($this->input->get('tab') && $this->input->get('tab') == 'contacts'){echo ' active';}; ?>" id="contacts">
         <?php if(has_permission('organizations','','create') || is_organization_admin($organization->userid)){
            $disable_new_contacts = false;
            if(is_empty_organization_company($organization->userid) && total_rows('tblcontacts',array('userid'=>$organization->userid)) == 1){
               $disable_new_contacts = true;
            }
            ?>
            <div class="inline-block new-contact-wrapper" data-title="<?php echo _l('organization_contact_person_only_one_allowed'); ?>"<?php if($disable_new_contacts){ ?> data-toggle="tooltip"<?php } ?>>
               <a href="#" onclick="contact(<?php echo $organization->userid; ?>); return false;" class="btn btn-info new-contact mbot25<?php if($disable_new_contacts){echo ' disabled';} ?>"><?php echo _l('new_contact'); ?></a>
            </div>
            <?php } ?>
            <?php
            $table_data = array(_l('organization_firstname'),_l('organization_lastname'),_l('organization_email'),_l('contact_position'),_l('organization_phonenumber'),_l('contact_active'),_l('organizations_list_last_login'));
            $custom_fields = get_custom_fields('contacts',array('show_on_table'=>1));
            foreach($custom_fields as $field){
               array_push($table_data,$field['name']);
            }
            array_push($table_data,_l('options'));
            echo render_datatable($table_data,'contacts'); ?>
         </div>
         <div role="tabpanel" class="tab-pane" id="organization_admins">
            <?php if (has_permission('organizations', '', 'create') || has_permission('organizations', '', 'edit')) { ?>
            <a href="#" data-toggle="modal" data-target="#organization_admins_assign" class="btn btn-info mbot30"><?php echo _l('assign_admin'); ?></a>
            <?php } ?>
            <table class="table dt-table">
               <thead>
                  <tr>
                     <th><?php echo _l('staff_member'); ?></th>
                     <th><?php echo _l('organization_admin_date_assigned'); ?></th>
                     <?php if(has_permission('organizations','','create') || has_permission('organizations','','edit')){ ?>
                     <th><?php echo _l('options'); ?></th>
                     <?php } ?>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach($organization_admins as $c_admin){ ?>
                  <tr>
                     <td><a href="<?php echo admin_url('profile/'.$c_admin['staff_id']); ?>">
                        <?php echo staff_profile_image($c_admin['staff_id'], array(
                           'staff-profile-image-small',
                           'mright5'
                        ));
                        echo get_staff_full_name($c_admin['staff_id']); ?></a>
                     </td>
                     <td data-order="<?php echo $c_admin['date_assigned']; ?>"><?php echo _dt($c_admin['date_assigned']); ?></td>
                     <?php if(has_permission('organizations','','create') || has_permission('organizations','','edit')){ ?>
                     <td>
                        <a href="<?php echo admin_url('organizations/delete_organization_admin/'.$organization->userid.'/'.$c_admin['staff_id']); ?>" class="btn btn-danger _delete btn-icon"><i class="fa fa-remove"></i></a>
                     </td>
                     <?php } ?>
                  </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
         <?php } ?>
         <div role="tabpanel" class="tab-pane" id="billing_and_shipping">
            <div class="row">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <h4 class="no-mtop"><?php echo _l('billing_address'); ?> <a href="#" class="pull-right billing-same-as-organization"><small class="font-medium-xs"><?php echo _l('organization_billing_same_as_profile'); ?></small></a></h4>
                        <hr />
                        <?php $value=( isset($organization) ? $organization->billing_street : ''); ?>
                        <?php echo render_textarea( 'billing_street', 'billing_street',$value); ?>
                        <?php $value=( isset($organization) ? $organization->billing_city : ''); ?>
                        <?php echo render_input( 'billing_city', 'billing_city',$value); ?>
                        <?php $value=( isset($organization) ? $organization->billing_state : ''); ?>
                        <?php echo render_input( 'billing_state', 'billing_state',$value); ?>
                        <?php $value=( isset($organization) ? $organization->billing_zip : ''); ?>
                        <?php echo render_input( 'billing_zip', 'billing_zip',$value); ?>
                        <?php $selected=( isset($organization) ? $organization->billing_country : '' ); ?>
                        <?php echo render_select( 'billing_country',$countries,array( 'country_id',array( 'short_name')), 'billing_country',$selected,array('data-none-selected-text'=>_l('dropdown_non_selected_tex'))); ?>
                     </div>
                     <div class="col-md-6">
                        <h4 class="no-mtop">
                           <i class="fa fa-question-circle" data-toggle="tooltip" data-title="<?php echo _l('organization_shipping_address_notice'); ?>"></i>
                           <?php echo _l('shipping_address'); ?> <a href="#" class="pull-right organization-copy-billing-address"><small class="font-medium-xs"><?php echo _l('organization_billing_copy'); ?></small></a>
                        </h4>
                        <hr />
                        <?php $value=( isset($organization) ? $organization->shipping_street : ''); ?>
                        <?php echo render_textarea( 'shipping_street', 'shipping_street',$value); ?>
                        <?php $value=( isset($organization) ? $organization->shipping_city : ''); ?>
                        <?php echo render_input( 'shipping_city', 'shipping_city',$value); ?>
                        <?php $value=( isset($organization) ? $organization->shipping_state : ''); ?>
                        <?php echo render_input( 'shipping_state', 'shipping_state',$value); ?>
                        <?php $value=( isset($organization) ? $organization->shipping_zip : ''); ?>
                        <?php echo render_input( 'shipping_zip', 'shipping_zip',$value); ?>
                        <?php $selected=( isset($organization) ? $organization->shipping_country : '' ); ?>
                        <?php echo render_select( 'shipping_country',$countries,array( 'country_id',array( 'short_name')), 'shipping_country',$selected,array('data-none-selected-text'=>_l('dropdown_non_selected_tex'))); ?>
                     </div>
                     <?php if(isset($organization) &&
                     (total_rows('tblinvoices',array('organizationid'=>$organization->userid)) > 0 || total_rows('tbltransfers',array('organizationid'=>$organization->userid)) > 0 || total_rows('tblcreditnotes',array('organizationid'=>$organization->userid)) > 0)){ ?>
                     <div class="col-md-12">
                        <div class="alert alert-warning">
                           <div class="checkbox checkbox-default">
                              <input type="checkbox" name="update_all_other_transactions" id="update_all_other_transactions">
                              <label for="update_all_other_transactions">
                                 <?php echo _l('organization_update_address_info_on_invoices'); ?><br />
                              </label>
                           </div>
                           <b><?php echo _l('organization_update_address_info_on_invoices_help'); ?></b>
                           <div class="checkbox checkbox-default">
                              <input type="checkbox" name="update_credit_notes" id="update_credit_notes">
                              <label for="update_credit_notes">
                                 <?php echo _l('organization_profile_update_credit_notes'); ?><br />
                              </label>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php echo form_close(); ?>
</div>
<div id="contact_data"></div>
<?php if(isset($organization)){ ?>
<?php if (has_permission('organizations', '', 'create') || has_permission('organizations', '', 'edit')) { ?>
<div class="modal fade" id="organization_admins_assign" tabindex="-1" role="dialog">
   <div class="modal-dialog">
      <?php echo form_open(admin_url('organizations/assign_admins/'.$organization->userid)); ?>
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('assign_admin'); ?></h4>
         </div>
         <div class="modal-body">
            <?php
            $selected = array();
            foreach($organization_admins as $c_admin){
               array_push($selected,$c_admin['staff_id']);
            }
            echo render_select('organization_admins[]',$staff,array('staffid',array('firstname','lastname')),'',$selected,array('multiple'=>true),array(),'','',false); ?>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
            <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
      <?php echo form_close(); ?>
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<?php } ?>
<?php $this->load->view('admin/organizations/organization_group'); ?>
