<?php if(isset($organization)){ ?>
<h4 class="organization-profile-group-heading"><?php echo _l('organization_payments_tab'); ?></h4>
<a href="#" class="btn btn-info mbot25" data-toggle="modal" data-target="#organization_zip_payments"><?php echo _l('zip_payments'); ?></a>
<?php render_datatable(array(
   _l('payments_table_number_heading'),
   _l('payments_table_invoicenumber_heading'),
   _l('payments_table_mode_heading'),
   _l('payment_transaction_id'),
   array(
       'name'=>_l('payments_table_organization_heading'),
       'th_attrs'=>array('class'=>'not_visible')
   ),
   _l('payments_table_amount_heading'),
   _l('payments_table_date_heading'),
   _l('options')
   ),'payments-single-organization'); ?>
<?php include_once(APPPATH . 'views/admin/organizations/modals/zip_payments.php'); ?>
<?php } ?>
