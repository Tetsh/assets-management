<?php if(isset($organization)){ ?>
<h4 class="organization-profile-group-heading"><?php echo _l('transfers'); ?></h4>
<?php if(has_permission('transfers','','create')){ ?>
<a href="<?php echo admin_url('transfers/transfer?organization_id='.$organization->userid); ?>" class="btn btn-info mbot25<?php if($organization->active == 0){echo ' disabled';} ?>"><?php echo _l('create_new_transfer'); ?></a>
<?php } ?>
<?php if(has_permission('transfers','','view') || has_permission('transfers','','view_own')){ ?>
<a href="#" class="btn btn-info mbot25" data-toggle="modal" data-target="#organization_zip_transfers"><?php echo _l('zip_transfers'); ?></a>
<?php } ?>
<?php

   $table_data = array(
      _l('transfer_dt_table_heading_number'),
      _l('transfer_dt_table_heading_amount'),
      _l('transfers_total_tax'),
       array(
         'name'=>_l('invoice_transfer_year'),
         'th_attrs'=>array('class'=>'not_visible')
      ),
      array(
         'name'=>_l('transfer_dt_table_heading_organization'),
         'th_attrs'=>array('class'=>'not_visible')
         ),
      _l('asset'),
      _l('tags'),
      _l('transfer_dt_table_heading_date'),
      _l('transfer_dt_table_heading_expirydate'),
      _l('reference_no'),
      _l('transfer_dt_table_heading_status'));

   $custom_fields = get_custom_fields('transfer',array('show_on_table'=>1));

   foreach($custom_fields as $field){
     array_push($table_data,$field['name']);
   }

   $table_data = do_action('transfers_table_columns',$table_data);

   render_datatable($table_data, 'transfers-single-organization');
   include_once(APPPATH . 'views/admin/organizations/modals/zip_transfers.php');
   ?>
<?php } ?>
