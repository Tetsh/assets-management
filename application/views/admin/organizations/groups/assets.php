<h4 class="organization-profile-group-heading"><?php echo _l('assets'); ?></h4>
<div class="row">
   <?php
      $_where = '';
      if(!has_permission('assets','','view')){
       $_where = 'id IN (SELECT asset_id FROM tblassetmembers WHERE staff_id='.get_staff_user_id().')';
      }
      ?>
   <?php foreach($asset_statuses as $status){ ?>
   <div class="col-md-5ths total-column">
      <div class="panel_s">
         <div class="panel-body">
            <h3 class="text-muted _total">
               <?php $where = ($_where == '' ? '' : $_where.' AND ').'status = '.$status['id']. ' AND organizationid='.$organization->userid; ?>
               <?php echo total_rows('tblassets',$where); ?>
            </h3>
            <span style="color:<?php echo $status['color']; ?>"><?php echo $status['name']; ?></span>
         </div>
      </div>
   </div>
   <?php } ?>
</div>
<?php if(isset($organization)){ ?>
<?php if(has_permission('assets','','create')){ ?>
<a href="<?php echo admin_url('assets/asset?organization_id='.$organization->userid); ?>" class="btn btn-info mbot25<?php if($organization->active == 0){echo ' disabled';} ?>"><?php echo _l('new_asset'); ?></a>
<?php }
   $table_data = array(
     '#',
     _l('asset_name'),
     array(
       'name'=>_l('asset_organization'),
       'th_attrs'=>array('class'=>'not_visible')
       ),
     _l('tags'),
     _l('asset_start_date'),
     _l('asset_deadline'),
     _l('asset_members'),
     );
   if(has_permission('assets','','create') || has_permission('assets','','edit')){
    array_push($table_data,_l('asset_billing_type'));
   }
   array_push($table_data, _l('asset_status'));
   $custom_fields = get_custom_fields('assets',array('show_on_table'=>1));
   foreach($custom_fields as $field){
     array_push($table_data,$field['name']);
   }

   $table_data = do_action('assets_table_columns',$table_data);

   array_push($table_data, _l('options'));

   render_datatable($table_data,'assets-single-organization');
   }
   ?>
