<h4 class="organization-profile-group-heading"><?php echo _l('organization_reminders_tab'); ?></h4>
<?php if(isset($organization)){ ?>
<a href="#" data-toggle="modal" data-target=".reminder-modal-organization-<?php echo $organization->userid; ?>" class="btn btn-info mbot25"><i class="fa fa-bell-o"></i> <?php echo _l('set_reminder'); ?></a>
<div class="clearfix"></div>

<?php render_datatable(array( _l( 'reminder_description'), _l( 'reminder_date'), _l( 'reminder_staff'), _l( 'reminder_is_notified'), _l( 'options'), ), 'reminders');
$this->load->view('admin/includes/modals/reminder',array('id'=>$organization->userid,'name'=>'organization','members'=>$members,'reminder_title'=>_l('set_reminder')));
} ?>
