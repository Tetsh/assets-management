<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="_filters _hidden_inputs hidden">
                    <?php
                    echo form_hidden('my_organizations');
                    foreach($groups as $group){
                       echo form_hidden('organization_group_'.$group['id']);
                   }
                   foreach($contract_types as $type){
                       echo form_hidden('contract_type_'.$type['id']);
                   }
                   foreach($invoice_statuses as $status){
                       echo form_hidden('invoices_'.$status);
                   }
                   foreach($transfer_statuses as $status){
                       echo form_hidden('transfers_'.$status);
                   }
                   foreach($asset_statuses as $status){
                    echo form_hidden('assets_'.$status['id']);
                }
                foreach($proposal_statuses as $status){
                    echo form_hidden('proposals_'.$status);
                }
                foreach($organization_admins as $cadmin){
                    echo form_hidden('responsible_admin_'.$cadmin['staff_id']);
                }
                foreach($countries as $country){
                    echo form_hidden('country_'.$country['country_id']);
                }
                ?>
            </div>
            <div class="panel_s">
                <div class="panel-body">
                    <div class="_buttons">
                        <?php if (has_permission('organizations','','create')) { ?>
                        <a href="<?php echo admin_url('organizations/organization'); ?>" class="btn btn-info mright5 test pull-left display-block">
                            <?php echo _l('new_organization'); ?></a>
                            <a href="<?php echo admin_url('organizations/import'); ?>" class="btn btn-info pull-left display-block mright5 hidden-xs">
                                <?php echo _l('import_organizations'); ?></a>
                                <?php } ?>
                                <a href="<?php echo admin_url('organizations/all_contacts'); ?>" class="btn btn-info pull-left display-block mright5">
                                    <?php echo _l('organization_contacts'); ?></a>
                                    <div class="visible-xs">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-filter" aria-hidden="true"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-left" style="width:300px;">
                                            <li class="active"><a href="#" data-cview="all" onclick="dt_custom_view('','.table-organizations',''); return false;"><?php echo _l('organizations_sort_all'); ?></a>
                                            </li>
                                             <li class="divider"></li>
                                             <li>
                                                  <a href="#" data-cview="my_organizations" onclick="dt_custom_view('my_organizations','.table-organizations','my_organizations'); return false;">
                                                           <?php echo _l('organizations_assigned_to_me'); ?>
                                                        </a>
                                             </li>
                                            <li class="divider"></li>
                                            <?php if(count($groups) > 0){ ?>
                                            <li class="dropdown-submenu pull-left groups">
                                                <a href="#" tabindex="-1"><?php echo _l('organization_groups'); ?></a>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <?php foreach($groups as $group){ ?>
                                                    <li><a href="#" data-cview="organization_group_<?php echo $group['id']; ?>" onclick="dt_custom_view('organization_group_<?php echo $group['id']; ?>','.table-organizations','organization_group_<?php echo $group['id']; ?>'); return false;"><?php echo $group['name']; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <div class="clearfix"></div>
                                            <li class="divider"></li>
                                            <?php } ?>
                                            <?php if(count($countries) > 1){ ?>
                                            <li class="dropdown-submenu pull-left countries">
                                                <a href="#" tabindex="-1"><?php echo _l('organizations_country'); ?></a>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <?php foreach($countries as $country){ ?>
                                                    <li><a href="#" data-cview="country_<?php echo $country['country_id']; ?>" onclick="dt_custom_view('country_<?php echo $country['country_id']; ?>','.table-organizations','country_<?php echo $country['country_id']; ?>'); return false;"><?php echo $country['short_name']; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <div class="clearfix"></div>
                                            <li class="divider"></li>
                                            <?php } ?>
                                            <li class="dropdown-submenu pull-left invoice">
                                                <a href="#" tabindex="-1"><?php echo _l('invoices'); ?></a>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <?php foreach($invoice_statuses as $status){ ?>
                                                    <li>
                                                        <a href="#" data-cview="invoices_<?php echo $status; ?>" onclick="dt_custom_view('invoices_<?php echo $status; ?>','.table-organizations','invoices_<?php echo $status; ?>'); return false;"><?php echo _l('organization_have_invoices_by',format_invoice_status($status,'',false)); ?></a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <div class="clearfix"></div>
                                            <li class="divider"></li>
                                            <li class="dropdown-submenu pull-left transfer">
                                                <a href="#" tabindex="-1"><?php echo _l('transfers'); ?></a>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <?php foreach($transfer_statuses as $status){ ?>
                                                    <li>
                                                        <a href="#" data-cview="transfers_<?php echo $status; ?>" onclick="dt_custom_view('transfers_<?php echo $status; ?>','.table-organizations','transfers_<?php echo $status; ?>'); return false;">
                                                            <?php echo _l('organization_have_transfers_by',format_transfer_status($status,'',false)); ?>
                                                        </a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <div class="clearfix"></div>
                                            <li class="divider"></li>
                                            <li class="dropdown-submenu pull-left asset">
                                                <a href="#" tabindex="-1"><?php echo _l('assets'); ?></a>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <?php foreach($asset_statuses as $status){ ?>
                                                    <li>
                                                        <a href="#" data-cview="assets_<?php echo $status['id']; ?>" onclick="dt_custom_view('assets_<?php echo $status['id']; ?>','.table-organizations','assets_<?php echo $status['id']; ?>'); return false;">
                                                            <?php echo _l('organization_have_assets_by',$status['name']); ?>
                                                        </a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>

                                            <div class="clearfix"></div>
                                            <?php if(count($contract_types) > 0) { ?>
                                            <li class="divider"></li>
                                            <li class="dropdown-submenu pull-left contract_types">
                                                <a href="#" tabindex="-1"><?php echo _l('contract_types'); ?></a>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <?php foreach($contract_types as $type){ ?>
                                                    <li>
                                                        <a href="#" data-cview="contract_type_<?php echo $type['id']; ?>" onclick="dt_custom_view('contract_type_<?php echo $type['id']; ?>','.table-organizations','contract_type_<?php echo $type['id']; ?>'); return false;">
                                                            <?php echo _l('organization_have_contracts_by_type',$type['name']); ?>
                                                        </a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <?php } ?>
                                            <?php if(count($organization_admins) > 0 && (has_permission('organizations','','create') || has_permission('organizations','','edit'))){ ?>
                                            <div class="clearfix"></div>
                                            <li class="divider"></li>
                                            <li class="dropdown-submenu pull-left responsible_admin">
                                                <a href="#" tabindex="-1"><?php echo _l('responsible_admin'); ?></a>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <?php foreach($organization_admins as $cadmin){ ?>
                                                    <li>
                                                        <a href="#" data-cview="responsible_admin_<?php echo $cadmin['staff_id']; ?>" onclick="dt_custom_view('responsible_admin_<?php echo $cadmin['staff_id']; ?>','.table-organizations','responsible_admin_<?php echo $cadmin['staff_id']; ?>'); return false;">
                                                            <?php echo get_staff_full_name($cadmin['staff_id']); ?>
                                                        </a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <?php if(has_permission('organizations','','view') || have_assigned_organizations()) {
                                    $where_summary = '';
                                    if(!has_permission('organizations','','view')){
                                        $where_summary = ' AND userid IN (SELECT organization_id FROM tblorganizationadmins WHERE staff_id='.get_staff_user_id().')';
                                    }
                                    ?>
                                    <hr class="hr-panel-heading" />
                                    <div class="row mbot15">
                                        <div class="col-md-12">
                                            <h4 class="no-margin"><?php echo _l('organizations_summary'); ?></h4>
                                        </div>
                                        <div class="col-md-2 col-xs-6 border-right">
                                            <h3 class="bold"><?php echo total_rows('tblorganizations',($where_summary != '' ? substr($where_summary,5) : '')); ?></h3>
                                            <span class="text-dark"><?php echo _l('organizations_summary_total'); ?></span>
                                        </div>
                                        <div class="col-md-2 col-xs-6 border-right">
                                            <h3 class="bold"><?php echo total_rows('tblorganizations','active=1'.$where_summary); ?></h3>
                                            <span class="text-success"><?php echo _l('active_organizations'); ?></span>
                                        </div>
                                        <div class="col-md-2 col-xs-6 border-right">
                                            <h3 class="bold"><?php echo total_rows('tblorganizations','active=0'.$where_summary); ?></h3>
                                            <span class="text-danger"><?php echo _l('inactive_active_organizations'); ?></span>
                                        </div>
                                        <div class="col-md-2 col-xs-6 border-right">
                                            <h3 class="bold"><?php echo total_rows('tblcontacts','active=1'.$where_summary); ?></h3>
                                            <span class="text-info"><?php echo _l('organizations_summary_active'); ?></span>
                                        </div>
                                        <div class="col-md-2  col-xs-6 border-right">
                                            <h3 class="bold"><?php echo total_rows('tblcontacts','active=0'.$where_summary); ?></h3>
                                            <span class="text-danger"><?php echo _l('organizations_summary_inactive'); ?></span>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <h3 class="bold"><?php echo total_rows('tblcontacts','last_login LIKE "'.date('Y-m-d').'%"'.$where_summary); ?></h3>
                                            <span class="text-muted">
                                                <?php
                                                $contactsTemplate = '';
                                                if(count($contacts_logged_in_today)> 0){
                                                   foreach($contacts_logged_in_today as $contact){
                                                    $url = admin_url('organizations/organization/'.$contact['userid'].'?contactid='.$contact['id']);
                                                    $fullName = $contact['firstname'] . ' ' . $contact['lastname'];
                                                    $dateLoggedIn = _dt($contact['last_login']);
                                                    $html = "<a href='$url' target='_blank'>$fullName</a><br /><small>$dateLoggedIn</small><br />";
                                                    $contactsTemplate .= htmlspecialchars('<p class="mbot5">'.$html.'</p>');
                                                }
                                                ?>
                                                <?php } ?>
                                                <span<?php if($contactsTemplate != ''){ ?> class="pointer text-has-action" data-toggle="popover" data-title="<?php echo _l('organizations_summary_logged_in_today'); ?>" data-html="true" data-content="<?php echo $contactsTemplate; ?>" data-placement="bottom" <?php } ?>><?php echo _l('organizations_summary_logged_in_today'); ?></span>
                                            </span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <hr class="hr-panel-heading" />
                                        <a href="#" data-toggle="modal" data-target="#organizations_bulk_action" class="bulk-actions-btn table-btn hide" data-table=".table-organizations"><?php echo _l('bulk_actions'); ?></a>
                                        <div class="modal fade bulk_actions" id="organizations_bulk_action" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                             <div class="modal-content">
                                              <div class="modal-header">
                                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                               <h4 class="modal-title"><?php echo _l('bulk_actions'); ?></h4>
                                           </div>
                                           <div class="modal-body">
                                              <?php if(has_permission('organizations','','delete')){ ?>
                                              <div class="checkbox checkbox-danger">
                                                <input type="checkbox" name="mass_delete" id="mass_delete">
                                                <label for="mass_delete"><?php echo _l('mass_delete'); ?></label>
                                            </div>
                                            <hr class="mass_delete_separator" />
                                            <?php } ?>
                                            <div id="bulk_change">
                                               <?php echo render_select('move_to_groups_organizations_bulk[]',$groups,array('id','name'),'organization_groups','', array('multiple'=>true),array(),'','',false); ?>
                                               <p class="text-danger"><?php echo _l('bulk_action_organizations_groups_warning'); ?></p>
                                           </div>
                                       </div>
                                       <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                                           <a href="#" class="btn btn-info" onclick="organizations_bulk_action(this); return false;"><?php echo _l('confirm'); ?></a>
                                       </div>
                                   </div><!-- /.modal-content -->
                               </div><!-- /.modal-dialog -->
                           </div><!-- /.modal -->
                           <div class="checkbox">
                                <input type="checkbox" checked id="exclude_inactive" name="exclude_inactive">
                                <label for="exclude_inactive"><?php echo _l('exclude_inactive'); ?> <?php echo _l('organizations'); ?></label>
                            </div>
                           <div class="clearfix mtop20"></div>
                           <?php
                           $table_data = array();
                           $_table_data = array(
                            '<span class="hide"> - </span><div class="checkbox mass_select_all_wrap"><input type="checkbox" id="mass_select_all" data-to-table="organizations"><label></label></div>',
                            '#',
                            _l('organizations_list_company'),
                            _l('contact_primary'),
                            _l('company_primary_email'),
                            _l('organizations_list_phone'),
                            _l('organization_active'),
                            _l('organization_groups'),
                            );

                           foreach($_table_data as $_t){
                            array_push($table_data,$_t);
                        }

                        $custom_fields = get_custom_fields('organizations',array('show_on_table'=>1));
                        foreach($custom_fields as $field){
                            array_push($table_data,$field['name']);
                        }

                        $table_data = do_action('organizations_table_columns',$table_data);

                        $_op = _l('options');

                        array_push($table_data, $_op);
                        render_datatable($table_data,'organizations');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<script>
    $(function(){
        var CustomersServerParams = {};
        $.each($('._hidden_inputs._filters input'),function(){
           CustomersServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
       });
        CustomersServerParams['exclude_inactive'] = '[name="exclude_inactive"]:checked';

        var headers_organizations = $('.table-organizations').find('th');
        var not_sortable_organizations = (headers_organizations.length - 1);
        var tAPI = initDataTable('.table-organizations', admin_url+'organizations/table', [not_sortable_organizations,0], [not_sortable_organizations,0], CustomersServerParams,<?php echo do_action('organizations_table_default_order',json_encode(array(2,'ASC'))); ?>);
        $('input[name="exclude_inactive"]').on('change',function(){
            tAPI.ajax.reload();
        });
    });
    function organizations_bulk_action(event) {
        var r = confirm(appLang.confirm_action_prompt);
        if (r == false) {
            return false;
        } else {
            var mass_delete = $('#mass_delete').prop('checked');
            var ids = [];
            var data = {};
            if(mass_delete == false || typeof(mass_delete) == 'undefined'){
                data.groups = $('select[name="move_to_groups_organizations_bulk[]"]').selectpicker('val');
                if (data.groups.length == 0) {
                    data.groups = 'remove_all';
                }
            } else {
                data.mass_delete = true;
            }
            var rows = $('.table-organizations').find('tbody tr');
            $.each(rows, function() {
                var checkbox = $($(this).find('td').eq(0)).find('input');
                if (checkbox.prop('checked') == true) {
                    ids.push(checkbox.val());
                }
            });
            data.ids = ids;
            $(event).addClass('disabled');
            setTimeout(function(){
              $.post(admin_url + 'organizations/bulk_action', data).done(function() {
               window.location.reload();
           });
          },50);
        }
    }


</script>
</body>
</html>
