<?php init_head(); ?>
<div id="wrapper" class="organization_profile">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <?php if(isset($organization) && $organization->active == 0){ ?>
            <div class="alert alert-warning">
               <?php echo _l('organization_inactive_message'); ?>
               <br />
               <a href="<?php echo admin_url('organizations/mark_as_active/'.$organization->userid); ?>"><?php echo _l('mark_as_active'); ?></a>
            </div>
            <?php } ?>
            <?php if(isset($organization) && (!has_permission('organizations','','view') && is_organization_admin($organization->userid))){?>
            <div class="alert alert-info">
               <?php echo _l('organization_admin_login_as_organization_message',get_staff_full_name(get_staff_user_id())); ?>
            </div>
            <?php } ?>
         </div>
         <?php if($group == 'profile'){ ?>
         <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
            <button class="btn btn-info only-save organization-form-submiter">
               <?php echo _l( 'submit'); ?>
            </button>
            <?php if(!isset($organization)){ ?>
            <button class="btn btn-info save-and-add-contact organization-form-submiter">
               <?php echo _l( 'save_organization_and_add_contact'); ?>
            </button>
            <?php } ?>
         </div>
         <?php } ?>
         <?php if(isset($organization)){ ?>
         <div class="col-md-3">
            <div class="panel_s">
               <div class="panel-body organization-profile-tabs">
                  <h4 class="organization-heading-profile bold">
                       <?php if(has_permission('organizations','','delete') || is_admin()){ ?>
                  <div class="btn-group pull-left mright10">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-left">
                        <?php if(is_admin()){ ?>
                        <li>
                           <a href="<?php echo admin_url('organizations/login_as_organization/'.$organization->userid); ?>" target="_blank">
                              <i class="fa fa-share-square-o"></i> <?php echo _l('login_as_organization'); ?>
                           </a>
                        </li>
                        <?php } ?>
                        <?php if(has_permission('organizations','','delete')){ ?>
                        <li>
                           <a href="<?php echo admin_url('organizations/delete/'.$organization->userid); ?>" class="text-danger delete-text _delete"><i class="fa fa-remove"></i> <?php echo _l('delete'); ?>
                           </a>
                        </li>
                        <?php } ?>
                     </ul>
                  </div>
                  <?php } ?>
                  #<?php echo $organization->userid . ' ' . $title; ?></h4>
                  <?php $this->load->view('admin/organizations/tabs'); ?>

               </div>
            </div>
         </div>
         <?php } ?>
         <div class="col-md-<?php if(isset($organization)){echo 9;} else {echo 12;} ?>">
            <div class="panel_s">
               <div class="panel-body">
                  <?php if(isset($organization)){ ?>
                  <?php echo form_hidden( 'isedit'); ?>
                  <?php echo form_hidden( 'userid',$organization->userid); ?>
                  <div class="clearfix"></div>
                  <?php } ?>
                  <div>
                     <div class="tab-content">
                        <?php $this->load->view('admin/organizations/groups/'.$group); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php if($group == 'profile'){ ?>
      <div class="btn-bottom-pusher"></div>
      <?php } ?>
   </div>
</div>
<?php init_tail(); ?>
<?php if(isset($organization)){ ?>
<script>
   $(function(){
      init_rel_tasks_table(<?php echo $organization->userid; ?>,'organization');
   });
</script>
<?php } ?>
<?php if(!empty($google_api_key) && !empty($organization->latitude) && !empty($organization->longitude)){ ?>
<script>
   var latitude = '<?php echo $organization->latitude; ?>';
   var longitude = '<?php echo $organization->longitude; ?>';
   var mapMarkerTitle = '<?php echo $organization->company; ?>';
</script>
<?php echo app_script('assets/js','map.js'); ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key; ?>&callback=initMap"></script>
<?php } ?>
<?php $this->load->view('admin/organizations/organization_js'); ?>
</body>
</html>
