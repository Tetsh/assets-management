<?php
defined('BASEPATH') or exit('No direct script access allowed');

$asset_id = $this->ci->input->post('asset_id');

$aColumns = array(
    'number',
    'total',
    'total_tax',
    'YEAR(date) as year',
    get_sql_select_organization_company(),
    '(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM tbltags_in JOIN tbltags ON tbltags_in.tag_id = tbltags.id WHERE rel_id = tbltransfers.id and rel_type="transfer" ORDER by tag_order ASC) as tags',
    'date',
    'expirydate',
    'reference_no',
    'tbltransfers.status',
    );

$join = array(
    'LEFT JOIN tblorganizations ON tblorganizations.userid = tbltransfers.organizationid',
    'LEFT JOIN tblcurrencies ON tblcurrencies.id = tbltransfers.currency',
);

$sIndexColumn = "id";
$sTable       = 'tbltransfers';

$custom_fields = get_table_custom_fields('transfer');

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_'.$key);
    array_push($customFieldsColumns,$selectAs);
    array_push($aColumns, 'ctable_'.$key.'.value as '.$selectAs);
    array_push($join, 'LEFT JOIN tblcustomfieldsvalues as ctable_'.$key . ' ON tbltransfers.id = ctable_'.$key . '.relid AND ctable_'.$key . '.fieldto="'.$field['fieldto'].'" AND ctable_'.$key . '.fieldid='.$field['id']);
}

$where                    = array();
$filter = array();

if ($this->ci->input->post('not_sent')) {
    array_push($filter, 'OR (sent= 0 AND tbltransfers.status NOT IN (2,3,4))');
}
if ($this->ci->input->post('invoiced')) {
    array_push($filter, 'OR invoiceid IS NOT NULL');
}

if ($this->ci->input->post('not_invoiced')) {
    array_push($filter, 'OR invoiceid IS NULL');
}
$statuses = $this->ci->transfers_model->get_statuses();
$statusIds = array();
foreach ($statuses as $status) {
    if ($this->ci->input->post('transfers_'.$status)) {
        array_push($statusIds, $status);
    }
}
if (count($statusIds) > 0) {
    array_push($filter, 'AND tbltransfers.status IN (' . implode(', ', $statusIds) . ')');
}

$agents = $this->ci->transfers_model->get_sale_agents();
$agentsIds = array();
foreach ($agents as $agent) {
    if ($this->ci->input->post('sale_agent_'.$agent['sale_agent'])) {
        array_push($agentsIds, $agent['sale_agent']);
    }
}
if (count($agentsIds) > 0) {
    array_push($filter, 'AND sale_agent IN (' . implode(', ', $agentsIds) . ')');
}

$years = $this->ci->transfers_model->get_transfers_years();
$yearsArray = array();
foreach ($years as $year) {
    if ($this->ci->input->post('year_'.$year['year'])) {
        array_push($yearsArray, $year['year']);
    }
}
if (count($yearsArray) > 0) {
    array_push($filter, 'AND YEAR(date) IN ('.implode(', ', $yearsArray).')');
}

if (count($filter) > 0) {
    array_push($where, 'AND ('.prepare_dt_filter($filter).')');
}

if ($organizationid != '') {
    array_push($where, 'AND tbltransfers.organizationid='.$organizationid);
}

if (!has_permission('transfers', '', 'view')) {
    array_push($where, 'AND tbltransfers.addedfrom='.get_staff_user_id());
}

$aColumns = do_action('transfers_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
    'tbltransfers.id',
    'tbltransfers.organizationid',
    'tbltransfers.invoiceid',
    'symbol',
));

$output       = $result['output'];
$rResult      = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = array();

    $numberOutput = '';
     // If is from organization area table or assets area request
        $numberOutput = '<a href="' . admin_url('transfers/list_transfers/' . $aRow['id']) . '" onclick="init_transfer(' . $aRow['id'] . '); return false;">' . format_transfer_number($aRow['id']) . '</a>';
    

    if($aRow['invoiceid']){
        $numberOutput .= '<br /><span class="hide"> - </span><span class="text-success">' . _l('transfer_invoiced') . '</span>';
    }

    $row[] = $numberOutput;

    $row[] = format_money($aRow['total'], $aRow['symbol']);

    $row[] = format_money($aRow['total_tax'], $aRow['symbol']);

    $row[] = $aRow['year'];

    $row[] = '<a href="' . admin_url('organizations/organization/' . $aRow['organizationid']) . '">' . $aRow['company'] . '</a>';

    $row[] = render_tags($aRow['tags']);

    $row[] = _d($aRow['date']);

    $row[] = _d($aRow['expirydate']);

    $row[] = $aRow['reference_no'];

    $row[] = format_transfer_status($aRow['tbltransfers.status']);

    // Custom fields add values
    foreach ($customFieldsColumns as $customFieldColumn) {
        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
    }

    $hook = do_action('transfers_table_row_data', array(
        'output' => $row,
        'row' => $aRow
    ));

    $row = $hook['output'];
    $output['aaData'][] = $row;
}

echo json_encode($output);
die();
