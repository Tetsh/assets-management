<?php
defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionDelete = has_permission('organizations', '', 'delete');

$custom_fields = get_table_custom_fields('organizations');

$aColumns = array(
    '1',
    'tblorganizations.userid as userid',
    'company',
    'CONCAT(firstname, " ", lastname) as contact_fullname',
    'email',
    'tblorganizations.phonenumber as phonenumber',
    'tblorganizations.active',
    '(SELECT GROUP_CONCAT(name ORDER BY name ASC) FROM tblorganizationsgroups LEFT JOIN tblorganizationgroups_in ON tblorganizationgroups_in.groupid = tblorganizationsgroups.id WHERE organization_id = tblorganizations.userid) as groups'
    );

$sIndexColumn = "userid";
$sTable       = 'tblorganizations';
$where   = array();
// Add blank where all filter can be stored
$filter  = array();

$join = array('LEFT JOIN tblcontacts ON tblcontacts.userid=tblorganizations.userid AND tblcontacts.is_primary=1');

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_'.$key);
    array_push($customFieldsColumns,$selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN tblcustomfieldsvalues as ctable_' . $key . ' ON tblorganizations.userid = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}
// Filter by custom groups
$groups  = $this->ci->organizations_model->get_groups();
$groupIds = array();
foreach ($groups as $group) {
    if ($this->ci->input->post('organization_group_' . $group['id'])) {
        array_push($groupIds, $group['id']);
    }
}
if (count($groupIds) > 0) {
    array_push($filter, 'AND tblorganizations.userid IN (SELECT organization_id FROM tblorganizationgroups_in WHERE groupid IN (' . implode(', ', $groupIds) . '))');
}

$countries  = $this->ci->organizations_model->get_organizations_distinct_countries();
$countryIds = array();
foreach ($countries as $country) {
    if ($this->ci->input->post('country_' . $country['country_id'])) {
        array_push($countryIds, $country['country_id']);
    }
}
if (count($countryIds) > 0) {
    array_push($filter, 'AND country IN ('.implode(',',$countryIds).')');
}


$this->ci->load->model('invoices_model');
// Filter by invoices
$invoiceStatusIds = array();
foreach ($this->ci->invoices_model->get_statuses() as $status) {
    if ($this->ci->input->post('invoices_' . $status)) {
        array_push($invoiceStatusIds, $status);
    }
}
if (count($invoiceStatusIds) > 0) {
    array_push($filter, 'AND tblorganizations.userid IN (SELECT organizationid FROM tblinvoices WHERE status IN (' . implode(', ', $invoiceStatusIds) . '))');
}

// Filter by transfers
$transferStatusIds = array();
$this->ci->load->model('transfers_model');
foreach ($this->ci->transfers_model->get_statuses() as $status) {
    if ($this->ci->input->post('transfers_' . $status)) {
        array_push($transferStatusIds, $status);
    }
}
if (count($transferStatusIds) > 0) {
    array_push($filter, 'AND tblorganizations.userid IN (SELECT organizationid FROM tbltransfers WHERE status IN (' . implode(', ', $transferStatusIds) . '))');
}

// Filter by assets
$assetStatusIds = array();
$this->ci->load->model('assets_model');
foreach ($this->ci->assets_model->get_asset_statuses() as $status) {
    if ($this->ci->input->post('assets_' . $status['id'])) {
        array_push($assetStatusIds, $status['id']);
    }
}
if (count($assetStatusIds) > 0) {
    array_push($filter, 'AND tblorganizations.userid IN (SELECT organizationid FROM tblassets WHERE status IN (' . implode(', ', $assetStatusIds) . '))');
}

// Filter by proposals
$proposalStatusIds = array();
$this->ci->load->model('proposals_model');
foreach ($this->ci->proposals_model->get_statuses() as $status) {
    if ($this->ci->input->post('proposals_' . $status)) {
        array_push($proposalStatusIds, $status);
    }
}
if (count($proposalStatusIds) > 0) {
    array_push($filter, 'AND tblorganizations.userid IN (SELECT rel_id FROM tblproposals WHERE status IN (' . implode(', ', $proposalStatusIds) . ') AND rel_type="organization")');
}

// Filter by having contracts by type
$this->ci->load->model('contracts_model');
$contractTypesIds = array();
$contract_types  = $this->ci->contracts_model->get_contract_types();

foreach ($contract_types as $type) {
    if ($this->ci->input->post('contract_type_' . $type['id'])) {
        array_push($contractTypesIds, $type['id']);
    }
}
if (count($contractTypesIds) > 0) {
    array_push($filter, 'AND tblorganizations.userid IN (SELECT organization FROM tblcontracts WHERE contract_type IN (' . implode(', ', $contractTypesIds) . '))');
}

// Filter by proposals
$customAdminIds = array();
foreach ($this->ci->organizations_model->get_organizations_admin_unique_ids() as $cadmin) {
    if ($this->ci->input->post('responsible_admin_' . $cadmin['staff_id'])) {
        array_push($customAdminIds, $cadmin['staff_id']);
    }
}

if (count($customAdminIds) > 0) {
    array_push($filter, 'AND tblorganizations.userid IN (SELECT organization_id FROM tblorganizationadmins WHERE staff_id IN (' . implode(', ', $customAdminIds) . '))');
}

if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}

if (!has_permission('organizations', '', 'view')) {
    array_push($where, 'AND tblorganizations.userid IN (SELECT organization_id FROM tblorganizationadmins WHERE staff_id=' . get_staff_user_id() . ')');
}

if($this->ci->input->post('exclude_inactive')){
    array_push($where,'AND tblorganizations.active=1');
}

if($this->ci->input->post('my_organizations')){
    array_push($where,'AND tblorganizations.userid IN (SELECT organization_id FROM tblorganizationadmins WHERE staff_id='.get_staff_user_id().')');
}

$aColumns = do_action('organizations_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
    'tblcontacts.id as contact_id',
    'tblorganizations.zip as zip'
));

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {

    $row = array();

    // Bulk actions
    $row[] = '<div class="checkbox"><input type="checkbox" value="' . $aRow['userid'] . '"><label></label></div>';
    // User id
    $row[] = $aRow['userid'];

    // Company
    $company = $aRow['company'];

    if ($company == '') {
        $company = _l('no_company_view_profile');
    }

    $row[] = '<a href="' . admin_url('organizations/organization/' . $aRow['userid']) . '">' . $company . '</a>';

    // Primary contact
    $row[] = ($aRow['contact_id'] ? '<a href="' . admin_url('organizations/organization/' . $aRow['userid'] . '?contactid=' . $aRow['contact_id']) . '" target="_blank">' . $aRow['contact_fullname'] . '</a>' : '');

    // Primary contact email
    $row[] = ($aRow['email'] ? '<a href="mailto:' . $aRow['email'] . '">' . $aRow['email'] . '</a>' : '');

    // Primary contact phone
    $row[] = ($aRow['phonenumber'] ? '<a href="tel:' . $aRow['phonenumber'] . '">' . $aRow['phonenumber'] . '</a>' : '');

    // Toggle active/inactive organization
    $toggleActive = '<div class="onoffswitch" data-toggle="tooltip" data-title="' . _l('organization_active_inactive_help') . '">
        <input type="checkbox" data-switch-url="' . admin_url().'organizations/change_organization_status" name="onoffswitch" class="onoffswitch-checkbox" id="' . $aRow['userid'] . '" data-id="' . $aRow['userid'] . '" ' . ($aRow['tblorganizations.active'] == 1 ? 'checked' : '') . '>
        <label class="onoffswitch-label" for="' . $aRow['userid'] . '"></label>
    </div>';

    // For exporting
    $toggleActive .= '<span class="hide">' . ($aRow['tblorganizations.active'] == 1 ? _l('is_active_export') : _l('is_not_active_export')) . '</span>';

    $row[] = $toggleActive;

    // Customer groups parsing
    $groupsRow  = '';
    if ($aRow['groups']) {
        $groups = explode(',', $aRow['groups']);
        foreach ($groups as $group) {
            $groupsRow .= '<span class="label label-default mleft5 inline-block organization-group-list pointer">' . $group . '</span>';
        }
    }

    $row[] = $groupsRow;

    // Custom fields add values
    foreach($customFieldsColumns as $customFieldColumn){
        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
    }

    $hook = do_action('organizations_table_row_data', array(
        'output' => $row,
        'row' => $aRow
    ));

    $row = $hook['output'];

    // Table options
    $options = icon_btn('organizations/organization/' . $aRow['userid'], 'pencil-square-o');

    // Show button delete if permission for delete exists
    if ($hasPermissionDelete) {
        $options .= icon_btn('organizations/delete/' . $aRow['userid'], 'remove', 'btn-danger _delete');
    }

    $row[] = $options;
    $output['aaData'][] = $row;
}
