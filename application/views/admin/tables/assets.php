<?php
defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionEdit = has_permission('assets', '', 'edit');
$hasPermissionDelete = has_permission('assets', '', 'delete');
$hasPermissionCreate = has_permission('assets', '', 'create');

$aColumns = array(
    'tblassets.id as id',
    'tblassets.name',
    'tblassets.location',
    get_sql_select_organization_company(),
    'class_id',
    'category_id',
    'start_date',
    'asset_cost',
    'status',
    );


$sIndexColumn = "id";
$sTable       = 'tblassets';

$join             = array(
    'JOIN tblorganizations ON tblorganizations.userid = tblassets.organizationid',
);

$where  = array();
$filter = array();

if ($organizationid != '') {
    array_push($where, ' AND organizationid=' . $organizationid);
}

$statusIds = array();

foreach ($this->ci->assets_model->get_asset_statuses() as $status) {
    if ($this->ci->input->post('asset_status_' . $status['id'])) {
        array_push($statusIds, $status['id']);
    }
}

if (count($statusIds) > 0) {
    array_push($filter, 'OR status IN (' . implode(', ', $statusIds) . ')');
}

if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}

$aColumns = do_action('assets_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit


$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
    'organizationid'
));

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = array();

    $row[] = '<a href="' . admin_url('assets/view/' . $aRow['id']) . '">' . $aRow['id'] . '</a>';

    $row[] = '<a href="' . admin_url('assets/view/' . $aRow['id']) . '">' . $aRow['tblassets.name'] . '</a>';

    $row[] = '<a href="' . admin_url('assets/view/' . $aRow['id']) . '">' . $aRow['tblassets.location'] . '</a>';

    $row[] = '<a href="' . admin_url('organizations/organization/' . $aRow['organizationid']) . '">' . $aRow['company'] . '</a>';

    $row[] = $aRow['class_id'];

    $row[] = $aRow['category_id'];
    
    $row[] = _d($aRow['start_date']);

    $row[] = $aRow['category_id'];

    $row[] = $aRow['asset_cost'];

    $status = get_asset_status_by_id($aRow['status']);
    $row[] = '<span class="label label inline-block asset-status-' . $aRow['status'] . '" style="color:'.$status['color'].';border:1px solid '.$status['color'].'">' . $status['name'] . '</span>';

    // Custom fields add values

    $hook = do_action('assets_table_row_data', array(
        'output' => $row,
        'row' => $aRow
    ));

    $row = $hook['output'];

    $options = '';

    if($hasPermissionCreate) {
        $options .= icon_btn('#', 'clone','btn-default',array('onclick'=>'copy_asset('.$aRow['id'].');return false','data-toggle'=>'tooltip','title'=>_l('copy_asset')));
    }

    if ($hasPermissionEdit) {
        $options .= icon_btn('assets/asset/' . $aRow['id'], 'pencil-square-o');
    }

    if ($hasPermissionDelete) {
        $options .= icon_btn('assets/delete/' . $aRow['id'], 'remove', 'btn-danger _delete');
    }

    $row[]              = $options;
    $output['aaData'][] = $row;
}
