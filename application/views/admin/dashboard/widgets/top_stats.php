<div class="widget relative" id="widget-<?php echo basename(__FILE__,".php"); ?>" data-name="<?php echo _l('quick_stats'); ?>">
      <div class="widget-dragger"></div>
      <div class="row">
      <?php
         $initial_column = 'col-lg-3';
         if(!is_staff_member() && (!has_permission('invoices','','view') && !has_permission('invoices','','view_own'))) {
            $initial_column = 'col-lg-6';
         } else if(!is_staff_member() || (!has_permission('invoices','','view') && !has_permission('invoices','','view_own'))) {
            $initial_column = 'col-lg-4';
         }
      ?>
         <?php if(has_permission('invoices','','view') || has_permission('invoices','','view_own')){ ?>
         <div class="col-xs-12 col-md-6 col-sm-6 <?php echo $initial_column; ?>">
            <div class="top_stats_wrapper">
               <?php
                  $total_invoices = total_rows('tblinvoices','status NOT IN (5,6)'.(!has_permission('invoices','','view') ? ' AND addedfrom='.get_staff_user_id() : ''));
                  $total_invoices_awaiting_payment = total_rows('tblinvoices','status NOT IN (2,5,6)'.(!has_permission('invoices','','view') ? ' AND addedfrom='.get_staff_user_id() : ''));
                  $percent_total_invoices_awaiting_payment = ($total_invoices > 0 ? number_format(($total_invoices_awaiting_payment * 100) / $total_invoices,2) : 0);
                  ?>
               <p class="text-uppercase mtop5"><i class="hidden-sm fa fa-balance-scale"></i> <?php echo _l('invoices_awaiting_payment'); ?>
                  <span class="pull-right"><?php echo $total_invoices_awaiting_payment; ?> / <?php echo $total_invoices; ?></span>
               </p>
               <div class="clearfix"></div>
               <div class="progress no-margin progress-bar-mini">
                  <div class="progress-bar progress-bar-danger no-percent-text not-dynamic" role="progressbar" aria-valuenow="<?php echo $percent_total_invoices_awaiting_payment; ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%" data-percent="<?php echo $percent_total_invoices_awaiting_payment; ?>">
                  </div>
               </div>
            </div>
         </div>
         <?php } ?>

         <div class="col-xs-12 col-md-6 col-sm-6 <?php echo $initial_column; ?>">
            <div class="top_stats_wrapper">
               <?php
                  $_where = '';
                  $asset_status = get_asset_status_by_id(2);
                  if(!has_permission('assets','','view')){
                    $_where = 'id IN (SELECT asset_id FROM tblassetmembers WHERE staff_id='.get_staff_user_id().')';
                  }
                  $total_assets = total_rows('tblassets',$_where);
                  $where = ($_where == '' ? '' : $_where.' AND ').'status = 2';
                  $total_assets_in_progress = total_rows('tblassets',$where);
                  $percent_in_progress_assets = ($total_assets > 0 ? number_format(($total_assets_in_progress * 100) / $total_assets,2) : 0);
                  ?>
               <p class="text-uppercase mtop5"><i class="hidden-sm fa fa-cubes"></i> <?php echo _l('assets') . ' ' . $asset_status['name']; ?><span class="pull-right"><?php echo $total_assets_in_progress; ?> / <?php echo $total_assets; ?></span></p>
               <div class="clearfix"></div>
               <div class="progress no-margin progress-bar-mini">
                  <div class="progress-bar no-percent-text not-dynamic" style="background:<?php echo $asset_status['color']; ?>" role="progressbar" aria-valuenow="<?php echo $percent_in_progress_assets; ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%" data-percent="<?php echo $percent_in_progress_assets; ?>">
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-md-6 col-sm-6 <?php echo $initial_column; ?>">
            <div class="top_stats_wrapper">
               <?php
                  $_where = '';
                  if (!has_permission('tasks', '', 'view')) {
                    $_where = 'tblstafftasks.id IN (SELECT taskid FROM tblstafftaskassignees WHERE staffid = ' . get_staff_user_id() . ')';
                  }
                  $total_tasks = total_rows('tblstafftasks',$_where);
                  $where = ($_where == '' ? '' : $_where.' AND ').'status != 5';
                  $total_not_finished_tasks = total_rows('tblstafftasks',$where);
                  $percent_not_finished_tasks = ($total_tasks > 0 ? number_format(($total_not_finished_tasks * 100) / $total_tasks,2) : 0);
                  ?>
               <p class="text-uppercase mtop5"><i class="hidden-sm fa fa-tasks"></i> <?php echo _l('tasks_not_finished'); ?> <span class="pull-right">
                  <?php echo $total_not_finished_tasks; ?> / <?php echo $total_tasks; ?>
                  </span>
               </p>
               <div class="clearfix"></div>
               <div class="progress no-margin progress-bar-mini">
                  <div class="progress-bar progress-bar-default no-percent-text not-dynamic" role="progressbar" aria-valuenow="<?php echo $percent_not_finished_tasks; ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%" data-percent="<?php echo $percent_not_finished_tasks; ?>">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
