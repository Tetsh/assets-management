<?php
if(!isset($filter_table_name)){
    $filter_table_name = '.table-assets';
}
?>
<div class="_filters _hidden_inputs hidden">
   <?php 

for ($m = 1; $m <= 12; $m++) {
   echo form_hidden('assets_by_month_'.$m);
}
foreach($classes as $class){
 echo form_hidden('assets_by_class_'.$class['id']);
}
?>
</div>
<div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-filter" aria-hidden="true"></i>
    </button>

    <ul class="dropdown-menu dropdown-menu-right width300">
        <li>
            <a href="#" data-cview="all" onclick="dt_custom_view('','<?php echo $filter_table_name; ?>',''); return false;">
                <?php echo _l('assets_list_all'); ?>
            </a>
        </li>
        <li class="divider"></li>
        <?php if(count($years) > 0){ ?>
            <li class="divider"></li>
            <?php foreach($years as $year){ ?>
                <li class="active">
                    <a href="#" data-cview="year_<?php echo $year['year']; ?>" onclick="dt_custom_view(<?php echo $year['year']; ?>,'<?php echo $filter_table_name; ?>','year_<?php echo $year['year']; ?>'); return false;"><?php echo $year['year']; ?>
                    </a>
                </li>
                <?php } ?>
                <?php } ?>
                <?php if(count($classes) > 0){ ?>
                   <div class="clearfix"></div>
                   <li class="divider"></li>
                   <li class="dropdown-submenu pull-left">
                     <a href="#" tabindex="-1"><?php echo _l('assets_filter_by_classes'); ?></a>
                     <ul class="dropdown-menu dropdown-menu-left">
                        <?php foreach($classes as $class){ ?>
                            <li>
                                <a href="#" data-cview="assets_by_class_<?php echo $class['id']; ?>" onclick="dt_custom_view(<?php echo $class['id']; ?>,'<?php echo $filter_table_name; ?>','assets_by_class_<?php echo $class['id']; ?>'); return false;"><?php echo $class['name']; ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                    <li class="dropdown-submenu pull-left">
                      <a href="#" tabindex="-1"><?php echo _l('months'); ?></a>
                      <ul class="dropdown-menu dropdown-menu-left">
                        <?php for ($m = 1; $m <= 12; $m++) { ?>
                          <li><a href="#" data-cview="assets_by_month_<?php echo $m; ?>" onclick="dt_custom_view(<?php echo $m; ?>,'<?php echo $filter_table_name; ?>','assets_by_month_<?php echo $m; ?>'); return false;"><?php echo _l(date('F', mktime(0, 0, 0, $m, 1))); ?></a></li>
                          <?php } ?>
                      </ul>
                  </li>
              </ul>
          </div>
