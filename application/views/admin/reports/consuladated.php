<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="<?php echo admin_url('reports/expenses/detailed_report'); ?>" class="btn btn-success"><?php echo _l('expenses_detailed_report'); ?></a>
                        </div>
                        <?php if($export_not_supported){ ?>
                        <p class="text-danger">Exporting not support in IE. To export this data please try another browser</p>
                        <?php } ?>
                        <a href="#" onclick="make_expense_pdf_export(); return false;" class="btn btn-default pull-left mright10<?php if($export_not_supported){echo ' disabled';} ?>"><i class="fa fa-file-pdf-o"></i></a>
                        <a download="expenses-report-<?php echo $current_year; ?>.xls" class="btn btn-default pull-left mright10<?php if($export_not_supported){echo ' disabled';} ?>" href="#" onclick="return ExcellentExport.excel(this, 'expenses-report-table', 'Expenses Report <?php echo $current_year; ?>');"><i class="fa fa-file-excel-o"></i></a>
                        <?php if(count($expense_years) > 0 ){ ?>
                        <select class="selectpicker" name="expense_year" onchange="filter_expenses();" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                            <?php foreach($expense_years as $year) { ?>
                            <option value="<?php echo $year['year']; ?>"<?php if($year['year'] == $current_year){echo 'selected';} ?>>
                                <?php echo $year['year']; ?>
                            </option>
                            <?php } ?>
                        </select>
                        <?php } ?>
                        <?php
                        $_currency = $base_currency;
                         ?>
                        </div>
                    </div>
                    <div class="panel_s">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover expenses-report" id="expenses-report-table">
                                    <thead>
                                        <tr>
                                            <th class="bold"><?php echo _l('expense_report_category'); ?></th>
                                           <?php
                                              foreach($expense_years as $year) {
                                                if ($year['year']) {
                                                    echo '  <th class="bold">' . $year['year']. '</th>';
                                                    
                                                }
                                            }?>
                                             
                                        </tr>
                                    </thead>
                                     <tbody>
                                           <?php foreach($categories as $category) { ?>                                        
                                               <tr>
                                                <?php if($category['name']) { ?>
                                                  <td class="bold"><?php echo $category['name']; ?></td>
                                                <?php }?>    
                                                <?php foreach($categories['costs'] as $cost) {?>  
                                                   <td class="bold"><?php echo $cost['cost']; ?></td>
                                                <?php }?>
                                               </tr>
                                            <?php }?>
                                     </tbody>
                                    </table>
                                    </div>
                                    <hr />
                                    <div class="row">
                                      <div class="col-md-6">
                                          <p class="text-muted mbot20"><?php echo _l('not_billable_expenses_by_categories'); ?></p>
                                      </div>
                                      <div class="col-md-6">
                                          <p class="text-muted mbot20"><?php echo _l('billable_expenses_by_categories'); ?></p>
                                      </div>
                                      <div class="col-md-6">
                                        <canvas id="expenses_chart_not_billable" height="390"></canvas>
                                    </div>
                                    <div class="col-md-6">
                                        <canvas id="expenses_chart_billable" height="390"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php init_tail(); ?>
        <script src="<?php echo base_url('assets/plugins/excellentexport/excellentexport.min.js'); ?>"></script>
        <script>
            new Chart($('#expenses_chart_not_billable'),{
                type:'bar',
                data: <?php echo $chart_not_billable; ?>,
                options:{
                    responsive:true,
                    maintainAspectRatio:false,
                    legend: {
                        display: false,
                    },
                    scales: {
                        yAxes: [{
                          ticks: {
                            beginAtZero: true,
                        }
                    }]
                }},
            });
            new Chart($('#expenses_chart_billable'),{
                type:'bar',
                data: <?php echo $chart_billable; ?>,
                options:{
                    responsive:true,
                    maintainAspectRatio:false,
                    legend: {
                        display: false,
                    },
                    scales: {
                        yAxes: [{
                          ticks: {
                            beginAtZero: true,
                        }
                    }]
                }},
            });
            function filter_expenses(){
                var parameters = new Array();
                var exclude_billable = ~~$('input[name="exclude_billable"]').prop('checked');
                var year = $('select[name="expense_year"]').val();
                var currency = ~~$('select[name="currencies"]').val();
                var location = window.location.href;
                location = location.split('?');
                if(exclude_billable){
                    parameters['exclude_billable'] = exclude_billable;
                }
                parameters['year'] = year;
                parameters['currency'] = currency;
                window.location.href = buildUrl(location[0], parameters);
            }
            function make_expense_pdf_export() {
                var body = [];
                var export_headings = [];
                var export_widths = [];
                var export_data = [];
                var headings = $('#expenses-report-table th');
                var data_tbody = $('#expenses-report-table tbody tr')
                var width = 47;
    // Prepare the pdf headings
    $.each(headings, function(i) {
        var heading = {};
        heading.text = stripTags($(this).text().trim());
        heading.fillColor = '#444A52';
        heading.color = '#fff';
        export_headings.push(heading);
        if(i == 0){
            export_widths.push(80);
        } else {
            export_widths.push(width);
        }
    });
    body.push(export_headings);
    // Categories total
    $.each(data_tbody, function() {
        var row = [];
        $.each($(this).find('td'), function() {
            var data = $(this);
            row.push($(data).text());
        });
       body.push(row);
    });


    // Pdf definition
    var docDefinition = {
        pageOrientation: 'landscape',
        pageMargins: [12, 12, 12, 12],
        "alignment":"center",
        content: [
        {
            text: '<?php echo _l("expenses_report_for"); ?> <?php echo $current_year; ?>:',
            bold: true,
            fontSize: 25,
            margin: [0, 5]
        },
        {
            text:'<?php echo get_option("companyname"); ?>',
            margin: [2,5]
        },
        {
            table: {
                headerRows: 1,
                widths: export_widths,
                body: body
            },
        }
        ],
        defaultStyle: {
            alignment: 'justify',
            fontSize: 10,
        }
    };
    // Open the pdf.
    pdfMake.createPdf(docDefinition).open();
}

</script>
</body>
</html>
