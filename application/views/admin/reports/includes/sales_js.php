<script>
 var salesChart;
 var groupsChart;
 var paymentMethodsChart;
 var organizationsTable;
 var report_from = $('input[name="report-from"]');
 var report_to = $('input[name="report-to"]');
 var report_organizations = $('#organizations-report');
 var report_classes = $('#classes-report');
 var report_assets_analysis = $('#assets-analysis-report');
 var report_assets_transfer = $('#assets-transfer-report');
 var report_assets_scrap = $('#assets-scrap-report');
 var report_assets_depreciation = $('#assets-depreciation-report');
 var report_organizations_groups = $('#organizations-group');
 var report_assets_register = $('#assets-register-report');
 var report_invoices = $('#invoices-report');
 var report_transfers = $('#transfers-report');
 var report_proposals = $('#proposals-reports');
 var report_items = $('#items-report');
 var report_payments_received = $('#payments-received-report');
 var date_range = $('#date-range');
 var report_from_choose = $('#report-time');
 var fnServerParams = {
   "report_months": '[name="months-report"]',
   "report_from": '[name="report-from"]',
   "report_to": '[name="report-to"]',
   "report_currency": '[name="currency"]',
   "invoice_status": '[name="invoice_status"]',
   "transfer_status": '[name="transfer_status"]',
   "asset_status": '[name="asset_status"]',
   "asset_category": '[name="asset_category"]',
   "asset_class": '[name="asset_class"]',
 }
 $(function() {
   $('select[name="currency"],select[name="invoice_status"],select[name="transfer_status"],select[name="asset_status"],select[name="asset_category"],select[name="asset_class"],select[name="sale_agent_items"],select[name="payments_years"]').on('change', function() {
     gen_reports();
   });

   $('select[name="invoice_status"],select[name="transfer_status"],select[name="asset_status"],select[name="asset_category"],select[name="asset_class"]').on('change', function() {
     var value = $(this).val();
     if (value != null) {
       if (value.indexOf('') > -1) {
         if (value.length > 1) {
           value.splice(0, 1);
           $(this).selectpicker('val', value);
         }
       }
     }
   });
   report_from.on('change', function() {
     var val = $(this).val();
     var report_to_val = report_to.val();
     if (val != '') {
       report_to.attr('disabled', false);
       if (report_to_val != '') {
         gen_reports();
       }
     } else {
       report_to.attr('disabled', true);
     }
   });

   report_to.on('change', function() {
     var val = $(this).val();
     if (val != '') {
       gen_reports();
     }
   });

   $('select[name="months-report"]').on('change', function() {
     var val = $(this).val();
     report_to.attr('disabled', true);
     report_to.val('');
     report_from.val('');
     if (val == 'custom') {
       date_range.addClass('fadeIn').removeClass('hide');
       return;
     } else {
       if (!date_range.hasClass('hide')) {
         date_range.removeClass('fadeIn').addClass('hide');
       }
     }
     gen_reports();
   });

   $('.table-payments-received-report').on('draw.dt', function() {
     var paymentReceivedReportsTable = $(this).DataTable();
     var sums = paymentReceivedReportsTable.ajax.json().sums;
     $(this).find('tfoot').addClass('bold');
     $(this).find('tfoot td').eq(0).html("<?php echo _l('invoice_total'); ?>");
     $(this).find('tfoot td.total').html(sums.total_amount);
   });


   $('.table-invoices-report').on('draw.dt', function() {
     var invoiceReportsTable = $(this).DataTable();
     var sums = invoiceReportsTable.ajax.json().sums;
     add_common_footer_sums($(this),sums);
     $(this).find('tfoot td.amount_open').html(sums.amount_open);
     $(this).find('tfoot td.applied_credits').html(sums.applied_credits);
     <?php foreach($invoice_taxes as $key => $tax){ ?>
        $(this).find('tfoot td.total_tax_single_<?php echo $key; ?>').html(sums['total_tax_single_<?php echo $key; ?>']);
     <?php } ?>
   });


   $('.table-transfers-report').on('draw.dt', function() {
     var transfersReportsTable = $(this).DataTable();
     var sums = transfersReportsTable.ajax.json().sums;
     add_common_footer_sums($(this),sums);
     <?php foreach($transfer_taxes as $key => $tax){ ?>
        $(this).find('tfoot td.total_tax_single_<?php echo $key; ?>').html(sums['total_tax_single_<?php echo $key; ?>']);
     <?php } ?>
   });

   $('.table-items-report').on('draw.dt', function() {
     var itemsTable = $(this).DataTable();
     var sums = itemsTable.ajax.json().sums;
     $(this).find('tfoot').addClass('bold');
     $(this).find('tfoot td').eq(0).html("<?php echo _l('invoice_total'); ?>");
     $(this).find('tfoot td.amount').html(sums.total_amount);
     $(this).find('tfoot td.qty').html(sums.total_qty);
   });

 });

  function add_common_footer_sums(table,sums) {
       table.find('tfoot').addClass('bold');
       table.find('tfoot td').eq(0).html("<?php echo _l('invoice_total'); ?>");
       table.find('tfoot td.subtotal').html(sums.subtotal);
       table.find('tfoot td.total').html(sums.total);
       table.find('tfoot td.total_tax').html(sums.total_tax);
       table.find('tfoot td.discount_total').html(sums.discount_total);
       table.find('tfoot td.adjustment').html(sums.adjustment);
  }

 function init_report(e, type) {
   var report_wrapper = $('#report');

   if (report_wrapper.hasClass('hide')) {
        report_wrapper.removeClass('hide');
   }

   $('head title').html($(e).text());
   $('.organizations-group-gen').addClass('hide');

   report_organizations_groups.addClass('hide');
   report_organizations.addClass('hide');
   report_classes.addClass('hide');
   report_invoices.addClass('hide');
   report_transfers.addClass('hide');
   report_assets_register.addClass('hide');
   report_payments_received.addClass('hide');
   report_items.addClass('hide');
   report_proposals.addClass('hide');

   $('#income-years').addClass('hide');
   $('.chart-income').addClass('hide');
   $('.chart-payment-modes').addClass('hide');


   report_from_choose.addClass('hide');

   $('select[name="months-report"]').selectpicker('val', 'this_month');
   // Clear custom date picker
       report_to.val('');
       report_from.val('');
       $('#currency').removeClass('hide');

       if (type != 'total-income' && type != 'payment-modes') {
         report_from_choose.removeClass('hide');
       }

       if (type == 'total-income') {
         $('.chart-income').removeClass('hide');
         $('#income-years').removeClass('hide');
         date_range.addClass('hide');
       } else if (type == 'organizations-report') {
         report_organizations.removeClass('hide');
       } else if (type == 'classes-report') {
         report_classes.removeClass('hide');
       }else if (type == 'assets-analysis-report') {
         report_assets_analysis.removeClass('hide');
       }else if (type == 'assets-transfer-report') {
         report_assets_transfer.removeClass('hide');
       }else if (type == 'assets-scrap-report') {
         report_assets_scrap.removeClass('hide');
       } else if (type == 'assets-depreciation-report') {
         report_assets_depreciation.removeClass('hide');
       } else if (type == 'organizations-group') {
         $('.organizations-group-gen').removeClass('hide');
       } else if (type == 'invoices-report') {
         report_invoices.removeClass('hide');
       } else if (type == 'credit-notes') {
         $('.chart-payment-modes').removeClass('hide');
         $('#income-years').removeClass('hide');
       } else if (type == 'payments-received') {
         report_payments_received.removeClass('hide');
       } else if (type == 'transfers-report') {
         report_transfers.removeClass('hide');
       } else if (type == 'assets-register-report') {
         report_assets_register.removeClass('hide');
       } else if(type == 'proposals-report'){
        report_proposals.removeClass('hide');
      } else if(type == 'items-report'){
          report_items.removeClass('hide');
      }
      gen_reports();
    }


   // Generate total income bar
   function total_income_bar_report() {
     if (typeof(salesChart) !== 'undefined') {
       salesChart.destroy();
     }
     var data = {};
     data.year = $('select[name="payments_years"]').val();
     var currency = $('#currency');
     if (currency.length > 0) {
       data.report_currency = $('select[name="currency"]').val();
     }
     $.post(admin_url + 'reports/total_income_report', data).done(function(response) {
       response = JSON.parse(response);
       salesChart = new Chart($('#chart-income'), {
         type: 'bar',
         data: response,
         options: {
           responsive: true,
           maintainAspectRatio:false,
           legend: {
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
              }
            }]
          },
        }
      })
     });
   }

   function report_by_payment_modes() {
     if (typeof(paymentMethodsChart) !== 'undefined') {
       paymentMethodsChart.destroy();
     }
     var data = {};
     data.year = $('select[name="payments_years"]').val();
     var currency = $('#currency');
     if (currency.length > 0) {
       data.report_currency = $('select[name="currency"]').val();
     }
     $.post(admin_url + 'reports/report_by_payment_modes', data).done(function(response) {
       response = JSON.parse(response);
       paymentMethodsChart = new Chart($('#chart-payment-modes'), {
         type: 'bar',
         data: response,
         options: {
           responsive: true,
           maintainAspectRatio:false,
           scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
              }
            }]
          },
        }
      })
     });
   }
   // Generate organizations report
   function organizations_report() {
     if ($.fn.DataTable.isDataTable('.table-organizations-report')) {
       $('.table-organizations-report').DataTable().destroy();
     }
     initDataTable('.table-organizations-report', admin_url + 'reports/organizations_report', false, false, fnServerParams, [0, 'ASC']);
   }

   // Generate organizations report
   function classes_report() {
     if ($.fn.DataTable.isDataTable('.table-classes-report')) {
       $('.table-classes-report').DataTable().destroy();
     }
     initDataTable('.table-classes-report', admin_url + 'reports/classes_report', false, false, fnServerParams, [0, 'ASC']);
   }

    function assets_analysis_report() {
     if ($.fn.DataTable.isDataTable('.table-assets-analysis-report')) {
       $('.table-assets-analysis-report').DataTable().destroy();
     }
     initDataTable('.table-assets-analysis-report', admin_url + 'reports/assets_analysis', false, false, fnServerParams, [0, 'ASC']);
   }

     function assets_transfer_report() {
     if ($.fn.DataTable.isDataTable('.table-assets-transfer-report')) {
       $('.table-assets-transfer-report').DataTable().destroy();
     }
     initDataTable('.table-assets-transfer-report', admin_url + 'reports/assets_transfer', false, false, fnServerParams, [0, 'ASC']);
   }

     function assets_scrap_report() {
     if ($.fn.DataTable.isDataTable('.table-assets-scrap-report')) {
       $('.table-assets-scrap-report').DataTable().destroy();
     }
     initDataTable('.table-assets-scrap-report', admin_url + 'reports/assets_scrap', false, false, fnServerParams, [0, 'ASC']);
   }

    function assets_depreciation_report() {
     if ($.fn.DataTable.isDataTable('.table-assets-depreciation-report')) {
       $('.table-assets-depreciation-report').DataTable().destroy();
     }
     initDataTable('.table-assets-depreciation-report', admin_url + 'reports/assets_depreciation_analysis', false, false, fnServerParams, [0, 'ASC']);
   }

   function report_by_organization_groups() {
     if (typeof(groupsChart) !== 'undefined') {
       groupsChart.destroy();
     } 
     var data = {};
     data.months_report = $('select[name="months-report"]').val();
     data.report_from = report_from.val();
     data.report_to = report_to.val();

     var currency = $('#currency');
     if (currency.length > 0) {
       data.report_currency = $('select[name="currency"]').val();
     }
     $.post(admin_url + 'reports/report_by_organization_groups', data).done(function(response) {
       response = JSON.parse(response);
       groupsChart = new Chart($('#organizations-group-gen'), {
         type: 'line',
         data: response,
         options:{
          maintainAspectRatio:false,
          legend: {
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
              }
            }]
          }}
        });
     });
   }
   function invoices_report() {
     if ($.fn.DataTable.isDataTable('.table-invoices-report')) {
       $('.table-invoices-report').DataTable().destroy();
     }
     _table_api = initDataTable('.table-invoices-report', admin_url + 'reports/invoices_report', false, false, fnServerParams, [
       [2, 'DESC'],
       [0, 'DESC']
       ]).column(2).visible(false, false).columns.adjust();
   }

   function transfers_report() {
     if ($.fn.DataTable.isDataTable('.table-transfers-report')) {
       $('.table-transfers-report').DataTable().destroy();
     }
     _table_api = initDataTable('.table-transfers-report', admin_url + 'reports/transfers_report', false, false, fnServerParams, [
       [3, 'DESC'],
       [0, 'DESC']
       ]).column(3).visible(false, false).columns.adjust();
   }

   function assets_register_report() {
     if ($.fn.DataTable.isDataTable('.table-assets-register-report')) {
       $('.table-assets-register-report').DataTable().destroy();
     }
     _table_api = initDataTable('.table-assets-register-report', admin_url + 'reports/assets_register_report', false, false, fnServerParams, [
       [2, 'DESC'],
       [0, 'DESC']
       ]).column(3).visible(false, false).columns.adjust();   }

   function payments_received_reports() {
     if ($.fn.DataTable.isDataTable('.table-payments-received-report')) {
       $('.table-payments-received-report').DataTable().destroy();
     }
     initDataTable('.table-payments-received-report', admin_url + 'reports/payments_received', false, false, fnServerParams, [1, 'DESC']);
   }

 function items_report(){
   if ($.fn.DataTable.isDataTable('.table-items-report')) {
     $('.table-items-report').DataTable().destroy();
   }
   initDataTable('.table-items-report', admin_url + 'reports/items', false, false, fnServerParams, [0, 'ASC']);
 }

   // Main generate report function
   function gen_reports() {

     if (!$('.chart-income').hasClass('hide')) {
       total_income_bar_report();
     } else if (!$('.chart-payment-modes').hasClass('hide')) {
       report_by_payment_modes();
     } else if (!report_organizations.hasClass('hide')) {
       organizations_report();
     } else if (!report_classes.hasClass('hide')) {
       classes_report();       
     }else if (!report_assets_analysis.hasClass('hide')) {
       assets_analysis_report();       
     }else if (!report_assets_transfer.hasClass('hide')) {
       assets_transfer_report();       
     }else if (!report_assets_scrap.hasClass('hide')) {
       assets_scrap_report();       
     }else if (!report_assets_depreciation.hasClass('hide')) {
       assets_depreciation_report();       
     } else if (!$('.organizations-group-gen').hasClass('hide')) {
       report_by_organization_groups();
     } else if (!report_invoices.hasClass('hide')) {
       invoices_report();
     } else if (!report_payments_received.hasClass('hide')) {
       payments_received_reports();
     } else if (!report_assets_register.hasClass('hide')) {
       assets_register_report();
     } else if (!report_transfers.hasClass('hide')) {
       transfers_report();       
    } else if(!report_items.hasClass('hide')) {
      items_report();
    } 
  }
</script>
