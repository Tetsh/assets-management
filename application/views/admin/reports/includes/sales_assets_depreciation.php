  <div id="assets-depreciation-report" class="hide">
      <?php render_datatable(array(
       _l('reports_sales_dt_organizations_classes'),
       _l('reports_sales_dt_assets_depreciation'),
       _l('reports_sales_dt_asset_current_period_depreciation'),
       _l('reports_sales_dt_assets_actual_total_depreciation'),
       ),'assets-depreciation-report scroll-responsive'); ?>
   </div>
