  <div id="items-report" class="hide">
    <?php if($mysqlVersion && strpos($mysqlVersion->version,'5.6') !== FALSE && $sqlMode && strpos($sqlMode->mode,'ONLY_FULL_GROUP_BY') !== FALSE){ ?>
    <div class="alert alert-danger">
      Sales Report may not work properly because ONLY_FULL_GROUP_BY is enabled, consult with your hosting provider to disable ONLY_FULL_GROUP_BY in sql_mode configuration. In case the items report is working properly you can just ignore this message.
    </div>
    <?php } ?>
    <p class="mbot20 text-info"><?php echo _l('item_report_paid_invoices_notice'); ?></p>

  <table class="table table-items-report scroll-responsive">
    <thead>
      <tr>
        <th><?php echo _l('reports_item'); ?></th>
        <th><?php echo _l('quantity_sold'); ?></th>
        <th><?php echo _l('total_amount'); ?></th>
        <th><?php echo _l('avg_price'); ?></th>
      </tr>
    </thead>
    <tbody>

    </tbody>
    <tfoot>
      <tr>
        <td></td>
        <td class="qty"></td>
        <td class="amount"></td>
        <td></td>
      </tr>
    </tfoot>
  </table>
</div>
