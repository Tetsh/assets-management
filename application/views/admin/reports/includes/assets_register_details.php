  <div id="assets-register-details-report" class="hide">
      <div class="row">
      <div class="col-md-3">
         <div class="form-group">
            <label for="asset_status"><?php echo _l('asset_status'); ?></label>
            <select name="asset_status" class="selectpicker" multiple data-width="100%">
               <option value="" selected><?php echo _l('invoice_status_report_all'); ?></option>
               <?php foreach($asset_statuses as $status){ ?>
               <option value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="asset_category"><?php echo _l('asset_category'); ?></label>
            <select name="asset_category" class="selectpicker" multiple data-width="100%">
               <option value="" selected><?php echo _l('invoice_status_report_all'); ?></option>
               <?php foreach($asset_categories as $category){ ?>
               <option value="<?php echo $category['code']; ?>"><?php echo $category['name']; ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="asset_category"><?php echo _l('asset_category'); ?></label>
            <select name="asset_category" class="selectpicker" multiple data-width="100%">
               <option value="" selected><?php echo _l('invoice_status_report_all'); ?></option>
               <?php foreach($asset_classes as $class){ ?>
               <option value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
      <table class="table table-assets-register-report scroll-responsive">
         <thead>
          <tr>
            <th><?php echo _l('#'); ?></th>
            <th><?php echo _l('asset_name'); ?></th>
            <th><?php echo _l('asset_location'); ?></th>
            <th><?php echo _l('asset_organization'); ?></th>
            <th><?php echo _l('Class'); ?></th>
            <th><?php echo _l('Category'); ?></th>
            <th><?php echo _l('asset_start_date'); ?></th>
            <th><?php echo _l('Departmenet'); ?></th>
            <th><?php echo _l('asset_cost'); ?></th>
            <th><?php echo _l('asset_status'); ?></th>
         </tr>
      </thead>
      <tbody></tbody>
   </table>
</div>
