  <div id="transfers-report" class="hide">
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="transfer_status"><?php echo _l('transfer_status'); ?></label>
            <select name="transfer_status" class="selectpicker" multiple data-width="100%">
               <option value="" selected><?php echo _l('invoice_status_report_all'); ?></option>
               <?php foreach($transfer_statuses as $status){ ?>
               <option value="<?php echo $status; ?>"><?php echo format_transfer_status($status,'',false) ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
      <table class="table table-transfers-report scroll-responsive">
         <thead>
          <tr>
            <th><?php echo _l('transfer_dt_table_heading_number'); ?></th>
            <th><?php echo _l('transfer_dt_table_heading_organization'); ?></th>
            <th><?php echo _l('report_invoice_number'); ?></th>
            <th><?php echo _l('invoice_transfer_year'); ?></th>
            <th><?php echo _l('transfer_dt_table_heading_date'); ?></th>
            <th><?php echo _l('transfer_dt_table_heading_expirydate'); ?></th>
            <th><?php echo _l('transfer_dt_table_heading_amount'); ?></th>
            <th><?php echo _l('report_invoice_amount_with_tax'); ?></th>
            <th><?php echo _l('report_invoice_total_tax'); ?></th>
            <?php foreach($transfer_taxes as $tax){ ?>
            <th><?php echo $tax['taxname']; ?> <small><?php echo $tax['taxrate']; ?>%</small></th>
            <?php } ?>
            <th><?php echo _l('transfer_discount'); ?></th>
            <th><?php echo _l('transfer_adjustment'); ?></th>
            <th><?php echo _l('reference_no'); ?></th>
            <th><?php echo _l('transfer_dt_table_heading_status'); ?></th>
         </tr>
      </thead>
      <tbody></tbody>
      <tfoot>
         <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="subtotal"></td>
            <td class="total"></td>
            <td class="total_tax"></td>
            <?php foreach($transfer_taxes as $key => $tax){ ?>
            <td class="total_tax_single_<?php echo $key; ?>"></td>
            <?php } ?>
            <td class="discount_total"></td>
            <td class="adjustment"></td>
            <td></td>
            <td></td>
         </tr>
      </tfoot>
   </table>
</div>
