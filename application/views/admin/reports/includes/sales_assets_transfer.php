  <div id="assets-transfer-report" class="hide">
      <?php render_datatable(array(
       _l('report_assets_asset_code'),
       _l('reports_sales_dt_asset_depreciation'),
       _l('reports_sales_dt_asset_calss'),
       _l('reports_sales_dt_assets_reason'),
       _l('report_assets_dt_transfer_date'),
       _l('reports_sales_dt_asset_cost'),
       _l('reports_sales_dt_accu_depreciation'),
       _l('reports_sales_dt_net_book'),
       ),'assets-transfer-report scroll-responsive'); ?>
   </div>
