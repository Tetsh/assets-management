<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="<?php echo admin_url('reports/assets/detailed_report'); ?>" class="btn btn-success"><?php echo _l('assets_detailed_report'); ?></a>
                        </div>
                        <?php if($export_not_supported){ ?>
                        <p class="text-danger">Exporting not support in IE. To export this data please try another browser</p>
                        <?php } ?>
                        <a href="#" onclick="make_asset_pdf_export(); return false;" class="btn btn-default pull-left mright10<?php if($export_not_supported){echo ' disabled';} ?>"><i class="fa fa-file-pdf-o"></i></a>
                        <a download="assets-report-<?php echo $current_year; ?>.xls" class="btn btn-default pull-left mright10<?php if($export_not_supported){echo ' disabled';} ?>" href="#" onclick="return ExcellentExport.excel(this, 'assets-report-table', 'Expenses Report <?php echo $current_year; ?>');"><i class="fa fa-file-excel-o"></i></a>
                        <?php if(count($asset_years) > 0 ){ ?>
                        <select class="selectpicker" name="asset_year" onchange="filter_assets();" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                            <?php foreach($asset_years as $year) { ?>
                            <option value="<?php echo $year['year']; ?>"<?php if($year['year'] == $current_year){echo 'selected';} ?>>
                                <?php echo $year['year']; ?>
                            </option>
                            <?php } ?>
                        </select>
                        <?php } ?>
                        <?php
                        $_currency = $base_currency;
                        if(is_using_multiple_currencies('tblexpenses')){ ?>

                            <?php } ?>

                        </div>
                    </div>
                    <div class="panel_s">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover expenses-report" id="expenses-report-table">
                                    <thead>
                                        <tr>
                                            <th class="bold"><?php echo _l('reports_sales_dt_organizations_classes'); ?></th>
                                            <?php
                                            for ($m=1; $m<=12; $m++) {
                                                echo '  <th class="bold">' . _l(date('F', mktime(0,0,0,$m,1))) . '</th>';
                                            }
                                            ?>
                                            <th class="bold"> <?php echo _l('year'); ?> (<?php echo $current_year; ?>)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $netAmount = array();
                                        $totalNetByAssetCategory = array();
                                        foreach($classes as $class) { ?>
                                        <tr>
                                            <td class="bold"><?php echo $class['name']; ?></td>
                                            <?php
                                            for ($m=1; $m<=12; $m++) {
                                            // Set the monthly total assets array
                                                if(!isset($netMonthlyTotal[$m])){
                                                    $netMonthlyTotal[$m] = array();
                                                }

                                                // Get the assets
                                                $this->db->select('id')
                                                ->from('tblassets')
                                                ->where('MONTH(start_date)',$m)
                                                ->where('YEAR(start_date)',$current_year)
                                                ->where('class_id',$class['id']);

                                                $assets = $this->db->get()->result_array();

                                                $total_assets = array();
                                                echo '<td>';
                                                foreach($assets as $asset){
                                                    $asset = $this->assets_model->get($asset['id']);
                                                    $total = $asset->asset_cost;

                                                    $totalTaxByAsset = 0;
                                                    // Check if tax is applied
                                                    $totalTaxByAsset += 0;

                                                    $totalTaxByAsset += 0;

                                                    $taxTotal[$m][] = $totalTaxByAsset;
                                                    $total_assets[] = $total;
                                                }
                                                $total_assets = array_sum($total_assets);
                                                // Add to total monthy assets
                                                array_push($netMonthlyTotal[$m],$total_assets);
                                                if(!isset($totalNetByAssetCategory[$class['id']])){
                                                    $totalNetByAssetCategory[$class['id']] = array();
                                                }
                                                array_push($totalNetByAssetCategory[$class['id']],$total_assets);
                                                // Output the total for this class
                                                if(count($classes) <= 8){
                                                    echo format_money($total_assets);
                                                } else {
                                                   // show tooltip for the month if more the 8 classes found. becuase when listing down you wont be able to see the month
                                                    echo '<span data-toggle="tooltip" title="'._l(date('F', mktime(0,0,0,$m,1))).'">'.format_money($total_assets) .'</span>';
                                                }
                                                echo '</td>';
                                                ?>
                                                <?php } ?>
                                                <td class="bg-odd">
                                                    <?php echo format_money(array_sum($totalNetByAssetCategory[$class['id']])); ?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <?php $current_year_total = array(); ?>
                                            <tr class="bg-odd">
                                                <td class="bold text-info">
                                                    <?php echo _l('assets_report_net'); ?>
                                                </td>
                                                <?php if(isset($netMonthlyTotal)) { ?>
                                                <?php foreach($netMonthlyTotal as $month => $total){
                                                    $total = array_sum($total);
                                                    $netMonthlyTotal[$month] = $total;
                                                    $current_year_total[] = $total;
                                                    ?>
                                                    <td class="bold">
                                                        <?php echo format_money($total); ?>
                                                    </td>
                                                    <?php } ?>
                                                    <?php } ?>
                                                    <td class="bold bg-odd">
                                                        <?php
                                                        $totalNetByAssetCategorySum = 0;
                                                        foreach($totalNetByAssetCategory as $totalCat) {
                                                            $totalNetByAssetCategorySum += array_sum($totalCat);
                                                        }
                                                        echo format_money($totalNetByAssetCategorySum);
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr />
                                    <div class="row">
                                      <div class="col-md-6">
                                          <p class="text-muted mbot20"><?php echo _l('not_billable_assets_by_classes'); ?></p>
                                      </div>
                                      <div class="col-md-6">
                                          <p class="text-muted mbot20"><?php echo _l('billable_assets_by_classes'); ?></p>
                                      </div>
                                      <div class="col-md-6">
                                        <canvas id="assets_chart_not_billable" height="390"></canvas>
                                    </div>
                                    <div class="col-md-6">
                                        <canvas id="assets_chart_billable" height="390"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php init_tail(); ?>
        <script src="<?php echo base_url('assets/plugins/excellentexport/excellentexport.min.js'); ?>"></script>
        <script>
            new Chart($('#assets_chart_not_billable'),{
                type:'bar',
                data: <?php echo $chart_not_billable; ?>,
                options:{
                    responsive:true,
                    maintainAspectRatio:false,
                    legend: {
                        display: false,
                    },
                    scales: {
                        yAxes: [{
                          ticks: {
                            beginAtZero: true,
                        }
                    }]
                }},
            });
            new Chart($('#assets_chart_billable'),{
                type:'bar',
                data: <?php echo $chart_billable; ?>,
                options:{
                    responsive:true,
                    maintainAspectRatio:false,
                    legend: {
                        display: false,
                    },
                    scales: {
                        yAxes: [{
                          ticks: {
                            beginAtZero: true,
                        }
                    }]
                }},
            });
            function filter_assets(){
                var parameters = new Array();
                var exclude_billable = ~~$('input[name="exclude_billable"]').prop('checked');
                var year = $('select[name="asset_year"]').val();
                var currency = ~~$('select[name="currencies"]').val();
                var location = window.location.href;
                location = location.split('?');
                if(exclude_billable){
                    parameters['exclude_billable'] = exclude_billable;
                }
                parameters['year'] = year;
                parameters['currency'] = currency;
                window.location.href = buildUrl(location[0], parameters);
            }
            function make_asset_pdf_export() {
                var body = [];
                var export_headings = [];
                var export_widths = [];
                var export_data = [];
                var headings = $('#assets-report-table th');
                var data_tbody = $('#assets-report-table tbody tr')
                var width = 47;
    // Prepare the pdf headings
    $.each(headings, function(i) {
        var heading = {};
        heading.text = stripTags($(this).text().trim());
        heading.fillColor = '#444A52';
        heading.color = '#fff';
        export_headings.push(heading);
        if(i == 0){
            export_widths.push(80);
        } else {
            export_widths.push(width);
        }
    });
    body.push(export_headings);
    // Categories total
    $.each(data_tbody, function() {
        var row = [];
        $.each($(this).find('td'), function() {
            var data = $(this);
            row.push($(data).text());
        });
       body.push(row);
    });


    // Pdf definition
    var docDefinition = {
        pageOrientation: 'landscape',
        pageMargins: [12, 12, 12, 12],
        "alignment":"center",
        content: [
        {
            text: '<?php echo _l("assets_report_for"); ?> <?php echo $current_year; ?>:',
            bold: true,
            fontSize: 25,
            margin: [0, 5]
        },
        {
            text:'<?php echo get_option("companyname"); ?>',
            margin: [2,5]
        },
        {
            table: {
                headerRows: 1,
                widths: export_widths,
                body: body
            },
        }
        ],
        defaultStyle: {
            alignment: 'justify',
            fontSize: 10,
        }
    };
    // Open the pdf.
    pdfMake.createPdf(docDefinition).open();
}

</script>
</body>
</html>
