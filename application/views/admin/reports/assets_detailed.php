<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <a href="<?php echo admin_url('reports/assets'); ?>" class="btn btn-default pull-left"><?php echo _l('go_back'); ?></a>
                        <?php $this->load->view('admin/reports/filter_by_template'); ?>
                    </div>
                </div>
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                         <table class="table dt-table-loading table-assets">
                         <thead>
                             <tr>
                                 <th><?php echo _l('expense_dt_table_heading_category'); ?></th>
                                 <th><?php echo _l('expense_dt_table_heading_amount'); ?></th>
                                 <th><?php echo _l('expense_name'); ?></th>
                                 <th><?php echo _l('expense_dt_table_heading_date'); ?></th>
                                 <th><?php echo _l('expense_dt_table_heading_organization'); ?></th>
                             </tr>
                         </thead>
                         <tbody></tbody>
                         <tfoot>
                             <tr>
                                 <td></td>
                                 <td class="subtotal"></td>
                                 <td></td>
                                 <td></td>
                                 <td></td>
                             </tr>
                         </tfoot>
                         </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<script>
    $(function(){
     var Expenses_ServerParams = {};
     $.each($('._hidden_inputs._filters input'),function(){
        Expenses_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
    });
     initDataTable('.table-assets', window.location.href, 'undefined', 'undefined', Expenses_ServerParams, [2, 'DESC']);

     $('.table-assets').on('draw.dt',function(){
        var assetReportsTable = $(this).DataTable();
        var sums = assetReportsTable.ajax.json().sums;
        $(this).find('tfoot').addClass('bold');
        $(this).find('tfoot td').eq(0).html("<?php echo _l('assets_report_total'); ?>");
        $(this).find('tfoot td.subtotal').html(sums.asset_cost);
    });

     $('select[name="currencies"]').on('change',function(){
        $('.table-assets').DataTable().ajax.reload();
    });
 })

</script>
</body>
</html>
