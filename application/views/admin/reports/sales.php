<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                  <div class="row">
                     <div class="col-md-4 border-right">
                      <h4 class="no-margin font-medium"><i class="fa fa-balance-scale" aria-hidden="true"></i> <?php echo _l('sales_report_heading'); ?></h4>
                      <hr />
                      <p>
                        <a href="#" class="font-medium" onclick="init_report(this,'invoices-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('invoice_report'); ?></a>
                      </p>
                        <hr class="hr-10" />
                      <p>
                        <a href="#" class="font-medium" onclick="init_report(this,'items-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('items_report'); ?></a>
                      </p>
                      <hr class="hr-10" />
                      <p>
                        <a href="#" class="font-medium" onclick="init_report(this,'payments-received'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('payments_received'); ?></a>
                      </p>
                      <hr class="hr-10" />
                      <p>
                        <a href="#" class="font-medium" onclick="init_report(this,'transfers-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('transfers_report'); ?></a>
                      </p>
                      <hr class="hr-10" />
                      <p>
                        <a href="#" class="font-medium" onclick="init_report(this,'organizations-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('report_sales_type_organization'); ?></a>
                      </p>
                      <hr class="hr-10" />
                     <p><a href="#" class="font-medium" onclick="init_report(this,'assets-scrap-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('reports_sales_dt_assets_actual_scrap'); ?></a></p>

                      <?php if(total_rows('tblinvoices',array('status'=>5)) > 0){ ?>
                      <hr class="hr-10" />
                      <p class="text-danger">
                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php echo _l('sales_report_cancelled_invoices_not_included'); ?>
                     </p>
                     <?php } ?>
                  </div>
                  <div class="col-md-4 border-right">
                    <h4 class="no-margin font-medium"><i class="fa fa-area-chart" aria-hidden="true"></i> <?php echo _l('charts_based_report'); ?></h4>
                    <hr />
                    <p><a href="#" class="font-medium" onclick="init_report(this,'organizations-group'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('report_by_organization_groups'); ?></a></p>
                    <hr class="hr-10" />
                    <p><a href="#" class="font-medium" onclick="init_report(this,'assets-register-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('report_by_assets_register'); ?></a></p>
                    <hr class="hr-10" />
                    <p><a href="#" class="font-medium" onclick="init_report(this,'classes-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('report_by_assets_class'); ?></a></p>
                    <hr class="hr-10" />
                     <p><a href="#" class="font-medium" onclick="init_report(this,'assets-analysis-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('report_assets_analysis'); ?></a></p>
                    <hr class="hr-10" />
                     <p><a href="#" class="font-medium" onclick="init_report(this,'assets-transfer-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('reports_sales_dt_assets_actual_transferred'); ?></a></p>
                       <hr class="hr-10" />
                     <p><a href="#" class="font-medium" onclick="init_report(this,'assets-depreciation-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('report_assets_depreciation'); ?></a></p>

                 </div>
                 <div class="col-md-4">
                     <div id="income-years" class="hide mbot15">
                        <label for="payments_years"><?php echo _l('year'); ?></label><br />
                        <select class="selectpicker" name="payments_years" data-width="100%" onchange="total_income_bar_report();" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <?php foreach($payments_years as $year) { ?>
                           <option value="<?php echo $year['year']; ?>"<?php if($year['year'] == date('Y')){echo 'selected';} ?>>
                              <?php echo $year['year']; ?>
                           </option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="form-group hide" id="report-time">
                        <label for="months-report"><?php echo _l('period_datepicker'); ?></label><br />
                        <select class="selectpicker" name="months-report" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <option value=""><?php echo _l('report_sales_months_all_time'); ?></option>
                           <option value="this_month"><?php echo _l('this_month'); ?></option>
                           <option value="1"><?php echo _l('last_month'); ?></option>
                           <option value="this_year"><?php echo _l('this_year'); ?></option>
                           <option value="last_year"><?php echo _l('last_year'); ?></option>
                           <option value="3" data-subtext="<?php echo _d(date('Y-m-01', strtotime("-2 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_three_months'); ?></option>
                           <option value="6" data-subtext="<?php echo _d(date('Y-m-01', strtotime("-5 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_six_months'); ?></option>
                           <option value="12" data-subtext="<?php echo _d(date('Y-m-01', strtotime("-11 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_twelve_months'); ?></option>
                           <option value="custom"><?php echo _l('period_datepicker'); ?></option>
                        </select>
                     </div>
                     <div id="date-range" class="hide mbot15">
                        <div class="row">
                           <div class="col-md-6">
                              <label for="report-from" class="control-label"><?php echo _l('report_sales_from_date'); ?></label>
                              <div class="input-group date">
                                 <input type="text" class="form-control datepicker" id="report-from" name="report-from">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar calendar-icon"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <label for="report-to" class="control-label"><?php echo _l('report_sales_to_date'); ?></label>
                              <div class="input-group date">
                                 <input type="text" class="form-control datepicker" disabled="disabled" id="report-to" name="report-to">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar calendar-icon"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="report" class="hide">
               <hr class="hr-panel-heading" />
               <h4 class="no-mtop"><?php echo _l('reports_sales_generated_report'); ?></h4>
               <hr class="hr-panel-heading" />
               <?php $this->load->view('admin/reports/includes/sales_income'); ?>
               <?php $this->load->view('admin/reports/includes/sales_payment_modes'); ?>
               <?php $this->load->view('admin/reports/includes/sales_organizations_groups'); ?>
               <?php $this->load->view('admin/reports/includes/sales_organizations'); ?>
               <?php $this->load->view('admin/reports/includes/sales_invoices'); ?>
               <?php $this->load->view('admin/reports/includes/sales_items'); ?>
               <?php $this->load->view('admin/reports/includes/sales_transfers'); ?>
               <?php $this->load->view('admin/reports/includes/sales_payments'); ?>
               <?php $this->load->view('admin/reports/includes/assets_register_report'); ?>
               <?php $this->load->view('admin/reports/includes/sales_classes'); ?>
               <?php $this->load->view('admin/reports/includes/sales_assets_analysis'); ?>
               <?php $this->load->view('admin/reports/includes/sales_assets_transfer'); ?>
               <?php $this->load->view('admin/reports/includes/sales_assets_scrap'); ?>
               <?php $this->load->view('admin/reports/includes/sales_assets_depreciation'); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<?php init_tail(); ?>
<?php $this->load->view('admin/reports/includes/sales_js'); ?>
</body>
</html>
