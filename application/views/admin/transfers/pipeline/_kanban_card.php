<?php
   if ($transfer['status'] == $status) { ?>
<li data-transfer-id="<?php echo $transfer['id']; ?>" class="<?php if($transfer['invoiceid'] != NULL){echo 'not-sortable';} ?>">
   <div class="panel-body">
      <div class="row">
         <div class="col-md-12">
            <h4 class="bold pipeline-heading"><a href="<?php echo admin_url('transfers/list_transfers/'.$transfer['id']); ?>" onclick="transfer_pipeline_open(<?php echo $transfer['id']; ?>); return false;"><?php echo format_transfer_number($transfer['id']); ?></a>
               <?php if(has_permission('transfers','','edit')){ ?>
               <a href="<?php echo admin_url('transfers/transfer/'.$transfer['id']); ?>" target="_blank" class="pull-right"><small><i class="fa fa-pencil-square-o" aria-hidden="true"></i></small></a>
               <?php } ?>
            </h4>
            <span class="inline-block full-width mbot10">
            <a href="<?php echo admin_url('organizations/organization/'.$transfer['organizationid']); ?>" target="_blank">
            <?php echo $transfer['company']; ?>
            </a>
            </span>
         </div>
         <div class="col-md-12">
            <div class="row">
               <div class="col-md-8">
                  <span class="bold">
                  <?php echo _l('transfer_total'); ?>
                  </span>
                  <br />
                  <?php echo _l('transfer_data_date') . ': ' . _d($transfer['date']); ?>
                  <?php if(is_date($transfer['expirydate']) || !empty($transfer['expirydate'])){
                     echo '<br />';
                     echo _l('transfer_data_expiry_date') . ': ' . _d($transfer['expirydate']);
                     } ?>
               </div>
               <div class="col-md-4 text-right">
                  <small><i class="fa fa-paperclip"></i> <?php echo _l('transfer_notes'); ?>: <?php echo total_rows('tblnotes', array(
                     'rel_id' => $transfer['id'],
                     'rel_type' => 'transfer',
                     )); ?></small>
               </div>
               <?php $tags = get_tags_in($transfer['id'],'transfer');
                  if(count($tags) > 0){ ?>
               <div class="col-md-12">
                  <div class="mtop5 kanban-tags">
                     <?php echo render_tags($tags); ?>
                  </div>
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</li>
<?php } ?>
