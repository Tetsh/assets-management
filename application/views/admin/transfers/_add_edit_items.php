<div class="panel-body mtop10">
   <div class="row">
      <div class="col-md-4">
         <div class="form-group no-mbot items-wrapper select-placeholder">
            <select name="item_select" class="selectpicker no-margin<?php if($ajaxAssets == false){echo ' ajax-search';} ?>" data-width="100%" id="item_select" data-none-selected-text="<?php echo _l('add_asset'); ?>" data-live-search="true">
               <option value=""></option>
               <?php foreach($assets as $class_id=>$_assets){ ?>

               <?php } ?>
            </select>
         </div>
      </div>
   </div>


   <div class="table-responsive s_table mtop10">
      <table class="table transfer-items-table items table-main-transfer-edit">
         <thead>
            <tr>
               <th></th>
               <th width="20%" align="left"><i class="fa fa-exclamation-circle" aria-hidden="true" data-toggle="tooltip" data-title="<?php echo _l('item_description_new_lines_notice'); ?>"></i> <?php echo _l('asset_name'); ?></th>
               <th width="25%" align="left"><?php echo _l('asset_description'); ?></th>
               <th width="10%" class="qty" align="right"><?php echo _l('asset_class'); ?></th>
               <th width="15%" align="right"><?php echo _l('asset_category'); ?></th>
               <th width="10%" align="right"><?php echo _l('asset_department'); ?></th>
               <th align="center"><i class="fa fa-cog"></i></th>
               <th></th>
            </tr>
         </thead>
         <tbody>
            <tr class="main">
               <td></td>
               <td>
                  <input name="name" class="form-control" placeholder="<?php echo _l('asset_name'); ?>" readonly>
               </td>
               <td>
                  <textarea name="description" rows="4" class="form-control" placeholder="<?php echo _l('asset_description'); ?>" readonly></textarea>
               </td>
               <td>
                  <input type="text" name="class_id" class="form-control" placeholder="<?php echo _l('asset_class'); ?>">
               </td>
               <td>
                  <input type="text" name="category_id" class="form-control" placeholder="<?php echo _l('asset_category'); ?>">
               </td>
               <td>
                  <input type="text" name="department_id" class="form-control" placeholder="<?php echo _l('asset_department'); ?>">
               </td>               
               <td></td>
               <td>
                  <?php
                     $new_asset = 'undefined';
                     if(isset($transfer)){
                       $new_asset = true;
                     }
                     ?>
                  <button type="button" onclick="add_asset_to_table('undefined','undefined',<?php echo $new_asset; ?>); return false;" class="btn pull-right btn-info"><i class="fa fa-check"></i></button>
               </td>
            </tr>
            <?php if (isset($transfer) || isset($add_assets)) {
               $i               = 1;
               $assets_indicator = 'newassets';
               if (isset($transfer)) {
                 $add_assets       = $transfer->assets;
                 $assets_indicator = 'assets';
               }

               foreach ($add_assets as $asset) {
                 $manual    = false;
                 $table_row = '<tr class="sortable asset">';
                 $table_row .= '<td class="dragger">';

                if ($asset['id'] == 0) {
                 $manual              = true;
               }
               $table_row .= form_hidden('' . $assets_indicator . '[' . $i . '][assetid]', $asset['id']);
               // order input
               $table_row .= '<input type="hidden" class="order" name="' . $assets_indicator . '[' . $i . '][order]">';
               $table_row .= '</td>';
               $table_row .= '<td class="bold description"><input name="' . $assets_indicator . '[' . $i . '][name]" class="form-control">' . clear_textarea_breaks($asset['name']) . '</td>';
               $table_row .= '<td class="bold description"><textarea name="' . $assets_indicator . '[' . $i . '][description]" class="form-control" rows="5">' . clear_textarea_breaks($asset['description']) . '</textarea></td>';

               $table_row .= '<td class="bold description"><input name="' . $assets_indicator . '[' . $i . '][class_id]" class="form-control">' . $asset['class_id'] . '</td>';
               $table_row .= '<td class="bold description"><input name="' . $assets_indicator . '[' . $i . '][category_id]" class="form-control">' . $asset['category_id'] . '</td>';
               $table_row .= '<td class="bold description"><input name="' . $assets_indicator . '[' . $i . '][department_id]" class="form-control">' . $asset['department_id'] . '</td>';

               $table_row .= '</tr>';
               echo $table_row;
               $i++;
               }
               }
               ?>
         </tbody>
      </table>
   </div>
   <div id="removed-items"></div>
</div>

<script>
    // Maybe in modal? Eq convert to invoice or convert proposal to transfer/invoice
    if(typeof(jQuery) != 'undefined'){
        init_asset_js();
    } else {
      window.addEventListener('load', function () {
        init_asset_js();
     });
  }

function init_asset_js() {
     // Add item to preview from the dropdown for invoices transfers
    $("body").on('change', 'select[name="item_select"]', function () {
        var assetid = $(this).selectpicker('val');
        if (assetid != '' && assetid !== 'newasset') {
            add_asset_to_preview(assetid);
        } else if (assetid == 'newasset') {
            // New item
            $('#sales_item_modal').modal('show');
        }
    });

}

</script>
