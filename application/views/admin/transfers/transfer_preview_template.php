<?php echo form_hidden('_attachment_sale_id',$transfer->id); ?>
<?php echo form_hidden('_attachment_sale_type','transfer'); ?>
<div class="col-md-12 no-padding">
   <div class="panel_s">
      <div class="panel-body">
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
               <a href="#tab_transfer" aria-controls="tab_transfer" role="tab" data-toggle="tab">
               <?php echo _l('transfer'); ?>
               </a>
            </li>
            <li role="presentation">
               <a href="#tab_activity" aria-controls="tab_activity" role="tab" data-toggle="tab">
               <?php echo _l('transfer_view_activity_tooltip'); ?>
               </a>
            </li>
            <li role="presentation">
               <a href="#tab_reminders" onclick="initDataTable('.table-reminders', admin_url + 'misc/get_reminders/' + <?php echo $transfer->id ;?> + '/' + 'transfer', [4], [4],undefined,[1,'ASC']); return false;" aria-controls="tab_reminders" role="tab" data-toggle="tab">
               <?php echo _l('transfer_reminders'); ?>
               <?php
                  $total_reminders = total_rows('tblreminders',
                    array(
                     'isnotified'=>0,
                     'staff'=>get_staff_user_id(),
                     'rel_type'=>'transfer',
                     'rel_id'=>$transfer->id
                     )
                    );
                  if($total_reminders > 0){
                    echo '<span class="badge">'.$total_reminders.'</span>';
                  }
                  ?>
               </a>
            </li>
            <li role="presentation" class="tab-separator">
               <a href="#tab_notes" onclick="get_sales_notes(<?php echo $transfer->id; ?>,'transfers'); return false" aria-controls="tab_notes" role="tab" data-toggle="tab">
               </span><?php echo _l('transfer_notes'); ?> <span class="notes-total">
               <?php if($totalNotes > 0){ ?>
               <span class="badge"><?php echo $totalNotes; ?><span>
               <?php } ?>
               </span>
               </a>
            </li>
            <li role="presentation" data-toggle="tooltip" data-title="<?php echo _l('view_tracking'); ?>" class="tab-separator">
               <a href="#tab_views" aria-controls="tab_views" role="tab" data-toggle="tab">
               <i class="fa fa-eye"></i>
               </a>
            </li>
            <li role="presentation" data-toggle="tooltip" data-title="<?php echo _l('toggle_full_view'); ?>" class="tab-separator">
               <a href="#" onclick="small_table_full_view(); return false;" class="toggle_view">
               <i class="fa fa-expand"></i></a>
            </li>
         </ul>
         <div class="row">
            <div class="col-md-3">
               <?php echo format_transfer_status($transfer->status,'mtop5');  ?>
            </div>
            <div class="col-md-9">
               <div class="visible-xs">
                  <div class="mtop10"></div>
               </div>
               <div class="pull-right _buttons">
                  <?php if(has_permission('transfers','','edit')){ ?>
                  <a href="<?php echo admin_url('transfers/transfer/'.$transfer->id); ?>" class="btn btn-default btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('edit_transfer_tooltip'); ?>" data-placement="bottom"><i class="fa fa-pencil-square-o"></i></a>
                  <?php } ?>
                  <a href="<?php echo admin_url('transfers/pdf/'.$transfer->id.'?print=true'); ?>" target="_blank" class="btn btn-default btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('print'); ?>" data-placement="bottom"><i class="fa fa-print"></i></a>
                  <a href="<?php echo admin_url('transfers/pdf/'.$transfer->id); ?>" class="btn btn-default btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('view_pdf'); ?>" data-placement="bottom"><i class="fa fa-file-pdf-o"></i></a>
                  <?php
                     $_tooltip = _l('transfer_sent_to_email_tooltip');
                     $_tooltip_already_send = '';
                     if($transfer->sent == 1){
                        $_tooltip_already_send = _l('transfer_already_send_to_organization_tooltip',time_ago($transfer->datesend));
                     }
                     ?>
                  <a href="#" class="transfer-send-to-organization btn btn-default btn-with-tooltip" data-toggle="tooltip" title="<?php echo $_tooltip; ?>" data-placement="bottom"><span data-toggle="tooltip" data-title="<?php echo $_tooltip_already_send; ?>"><i class="fa fa-envelope"></i></span></a>
                  <div class="btn-group">
                     <button type="button" class="btn btn-default pull-left dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <?php echo _l('more'); ?> <span class="caret"></span>
                     </button>
                     <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                           <a href="<?php echo site_url('viewtransfer/' . $transfer->id . '/' .  $transfer->hash) ?>" target="_blank">
                           <?php echo _l('view_transfer_as_organization'); ?>
                           </a>
                        </li>
                        <?php if(!empty($transfer->expirydate) && date('Y-m-d') < $transfer->expirydate && ($transfer->status == 2 || $transfer->status == 5) && is_transfers_expiry_reminders_enabled()){ ?>
                        <li>
                           <a href="<?php echo admin_url('transfers/send_expiry_reminder/'.$transfer->id); ?>">
                           <?php echo _l('send_expiry_reminder'); ?>
                           </a>
                        </li>
                        <?php } ?>
                        <li>
                           <a href="#" data-toggle="modal" data-target="#sales_attach_file"><?php echo _l('invoice_attach_file'); ?></a>
                        </li>
                        <?php if($transfer->invoiceid == NULL){
                           if(has_permission('transfers','','edit')){
                             foreach($transfer_statuses as $status){
                               if($transfer->status != $status){ ?>
                        <li>
                           <a href="<?php echo admin_url() . 'transfers/mark_action_status/'.$status.'/'.$transfer->id; ?>">
                           <?php echo _l('transfer_mark_as',format_transfer_status($status,'',false)); ?></a>
                        </li>
                        <?php }
                           }
                           ?>
                        <?php } ?>
                        <?php } ?>
                        <?php if(has_permission('transfers','','create')){ ?>
                        <li>
                           <a href="<?php echo admin_url('transfers/copy/'.$transfer->id); ?>">
                           <?php echo _l('copy_transfer'); ?>
                           </a>
                        </li>
                        <?php } ?>
                        <?php if(has_permission('transfers','','delete')){ ?>
                        <?php
                           if((get_option('delete_only_on_last_transfer') == 1 && is_last_transfer($transfer->id)) || (get_option('delete_only_on_last_transfer') == 0)){ ?>
                        <li>
                           <a href="<?php echo admin_url('transfers/delete/'.$transfer->id); ?>" class="text-danger delete-text _delete"><?php echo _l('delete_transfer_tooltip'); ?></a>
                        </li>
                        <?php
                           }
                           }
                           ?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
         <hr class="hr-panel-heading" />
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane ptop10 active" id="tab_transfer">
               <div id="transfer-preview">
                  <div class="row">
                     <?php if($transfer->status == 4 && !empty($transfer->acceptance_firstname) && !empty($transfer->acceptance_lastname) && !empty($transfer->acceptance_email)){ ?>
                     <div class="col-md-12">
                        <div class="alert alert-info mbot15">
                           <?php echo _l('accepted_identity_info',array(
                              _l('transfer_lowercase'),
                              '<b>'.$transfer->acceptance_firstname . ' ' . $transfer->acceptance_lastname . '</b> (<a href="mailto:'.$transfer->acceptance_email.'">'.$transfer->acceptance_email.'</a>)',
                              '<b>'. _dt($transfer->acceptance_date).'</b>',
                              '<b>'.$transfer->acceptance_ip.'</b>'.(is_admin() ? '&nbsp;<a href="'.admin_url('transfers/clear_acceptance_info/'.$transfer->id).'" class="_delete text-muted" data-toggle="tooltip" data-title="'._l('clear_this_information').'"><i class="fa fa-remove"></i></a>' : '')
                              )); ?>
                        </div>
                     </div>
                     <?php } ?>
                     <div class="col-md-6">
                        <h4 class="bold">
                           <?php
                              $tags = get_tags_in($transfer->id,'transfer');
                              if(count($tags) > 0){
                                echo '<i class="fa fa-tag" aria-hidden="true" data-toggle="tooltip" data-title="'.implode(', ',$tags).'"></i>';
                              }
                              ?>
                           <a href="<?php echo admin_url('transfers/transfer/'.$transfer->id); ?>">
                           <span id="transfer-number">
                           <?php echo format_transfer_number($transfer->id); ?>
                           </span>
                           </a>
                        </h4>
                        <address>
                           <?php echo format_main_company_info(); ?>
                        </address>
                     </div>
                     <div class="col-sm-6 text-right">
                        <span class="bold"><?php echo _l('transfer_to'); ?>:</span>
                        <address>
                           <?php echo format_organization_info($transfer, 'transfer', 'billing', true); ?>
                        </address>
                        <?php if($transfer->include_shipping == 1 && $transfer->show_shipping_on_transfer == 1){ ?>
                        <span class="bold"><?php echo _l('ship_to'); ?>:</span>
                        <address>
                           <?php echo format_organization_info($transfer, 'transfer', 'shipping'); ?>
                        </address>
                        <?php } ?>
                        <p class="no-mbot">
                           <span class="bold">
                           <?php echo _l('transfer_data_date'); ?>:
                           </span>
                           <?php echo $transfer->date; ?>
                        </p>
                        <?php if(!empty($transfer->expirydate)){ ?>
                        <p class="no-mbot">
                           <span class="bold"><?php echo _l('transfer_data_expiry_date'); ?>:</span>
                           <?php echo $transfer->expirydate; ?>
                        </p>
                        <?php } ?>
                        <?php if(!empty($transfer->reference_no)){ ?>
                        <p class="no-mbot">
                           <span class="bold"><?php echo _l('reference_no'); ?>:</span>
                           <?php echo $transfer->reference_no; ?>
                        </p>
                        <?php } ?>
                        <?php if($transfer->sale_agent != 0 && get_option('show_sale_agent_on_transfers') == 1){ ?>
                        <p class="no-mbot">
                           <span class="bold"><?php echo _l('sale_agent_string'); ?>:</span>
                           <?php echo get_staff_full_name($transfer->sale_agent); ?>
                        </p>
                        <?php } ?>
                        <?php if($transfer->asset_id != 0 && get_option('show_asset_on_transfer') == 1){ ?>
                        <p class="no-mbot">
                           <span class="bold"><?php echo _l('asset'); ?>:</span>
                           <?php echo get_asset_name_by_id($transfer->asset_id); ?>
                        </p>
                        <?php } ?>
                        <?php $pdf_custom_fields = get_custom_fields('transfer',array('show_on_pdf'=>1));
                           foreach($pdf_custom_fields as $field){
                           $value = get_custom_field_value($transfer->id,$field['id'],'transfer');
                           if($value == ''){continue;} ?>
                        <p class="no-mbot">
                           <span class="bold"><?php echo $field['name']; ?>: </span>
                           <?php echo $value; ?>
                        </p>
                        <?php } ?>
                     </div>
                  </div>
                  <div class="row">


                     <div class="col-md-12">
                        <div class="table-responsive">
                           <table class="table items transfer-items-preview">
                              <thead>
                                 <tr>
                                    <th align="left">#</th>
                                    <th class="description" align="left"><?php echo _l('asset_name'); ?></th>
                                    <th class="description" align="left"><?php echo _l('asset_description'); ?></th>
                                    <th align="right"><?php echo _l('asset_class'); ?></th>
                                    <th align="right"><?php echo _l('asset_category'); ?></th>
                                    <th align="right"><?php echo _l('asset_department'); ?></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    $assets_data = get_table_assets($transfer->assets,'transfer',true);
                                    echo $assets_data['html'];
                                    ?>
                              </tbody>
                           </table>
                        </div>
                     </div>





                     <?php if(count($transfer->attachments) > 0){ ?>
                     <div class="clearfix"></div>
                     <hr />
                     <div class="col-md-12">
                        <p class="bold text-muted"><?php echo _l('transfer_files'); ?></p>
                     </div>
                     <?php foreach($transfer->attachments as $attachment){
                        $attachment_url = site_url('download/file/sales_attachment/'.$attachment['attachment_key']);
                        if(!empty($attachment['external'])){
                          $attachment_url = $attachment['external_link'];
                        }
                        ?>
                     <div class="mbot15 row col-md-12" data-attachment-id="<?php echo $attachment['id']; ?>">
                        <div class="col-md-8">
                           <div class="pull-left"><i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i></div>
                           <a href="<?php echo $attachment_url; ?>" target="_blank"><?php echo $attachment['file_name']; ?></a>
                           <br />
                           <small class="text-muted"> <?php echo $attachment['filetype']; ?></small>
                        </div>
                        <div class="col-md-4 text-right">
                           <?php if($attachment['visible_to_organization'] == 0){
                              $icon = 'fa fa-toggle-off';
                              $tooltip = _l('show_to_organization');
                              } else {
                              $icon = 'fa fa-toggle-on';
                              $tooltip = _l('hide_from_organization');
                              }
                              ?>
                           <a href="#" data-toggle="tooltip" onclick="toggle_file_visibility(<?php echo $attachment['id']; ?>,<?php echo $transfer->id; ?>,this); return false;" data-title="<?php echo $tooltip; ?>"><i class="<?php echo $icon; ?>" aria-hidden="true"></i></a>
                           <?php if($attachment['staffid'] == get_staff_user_id() || is_admin()){ ?>
                           <a href="#" class="text-danger" onclick="delete_transfer_attachment(<?php echo $attachment['id']; ?>); return false;"><i class="fa fa-times"></i></a>
                           <?php } ?>
                        </div>
                     </div>
                     <?php } ?>
                     <?php } ?>
                     <?php if($transfer->organizationnote != ''){ ?>
                     <div class="col-md-12 mtop15">
                        <p class="bold text-muted"><?php echo _l('transfer_note'); ?></p>
                        <p><?php echo $transfer->organizationnote; ?></p>
                     </div>
                     <?php } ?>
                     <?php if($transfer->terms != ''){ ?>
                     <div class="col-md-12 mtop15">
                        <p class="bold text-muted"><?php echo _l('terms_and_conditions'); ?></p>
                        <p><?php echo $transfer->terms; ?></p>
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab_tasks">
               <?php init_relation_tasks_table(array('data-new-rel-id'=>$transfer->id,'data-new-rel-type'=>'transfer')); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab_reminders">
               <a href="#" data-toggle="modal" class="btn btn-info" data-target=".reminder-modal-transfer-<?php echo $transfer->id; ?>"><i class="fa fa-bell-o"></i> <?php echo _l('transfer_set_reminder_title'); ?></a>
               <hr />
               <?php render_datatable(array( _l( 'reminder_description'), _l( 'reminder_date'), _l( 'reminder_staff'), _l( 'reminder_is_notified'), _l( 'options'), ), 'reminders'); ?>
               <?php $this->load->view('admin/includes/modals/reminder',array('id'=>$transfer->id,'name'=>'transfer','members'=>$members,'reminder_title'=>_l('transfer_set_reminder_title'))); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab_notes">
               <?php echo form_open(admin_url('transfers/add_note/'.$transfer->id),array('id'=>'sales-notes','class'=>'transfer-notes-form')); ?>
               <?php echo render_textarea('description'); ?>
               <div class="text-right">
                  <button type="submit" class="btn btn-info mtop15 mbot15"><?php echo _l('transfer_add_note'); ?></button>
               </div>
               <?php echo form_close(); ?>
               <hr />
            <div class="panel_s mtop20 no-shadow" id="sales_notes_area">
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab_activity">
               <div class="row">
                  <div class="col-md-12">
                     <div class="activity-feed">
                        <?php foreach($activity as $activity){
                           $_custom_data = false;
                           ?>
                        <div class="feed-item" data-sale-activity-id="<?php echo $activity['id']; ?>">
                           <div class="date">
                              <span class="text-has-action" data-toggle="tooltip" data-title="<?php echo _dt($activity['date']); ?>">
                              <?php echo time_ago($activity['date']); ?>
                              </span>
                              </div>
                           <div class="text">
                              <?php if(is_numeric($activity['staffid']) && $activity['staffid'] != 0){ ?>
                              <a href="<?php echo admin_url('profile/'.$activity["staffid"]); ?>">
                              <?php echo staff_profile_image($activity['staffid'],array('staff-profile-xs-image pull-left mright5'));
                                 ?>
                              </a>
                              <?php } ?>
                              <?php
                                 $additional_data = '';
                                 if(!empty($activity['additional_data'])){
                                  $additional_data = unserialize($activity['additional_data']);
                                  $i = 0;
                                  foreach($additional_data as $data){
                                    if(strpos($data,'<original_status>') !== false){
                                      $original_status = get_string_between($data, '<original_status>', '</original_status>');
                                      $additional_data[$i] = format_transfer_status($original_status,'',false);
                                    } else if(strpos($data,'<new_status>') !== false){
                                      $new_status = get_string_between($data, '<new_status>', '</new_status>');
                                      $additional_data[$i] = format_transfer_status($new_status,'',false);
                                    } else if(strpos($data,'<status>') !== false){
                                      $status = get_string_between($data, '<status>', '</status>');
                                      $additional_data[$i] = format_transfer_status($status,'',false);
                                    } else if(strpos($data,'<custom_data>') !== false){
                                      $_custom_data = get_string_between($data, '<custom_data>', '</custom_data>');
                                      unset($additional_data[$i]);
                                    }
                                    $i++;
                                  }
                                 }
                                 $_formatted_activity = _l($activity['description'],$additional_data);
                                 if($_custom_data !== false){
                                 $_formatted_activity .= ' - ' .$_custom_data;
                                 }
                                 if(!empty($activity['full_name'])){
                                 $_formatted_activity = $activity['full_name'] . ' - ' . $_formatted_activity;
                                 }
                                 echo $_formatted_activity;
                                 if(is_admin()){
                                 echo '<a href="#" class="pull-right text-danger" onclick="delete_sale_activity('.$activity['id'].'); return false;"><i class="fa fa-remove"></i></a>';
                                 }
                                 ?>
                           </div>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane ptop10" id="tab_views">
               <?php
                  $views_activity = get_views_tracking('transfer',$transfer->id);
                  foreach($views_activity as $activity){ ?>
               <p class="text-success no-margin">
                  <?php echo _l('view_date') . ': ' . _dt($activity['date']); ?>
               </p>
               <p class="text-muted">
                  <?php echo _l('view_ip') . ': ' . $activity['view_ip']; ?>
               </p>
               <hr />
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   init_items_sortable(true);
   init_btn_with_tooltips();
   init_datepicker();
   init_selectpicker();
   init_form_reminder();
</script>
<?php $this->load->view('admin/transfers/transfer_send_to_organization'); ?>
