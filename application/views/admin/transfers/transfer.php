<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<?php
			echo form_open($this->uri->uri_string(),array('id'=>'transfer-form','class'=>'_transaction_form'));
			if(isset($transfer)){
				echo form_hidden('isedit');
			}
			?>
			<div class="col-md-12">
				<?php $this->load->view('admin/transfers/transfer_template'); ?>
			</div>
			<?php echo form_close(); ?>
 			<?php $this->load->view('admin/invoice_items/item'); ?> 
		</div>
	</div>
</div>
</div>
<?php init_tail(); ?>
<script>
	$(function(){
		validate_transfer_form();
		// Init accountacy currency symbol
		init_currency_symbol();
		// Project ajax search
		init_ajax_asset_search_by_organization_id();
		init_ajax_asset_search_by_organization_from();
		// Maybe items ajax search
	    init_ajax_search('assets','#asset_select.ajax-search',undefined,admin_url+'assets/search');
	});
</script>
</body>
</html>
