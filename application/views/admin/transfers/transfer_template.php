<div class="panel_s accounting-template transfer">
   <div class="panel-body">
      <?php if(isset($transfer)){ ?>
      <?php echo format_transfer_status($transfer->status); ?>
      <hr class="hr-panel-heading" />
      <?php } ?>
      <div class="row">
         <div class="col-md-6">
            <div class="f_organization_id">
             <div class="form-group select-placeholder">
                <label for="organizationid" class="control-label"><?php echo _l('transfer_select_organization'); ?></label>
                <select id="organizationid" name="organizationid" data-live-search="true" data-width="100%" class="ajax-search" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
               <?php $selected = (isset($transfer) ? $transfer->organizationid : '');
                 if($selected == ''){
                   $selected = (isset($organization_id) ? $organization_id: '');
                 }
                 if($selected != ''){
                    $rel_data = get_relation_data('organization',$selected);
                    $rel_val = get_relation_values($rel_data,'organization');
                    echo '<option value="'.$rel_val['id'].'" selected>'.$rel_val['name'].'</option>';
                 } ?>
                </select>
              </div>
            </div>
<!--             <div class="form-group select-placeholder assets-wrapper<?php if((!isset($transfer)) || (isset($transfer) && !organization_has_assets($transfer->organizationid))){ echo ' hide';} ?>">
             <label for=""><?php echo _l('asset'); ?></label>
             <div id="asset_ajax_search_wrapper">
               <select name="asset_id" id="asset_id" class="assets ajax-search" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                <?php
                  if(isset($transfer) && $transfer->asset_id != 0){
                    echo '<option value="'.$transfer->asset_id.'" selected>'.get_asset_name_by_id($transfer->asset_id).'</option>';
                  }
                ?>
              </select>
            </div>
           </div> -->

           <div class="f_transfer_type">
              <div class="form-group select-placeholder">
                 <label for="rel_type" class="control-label"><?php echo _l('transfer_related'); ?></label>
                 <select name="transfer_type" id="transfer_type" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                    <option value=""></option>
                    <option value="1" <?php if((isset($transfer) && $transfer->transfer_type == 'internal') || $this->input->get('transfer_type')){if($transfer_type == 'internal'){echo 'selected';}} ?>><?php echo _l('internal_transfer'); ?></option>
                    <option value="2" <?php if((isset($transfer) &&  $transfer->transfer_type == 'external') || $this->input->get('transfer_type')){if($transfer_type == 'external'){echo 'selected';}} ?>><?php echo _l('external_transfer'); ?></option>
                 </select>
              </div>            
            </div>

            
<!--             <div class="form-group select-placeholder assets-wrapper-from">
                  <label for="organizationid_from" class="control-label"><?php echo _l('transfer_select_organization'); ?></label>
                  <div id="transfer_ajax_search_wrapper">
                    <select id="organizationid_from" name="organizationid_from" data-live-search="true" data-width="100%" class="ajax-search" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                   <?php $selected = (isset($transfer) ? $transfer->organizationid_from : '');
                     if($selected == ''){
                       $selected = (isset($organization_id) ? $organization_id: '');
                     }
                     if($selected != ''){
                        $rel_data = get_relation_data('organization',$selected);
                        $rel_val = get_relation_values($rel_data,'organization');
                        echo '<option value="'.$rel_val['id'].'" selected>'.$rel_val['name'].'</option>';
                     } ?>
                    </select>
                  </div>
                </div> -->


<!--             <div class="row">
               <div class="col-md-12">
                  <a href="#" class="edit_shipping_billing_info" data-toggle="modal" data-target="#billing_and_shipping_details"><i class="fa fa-pencil-square-o"></i></a>
                  <?php include_once(APPPATH .'views/admin/transfers/billing_and_shipping_template.php'); ?>
               </div>
               <div class="col-md-6">
                  <p class="bold"><?php echo _l('ship_from'); ?></p>
                  <address>
                     <span class="shipping_street">
                     <?php $shipping_street = (isset($transfer) ? $transfer->shipping_street : '--'); ?>
                     <?php $shipping_street = ($shipping_street == '' ? '--' :$shipping_street); ?>
                     <?php echo $shipping_street; ?></span><br>
                     <span class="shipping_city">
                     <?php $shipping_city = (isset($transfer) ? $transfer->shipping_city : '--'); ?>
                     <?php $shipping_city = ($shipping_city == '' ? '--' :$shipping_city); ?>
                     <?php echo $shipping_city; ?></span>,
                     <span class="shipping_state">
                     <?php $shipping_state = (isset($transfer) ? $transfer->shipping_state : '--'); ?>
                     <?php $shipping_state = ($shipping_state == '' ? '--' :$shipping_state); ?>
                     <?php echo $shipping_state; ?></span>
                     <br/>
                     <span class="shipping_country">
                     <?php $shipping_country = (isset($transfer) ? get_country_short_name($transfer->shipping_country) : '--'); ?>
                     <?php $shipping_country = ($shipping_country == '' ? '--' :$shipping_country); ?>
                     <?php echo $shipping_country; ?></span>,
                     <span class="shipping_zip">
                     <?php $shipping_zip = (isset($transfer) ? $transfer->shipping_zip : '--'); ?>
                     <?php $shipping_zip = ($shipping_zip == '' ? '--' :$shipping_zip); ?>
                     <?php echo $shipping_zip; ?></span>
                  </address>
               </div>
            </div> -->
            <?php
               $next_transfer_number = get_option('next_transfer_number');
               $format = get_option('transfer_number_format');

                if(isset($transfer)){
                  $format = $transfer->number_format;
                }

               $prefix = get_option('transfer_prefix');

               if ($format == 1) {
                 $__number = $next_transfer_number;
                 if(isset($transfer)){
                   $__number = $transfer->number;
                   $prefix = '<span id="prefix">' . $transfer->prefix . '</span>';
                 }
               } else if($format == 2) {
                 if(isset($transfer)){
                   $__number = $transfer->number;
                   $prefix = $transfer->prefix;
                   $prefix = '<span id="prefix">'. $prefix . '</span><span id="prefix_year">' . date('Y',strtotime($transfer->date)).'</span>/';
                 } else {
                   $__number = $next_transfer_number;
                   $prefix = $prefix.'<span id="prefix_year">'.date('Y').'</span>/';
                 }
               } else if($format == 3) {
                  if(isset($transfer)){
                   $yy = date('y',strtotime($transfer->date));
                   $__number = $transfer->number;
                   $prefix = '<span id="prefix">'. $transfer->prefix . '</span>';
                 } else {
                  $yy = date('y');
                  $__number = $next_transfer_number;
                }
               } else if($format == 4) {
                  if(isset($transfer)){
                   $yyyy = date('Y',strtotime($transfer->date));
                   $mm = date('m',strtotime($transfer->date));
                   $__number = $transfer->number;
                   $prefix = '<span id="prefix">'. $transfer->prefix . '</span>';
                 } else {
                  $yyyy = date('Y');
                  $mm = date('m');
                  $__number = $next_transfer_number;
                }
               }

               $_transfer_number = str_pad($__number, get_option('number_padding_prefixes'), '0', STR_PAD_LEFT);
               $isedit = isset($transfer) ? 'true' : 'false';
               $data_original_number = isset($transfer) ? $transfer->number : 'false';
               ?>
            <div class="form-group">
               <label for="number"><?php echo _l('transfer_add_edit_number'); ?></label>
               <div class="input-group">
                  <span class="input-group-addon">
                  <?php if(isset($transfer)){ ?>
                  <a href="#" onclick="return false;" data-toggle="popover" data-container='._transaction_form' data-html="true" data-content="<label class='control-label'><?php echo _l('settings_sales_transfer_prefix'); ?></label><div class='input-group'><input name='s_prefix' type='text' class='form-control' value='<?php echo $transfer->prefix; ?>'></div><button type='button' onclick='save_sales_number_settings(this); return false;' data-url='<?php echo admin_url('transfers/update_number_settings/'.$transfer->id); ?>' class='btn btn-info btn-block mtop15'><?php echo _l('submit'); ?></button>"><i class="fa fa-cog"></i></a>
                   <?php }
                    echo $prefix;
                  ?>
                 </span>
                  <input type="text" name="number" class="form-control" value="<?php echo $_transfer_number; ?>" data-isedit="<?php echo $isedit; ?>" data-original-number="<?php echo $data_original_number; ?>" readonly>
                  <?php if($format == 3) { ?>
                  <span class="input-group-addon">
                     <span id="prefix_year" class="format-n-yy"><?php echo $yy; ?></span>
                  </span>
                  <?php } else if($format == 4) { ?>
                   <span class="input-group-addon">
                     <span id="prefix_month" class="format-mm-yyyy"><?php echo $mm; ?></span>
                     /
                     <span id="prefix_year" class="format-mm-yyyy"><?php echo $yyyy; ?></span>
                  </span>
                  <?php } ?>
               </div>
            </div>

            <div class="row">
               <div class="col-md-6">
                  <?php $value = (isset($transfer) ? _d($transfer->date) : _d(date('Y-m-d'))); ?>
                  <?php echo render_date_input('date','transfer_add_edit_date',$value); ?>
               </div>
               <div class="col-md-6">
                  <?php
                  $value = '';
                  if(isset($transfer)){
                    $value = _d($transfer->expirydate);
                  } else {
                      if(get_option('transfer_due_after') != 0){
                          $value = _d(date('Y-m-d', strtotime('+' . get_option('transfer_due_after') . ' DAY', strtotime(date('Y-m-d')))));
                      }
                  }
                  echo render_date_input('expirydate','transfer_add_edit_expirydate',$value); ?>
               </div>
            </div>
            <div class="clearfix mbot15"></div>
            <?php $rel_id = (isset($transfer) ? $transfer->id : false); ?>
            <?php echo render_custom_fields('transfer',$rel_id); ?>
         </div>
         <div class="col-md-6">


            <div class="panel_s no-shadow">
               <div class="form-group">
                  <label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
                  <input type="text" class="tagsinput" id="tags" name="tags" value="<?php echo (isset($transfer) ? prep_tags_input(get_tags_in($transfer->id,'transfer')) : ''); ?>" data-role="tagsinput">
               </div>
               <div class="row">
                   <div class="col-md-6">
                     <div class="form-group select-placeholder">
                        <label class="control-label"><?php echo _l('transfer_status'); ?></label>
                        <select class="selectpicker display-block mbot15" name="status" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <?php foreach($transfer_statuses as $status){ ?>
                           <option value="<?php echo $status; ?>" <?php if(isset($transfer) && $transfer->status == $status){echo 'selected';} ?>><?php echo format_transfer_status($status,'',false); ?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>
               <?php $value = (isset($transfer) ? $transfer->adminnote : ''); ?>
               <?php echo render_textarea('adminnote','transfer_add_edit_admin_note',$value,array(),array(),'mtop15'); ?>

            </div>
         </div>
      </div>
   </div>
   <?php $this->load->view('admin/transfers/_add_edit_items'); ?>
   <div class="row">
      <div class="col-md-12 mtop15">
         <div class="panel-body bottom-transaction">
            <?php $value = (isset($transfer) ? $transfer->organizationnote : get_option('predefined_organizationnote_transfer')); ?>
            <?php echo render_textarea('organizationnote','transfer_add_edit_organization_note',$value,array(),array(),'mtop15'); ?>
            <?php $value = (isset($transfer) ? $transfer->terms : get_option('predefined_terms_transfer')); ?>
            <?php echo render_textarea('terms','terms_and_conditions',$value,array(),array(),'mtop15'); ?>
            <div class="btn-bottom-toolbar text-right">
              <button type="button" class="btn-tr btn btn-info mleft10 transfer-form-submit save-and-send transaction-submit">
              <?php echo _l('save_and_send'); ?>
              </button>
              <button type="button" class="btn-tr btn btn-info mleft10 transfer-form-submit transaction-submit">
              <?php echo _l('submit'); ?>
              </button>
            </div>
         </div>
           <div class="btn-bottom-pusher"></div>
      </div>
   </div>
</div>
