<?php echo form_hidden('asset_id',$asset->id); ?>
<div class="panel_s">
    <div class="panel-body">
       <h3 class="bold mtop10 asset-name pull-left"><?php echo $asset->name; ?></h3>
        <?php if($asset->settings->view_tasks == 1 && $asset->settings->create_tasks == 1){ ?>
        <a href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=new_task'); ?>" class="btn btn-info pull-right mtop5"><?php echo _l('new_task'); ?></a>
        <?php } ?>
   </div>
</div>
<div class="panel_s">
    <div class="panel-body">
        <?php get_template_part('assets/asset_tabs'); ?>
        <div class="clearfix mtop15"></div>
        <?php get_template_part('assets/'.$group); ?>
    </div>
</div>
