<div class="col-md-12 page-pdf-html-logo">
    <?php get_company_logo('','pull-left'); ?>
    <?php if(is_staff_logged_in()){ ?>
    <a href="<?php echo admin_url(); ?>transfers/list_transfers/<?php echo $transfer->id; ?>" class="btn btn-info pull-right"><?php echo _l('goto_admin_area'); ?>
    </a>
    <?php } else if(is_organization_logged_in() && has_contact_permission('transfers')){ ?>
    <a href="<?php echo site_url('organizations/transfers/'); ?>" class="btn btn-info pull-right"><?php echo _l('organization_go_to_dashboard'); ?></a>
    <?php } ?>
</div>
<div class="clearfix"></div>
<div class="panel_s mtop20">
    <div class="panel-body">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="mtop10 display-block">
                        <?php echo format_transfer_status($transfer->status,'',true); ?>
                    </div>
                </div>
                <div class="col-md-6 text-right _buttons">
                   <div class="visible-xs">
                    <div class="mtop10"></div>
                </div>
                <?php echo form_open($this->uri->uri_string(),array('class'=>'pull-right')); ?>
                <button type="submit" name="transferpdf" class="btn btn-info" value="transferpdf">
                    <i class="fa fa-file-pdf-o"></i> <?php echo _l('organizations_invoice_html_btn_download'); ?>
                </button>
                <?php echo form_close(); ?>
                <?php
                if ($transfer->status != 4 && $transfer->status != 3) {
                    echo form_open($this->uri->uri_string(),array('class'=>'pull-right mright10'));
                    echo form_hidden('transfer_action',3);
                    echo '<button type="submit" data-loading-text="'._l('wait_text').'" autocomplete="off" class="btn btn-default"><i class="fa fa-remove"></i> '._l('organizations_decline_transfer').'</button>';
                    echo form_close();
                    $can_be_accepted = true;
                    if($identity_confirmation_enabled == '0'){
                        echo form_open($this->uri->uri_string(),array('class'=>'pull-right mright10'));
                        echo form_hidden('transfer_action',4);
                        echo '<button type="submit" data-loading-text="'._l('wait_text').'" autocomplete="off" class="btn btn-success"><i class="fa fa-check"></i> '._l('organizations_accept_transfer').'</button>';
                        echo form_close();
                    } else {
                        echo '<button type="button" id="accept_action" class="btn btn-success mright10 pull-right"><i class="fa fa-check"></i> '._l('organizations_accept_transfer').'</button>';
                    }
                } else if($transfer->status == 3){
                    if ($transfer->expirydate >= date('Y-m-d') && $transfer->status != 5) {
                        $can_be_accepted = true;
                        if($identity_confirmation_enabled == '0'){
                            echo form_open($this->uri->uri_string(),array('class'=>'pull-right mright10'));
                            echo form_hidden('transfer_action',4);
                            echo '<button type="submit" data-loading-text="'._l('wait_text').'" autocomplete="off" class="btn btn-success"><i class="fa fa-check"></i> '._l('organizations_accept_transfer').'</button>';
                            echo form_close();
                        } else {
                            echo '<button type="button" id="accept_action" class="btn btn-success mright10 pull-right"><i class="fa fa-check"></i> '._l('organizations_accept_transfer').'</button>';
                        }
                    }
                }
                ?>
            </div>
        </div>
        <div class="row mtop40">
            <div class="col-md-6">
                <h4 class="bold"><?php echo format_transfer_number($transfer->id); ?></h4>
                <address>
                    <?php echo format_main_company_info(); ?>
                </address>
            </div>
            <div class="col-sm-6 text-right">
                <span class="bold"><?php echo _l('transfer_to'); ?>:</span>
                <address>
                 <?php echo format_organization_info($transfer, 'transfer', 'billing'); ?>
             </address>
             <!-- shipping details -->
             <?php if($transfer->include_shipping == 1 && $transfer->show_shipping_on_transfer == 1){ ?>
             <span class="bold"><?php echo _l('ship_to'); ?>:</span>
             <address>
                <?php echo format_organization_info($transfer, 'transfer', 'shipping'); ?>
            </address>
            <?php } ?>
            <p class="no-mbot">
                <span class="bold">
                    <?php echo _l('transfer_data_date'); ?>
                </span>
                <?php echo _d($transfer->date); ?>
            </p>
            <?php if(!empty($transfer->expirydate)){ ?>
            <p class="no-mbot">
                <span class="bold"><?php echo _l('transfer_data_expiry_date'); ?></span>
                <?php echo _d($transfer->expirydate); ?>
            </p>
            <?php } ?>
            <?php if(!empty($transfer->reference_no)){ ?>
            <p class="no-mbot">
                <span class="bold"><?php echo _l('reference_no'); ?>:</span>
                <?php echo $transfer->reference_no; ?>
            </p>
            <?php } ?>
            <?php if($transfer->sale_agent != 0 && get_option('show_sale_agent_on_transfers') == 1){ ?>
            <p class="no-mbot">
                <span class="bold"><?php echo _l('sale_agent_string'); ?>:</span>
                <?php echo get_staff_full_name($transfer->sale_agent); ?>
            </p>
            <?php } ?>
            <?php if($transfer->asset_id != 0 && get_option('show_asset_on_transfer') == 1){ ?>
            <p class="no-mbot">
                <span class="bold"><?php echo _l('asset'); ?>:</span>
                <?php echo get_asset_name_by_id($transfer->asset_id); ?>
            </p>
            <?php } ?>
            <?php $pdf_custom_fields = get_custom_fields('transfer',array('show_on_pdf'=>1,'show_on_organization_portal'=>1));
            foreach($pdf_custom_fields as $field){
                $value = get_custom_field_value($transfer->id,$field['id'],'transfer');
                if($value == ''){continue;} ?>
                <p class="no-mbot">
                    <span class="bold"><?php echo $field['name']; ?>: </span>
                    <?php echo $value; ?>
                </p>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table items">
                        <thead>
                                 <tr>
                                    <th align="left">#</th>
                                    <th class="description" align="left"><?php echo _l('asset_name'); ?></th>
                                    <th class="description" align="left"><?php echo _l('asset_description'); ?></th>
                                    <th align="right"><?php echo _l('asset_class'); ?></th>
                                    <th align="right"><?php echo _l('asset_category'); ?></th>
                                    <th align="right"><?php echo _l('asset_department'); ?></th>
                                 </tr>
                        </thead>
                        <tbody>
                                 <?php
                                    $assets_data = get_table_assets($transfer->assets,'transfer',true);
                                    echo $assets_data['html'];
                                ?>
                       </tbody>
                   </table>
               </div>
           </div>

        <?php
        if(get_option('total_to_words_enabled') == 1){ ?>
        <div class="col-md-12 text-center">
           <p class="bold"><?php echo  _l('num_word').': '.$this->numberword->convert($transfer->total,$transfer->currency_name); ?></p>
       </div>
       <?php } ?>
       <?php if(count($transfer->attachments) > 0 && $transfer->visible_attachments_to_organization_found == true){ ?>
       <div class="clearfix"></div>
       <div class="col-md-12"><hr />
        <p class="bold mbot15 font-medium"><?php echo _l('transfer_files'); ?></p>
    </div>
    <?php foreach($transfer->attachments as $attachment){
        // Do not show hidden attachments to organization
        if($attachment['visible_to_organization'] == 0){continue;}
        $attachment_url = site_url('download/file/sales_attachment/'.$attachment['attachment_key']);
        if(!empty($attachment['external'])){
            $attachment_url = $attachment['external_link'];
        }
        ?>
        <div class="col-md-12 mbot15">
            <div class="pull-left"><i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i></div>
            <a href="<?php echo $attachment_url; ?>"><?php echo $attachment['file_name']; ?></a>
        </div>
        <?php } ?>
        <?php } ?>
        <?php if(!empty($transfer->organizationnote)){ ?>
        <div class="col-md-12">
            <b><?php echo _l('transfer_note'); ?></b><br /><br /><?php echo $transfer->organizationnote; ?>
        </div>
        <?php } ?>
        <?php if(!empty($transfer->terms)){ ?>
        <div class="col-md-12">
            <hr />
            <b><?php echo _l('terms_and_conditions'); ?></b><br /><br /><?php echo $transfer->terms; ?>
        </div>
        <?php } ?>
    </div>
</div>
</div>
</div>
<?php
if($identity_confirmation_enabled == '1' && $can_be_accepted){
    get_template_part('identity_confirmation_form',array('formData'=>form_hidden('transfer_action',4)));
}
?>

