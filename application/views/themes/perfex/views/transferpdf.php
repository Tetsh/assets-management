<?php

$dimensions = $pdf->getPageDimensions();

$info_right_column = '';
$info_left_column = '';

$info_right_column .= '<span style="font-weight:bold;font-size:27px;">'._l('transfer_pdf_heading').'</span><br />';
$info_right_column .= '<b style="color:#4e4e4e;"># ' . $transfer_number . '</b>';

if(get_option('show_status_on_pdf_ei') == 1){
    $info_right_column .= '<br /><span style="color:rgb('.transfer_status_color_pdf($status).');text-transform:uppercase;">' . format_transfer_status($status,'',false) . '</span>';
}

// write the first column
$info_left_column .= pdf_logo_url();
$pdf->MultiCell(($dimensions['wk'] / 2) - $dimensions['lm'], 0, $info_left_column, 0, 'J', 0, 0, '', '', true, 0, true, true, 0);
// write the second column
$pdf->MultiCell(($dimensions['wk'] / 2) - $dimensions['rm'], 0, $info_right_column, 0, 'R', 0, 1, '', '', true, 0, true, false, 0);
$pdf->ln(10);

// Get Y position for the separation
$y            = $pdf->getY();
$organization_info = '<div style="color:#424242;">';
    $organization_info .= format_main_company_info();
$organization_info .= '</div>';

$pdf->writeHTMLCell(($swap == '1' ? ($dimensions['wk']) - ($dimensions['lm'] * 2) : ($dimensions['wk'] / 2) - $dimensions['lm']), '', '', $y, $organization_info, 0, 0, false, true, ($swap == '1' ? 'R' : 'J'), true);

// Estimate to
$transfer_info = '<b>' ._l('transfer_to') . '</b>';
$transfer_info .= '<div style="color:#424242;">';
$transfer_info .= format_organization_info($transfer, 'transfer', 'billing');
$transfer_info .= '</div>';

// ship to to
if($transfer->include_shipping == 1 && $transfer->show_shipping_on_transfer == 1){
    $transfer_info .= '<br /><b>' . _l('ship_to') . '</b>';
    $transfer_info .= '<div style="color:#424242;">';
    $transfer_info .= format_organization_info($transfer, 'transfer', 'shipping');
    $transfer_info .= '</div>';
}

$transfer_info .= '<br />'._l('transfer_data_date') . ': ' . _d($transfer->date).'<br />';

if (!empty($transfer->expirydate)) {
    $transfer_info .= _l('transfer_data_expiry_date') . ': ' . _d($transfer->expirydate) . '<br />';
}

if (!empty($transfer->reference_no)) {
    $transfer_info .= _l('reference_no') . ': ' . $transfer->reference_no. '<br />';
}

if($transfer->sale_agent != 0 && get_option('show_sale_agent_on_transfers') == 1){
    $transfer_info .= _l('sale_agent_string') . ': ' .  get_staff_full_name($transfer->sale_agent). '<br />';
}

if ($transfer->asset_id != 0 && get_option('show_asset_on_transfer') == 1) {
    $transfer_info .= _l('asset') . ': ' . get_asset_name_by_id($transfer->asset_id). '<br />';
}

foreach($pdf_custom_fields as $field){
    $value = get_custom_field_value($transfer->id,$field['id'],'transfer');
    if($value == ''){continue;}
    $transfer_info .= $field['name'] . ': ' . $value. '<br />';
}

$pdf->writeHTMLCell(($dimensions['wk'] / 2) - $dimensions['rm'], '', '', ($swap == '1' ? $y : ''), $transfer_info, 0, 1, false, true, ($swap == '1' ? 'J' : 'R'), true);

// The Table
$pdf->Ln(6);
$item_width = 38;
// If show item taxes is disabled in PDF we should increase the item width table heading
$item_width = get_option('show_tax_per_item') == 0 ? $item_width+15 : $item_width;
$custom_fields_items = get_items_custom_fields_for_table_html($transfer->id,'transfer');

// Calculate headings width, in case there are custom fields for items
$total_headings = get_option('show_tax_per_item') == 1 ? 4 : 3;
$total_headings += count($custom_fields_items);
$headings_width = (100-($item_width+6)) / $total_headings;

$qty_heading = _l('transfer_table_quantity_heading');
if($transfer->show_quantity_as == 2){
    $qty_heading = _l('transfer_table_hours_heading');
} else if($transfer->show_quantity_as == 3){
    $qty_heading = _l('transfer_table_quantity_heading') .'/'._l('transfer_table_hours_heading');
}

// Header
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="8">';

$tblhtml .= '<tr height="30" bgcolor="' . get_option('pdf_table_heading_color') . '" style="color:' . get_option('pdf_table_heading_text_color') . ';">';

$tblhtml .= '<th width="5%;" align="center">#</th>';
$tblhtml .= '<th width="'.$item_width.'%" align="left">' . _l('transfer_table_item_heading') . '</th>';

foreach ($custom_fields_items as $cf) {
    $tblhtml .= '<th width="'.$headings_width.'%" align="left">' . $cf['name'] . '</th>';
}

$tblhtml .= '<th width="'.$headings_width.'%" align="right">' . $qty_heading . '</th>';
$tblhtml .= '<th width="'.$headings_width.'%" align="right">' . _l('transfer_table_rate_heading') . '</th>';

if (get_option('show_tax_per_item') == 1) {
    $tblhtml .= '<th width="'.$headings_width.'%" align="right">' . _l('transfer_table_tax_heading') . '</th>';
}

$tblhtml .= '<th width="'.$headings_width.'%" align="right">' . _l('transfer_table_amount_heading') . '</th>';
$tblhtml .= '</tr>';

$tblhtml .= '<tbody>';

$items_data = get_table_items_and_taxes($transfer->items,'transfer');

$tblhtml .= $items_data['html'];
$taxes = $items_data['taxes'];

$tblhtml .= '</tbody>';
$tblhtml .= '</table>';

$pdf->writeHTML($tblhtml, true, false, false, false, '');

$pdf->Ln(8);
$tbltotal = '';
$tbltotal .= '<table cellpadding="6" style="font-size:'.($font_size+4).'px">';
$tbltotal .= '
<tr>
    <td align="right" width="85%"><strong>'._l('transfer_subtotal').'</strong></td>
    <td align="right" width="15%">' . format_money($transfer->subtotal,$transfer->symbol) . '</td>
</tr>';

if(is_sale_discount_applied($transfer)){
    $tbltotal .= '
    <tr>
        <td align="right" width="85%"><strong>' . _l('transfer_discount');
        if(is_sale_discount($transfer,'percent')){
            $tbltotal .= '(' . _format_number($transfer->discount_percent, true) . '%)';
        }
        $tbltotal .= '</strong>';
        $tbltotal .= '</td>';
        $tbltotal .= '<td align="right" width="15%">-' . format_money($transfer->discount_total, $transfer->symbol) . '</td>
    </tr>';
}

foreach ($taxes as $tax) {
    $tbltotal .= '<tr>
    <td align="right" width="85%"><strong>' . $tax['taxname'] . ' (' . _format_number($tax['taxrate']) . '%)' . '</strong></td>
    <td align="right" width="15%">' . format_money($tax['total_tax'], $transfer->symbol) . '</td>
</tr>';
}

if ((int)$transfer->adjustment != 0) {
    $tbltotal .= '<tr>
    <td align="right" width="85%"><strong>'._l('transfer_adjustment').'</strong></td>
    <td align="right" width="15%">' . format_money($transfer->adjustment,$transfer->symbol) . '</td>
</tr>';
}

$tbltotal .= '
<tr style="background-color:#f0f0f0;">
    <td align="right" width="85%"><strong>'._l('transfer_total').'</strong></td>
    <td align="right" width="15%">' . format_money($transfer->total, $transfer->symbol) . '</td>
</tr>';

$tbltotal .= '</table>';

$pdf->writeHTML($tbltotal, true, false, false, false, '');

if(get_option('total_to_words_enabled') == 1){
     // Set the font bold
     $pdf->SetFont($font_name,'B',$font_size);
     $pdf->Cell(0, 0, _l('num_word').': '.$CI->numberword->convert($transfer->total,$transfer->currency_name), 0, 1, 'C', 0, '', 0);
     // Set the font again to normal like the rest of the pdf
     $pdf->SetFont($font_name,'',$font_size);
     $pdf->Ln(4);
}

if (!empty($transfer->organizationnote)) {
    $pdf->Ln(4);
    $pdf->SetFont($font_name,'B',$font_size);
    $pdf->Cell(0, 0, _l('transfer_note'), 0, 1, 'L', 0, '', 0);
    $pdf->SetFont($font_name,'',$font_size);
    $pdf->Ln(2);
    $pdf->writeHTMLCell('', '', '', '', $transfer->organizationnote, 0, 1, false, true, 'L', true);
}

if (!empty($transfer->terms)) {
    $pdf->Ln(4);
    $pdf->SetFont($font_name,'B',$font_size);
    $pdf->Cell(0, 0, _l('terms_and_conditions'), 0, 1, 'L', 0, '', 0);
    $pdf->SetFont($font_name,'',$font_size);
    $pdf->Ln(2);
    $pdf->writeHTMLCell('', '', '', '', $transfer->terms, 0, 1, false, true, 'L', true);
}
