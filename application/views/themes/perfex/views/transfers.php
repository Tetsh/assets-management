    <div class="panel_s">
        <div class="panel-body">
            <h4 class="no-margin"><?php echo _l('organizations_my_transfers'); ?></h4>
        </div>
    </div>
    <div class="panel_s">
        <div class="panel-body">
            <?php get_template_part('transfers_stats'); ?>
            <hr />
            <?php get_template_part('transfers_table'); ?>
        </div>
    </div>
