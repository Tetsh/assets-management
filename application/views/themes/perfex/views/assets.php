<div class="panel_s">
   <div class="panel-body">
      <h4 class="no-margin"><?php echo _l('organizations_my_assets'); ?></h4>
   </div>
</div>
<div class="panel_s">
   <div class="panel-body">
      <div class="row mbot15">
         <div class="col-md-12">
            <h3 class="text-success no-mtop"><?php echo _l('assets_summary'); ?></h3>
         </div>
         <?php
            $where = array('organizationid'=>get_organization_user_id());
            foreach($asset_statuses as $status){ ?>
         <div class="col-md-2 border-right">
            <?php $where['status'] = $status['id']; ?>
            <h3 class="bold"><a href="<?php echo site_url('organizations/assets/'.$status['id']); ?>"><?php echo total_rows('tblassets',$where); ?></a></h3>
            <span style="color:<?php echo $status['color']; ?>">
            <?php echo $status['name']; ?>
         </div>
         <?php } ?>
      </div>
      <hr />
         <table class="table dt-table" data-order-col="2" data-order-type="desc">
            <thead>
               <tr>
                  <th><?php echo _l('asset_name'); ?></th>
                  <th><?php echo _l('asset_start_date'); ?></th>
                  <th><?php echo _l('asset_deadline'); ?></th>
                  <th><?php echo _l('asset_billing_type'); ?></th>
                  <?php
                     $custom_fields = get_custom_fields('assets',array('show_on_organization_portal'=>1));
                     foreach($custom_fields as $field){ ?>
                  <th><?php echo $field['name']; ?></th>
                  <?php } ?>
                  <th><?php echo _l('asset_status'); ?></th>
               </tr>
            </thead>
            <tbody>
               <?php foreach($assets as $asset){ ?>
               <tr>
                  <td><a href="<?php echo site_url('organizations/asset/'.$asset['id']); ?>"><?php echo $asset['name']; ?></a></td>
                  <td data-order="<?php echo $asset['start_date']; ?>"><?php echo _d($asset['start_date']); ?></td>
                  <td data-order="<?php echo $asset['deadline']; ?>"><?php echo _d($asset['deadline']); ?></td>
                  <td>
                     <?php
                        if($asset['billing_type'] == 1){
                          $type_name = 'asset_billing_type_fixed_cost';
                        } else if($asset['billing_type'] == 2){
                          $type_name = 'asset_billing_type_asset_hours';
                        } else {
                          $type_name = 'asset_billing_type_asset_task_hours';
                        }
                        echo _l($type_name);
                        ?>
                  </td>
                  <?php foreach($custom_fields as $field){ ?>
                  <td><?php echo get_custom_field_value($asset['id'],$field['id'],'assets'); ?></td>
                  <?php } ?>
                  <td>
                     <?php
                        $status = get_asset_status_by_id($asset['status']);
                        echo '<span class="label inline-block" style="color:'.$status['color'].';border:1px solid '.$status['color'].'">'.$status['name'].'</span>';
                        ?>
                  </td>
               </tr>
               <?php } ?>
            </tbody>
         </table>
   </div>
</div>
