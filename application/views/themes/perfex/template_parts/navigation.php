<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php get_company_logo('','navbar-brand'); ?>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php do_action('organizations_navigation_start'); ?>
        <?php if((get_option('use_knowledge_base') == 1 && !is_organization_logged_in() && get_option('knowledge_base_without_registration') == 1) || (get_option('use_knowledge_base') == 1 && is_organization_logged_in())){ ?>
        <li class="organizations-nav-item-kb"><a href="<?php echo site_url('knowledge-base'); ?>"><?php echo _l('organizations_nav_kb'); ?></a></li>
        <?php } ?>
        <?php if(!is_organization_logged_in() && get_option('allow_registration') == 1){ ?>
        <li class="organizations-nav-item-register"><a href="<?php echo site_url('organizations/register'); ?>"><?php echo _l('organizations_nav_register'); ?></a></li>
        <?php } ?>
        <?php if(!is_organization_logged_in()){ ?>
        <li class="organizations-nav-item-login"><a href="<?php echo site_url('organizations/login'); ?>"><?php echo _l('organizations_nav_login'); ?></a></li>
        <?php } else { ?>
        <?php if(has_contact_permission('assets')){ ?>
        <li class="organizations-nav-item-assets"><a href="<?php echo site_url('organizations/assets'); ?>"><?php echo _l('organizations_nav_assets'); ?></a></li>
        <?php } ?>
        <?php if(has_contact_permission('invoices')){ ?>
        <li class="organizations-nav-item-invoices"><a href="<?php echo site_url('organizations/invoices'); ?>"><?php echo _l('organizations_nav_invoices'); ?></a></li>
        <?php } ?>
        <?php if(has_contact_permission('contracts')){ ?>
        <li class="organizations-nav-item-contracts"><a href="<?php echo site_url('organizations/contracts'); ?>"><?php echo _l('organizations_nav_contracts'); ?></a></li>
        <?php } ?>
        <?php if(has_contact_permission('transfers')){ ?>
        <li class="organizations-nav-item-transfers"><a href="<?php echo site_url('organizations/transfers'); ?>"><?php echo _l('organizations_nav_transfers'); ?></a></li>
        <?php } ?>
        <?php if(has_contact_permission('proposals')){ ?>
        <li class="organizations-nav-item-proposals"><a href="<?php echo site_url('organizations/proposals'); ?>"><?php echo _l('organizations_nav_proposals'); ?></a></li>
        <?php } ?>
        <?php if(has_contact_permission('support')){ ?>
        <li class="organizations-nav-item-tickets"><a href="<?php echo site_url('organizations/tickets'); ?>"><?php echo _l('organizations_nav_support'); ?></a></li>
        <?php } ?>
        <?php do_action('organizations_navigation_end'); ?>
        <li class="dropdown organizations-nav-item-profile">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="<?php echo contact_profile_image_url($contact->id,'thumb'); ?>" class="organization-profile-image-small mright5">
            <?php echo $contact->firstname . ' ' .$contact->lastname; ?>
            <span class="caret"></span></a>
            <ul class="dropdown-menu animated fadeIn">
              <li class="organizations-nav-item-edit-profile"><a href="<?php echo site_url('organizations/profile'); ?>"><?php echo _l('organizations_nav_profile'); ?></a></li>
              <li class="organizations-nav-item-company-info"><a href="<?php echo site_url('organizations/company'); ?>"><?php echo _l('organization_company_info'); ?></a></li>
              <li class="organizations-nav-item-announcements">
                <a href="<?php echo site_url('organizations/announcements'); ?>"><?php echo _l('announcements'); ?>
                 <?php if($total_undismissed_announcements != 0){ ?>
                 <span class="badge"><?php echo $total_undismissed_announcements; ?></span>
                 <?php } ?>
               </a>
             </li>
             <?php if(is_primary_contact() && get_option('disable_language') == 0){
              ?>
              <li class="dropdown-submenu pull-left organizations-nav-item-languages">
               <a href="#" tabindex="-1"><?php echo _l('language'); ?></a>
               <ul class="dropdown-menu dropdown-menu-left">
                 <li class="<?php if($organization->default_language == ""){echo 'active';} ?>"><a href="<?php echo site_url('organizations/change_language'); ?>"><?php echo _l('system_default_string'); ?></a></li>
                 <?php foreach($this->app->get_available_languages() as $user_lang) { ?>
                 <li <?php if($organization->default_language == $user_lang){echo 'class="active"';} ?>>
                   <a href="<?php echo site_url('organizations/change_language/'.$user_lang); ?>"><?php echo ucfirst($user_lang); ?></a>
                 </li>
                 <?php } ?>
               </ul>
             </li>
             <?php } ?>
             <li class="organizations-nav-item-logout"><a href="<?php echo site_url('organizations/logout'); ?>"><?php echo _l('organizations_nav_logout'); ?></a></li>
           </ul>
         </li>
         <?php } ?>
         <?php do_action('organizations_navigation_after_profile'); ?>
       </ul>
     </div>
     <!-- /.navbar-collapse -->
   </div>
   <!-- /.container-fluid -->
 </nav>
