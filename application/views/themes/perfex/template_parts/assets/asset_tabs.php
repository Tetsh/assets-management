<ul class="nav nav-tabs no-margin" role="tablist">

    <li role="presentation" class="active asset_tab_overview">
        <a data-group="asset_overview" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_overview'); ?>" role="tab"><i class="fa fa-th" aria-hidden="true"></i> <?php echo _l('asset_overview'); ?></a>
    </li>

    <?php if($asset->settings->view_tasks == 1 && $asset->settings->available_features['asset_tasks'] == 1){ ?>
    <li role="presentation" class="asset_tab_tasks">
        <a data-group="asset_tasks" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_tasks'); ?>" role="tab"><i class="fa fa-check-circle" aria-hidden="true"></i> <?php echo _l('tasks'); ?></a>
    </li>
    <?php } ?>

    <?php if($asset->settings->view_timesheets == 1 && $asset->settings->available_features['asset_timesheets'] == 1){ ?>
    <li role="presentation" class="asset_tab_timesheets">
        <a data-group="asset_timesheets" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_timesheets'); ?>" role="tab"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo _l('asset_timesheets'); ?></a>
    </li>
    <?php } ?>

    <?php if($asset->settings->view_milestones == 1 && $asset->settings->available_features['asset_milestones'] == 1){ ?>
    <li role="presentation" class="asset_tab_milestones">
        <a data-group="asset_milestones" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_milestones'); ?>" role="tab"><i class="fa fa-rocket" aria-hidden="true"></i> <?php echo _l('asset_milestones'); ?></a>
    </li>
    <?php } ?>

    <?php if($asset->settings->available_features['asset_files'] == 1) { ?>
    <li role="presentation" class="asset_tab_files">
        <a data-group="asset_files" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_files'); ?>" role="tab"><i class="fa fa-files-o" aria-hidden="true"></i> <?php echo _l('asset_files'); ?></a>
    </li>
    <?php } ?>

    <?php if($asset->settings->available_features['asset_discussions'] == 1) { ?>
    <li role="presentation" class="asset_tab_discussions">
        <a data-group="asset_discussions" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_discussions'); ?>" role="tab"><i class="fa fa-commenting" aria-hidden="true"></i> <?php echo _l('asset_discussions'); ?></a>
    </li>
    <?php } ?>

    <?php if($asset->settings->view_gantt == 1 && $asset->settings->available_features['asset_gantt'] == 1){ ?>
    <li role="presentation" class="asset_tab_gantt">
        <a data-group="asset_gantt" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_gantt'); ?>" role="tab"><i class="fa fa-line-chart" aria-hidden="true"></i> <?php echo _l('asset_gant'); ?></a>
    </li>
    <?php } ?>

    <?php if(has_contact_permission('support') && $asset->settings->available_features['asset_tickets'] == 1){ ?>
    <li role="presentation" class="asset_tab_tickets">
        <a data-group="asset_tickets" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_tickets'); ?>" role="tab"><i class="fa fa-life-ring" aria-hidden="true"></i> <?php echo _l('asset_tickets'); ?></a>
    </li>
    <?php } ?>

    <?php if(has_contact_permission('transfers') && $asset->settings->available_features['asset_transfers'] == 1){ ?>
    <li role="presentation" class="asset_tab_transfers">
        <a data-group="asset_transfers" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_transfers'); ?>" role="tab"><i class="fa fa-sun-o" aria-hidden="true"></i> <?php echo _l('transfers'); ?></a>
    </li>
    <?php } ?>

    <?php if(has_contact_permission('invoices') && $asset->settings->available_features['asset_invoices'] == 1){ ?>
    <li role="presentation" class="asset_tab_invoices">
        <a data-group="asset_invoices" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_invoices'); ?>" role="tab"><i class="fa fa-sun-o" aria-hidden="true"></i> <?php echo _l('asset_invoices'); ?></a>
    </li>
    <?php } ?>

    <?php if($asset->settings->view_activity_log == 1 && $asset->settings->available_features['asset_activity'] == 1){ ?>
    <li role="presentation" class="asset_tab_activity">
        <a data-group="asset_activity" href="<?php echo site_url('organizations/asset/'.$asset->id.'?group=asset_activity'); ?>" role="tab"><i class="fa fa-exclamation" aria-hidden="true"></i> <?php echo _l('asset_activity'); ?></a>
    </li>
    <?php } ?>

</ul>
