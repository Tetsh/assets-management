    <table class="table dt-table" data-order-col="1" data-order-type="desc">
       <thead>
        <tr>
            <th><?php echo _l('organizations_transfer_dt_number'); ?></th>
            <th><?php echo _l('organizations_transfer_dt_date'); ?></th>
            <th><?php echo _l('organizations_transfer_dt_duedate'); ?></th>
            <th><?php echo _l('organizations_transfer_dt_amount'); ?></th>
            <th><?php echo _l('reference_no'); ?></th>
            <th><?php echo _l('organizations_transfer_dt_status'); ?></th>
            <?php
            $custom_fields = get_custom_fields('transfer',array('show_on_organization_portal'=>1));
            foreach($custom_fields as $field){ ?>
            <th><?php echo $field['name']; ?></th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach($transfers as $transfer){ ?>
        <tr>
            <td data-order="<?php echo $transfer['number']; ?>"><a href="<?php echo site_url('viewtransfer/' . $transfer['id'] . '/' . $transfer['hash']); ?>" class="transfer-number"><?php echo format_transfer_number($transfer['id']); ?></a></td>
            <td data-order="<?php echo $transfer['date']; ?>"><?php echo _d($transfer['date']); ?></td>
            <td data-order="<?php echo $transfer['expirydate']; ?>"><?php echo _d($transfer['expirydate']); ?></td>
            <td data-order="<?php echo $transfer['total']; ?>"><?php echo format_money($transfer['total'], $transfer['symbol']);; ?></td>
            <td><?php echo $transfer['reference_no']; ?></td>
            <td><?php echo format_transfer_status($transfer['status'], 'inline-block', true); ?></td>
            <?php foreach($custom_fields as $field){ ?>
            <td><?php echo get_custom_field_value($transfer['id'],$field['id'],'transfer'); ?></td>
            <?php } ?>
        </tr>
        <?php } ?>
    </tbody>
</table>
