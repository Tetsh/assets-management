<?php

/**
 * Default asset tabs
 * @param  mixed $asset_id asset id to format the url
 * @return array
 */
function get_asset_tabs_admin($asset_id)
{
    $asset_tabs = array(
    array(
        'name'=>'asset_overview',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_overview'),
        'icon'=>'fa fa-th',
        'lang'=>_l('asset_overview'),
        'visible'=>true,
        'order'=>1,
        ),
    array(
        'name'=>'asset_tasks',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_tasks'),
        'icon'=>'fa fa-check-circle',
        'lang'=>_l('tasks'),
        'visible'=>true,
        'order'=>2,
        'linked_to_organization_option'=>array('view_tasks'),
        ),
    array(
        'name'=>'asset_timesheets',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_timesheets'),
        'icon'=>'fa fa-clock-o',
        'lang'=>_l('asset_timesheets'),
        'visible'=>true,
        'order'=>3,
        'linked_to_organization_option'=>array('view_timesheets'),
        ),
    array(
        'name'=>'asset_milestones',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_milestones'),
        'icon'=>'fa fa-rocket',
        'lang'=>_l('asset_milestones'),
        'visible'=>true,
        'order'=>4,
        'linked_to_organization_option'=>array('view_milestones'),
        ),
    array(
        'name'=>'asset_files',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_files'),
        'icon'=>'fa fa-files-o',
        'lang'=>_l('asset_files'),
        'visible'=>true,
        'order'=>5,
        'linked_to_organization_option'=>array('upload_files'),
        ),
    array(
        'name'=>'asset_discussions',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_discussions'),
        'icon'=>'fa fa-commenting',
        'lang'=>_l('asset_discussions'),
        'visible'=>true,
        'order'=>6,
        'linked_to_organization_option'=>array('open_discussions'),
        ),
    array(
        'name'=>'asset_gantt',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_gantt'),
        'icon'=>'fa fa-line-chart',
        'lang'=>_l('asset_gant'),
        'visible'=>true,
        'order'=>7,
        'linked_to_organization_option'=>array('view_gantt'),
        ),
    array(
        'name'=>'asset_tickets',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_tickets'),
        'icon'=>'fa fa-life-ring',
        'lang'=>_l('asset_tickets'),
        'visible'=>(get_option('access_tickets_to_none_staff_members') == 1 && !is_staff_member()) || is_staff_member(),
        'order'=>8,
        ),
    array(
        'name'=>"sales",
        'url'=>'#',
        'icon'=>'',
        'lang'=>_l('sales_string'),
        'visible'=>(has_permission('transfers', '', 'view') || has_permission('transfers', '', 'view_own')) || (has_permission('invoices', '', 'view') || has_permission('invoices', '', 'view_own')) || (has_permission('expenses', '', 'view') || has_permission('expenses', '', 'view_own')),
        'order'=>9,
        'dropdown'=>array(
          array(
            'name'=>'asset_invoices',
            'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_invoices'),
            'icon'=>'fa fa-sun-o',
            'lang'=>_l('asset_invoices'),
            'visible'=>has_permission('invoices', '', 'view') || has_permission('invoices', '', 'view_own'),
            'order'=>1,
            ),
          array(
            'name'=>'asset_transfers',
            'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_transfers'),
            'icon'=>'fa fa-sun-o',
            'lang'=>_l('transfers'),
            'visible'=>has_permission('transfers', '', 'view') || has_permission('transfers', '', 'view_own'),
            'order'=>2,
            ),
          array(
            'name'=>'asset_expenses',
            'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_expenses'),
            'icon'=>'fa fa-sort-amount-asc',
            'lang'=>_l('asset_expenses'),
            'visible'=>has_permission('expenses', '', 'view') || has_permission('expenses', '', 'view_own'),
            'order'=>3,
            ),
          array(
            'name'=>'asset_credit_notes',
            'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_credit_notes'),
            'icon'=>'fa fa-sort-amount-asc',
            'lang'=>_l('credit_notes'),
            'visible'=>has_permission('credit_notes', '', 'view') || has_permission('credit_notes', '', 'view_own'),
            'order'=>3,
            ),
          ),
        ),
    array(
        'name'=>'asset_notes',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_notes'),
        'icon'=>'fa fa-clock-o',
        'lang'=>_l('asset_notes'),
        'visible'=>true,
        'order'=>10,
        ),
    array(
        'name'=>'asset_activity',
        'url'=>admin_url('assets/view/'.$asset_id.'?group=asset_activity'),
        'icon'=>'fa fa-exclamation',
        'lang'=>_l('asset_activity'),
        'visible'=>has_permission('assets', '', 'create'),
        'order'=>11,
        'linked_to_organization_option'=>array('view_activity_log'),
        ),
    );

    $asset_tabs = do_action('asset_tabs_admin', $asset_tabs);

    usort($asset_tabs, function ($a, $b) {
        return $a['order'] - $b['order'];
    });

    return $asset_tabs;
}

/**
 * Get asset status by passed asset id
 * @param  mixed $id asset id
 * @return array
 */
function get_asset_status_by_id($id)
{
    $CI = &get_instance();
    if (!class_exists('assets_model')) {
        $CI->load->model('assets_model');
    }

    $statuses = $CI->assets_model->get_asset_statuses();

    $status = array(
      'id'=>0,
      'bg_color'=>'#333',
      'text_color'=>'#333',
      'name'=>'[Status Not Found]',
      'order'=>1,
      );

    foreach ($statuses as $s) {
        if ($s['id'] == $id) {
            $status = $s;
            break;
        }
    }

    return $status;
}

/**
 * Get asset class by passed asset id
 * @param  mixed $id asset id
 * @return array
 */
function get_asset_class_by_id($id)
{
    $CI = &get_instance();
    if (!class_exists('assets_model')) {
        $CI->load->model('assets_model');
    }

    $asset_classes = $CI->assets_model->get_asset_class();

    $asset_class = array(
      'id'=>0,
      'bg_color'=>'#333',
      'text_color'=>'#333',
      'name'=>'[Class Not Found]',
      'order'=>1,
      );

    foreach ($asset_classes as $s) {
        if ($s['id'] == $id) {
            $asset_class = $s;
            break;
        }
    }

    return $asset_class;
}


/**
 * Get asset category by passed asset id
 * @param  mixed $id asset id
 * @return array
 */
function get_asset_category_by_id($id)
{
    $CI = &get_instance();
    if (!class_exists('assets_model')) {
        $CI->load->model('assets_model');
    }

    $asset_categories = $CI->assets_model->get_asset_category();

    $asset_category = array(
      'id'=>0,
      'bg_color'=>'#333',
      'text_color'=>'#333',
      'name'=>'[Category Not Found]',
      'order'=>1,
      );

    foreach ($asset_categories as $s) {
        if ($s['id'] == $id) {
            $asset_category = $s;
            break;
        }
    }

    return $asset_category;
}

/**
 * Get asset department by passed asset id
 * @param  mixed $id asset id
 * @return array
 */
function get_asset_department_by_id($id)
{
    $CI = &get_instance();
    if (!class_exists('departments_model')) {
        $CI->load->model('departments_model');
    }

    $asset_departments = $CI->departments_model->get();

    $asset_department = array(
      'id'=>0,
      'bg_color'=>'#333',
      'text_color'=>'#333',
      'name'=>'[Department Not Found]',
      'order'=>1,
      );

    foreach ($asset_departments as $s) {
        if ($s['departmentid'] == $id) {
            $asset_department = $s;
            break;
        }
    }

    return $asset_department;
}
/**
 * Return logged in user pinned assets
 * @return array
 */
function get_user_pinned_assets()
{
    $CI = &get_instance();
    $CI->db->select('tblassets.id, tblassets.name, tblassets.organizationid, ' . get_sql_select_organization_company());
    $CI->db->join('tblassets', 'tblassets.id=tblpinnedassets.asset_id');
    $CI->db->join('tblorganizations', 'tblorganizations.userid=tblassets.organizationid');
    $CI->db->where('tblpinnedassets.staff_id', get_staff_user_id());
    $assets = $CI->db->get('tblpinnedassets')->result_array();
    $CI->load->model('assets_model');
    $i        = 0;
    foreach ($assets as $asset) {
        $assets[$i]['progress'] = $CI->assets_model->calc_progress($asset['id']);
        $i++;
    }

    return $assets;
}


/**
 * Get asset name by passed id
 * @param  mixed $id
 * @return string
 */
function get_asset_name_by_id($id)
{
    $CI =& get_instance();
    $asset = $CI->object_cache->get('asset-name-data-'.$id);

    if(!$asset){
        $CI->db->select('name');
        $CI->db->where('id', $id);
        $asset = $CI->db->get('tblassets')->row();
        $CI->object_cache->add('asset-name-data-'.$id,$asset);
    }

    if ($asset) {
        return $asset->name;
    }

    return '';
}

/**
 * Return asset milestones
 * @param  mixed $asset_id asset id
 * @return array
 */
function get_asset_milestones($asset_id) {
    $CI = &get_instance();
    $CI->db->where('asset_id', $asset_id);
    $CI->db->order_by('milestone_order', 'ASC');
    return $CI->db->get('tblmilestones')->result_array();
}

/**
 * Get asset organization id by passed asset id
 * @param  mixed $id asset id
 * @return mixed
 */
function get_organization_id_by_asset_id($id)
{
    $CI =& get_instance();
    $CI->db->select('organizationid');
    $CI->db->where('id', $id);
    $asset = $CI->db->get('tblassets')->row();
    if ($asset) {
        return $asset->organizationid;
    }

    return false;
}

/**
 * Check if organization has asset assigned
 * @param  mixed $organization_id organization id to check
 * @return boolean
 */
function organization_has_assets($organization_id)
{
    $totalCustomerProjects = total_rows('tblassets', 'organizationid='.$organization_id);

    return ($totalCustomerProjects > 0 ? true : false);
}


/**
 * Translated jquery-comment language based on app languages
 * This feature is used on both admin and organization area
 * @return array
 */
function get_asset_discussions_language_array()
{
    $lang = array(
        'discussion_add_comment' => _l('discussion_add_comment'),
        'discussion_newest' => _l('discussion_newest'),
        'discussion_oldest' => _l('discussion_oldest'),
        'discussion_attachments' => _l('discussion_attachments'),
        'discussion_send' => _l('discussion_send'),
        'discussion_reply' => _l('discussion_reply'),
        'discussion_edit' => _l('discussion_edit'),
        'discussion_edited' => _l('discussion_edited'),
        'discussion_you' => _l('discussion_you'),
        'discussion_save' => _l('discussion_save'),
        'discussion_delete' => _l('discussion_delete'),
        'discussion_view_all_replies' => _l('discussion_view_all_replies'),
        'discussion_hide_replies' => _l('discussion_hide_replies'),
        'discussion_no_comments' => _l('discussion_no_comments'),
        'discussion_no_attachments' => _l('discussion_no_attachments'),
        'discussion_attachments_drop' => _l('discussion_attachments_drop'),
    );

    return $lang;
}
