<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Organizations extends Admin_controller
{
    private $not_importable_organizations_fields;
    public $pdf_zip;

    public function __construct()
    {
        parent::__construct();
        $this->not_importable_organizations_fields = do_action('not_importable_organizations_fields',array('userid', 'id', 'is_primary', 'password', 'datecreated', 'last_ip', 'last_login', 'last_password_change', 'active', 'new_pass_key', 'new_pass_key_requested', 'leadid', 'default_currency', 'profile_image', 'default_language', 'direction', 'show_primary_contact', 'invoice_emails', 'transfer_emails', 'asset_emails', 'task_emails', 'contract_emails', 'credit_note_emails','addedfrom','last_active_time'));
        // last_active_time is from Chattr plugin, causing issue
    }

    /* List all organizations */
    public function index()
    {
        if (!has_permission('organizations', '', 'view')) {
            if (!have_assigned_organizations() && !has_permission('organizations', '', 'create')) {
                access_denied('organizations');
            }
        }

        $this->load->model('contracts_model');
        $data['contract_types'] = $this->contracts_model->get_contract_types();
        $data['groups']         = $this->organizations_model->get_groups();
        $data['title']          = _l('organizations');

        $this->load->model('proposals_model');
        $data['proposal_statuses'] = $this->proposals_model->get_statuses();

        $this->load->model('invoices_model');
        $data['invoice_statuses'] = $this->invoices_model->get_statuses();

        $this->load->model('transfers_model');
        $data['transfer_statuses'] = $this->transfers_model->get_statuses();

        $this->load->model('assets_model');
        $data['asset_statuses'] = $this->assets_model->get_asset_statuses();

        $data['organization_admins'] = $this->organizations_model->get_organizations_admin_unique_ids();

        $whereContactsLoggedIn = '';
        if (!has_permission('organizations', '', 'view')) {
            $whereContactsLoggedIn = ' AND userid IN (SELECT organization_id FROM tblorganizationadmins WHERE staff_id='.get_staff_user_id().')';
        }

        $data['contacts_logged_in_today'] = $this->organizations_model->get_contacts('', 'last_login LIKE "'.date('Y-m-d').'%"'.$whereContactsLoggedIn);

        $data['countries'] = $this->organizations_model->get_organizations_distinct_countries();

        $this->load->view('admin/organizations/manage', $data);
    }

    public function table()
    {
        if (!has_permission('organizations', '', 'view')) {
            if (!have_assigned_organizations() && !has_permission('organizations', '', 'create')) {
                ajax_access_denied();
            }
        }

        $this->app->get_table_data('organizations');
    }

    public function all_contacts()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('all_contacts');
        }
        $data['title'] = _l('organization_contacts');
        $this->load->view('admin/organizations/all_contacts', $data);
    }

    /* Edit organization or add new organization*/
    public function organization($id = '')
    {
        if (!has_permission('organizations', '', 'view')) {
            if ($id != '' && !is_organization_admin($id)) {
                access_denied('organizations');
            }
        }

        if ($this->input->post() && !$this->input->is_ajax_request()) {
            if ($id == '') {
                if (!has_permission('organizations', '', 'create')) {
                    access_denied('organizations');
                }

                $data                 = $this->input->post();

                $save_and_add_contact = false;
                if (isset($data['save_and_add_contact'])) {
                    unset($data['save_and_add_contact']);
                    $save_and_add_contact = true;
                }
                $id = $this->organizations_model->add($data);
                if (!has_permission('organizations', '', 'view')) {
                    $assign['organization_admins']   = array();
                    $assign['organization_admins'][] = get_staff_user_id();
                    $this->organizations_model->assign_admins($assign, $id);
                }
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('organization')));
                    if ($save_and_add_contact == false) {
                        redirect(admin_url('organizations/organization/' . $id));
                    } else {
                        redirect(admin_url('organizations/organization/' . $id . '?new_contact=true&tab=contacts'));
                    }
                }
            } else {
                if (!has_permission('organizations', '', 'edit')) {
                    if (!is_organization_admin($id)) {
                        access_denied('organizations');
                    }
                }
                $success = $this->organizations_model->update($this->input->post(), $id);
                if ($success == true) {
                    set_alert('success', _l('updated_successfully', _l('organization')));
                }
                redirect(admin_url('organizations/organization/' . $id));
            }
        }

        if (!$this->input->get('group')) {
            $group = 'profile';
        } else {
            $group = $this->input->get('group');
        }
        // View group
        $data['group']  = $group;
        // Customer groups
        $data['groups'] = $this->organizations_model->get_groups();

        if ($id == '') {
            $title = _l('add_new', _l('organization_lowercase'));
        } else {
            $organization = $this->organizations_model->get($id);
            if (!$organization) {
                blank_page('Organization Not Found');
            }

            $data['contacts']         = $this->organizations_model->get_contacts($id);

            // Fetch data based on groups
            if ($group == 'profile') {
                $data['organization_groups'] = $this->organizations_model->get_organization_groups($id);
                $data['organization_admins'] = $this->organizations_model->get_admins($id);
            } elseif ($group == 'attachments') {
                $data['attachments']   = get_all_organization_attachments($id);
            } elseif ($group == 'vault') {
                $data['vault_entries'] = do_action('check_vault_entries_visibility', $this->organizations_model->get_vault_entries($id));
                if ($data['vault_entries'] === -1) {
                    $data['vault_entries'] = array();
                }
            } elseif ($group == 'transfers') {
                $this->load->model('transfers_model');
                $data['transfer_statuses'] = $this->transfers_model->get_statuses();
            } elseif ($group == 'invoices') {
                $this->load->model('invoices_model');
                $data['invoice_statuses'] = $this->invoices_model->get_statuses();
            } elseif ($group == 'credit_notes') {
                $this->load->model('credit_notes_model');
                $data['credit_notes_statuses'] = $this->credit_notes_model->get_statuses();
                $data['credits_available'] = $this->credit_notes_model->total_remaining_credits_by_organization($id);
            } elseif ($group == 'payments') {
                $this->load->model('payment_modes_model');
                $data['payment_modes'] = $this->payment_modes_model->get();
            } elseif ($group == 'notes') {
                $data['user_notes'] = $this->misc_model->get_notes($id, 'organization');
            } elseif ($group == 'assets') {
                $this->load->model('assets_model');
                $data['asset_statuses'] = $this->assets_model->get_asset_statuses();
            } elseif ($group == 'statement') {
                if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
                    set_alert('danger', _l('access_denied'));
                    redirect(admin_url('organizations/organization/'.$id));
                }
                $contact = $this->organizations_model->get_contact(get_primary_contact_user_id($id));
                $email   = '';
                if ($contact) {
                    $email = $contact->email;
                }

                $template_name = 'organization-statement';
                $data['template'] = get_email_template_for_sending($template_name, $email);

                $data['template_name']     = $template_name;
                $this->db->where('slug', $template_name);
                $this->db->where('language', 'english');
                $template_result = $this->db->get('tblemailtemplates')->row();

                $data['template_system_name'] = $template_result->name;
                $data['template_id'] = $template_result->emailtemplateid;

                $data['template_disabled'] = false;
                if (total_rows('tblemailtemplates', array('slug'=>$data['template_name'], 'active'=>0)) > 0) {
                    $data['template_disabled'] = true;
                }
            }

            $data['staff']           = $this->staff_model->get('', 1);

            $data['organization']        = $organization;
            $title                 = $organization->company;

            // Get all active staff members (used to add reminder)
            $data['members'] = $data['staff'];

            if (!empty($data['organization']->company)) {
                // Check if is realy empty organization company so we can set this field to empty
                // The query where fetch the organization auto populate firstname and lastname if company is empty
                if (is_empty_organization_company($data['organization']->userid)) {
                    $data['organization']->company = '';
                }
            }
        }

        $this->load->model('currencies_model');
        $data['currencies'] = $this->currencies_model->get();

        if ($id != '') {
            $organization_currency = $data['organization']->default_currency;

            foreach ($data['currencies'] as $currency) {
                if ($organization_currency != 0) {
                    if ($currency['id'] == $organization_currency) {
                        $organization_currency = $currency;
                        break;
                    }
                } else {
                    if ($currency['isdefault'] == 1) {
                        $organization_currency = $currency;
                        break;
                    }
                }
            }

            if (is_array($organization_currency)) {
                $organization_currency = (object) $organization_currency;
            }

            $data['organization_currency'] = $organization_currency;
        }

        $data['bodyclass'] = 'organization-profile dynamic-create-groups';
        $data['title'] = $title;

        $this->load->view('admin/organizations/organization', $data);
    }

    public function save_longitude_and_latitude($organization_id)
    {
        if (!has_permission('organizations', '', 'edit')) {
            if (!is_organization_admin($organization_id)) {
                ajax_access_denied();
            }
        }

        $this->db->where('userid', $organization_id);
        $this->db->update('tblorganizations', array(
            'longitude'=>$this->input->post('longitude'),
            'latitude'=>$this->input->post('latitude'),
        ));
        if ($this->db->affected_rows() > 0) {
            echo 'success';
        } else {
            echo 'false';
        }
    }

    public function contact($organization_id, $contact_id = '')
    {
        if (!has_permission('organizations', '', 'view')) {
            if (!is_organization_admin($organization_id)) {
                echo _l('access_denied');
                die;
            }
        }
        $data['organization_id'] = $organization_id;
        $data['contactid']   = $contact_id;
        if ($this->input->post()) {
            $data = $this->input->post();
            $data['password'] = $this->input->post('password',false);

            unset($data['contactid']);
            if ($contact_id == '') {
                if (!has_permission('organizations', '', 'create')) {
                    if (!is_organization_admin($organization_id)) {
                        header('HTTP/1.0 400 Bad error');
                        echo json_encode(array(
                            'success' => false,
                            'message' => _l('access_denied'),
                        ));
                        die;
                    }
                }
                $id      = $this->organizations_model->add_contact($data, $organization_id);
                $message = '';
                $success = false;
                if ($id) {
                    handle_contact_profile_image_upload($id);
                    $success = true;
                    $message = _l('added_successfully', _l('contact'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message,
                    'has_primary_contact'=>(total_rows('tblcontacts', array('userid'=>$organization_id, 'is_primary'=>1)) > 0 ? true : false),
                    'is_individual'=>is_empty_organization_company($organization_id) && total_rows('tblcontacts',array('userid'=>$organization_id)) == 1,
                ));
                die;
            } else {
                if (!has_permission('organizations', '', 'edit')) {
                    if (!is_organization_admin($organization_id)) {
                        header('HTTP/1.0 400 Bad error');
                        echo json_encode(array(
                            'success' => false,
                            'message' => _l('access_denied'),
                        ));
                        die;
                    }
                }
                $original_contact = $this->organizations_model->get_contact($contact_id);
                $success          = $this->organizations_model->update_contact($data, $contact_id);
                $message          = '';
                $proposal_warning = false;
                $original_email   = '';
                $updated          = false;
                if (is_array($success)) {
                    if (isset($success['set_password_email_sent'])) {
                        $message = _l('set_password_email_sent_to_organization');
                    } elseif (isset($success['set_password_email_sent_and_profile_updated'])) {
                        $updated = true;
                        $message = _l('set_password_email_sent_to_organization_and_profile_updated');
                    }
                } else {
                    if ($success == true) {
                        $updated = true;
                        $message = _l('updated_successfully', _l('contact'));
                    }
                }
                if (handle_contact_profile_image_upload($contact_id) && !$updated) {
                    $message = _l('updated_successfully', _l('contact'));
                    $success = true;
                }
                if ($updated == true) {
                    $contact = $this->organizations_model->get_contact($contact_id);
                    if (total_rows('tblproposals', array(
                        'rel_type' => 'organization',
                        'rel_id' => $contact->userid,
                        'email' => $original_contact->email,
                    )) > 0 && ($original_contact->email != $contact->email)) {
                        $proposal_warning = true;
                        $original_email   = $original_contact->email;
                    }
                }
                echo json_encode(array(
                    'success' => $success,
                    'proposal_warning' => $proposal_warning,
                    'message' => $message,
                    'original_email' => $original_email,
                    'has_primary_contact'=>(total_rows('tblcontacts', array('userid'=>$organization_id, 'is_primary'=>1)) > 0 ? true : false),
                ));
                die;
            }
        }
        if ($contact_id == '') {
            $title = _l('add_new', _l('contact_lowercase'));
        } else {
            $data['contact'] = $this->organizations_model->get_contact($contact_id);

            if (!$data['contact']) {
                header('HTTP/1.0 400 Bad error');
                echo json_encode(array(
                    'success' => false,
                    'message' => 'Contact Not Found',
                ));
                die;
            }
            $title = $data['contact']->firstname . ' ' . $data['contact']->lastname;
        }

        // Customer groups
        $data['positions'] = $this->organizations_model->get_positions();

        $data['organization_permissions'] = get_contact_permissions();
        $data['contact_positions'] = $this->organizations_model->get_contact_positions($contact_id);

        $data['title']                = $title;
        $this->load->view('admin/organizations/modals/contact', $data);
    }

    public function update_file_share_visibility()
    {
        if ($this->input->post()) {
            $file_id           = $this->input->post('file_id');
            $share_contacts_id = array();

            if ($this->input->post('share_contacts_id')) {
                $share_contacts_id = $this->input->post('share_contacts_id');
            }

            $this->db->where('file_id', $file_id);
            $this->db->delete('tblorganizationfiles_shares');

            foreach ($share_contacts_id as $share_contact_id) {
                $this->db->insert('tblorganizationfiles_shares', array(
                    'file_id' => $file_id,
                    'contact_id' => $share_contact_id,
                ));
            }
        }
    }

    public function delete_contact_profile_image($contact_id)
    {
        do_action('before_remove_contact_profile_image');
        if (file_exists(get_upload_path_by_type('contact_profile_images') . $contact_id)) {
            delete_dir(get_upload_path_by_type('contact_profile_images') . $contact_id);
        }
        $this->db->where('id', $contact_id);
        $this->db->update('tblcontacts', array(
            'profile_image' => null,
        ));
    }

    public function mark_as_active($id)
    {
        $this->db->where('userid', $id);
        $this->db->update('tblorganizations', array(
            'active' => 1,
        ));
        redirect(admin_url('organizations/organization/' . $id));
    }

    public function update_all_proposal_emails_linked_to_organization($contact_id)
    {
        $success = false;
        $email   = '';
        if ($this->input->post('update')) {
            $this->load->model('proposals_model');

            $this->db->select('email,userid');
            $this->db->where('id', $contact_id);
            $contact = $this->db->get('tblcontacts')->row();

            $proposals     = $this->proposals_model->get('', array(
                'rel_type' => 'organization',
                'rel_id' => $contact->userid,
                'email' => $this->input->post('original_email'),
            ));
            $affected_rows = 0;

            foreach ($proposals as $proposal) {
                $this->db->where('id', $proposal['id']);
                $this->db->update('tblproposals', array(
                    'email' => $contact->email,
                ));
                if ($this->db->affected_rows() > 0) {
                    $affected_rows++;
                }
            }

            if ($affected_rows > 0) {
                $success = true;
            }
        }
        echo json_encode(array(
            'success' => $success,
            'message' => _l('proposals_emails_updated', array(
                _l('contact_lowercase'),
                $contact->email,
            )),
        ));
    }

    public function assign_admins($id)
    {
        if (!has_permission('organizations', '', 'create') && !has_permission('organizations', '', 'edit')) {
            access_denied('organizations');
        }
        $success = $this->organizations_model->assign_admins($this->input->post(), $id);
        if ($success == true) {
            set_alert('success', _l('updated_successfully', _l('organization')));
        }

        redirect(admin_url('organizations/organization/' . $id . '?tab=organization_admins'));
    }

    public function delete_organization_admin($organization_id, $staff_id)
    {
        if (!has_permission('organizations', '', 'create') && !has_permission('organizations', '', 'edit')) {
            access_denied('organizations');
        }

        $this->db->where('organization_id', $organization_id);
        $this->db->where('staff_id', $staff_id);
        $this->db->delete('tblorganizationadmins');
        redirect(admin_url('organizations/organization/'.$organization_id).'?tab=organization_admins');
    }

    public function delete_contact($organization_id, $id)
    {
        if (!has_permission('organizations', '', 'delete')) {
            if (!is_organization_admin($organization_id)) {
                access_denied('organizations');
            }
        }

        $this->organizations_model->delete_contact($id);
        redirect(admin_url('organizations/organization/' . $organization_id . '?tab=contacts'));
    }

    public function contacts($organization_id)
    {
        $this->app->get_table_data('contacts', array(
            'organization_id' => $organization_id,
        ));
    }

    public function upload_attachment($id)
    {
        handle_organization_attachments_upload($id);
    }

    public function add_external_attachment()
    {
        if ($this->input->post()) {
            $this->misc_model->add_attachment_to_database($this->input->post('organizationid'), 'organization', $this->input->post('files'), $this->input->post('external'));
        }
    }

    public function delete_attachment($organization_id, $id)
    {
        if (has_permission('organizations', '', 'delete') || is_organization_admin($organization_id)) {
            $this->organizations_model->delete_attachment($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* Delete organization */
    public function delete($id)
    {
        if (!has_permission('organizations', '', 'delete')) {
            access_denied('organizations');
        }
        if (!$id) {
            redirect(admin_url('organizations'));
        }
        $response = $this->organizations_model->delete($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('organization_delete_transactions_warning',_l('invoices').', '._l('transfers').', '._l('credit_notes')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('organization')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('organization_lowercase')));
        }
        redirect(admin_url('organizations'));
    }

    /* Staff can login as organization */
    public function login_as_organization($id)
    {
        if (is_admin()) {
            login_as_organization($id);
        }
        do_action('after_contact_login');
        redirect(site_url());
    }

    public function get_organization_billing_and_shipping_details($id)
    {
        echo json_encode($this->organizations_model->get_organization_billing_and_shipping_details($id));
    }

    /* Change organization status / active / inactive */
    public function change_contact_status($id, $status)
    {
        if (has_permission('organizations', '', 'edit') || is_organization_admin(get_user_id_by_contact_id($id))) {
            if ($this->input->is_ajax_request()) {
                $this->organizations_model->change_contact_status($id, $status);
            }
        }
    }

    /* Change organization status / active / inactive */
    public function change_organization_status($id, $status)
    {
        if ($this->input->is_ajax_request()) {
            $this->organizations_model->change_organization_status($id, $status);
        }
    }

    /* Zip function for credit notes */
    public function zip_credit_notes($id)
    {
        $has_permission_view = has_permission('credit_notes', '', 'view');

        if (!$has_permission_view && !has_permission('credit_notes', '', 'view_own')) {
            access_denied('Zip Customer Credit Notes');
        }

        if ($this->input->post()) {
            $status        = $this->input->post('credit_note_zip_status');
            $zip_file_name = $this->input->post('file_name');
            if ($this->input->post('zip-to') && $this->input->post('zip-from')) {
                $from_date = to_sql_date($this->input->post('zip-from'));
                $to_date   = to_sql_date($this->input->post('zip-to'));
                if ($from_date == $to_date) {
                    $this->db->where('date', $from_date);
                } else {
                    $this->db->where('date BETWEEN "' . $from_date . '" AND "' . $to_date . '"');
                }
            }
            $this->db->select('id');
            $this->db->from('tblcreditnotes');
            if ($status != 'all') {
                $this->db->where('status', $status);
            }
            $this->db->where('organizationid', $id);
            $this->db->order_by('number', 'desc');

            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }
            $credit_notes = $this->db->get()->result_array();

            $this->load->model('credit_notes_model');

            $this->load->helper('file');
            if (!is_really_writable(TEMP_FOLDER)) {
                show_error('/temp folder is not writable. You need to change the permissions to 755');
            }

            $dir = TEMP_FOLDER . $zip_file_name;

            if (is_dir($dir)) {
                delete_dir($dir);
            }

            if (count($credit_notes) == 0) {
                set_alert('warning', _l('organization_zip_no_data_found', _l('credit_notes')));
                redirect(admin_url('organizations/organization/' . $id . '?group=credit_notes'));
            }

            mkdir($dir, 0777);

            foreach ($credit_notes as $credit_note) {
                $credit_note    = $this->credit_notes_model->get($credit_note['id']);
                $this->pdf_zip   = credit_note_pdf($credit_note);
                $_temp_file_name = slug_it(format_credit_note_number($credit_note->id));
                $file_name       = $dir . '/' . strtoupper($_temp_file_name);
                $this->pdf_zip->Output($file_name . '.pdf', 'F');
            }

            $this->load->library('zip');
            // Read the credit notes
            $this->zip->read_dir($dir, false);
            // Delete the temp directory for the organization
            delete_dir($dir);
            $this->zip->download(slug_it(get_option('companyname')) . '-credit-notes-' . $zip_file_name . '.zip');
            $this->zip->clear_data();
        }
    }

    public function zip_invoices($id)
    {
        $has_permission_view = has_permission('invoices', '', 'view');
        if (!$has_permission_view && !has_permission('invoices', '', 'view_own')) {
            access_denied('Zip Customer Invoices');
        }
        if ($this->input->post()) {
            $status        = $this->input->post('invoice_zip_status');
            $zip_file_name = $this->input->post('file_name');
            if ($this->input->post('zip-to') && $this->input->post('zip-from')) {
                $from_date = to_sql_date($this->input->post('zip-from'));
                $to_date   = to_sql_date($this->input->post('zip-to'));
                if ($from_date == $to_date) {
                    $this->db->where('date', $from_date);
                } else {
                    $this->db->where('date BETWEEN "' . $from_date . '" AND "' . $to_date . '"');
                }
            }
            $this->db->select('id');
            $this->db->from('tblinvoices');
            if ($status != 'all') {
                $this->db->where('status', $status);
            }
            $this->db->where('organizationid', $id);
            $this->db->order_by('number,YEAR(date)', 'desc');

            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }

            $invoices = $this->db->get()->result_array();
            $this->load->model('invoices_model');
            $this->load->helper('file');
            if (!is_really_writable(TEMP_FOLDER)) {
                show_error('/temp folder is not writable. You need to change the permissions to 755');
            }
            $dir = TEMP_FOLDER . $zip_file_name;
            if (is_dir($dir)) {
                delete_dir($dir);
            }
            if (count($invoices) == 0) {
                set_alert('warning', _l('organization_zip_no_data_found', _l('invoices')));
                redirect(admin_url('organizations/organization/' . $id . '?group=invoices'));
            }
            mkdir($dir, 0777);
            foreach ($invoices as $invoice) {
                $invoice_data    = $this->invoices_model->get($invoice['id']);
                $this->pdf_zip   = invoice_pdf($invoice_data);
                $_temp_file_name = slug_it(format_invoice_number($invoice_data->id));
                $file_name       = $dir . '/' . strtoupper($_temp_file_name);
                $this->pdf_zip->Output($file_name . '.pdf', 'F');
            }
            $this->load->library('zip');
            // Read the invoices
            $this->zip->read_dir($dir, false);
            // Delete the temp directory for the organization
            delete_dir($dir);
            $this->zip->download(slug_it(get_option('companyname')) . '-invoices-' . $zip_file_name . '.zip');
            $this->zip->clear_data();
        }
    }

    /* Since version 1.0.2 zip organization invoices */
    public function zip_transfers($id)
    {
        $has_permission_view = has_permission('transfers', '', 'view');
        if (!$has_permission_view && !has_permission('transfers', '', 'view_own')) {
            access_denied('Zip Customer Estimates');
        }


        if ($this->input->post()) {
            $status        = $this->input->post('transfer_zip_status');
            $zip_file_name = $this->input->post('file_name');
            if ($this->input->post('zip-to') && $this->input->post('zip-from')) {
                $from_date = to_sql_date($this->input->post('zip-from'));
                $to_date   = to_sql_date($this->input->post('zip-to'));
                if ($from_date == $to_date) {
                    $this->db->where('date', $from_date);
                } else {
                    $this->db->where('date BETWEEN "' . $from_date . '" AND "' . $to_date . '"');
                }
            }
            $this->db->select('id');
            $this->db->from('tbltransfers');
            if ($status != 'all') {
                $this->db->where('status', $status);
            }
            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }
            $this->db->where('organizationid', $id);
            $this->db->order_by('number,YEAR(date)', 'desc');
            $transfers = $this->db->get()->result_array();
            $this->load->helper('file');
            if (!is_really_writable(TEMP_FOLDER)) {
                show_error('/temp folder is not writable. You need to change the permissions to 777');
            }
            $this->load->model('transfers_model');
            $dir = TEMP_FOLDER . $zip_file_name;
            if (is_dir($dir)) {
                delete_dir($dir);
            }
            if (count($transfers) == 0) {
                set_alert('warning', _l('organization_zip_no_data_found', _l('transfers')));
                redirect(admin_url('organizations/organization/' . $id . '?group=transfers'));
            }
            mkdir($dir, 0777);
            foreach ($transfers as $transfer) {
                $transfer_data   = $this->transfers_model->get($transfer['id']);
                $this->pdf_zip   = transfer_pdf($transfer_data);
                $_temp_file_name = slug_it(format_transfer_number($transfer_data->id));
                $file_name       = $dir . '/' . strtoupper($_temp_file_name);
                $this->pdf_zip->Output($file_name . '.pdf', 'F');
            }
            $this->load->library('zip');
            // Read the invoices
            $this->zip->read_dir($dir, false);
            // Delete the temp directory for the organization
            delete_dir($dir);
            $this->zip->download(slug_it(get_option('companyname')) . '-transfers-' . $zip_file_name . '.zip');
            $this->zip->clear_data();
        }
    }

    public function zip_payments($id)
    {
        if (!$id) {
            die('No user id');
        }

        $has_permission_view = has_permission('payments', '', 'view');
        if (!$has_permission_view && !has_permission('invoices', '', 'view_own')) {
            access_denied('Zip Customer Payments');
        }

        if ($this->input->post('zip-to') && $this->input->post('zip-from')) {
            $from_date = to_sql_date($this->input->post('zip-from'));
            $to_date   = to_sql_date($this->input->post('zip-to'));
            if ($from_date == $to_date) {
                $this->db->where('tblinvoicepaymentrecords.date', $from_date);
            } else {
                $this->db->where('tblinvoicepaymentrecords.date BETWEEN "' . $from_date . '" AND "' . $to_date . '"');
            }
        }
        $this->db->select('tblinvoicepaymentrecords.id as paymentid');
        $this->db->from('tblinvoicepaymentrecords');
        $this->db->where('tblorganizations.userid', $id);
        if (!$has_permission_view) {
            $this->db->where('invoiceid IN (SELECT id FROM tblinvoices WHERE addedfrom=' . get_staff_user_id() . ')');
        }
        $this->db->join('tblinvoices', 'tblinvoices.id = tblinvoicepaymentrecords.invoiceid', 'left');
        $this->db->join('tblorganizations', 'tblorganizations.userid = tblinvoices.organizationid', 'left');
        if ($this->input->post('paymentmode')) {
            $this->db->where('paymentmode', $this->input->post('paymentmode'));
        }
        $payments      = $this->db->get()->result_array();
        $zip_file_name = $this->input->post('file_name');
        $this->load->helper('file');
        if (!is_really_writable(TEMP_FOLDER)) {
            show_error('/temp folder is not writable. You need to change the permissions to 777');
        }
        $dir = TEMP_FOLDER . $zip_file_name;
        if (is_dir($dir)) {
            delete_dir($dir);
        }
        if (count($payments) == 0) {
            set_alert('warning', _l('organization_zip_no_data_found', _l('payments')));
            redirect(admin_url('organizations/organization/' . $id . '?group=payments'));
        }
        mkdir($dir, 0777);
        $this->load->model('payments_model');
        $this->load->model('invoices_model');
        foreach ($payments as $payment) {
            $payment_data               = $this->payments_model->get($payment['paymentid']);
            $payment_data->invoice_data = $this->invoices_model->get($payment_data->invoiceid);
            $this->pdf_zip              = payment_pdf($payment_data);
            $file_name                  = $dir;
            $file_name .= '/' . strtoupper(_l('payment'));
            $file_name .= '-' . strtoupper($payment_data->paymentid) . '.pdf';
            $this->pdf_zip->Output($file_name, 'F');
        }
        $this->load->library('zip');
        // Read the invoices
        $this->zip->read_dir($dir, false);
        // Delete the temp directory for the organization
        delete_dir($dir);
        $this->zip->download(slug_it(get_option('companyname')) . '-payments-' . $zip_file_name . '.zip');
        $this->zip->clear_data();
    }

    public function import()
    {
        if (!has_permission('organizations', '', 'create')) {
            access_denied('organizations');
        }
        $country_fields = array('country', 'billing_country', 'shipping_country');

        $simulate_data  = array();
        $total_imported = 0;
        if ($this->input->post()) {

            // Used when checking existing company to merge contact
            $contactFields = $this->db->list_fields('tblcontacts');

            if (isset($_FILES['file_csv']['name']) && $_FILES['file_csv']['name'] != '') {
                // Get the temp file path
                $tmpFilePath = $_FILES['file_csv']['tmp_name'];
                // Make sure we have a filepath
                if (!empty($tmpFilePath) && $tmpFilePath != '') {
                    // Setup our new file path
                    $newFilePath = TEMP_FOLDER . $_FILES['file_csv']['name'];
                    if (!file_exists(TEMP_FOLDER)) {
                        mkdir(TEMP_FOLDER, 777);
                    }
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $import_result = true;
                        $fd            = fopen($newFilePath, 'r');
                        $rows          = array();
                        while ($row = fgetcsv($fd)) {
                            $rows[] = $row;
                        }

                        $data['total_rows_post'] = count($rows);
                        fclose($fd);
                        if (count($rows) <= 1) {
                            set_alert('warning', 'Not enought rows for importing');
                            redirect(admin_url('organizations/import'));
                        }
                        unset($rows[0]);
                        if ($this->input->post('simulate')) {
                            if (count($rows) > 500) {
                                set_alert('warning', 'Recommended splitting the CSV file into smaller files. Our recomendation is 500 row, your CSV file has ' . count($rows));
                            }
                        }
                        $organization_contacts_fields = $this->db->list_fields('tblcontacts');
                        $i                      = 0;
                        foreach ($organization_contacts_fields as $cf) {
                            if ($cf == 'phonenumber') {
                                $organization_contacts_fields[$i] = 'contact_phonenumber';
                            }
                            $i++;
                        }
                        $db_temp_fields = $this->db->list_fields('tblorganizations');
                        $db_temp_fields = array_merge($organization_contacts_fields, $db_temp_fields);
                        $db_fields      = array();
                        foreach ($db_temp_fields as $field) {
                            if (in_array($field, $this->not_importable_organizations_fields)) {
                                continue;
                            }
                            $db_fields[] = $field;
                        }
                        $custom_fields = get_custom_fields('organizations');
                        $_row_simulate = 0;

                        $required = array(
                            'firstname',
                            'lastname',
                            'email',
                        );

                        if (get_option('company_is_required') == 1) {
                            array_push($required, 'company');
                        }

                        foreach ($rows as $row) {
                            // do for db fields
                            $insert    = array();
                            $duplicate = false;
                            for ($i = 0; $i < count($db_fields); $i++) {
                                if (!isset($row[$i])) {
                                    continue;
                                }
                                if ($db_fields[$i] == 'email') {
                                    $email_exists = total_rows('tblcontacts', array(
                                        'email' => $row[$i],
                                    ));
                                    // don't insert duplicate emails
                                    if ($email_exists > 0) {
                                        $duplicate = true;
                                    }
                                }
                                // Avoid errors on required fields;
                                if (in_array($db_fields[$i], $required) && $row[$i] == '' && $db_fields[$i] != 'company') {
                                    $row[$i] = '/';
                                } elseif (in_array($db_fields[$i], $country_fields)) {
                                    if ($row[$i] != '') {
                                        if (!is_numeric($row[$i])) {
                                            $this->db->where('iso2', $row[$i]);
                                            $this->db->or_where('short_name', $row[$i]);
                                            $this->db->or_where('long_name', $row[$i]);
                                            $country = $this->db->get('tblcountries')->row();
                                            if ($country) {
                                                $row[$i] = $country->country_id;
                                            } else {
                                                $row[$i] = 0;
                                            }
                                        }
                                    } else {
                                        $row[$i] = 0;
                                    }
                                }
                                if($row[$i] === 'NULL' || $row[$i] === 'null') {
                                    $row[$i] = '';
                                }
                                $insert[$db_fields[$i]] = $row[$i];
                            }


                            if ($duplicate == true) {
                                continue;
                            }
                            if (count($insert) > 0) {
                                $total_imported++;
                                $insert['datecreated'] = date('Y-m-d H:i:s');
                                if ($this->input->post('default_pass_all')) {
                                    $insert['password'] = $this->input->post('default_pass_all',false);
                                }
                                if (!$this->input->post('simulate')) {
                                    $insert['donotsendwelcomeemail'] = true;
                                    foreach ($insert as $key =>$val) {
                                        $insert[$key] = trim($val);
                                    }

                                    if (isset($insert['company']) && $insert['company'] != '' && $insert['company'] != '/') {
                                        if (total_rows('tblorganizations', array('company'=>$insert['company'])) === 1) {
                                            $this->db->where('company', $insert['company']);
                                            $existingCompany = $this->db->get('tblorganizations')->row();
                                            $tmpInsert = array();

                                            foreach ($insert as $key=>$val) {
                                                foreach ($contactFields as $tmpContactField) {
                                                    if (isset($insert[$tmpContactField])) {
                                                        $tmpInsert[$tmpContactField] = $insert[$tmpContactField];
                                                    }
                                                }
                                            }
                                            $tmpInsert['donotsendwelcomeemail'] = true;
                                            if (isset($insert['contact_phonenumber'])) {
                                                $tmpInsert['phonenumber'] = $insert['contact_phonenumber'];
                                            }

                                            $contactid = $this->organizations_model->add_contact($tmpInsert, $existingCompany->userid, true);

                                            continue;
                                        }
                                    }
                                    $insert['is_primary'] = 1;

                                    $organizationid                        = $this->organizations_model->add($insert, true);
                                    if ($organizationid) {
                                        if ($this->input->post('groups_in[]')) {
                                            $groups_in = $this->input->post('groups_in[]');
                                            foreach ($groups_in as $group) {
                                                $this->db->insert('tblorganizationgroups_in', array(
                                                    'organization_id' => $organizationid,
                                                    'groupid' => $group,
                                                ));
                                            }
                                        }
                                        if (!has_permission('organizations', '', 'view')) {
                                            $assign['organization_admins']   = array();
                                            $assign['organization_admins'][] = get_staff_user_id();
                                            $this->organizations_model->assign_admins($assign, $organizationid);
                                        }
                                    }
                                } else {
                                    foreach ($country_fields as $country_field) {
                                        if (array_key_exists($country_field, $insert)) {
                                            if ($insert[$country_field] != 0) {
                                                $c = get_country($insert[$country_field]);
                                                if ($c) {
                                                    $insert[$country_field] = $c->short_name;
                                                }
                                            } elseif ($insert[$country_field] == 0) {
                                                $insert[$country_field] = '';
                                            }
                                        }
                                    }
                                    $simulate_data[$_row_simulate] = $insert;
                                    $organizationid                      = true;
                                }
                                if ($organizationid) {
                                    $insert = array();
                                    foreach ($custom_fields as $field) {
                                        if (!$this->input->post('simulate')) {
                                            if ($row[$i] != '' && $row[$i] !== 'NULL' && $row[$i] !== 'null') {
                                                $this->db->insert('tblcustomfieldsvalues', array(
                                                    'relid' => $organizationid,
                                                    'fieldid' => $field['id'],
                                                    'value' => $row[$i],
                                                    'fieldto' => 'organizations',
                                                ));
                                            }
                                        } else {
                                            $simulate_data[$_row_simulate][$field['name']] = $row[$i];
                                        }
                                        $i++;
                                    }
                                }
                            }
                            $_row_simulate++;
                            if ($this->input->post('simulate') && $_row_simulate >= 100) {
                                break;
                            }
                        }
                        unlink($newFilePath);
                    }
                } else {
                    set_alert('warning', _l('import_upload_failed'));
                }
            }
        }
        if (count($simulate_data) > 0) {
            $data['simulate'] = $simulate_data;
        }
        if (isset($import_result)) {
            set_alert('success', _l('import_total_imported', $total_imported));
        }
        $data['groups']         = $this->organizations_model->get_groups();
        $data['not_importable'] = $this->not_importable_organizations_fields;
        $data['title']          = _l('import');
        $data['bodyclass'] = 'dynamic-create-groups';
        $this->load->view('admin/organizations/import', $data);
    }


    public function groups()
    {
        if (!is_admin()) {
            access_denied('Orgnization Groups');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('organizations_groups');
        }
        $data['title'] = _l('organization_groups');
        $this->load->view('admin/organizations/groups_manage', $data);
    }

    public function group()
    {
        if (!is_admin() && get_option('staff_members_create_inline_organization_groups') == '0') {
            access_denied('Orgnization Groups');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id = $this->organizations_model->add_group($data);
                $message = $id ? _l('added_successfully', _l('organization_group')) : '';
                echo json_encode(array(
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'=>$id,
                    'name'=>$data['name'],
                ));
            } else {
                $success = $this->organizations_model->edit_group($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('organization_group'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message,
                ));
            }
        }
    }

    public function delete_group($id)
    {
        if (!is_admin()) {
            access_denied('Delete Orgnization Group');
        }
        if (!$id) {
            redirect(admin_url('organizations/groups'));
        }
        $response = $this->organizations_model->delete_group($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('organization_group')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('organization_group_lowercase')));
        }
        redirect(admin_url('organizations/groups'));
    }

    public function bulk_action()
    {
        do_action('before_do_bulk_action_for_organizations');
        $total_deleted = 0;
        if ($this->input->post()) {
            $ids    = $this->input->post('ids');
            $groups = $this->input->post('groups');

            if (is_array($ids)) {
                foreach ($ids as $id) {
                    if ($this->input->post('mass_delete')) {
                        if ($this->organizations_model->delete($id)) {
                            $total_deleted++;
                        }
                    } else {
                        if (!is_array($groups)) {
                            $groups = false;
                        }
                        $this->organization_groups_model->sync_organization_groups($id, $groups);
                    }
                }
            }
        }

        if ($this->input->post('mass_delete')) {
            set_alert('success', _l('total_organizations_deleted', $total_deleted));
        }
    }

    public function vault_entry_create($organization_id)
    {
        $data = $this->input->post();

        if (isset($data['fakeusernameremembered'])) {
            unset($data['fakeusernameremembered']);
        }

        if (isset($data['fakepasswordremembered'])) {
            unset($data['fakepasswordremembered']);
        }

        unset($data['id']);
        $data['creator'] = get_staff_user_id();
        $data['creator_name'] = get_staff_full_name($data['creator']);
        $data['description'] = nl2br($data['description']);
        $data['password'] = $this->encryption->encrypt($this->input->post('password',false));

        if (empty($data['port'])) {
            unset($data['port']);
        }

        $this->organizations_model->vault_entry_create($data, $organization_id);
        set_alert('success', _l('added_successfully', _l('vault_entry')));
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function vault_entry_update($entry_id)
    {
        $entry = $this->organizations_model->get_vault_entry($entry_id);

        if ($entry->creator == get_staff_user_id() || is_admin()) {
            $data = $this->input->post();

            if (isset($data['fakeusernameremembered'])) {
                unset($data['fakeusernameremembered']);
            }
            if (isset($data['fakepasswordremembered'])) {
                unset($data['fakepasswordremembered']);
            }

            $data['last_updated_from'] = get_staff_full_name(get_staff_user_id());
            $data['description'] = nl2br($data['description']);

            if (!empty($data['password'])) {
                $data['password'] = $this->encryption->encrypt($this->input->post('password',false));
            } else {
                unset($data['password']);
            }

            if (empty($data['port'])) {
                unset($data['port']);
            }

            $this->organizations_model->vault_entry_update($entry_id, $data);
            set_alert('success', _l('updated_successfully', _l('vault_entry')));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function vault_entry_delete($id)
    {
        $entry = $this->organizations_model->get_vault_entry($id);
        if ($entry->creator == get_staff_user_id() || is_admin()) {
            $this->organizations_model->vault_entry_delete($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function vault_encrypt_password()
    {
        $id = $this->input->post('id');
        $user_password = $this->input->post('user_password', false);
        $user = $this->staff_model->get(get_staff_user_id());

        $this->load->helper('phpass');

        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        if (!$hasher->CheckPassword($user_password, $user->password)) {
            header('HTTP/1.1 401 Unauthorized');
            echo json_encode(array('error_msg'=>_l('vault_password_user_not_correct')));
            die;
        }

        $vault = $this->organizations_model->get_vault_entry($id);
        $password = $this->encryption->decrypt($vault->password);

        $password = html_escape($password);

        // Failed to decrypt
        if (!$password) {
            header('HTTP/1.0 400 Bad error');
            echo json_encode(array('error_msg'=>_l('failed_to_decrypt_password')));
            die;
        }

        echo json_encode(array('password'=>$password));
    }

    public function get_vault_entry($id)
    {
        $entry = $this->organizations_model->get_vault_entry($id);
        unset($entry->password);
        $entry->description = clear_textarea_breaks($entry->description);
        echo json_encode($entry);
    }

    public function statement_pdf()
    {
        $organization_id = $this->input->get('organization_id');

        if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
            set_alert('danger', _l('access_denied'));
            redirect(admin_url('organizations/organization/'.$organization_id));
        }

        $from = $this->input->get('from');
        $to = $this->input->get('to');

        $data['statement'] = $this->organizations_model->get_statement($organization_id, to_sql_date($from), to_sql_date($to));

        try {
            $pdf            = statement_pdf($data['statement']);
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            if (strpos($message, 'Unable to get the size of the image') !== false) {
                show_pdf_unable_to_get_image_size_error();
            }
            die;
        }

        $type           = 'D';
        if ($this->input->get('print')) {
            $type = 'I';
        }

        $pdf->Output(slug_it(_l('organization_statement').'-'.$data['statement']['organization']->company) . '.pdf', $type);
    }

    public function send_statement()
    {
        $organization_id = $this->input->get('organization_id');

        if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
            set_alert('danger', _l('access_denied'));
            redirect(admin_url('organizations/organization/'.$organization_id));
        }

        $from = $this->input->get('from');
        $to = $this->input->get('to');

        $send_to = $this->input->post('send_to');
        $cc = $this->input->post('cc');

        $success = $this->organizations_model->send_statement_to_email($organization_id, $send_to, $from, $to, $cc);
        // In case organization use another language
        load_admin_language();
        if ($success) {
            set_alert('success', _l('statement_sent_to_organization_success'));
        } else {
            set_alert('danger', _l('statement_sent_to_organization_fail'));
        }

        redirect(admin_url('organizations/organization/' . $organization_id.'?group=statement'));
    }

    public function statement()
    {
        if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
            header('HTTP/1.0 400 Bad error');
            echo _l('access_denied');
            die;
        }

        $organization_id = $this->input->get('organization_id');
        $from = $this->input->get('from');
        $to = $this->input->get('to');

        $data['statement'] = $this->organizations_model->get_statement($organization_id, to_sql_date($from), to_sql_date($to));

        $data['from'] = $from;
        $data['to'] = $to;

        $viewData['html'] = $this->load->view('admin/organizations/groups/_statement', $data, true);

        echo json_encode($viewData);
    }



    public function positions()
    {
        if (!is_admin()) {
            access_denied('Contacts Positions');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('organizations_contacts_positions');
        }
        $data['title'] = _l('contacts_positions');
        $this->load->view('admin/organizations/positions_manage', $data);
    }

    public function position()
    {
        if (!is_admin() && get_option('staff_members_create_inline_organization_groups') == '0') {
            access_denied('Contacts Positions');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id = $this->organizations_model->add_position($data);
                $message = $id ? _l('added_successfully', _l('organization_position')) : '';
                echo json_encode(array(
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'=>$id,
                    'name'=>$data['name'],
                ));
            } else {
                $success = $this->organizations_model->edit_position($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('organization_position'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message,
                ));
            }
        }
    }

    public function delete_position($id)
    {
        if (!is_admin()) {
            access_denied('Delete Contact Position');
        }
        if (!$id) {
            redirect(admin_url('organizations/positions'));
        }
        $response = $this->organizations_model->delete_position($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('organization_position')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('contact_position_lowercase')));
        }
        redirect(admin_url('organizations/positions'));
    }




}
