<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Assets extends Admin_controller
{
    private $not_importable_assets_fields;

    public function __construct()
    {
        parent::__construct();
        $this->not_importable_assets_fields = do_action('not_importable_assets_fields',array('id', 'billing_type', 'asset_created', 'asset_rate_per_hour', 'transferd_hours', 'progress_from_tasks','date_finished', 'date_finished', 'progress', 'addedfrom'));

        $this->load->model('assets_model');
        $this->load->model('currencies_model');
        $this->load->helper('date');
    }

    public function index()
    {
        close_setup_menu();
        $data['statuses'] = $this->assets_model->get_asset_statuses();
        $data['title']    = _l('assets');
        $this->load->view('admin/assets/manage', $data);
    }

    public function table($organizationid = '')
    {
        $this->app->get_table_data('assets', array(
            'organizationid' => $organizationid,
        ));
    }

    public function staff_assets()
    {
        $this->app->get_table_data('staff_assets');
    }

    public function expenses($id)
    {
        $this->load->model('expenses_model');
        $this->app->get_table_data('asset_expenses', array(
            'asset_id' => $id,
        ));
    }

    public function add_expense()
    {
        if ($this->input->post()) {
            $this->load->model('expenses_model');
            $id = $this->expenses_model->add($this->input->post());
            if ($id) {
                set_alert('success', _l('added_successfully', _l('expense')));
                echo json_encode(array(
                    'url' => admin_url('assets/view/' . $this->input->post('asset_id') . '/?group=asset_expenses'),
                    'expenseid' => $id,
                ));
                die;
            }
            echo json_encode(array(
                'url' => admin_url('assets/view/' . $this->input->post('asset_id') . '/?group=asset_expenses'),
            ));
            die;
        }
    }

    public function asset($id = '')
    {
        if (!has_permission('assets', '', 'edit') && !has_permission('assets', '', 'create')) {
            access_denied('Assets');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            $data['description'] = $this->input->post('description', false);
            if ($id == '') {
                if (!has_permission('assets', '', 'create')) {
                    access_denied('Assets');
                }
                $id = $this->assets_model->add($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('asset')));
                    redirect(admin_url('assets/view/' . $id));
                }
            } else {
                if (!has_permission('assets', '', 'edit')) {
                    access_denied('Assets');
                }
                $success = $this->assets_model->update($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('asset')));
                }
                redirect(admin_url('assets/view/' . $id));
            }
        }
        if ($id == '') {
            $title                            = _l('add_new', _l('asset_lowercase'));
            $data['auto_select_billing_type'] = $this->assets_model->get_most_used_billing_type();
        } else {
            $data['asset']         = $this->assets_model->get($id);
            $data['asset']->settings->available_features = unserialize($data['asset']->settings->available_features);

            $data['asset_members'] = $this->assets_model->get_asset_members($id);
            $title                   = _l('edit', _l('asset_lowercase'));
        }

        if ($this->input->get('organization_id')) {
            $data['organization_id']        = $this->input->get('organization_id');
        }

        $data['last_asset_settings'] = $this->assets_model->get_last_asset_settings();

        if (count($data['last_asset_settings'])) {
            $key = array_search('available_features', array_column($data['last_asset_settings'], 'name'));
            $data['last_asset_settings'][$key]['value'] = unserialize($data['last_asset_settings'][$key]['value']);
        }

        $this->load->model('departments_model');

        $data['settings']              = $this->assets_model->get_settings();
        $data['statuses']              = $this->assets_model->get_asset_statuses();
        $data['assets_classes']        = $this->assets_model->get_asset_class();
        $data['assets_categories']     = $this->assets_model->get_asset_category();
        $data['departments']           = $this->departments_model->get();
        $data['staff']                 = $this->staff_model->get('', 1);

        $data['title'] = $title;
        $this->load->view('admin/assets/asset', $data);
    }

    public function view($id)
    {
        if ($this->assets_model->is_member($id) || has_permission('assets', '', 'view')) {
            close_setup_menu();
            $asset = $this->assets_model->get($id);

            if (!$asset) {
                blank_page(_l('asset_not_found'));
            }

            $data['statuses'] = $this->assets_model->get_asset_statuses();

            if (!$this->input->get('group')) {
                $view = 'asset_overview';
            } else {
                $view = $this->input->get('group');
            }

            $this->load->model('payment_modes_model');
            $data['payment_modes'] = $this->payment_modes_model->get('', array(), true);

            $data['asset']              = $asset;
            $data['currency'] = $this->assets_model->get_currency($id);

            $data['asset_total_logged_time'] = $this->assets_model->total_logged_time($id);

            $data['staff']         = $this->staff_model->get('', 1);
            $percent             = "";
            $data['bodyclass'] = '';
            if ($view == 'asset_overview') {
                $data['members']       = $this->assets_model->get_asset_members($id);
                $i = 0;
                foreach ($data['members'] as $member) {
                    $data['members'][$i]['total_logged_time'] = 0;
                    $member_timesheets = $this->tasks_model->get_unique_member_logged_task_ids($member['staff_id'], ' AND task_id IN (SELECT id FROM tblstafftasks WHERE rel_type="asset" AND rel_id="'.$id.'")');

                    foreach ($member_timesheets as $member_task) {
                        $data['members'][$i]['total_logged_time'] += $this->tasks_model->calc_task_total_time($member_task->task_id, ' AND staff_id='.$member['staff_id']);
                    }

                    $i++;
                }

                $data['asset_total_days']        = round((human_to_unix($data['asset']->date_finished . ' 00:00') - human_to_unix($data['asset']->start_date . ' 00:00')) / 3600 / 24);
                $data['asset_days_left']         = $data['asset_total_days'];
                $data['asset_time_left_percent'] = 100;
                if ($data['asset']->date_finished) {
                    if (human_to_unix($data['asset']->start_date . ' 00:00') < time() && human_to_unix($data['asset']->date_finished . ' 00:00') > time()) {
                        $data['asset_days_left']         = round((human_to_unix($data['asset']->date_finished . ' 00:00') - time()) / 3600 / 24);
                        $data['asset_time_left_percent'] = $data['asset_days_left'] / $data['asset_total_days'] * 100;
                    }
                    if (human_to_unix($data['asset']->date_finished . ' 00:00') < time()) {
                        $data['asset_days_left']         = 0;
                        $data['asset_time_left_percent'] = 0;
                    }
                }

                $__total_where_tasks = 'rel_type = "asset" AND rel_id=' . $id;
                if (!has_permission('tasks', '', 'view')) {
                    $__total_where_tasks .= ' AND tblstafftasks.id IN (SELECT taskid FROM tblstafftaskassignees WHERE staffid = ' . get_staff_user_id() . ')';

                    if (get_option('show_all_tasks_for_asset_member') == 1) {
                        $__total_where_tasks .= ' AND (rel_type="asset" AND rel_id IN (SELECT asset_id FROM tblassetmembers WHERE staff_id=' . get_staff_user_id() . '))';
                    }
                }
                $where = ($__total_where_tasks == '' ? '' : $__total_where_tasks . ' AND ') . 'status != 5';

                $data['tasks_not_completed'] = total_rows('tblstafftasks', $where);
                $total_tasks                 = total_rows('tblstafftasks', $__total_where_tasks);
                $data['total_tasks']         = $total_tasks;

                $where = ($__total_where_tasks == '' ? '' : $__total_where_tasks . ' AND ') . 'status = 5 AND rel_type="asset" AND rel_id="' . $id . '"';

                $data['tasks_completed'] = total_rows('tblstafftasks', $where);

                $data['tasks_not_completed_progress'] = ($total_tasks > 0 ? number_format(($data['tasks_completed'] * 100) / $total_tasks, 2) : 0);

                @$percent_circle = $percent / 100;
                $data['percent_circle'] = $percent_circle;


                $data['asset_overview_chart'] = $this->assets_model->get_asset_overview_weekly_chart_data($id, ($this->input->get('overview_chart') ? $this->input->get('overview_chart'):'this_week'));
            } elseif ($view == 'asset_invoices') {
                $this->load->model('invoices_model');

                $data['invoiceid']   = '';
                $data['status']      = '';
                $data['custom_view'] = '';

                $data['invoices_years']       = $this->invoices_model->get_invoices_years();
                $data['invoices_sale_agents'] = $this->invoices_model->get_sale_agents();
                $data['invoices_statuses']    = $this->invoices_model->get_statuses();
            } elseif ($view == 'asset_gantt') {
                $gantt_type          = (!$this->input->get('gantt_type') ? 'milestones' : $this->input->get('gantt_type'));
                $taskStatus = (!$this->input->get('gantt_task_status') ? null : $this->input->get('gantt_task_status'));
                $data['gantt_data']  = $this->assets_model->get_gantt_data($id, $gantt_type, $taskStatus);
            } elseif ($view == 'asset_milestones') {

                $data['bodyclass'] .= 'asset-milestones ';
                $data['milestones_exclude_completed_tasks'] = $this->input->get('exclude_completed') && $this->input->get('exclude_completed') == 'yes' || !$this->input->get('exclude_completed');

                $data['total_milestones'] = total_rows('tblmilestones',array('asset_id'=>$id));
                $data['milestones_found'] = $data['total_milestones'] > 0 || (!$data['total_milestones'] && total_rows('tblstafftasks',array('rel_id'=>$id,'rel_type'=>'asset','milestone'=>0)) > 0);

            } elseif ($view == 'asset_files') {
                $data['files']       = $this->assets_model->get_files($id);
            } elseif ($view == 'asset_expenses') {
                $this->load->model('taxes_model');
                $this->load->model('expenses_model');
                $data['taxes']              = $this->taxes_model->get();
                $data['expense_categories'] = $this->expenses_model->get_category();
                $data['currencies']         = $this->currencies_model->get();
            } elseif ($view == 'asset_activity') {
                $data['activity']             = $this->assets_model->get_activity($id);
            } elseif ($view == 'asset_notes') {
                $data['staff_notes']          = $this->assets_model->get_staff_notes($id);
            } elseif ($view == 'asset_transfers') {
                $this->load->model('transfers_model');
                $data['transfers_years']       = $this->transfers_model->get_transfers_years();
                $data['transfers_sale_agents'] = $this->transfers_model->get_sale_agents();
                $data['transfer_statuses']     = $this->transfers_model->get_statuses();
                $data['transferid']            = '';
                $data['switch_pipeline']       = '';
            } elseif ($view == 'asset_tickets') {
                $data['chosen_ticket_status'] = '';
                $this->load->model('tickets_model');
                $data['ticket_assignees'] = $this->tickets_model->get_tickets_assignes_disctinct();

                $this->load->model('departments_model');
                $data['staff_deparments_ids'] = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                $data['default_tickets_list_statuses'] = do_action('default_tickets_list_statuses', array(1, 2, 4));
            } elseif ($view == 'asset_timesheets') {
                // Tasks are used in the timesheet dropdown
                // Completed tasks are excluded from this list because you can't add timesheet on completed task.
                $data['tasks'] = $this->assets_model->get_tasks($id, 'status != 5 AND billed=0');
                $data['timesheets_staff_ids'] = $this->assets_model->get_distinct_tasks_timesheets_staff($id);
            }

            // Discussions
            if ($this->input->get('discussion_id')) {
                $data['discussion_user_profile_image_url'] = staff_profile_image_url(get_staff_user_id());
                $data['discussion']                        = $this->assets_model->get_discussion($this->input->get('discussion_id'), $id);
                $data['current_user_is_admin']             = is_admin();
            }

            $data['percent']              = $percent;

            $data['assets_assets']       = true;
            $data['circle_progress_asset'] = true;

            $other_assets = array();
            $other_assets_where = 'id !='.$id. ' and status = 2';

            if (!has_permission('assets', '', 'view')) {
                $other_assets_where .= ' AND tblassets.id IN (SELECT asset_id FROM tblassetmembers WHERE staff_id=' . get_staff_user_id() .')';
            }

            $this->load->model('departments_model');

            $data['other_assets']       =  $this->assets_model->get('', $other_assets_where);
            $data['title']              = $data['asset']->name;
            $data['bodyclass']          .= 'asset invoices_total_manual transfers_total_manual';
            $data['asset_status']       =  get_asset_status_by_id($asset->status);
            $data['asset_class']        =  get_asset_class_by_id($asset->class_code);
            $data['asset_category']     =  get_asset_category_by_id($asset->category_code);
            $data['asset_department']   =  get_asset_department_by_id($asset->department_code);

            $hook_data                     = do_action('asset_group_access_admin', array(
                'id' => $asset->id,
                'view' => $view,
                'all_data' => $data,
            ));

            $data                          = $hook_data['all_data'];
            $view                          = $hook_data['view'];

            // Unable to load the requested file: admin/assets/asset_tasks#.php - FIX
            if (strpos($view, '#') !== false) {
                $view = str_replace('#', '', $view);
            }

            $view = trim($view);
            $data['view'] = $view;
            $data['group_view']            = $this->load->view('admin/assets/' . $view, $data, true);

            $this->load->view('admin/assets/view', $data);
        } else {
            access_denied('Asset View');
        }
    }





    public function import()
    {
        if (!has_permission('assets', '', 'create')) {
            access_denied('assets');
        }

        $orgnization_fields = array('organizationid');

        $simulate_data  = array();
        $total_imported = 0;
        if ($this->input->post()) {

            if (isset($_FILES['file_csv']['name']) && $_FILES['file_csv']['name'] != '') {
                // Get the temp file path
                $tmpFilePath = $_FILES['file_csv']['tmp_name'];
                // Make sure we have a filepath
                if (!empty($tmpFilePath) && $tmpFilePath != '') {
                    // Setup our new file path
                    $newFilePath = TEMP_FOLDER . $_FILES['file_csv']['name'];
                    if (!file_exists(TEMP_FOLDER)) {
                        mkdir(TEMP_FOLDER, 777);
                    }
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $import_result = true;
                        $fd            = fopen($newFilePath, 'r');
                        $rows          = array();
                        while ($row = fgetcsv($fd)) {
                            $rows[] = $row;
                        }

                        $data['total_rows_post'] = count($rows);
                        fclose($fd);
                        if (count($rows) <= 1) {
                            set_alert('warning', 'Not enought rows for importing');
                            redirect(admin_url('assets/import'));
                        }
                        unset($rows[0]);
                        if ($this->input->post('simulate')) {
                            if (count($rows) > 500) {
                                set_alert('warning', 'Recommended splitting the CSV file into smaller files. Our recomendation is 500 row, your CSV file has ' . count($rows));
                            }
                        }

                        $db_temp_fields = $this->db->list_fields('tblassets');
                        $db_fields      = array();
                        foreach ($db_temp_fields as $field) {
                            if (in_array($field, $this->not_importable_assets_fields)) {
                                continue;
                            }
                            $db_fields[] = $field;
                        }
                        $custom_fields = get_custom_fields('assets');
                        $_row_simulate = 0;

                        $required = array(
                            'code',
                            'organizationid',
                        );

                        foreach ($rows as $row) {
                            // do for db fields
                            $insert    = array();
                            $duplicate = false;
                            for ($i = 0; $i < count($db_fields); $i++) {
                                if (!isset($row[$i])) {
                                    continue;
                                }

/*                                if ($db_fields[$i] == 'reservation_id') {
                                    $invoice_exists = total_rows('tblinvoices', array(
                                        'reservation_id' => $row[$i],
                                    ));
                                    // don't insert duplicate emails
                                    if ($invoice_exists > 0) {
                                        $duplicate = true;
                                    }
                                }*/

                                // Avoid errors on required fields;
                                if (in_array($db_fields[$i], $required) && $row[$i] == '') {
                                    $row[$i] = '/';
                                } elseif (in_array($db_fields[$i], $orgnization_fields)) {
                                    if ($row[$i] != '') {
                                        if (!is_numeric($row[$i]) || is_numeric($row[$i])) {
                                            $this->db->where('code', $row[$i]);
                                            $this->db->or_where('userid', $row[$i]);
                                            $organization = $this->db->get('tblorganizations')->row();
                                            if ($organization) {
                                                $row[$i] = $organization->userid;
                                            } else {
                                                $row[$i] = 0;
                                            }
                                        }
                                    } else {
                                        $row[$i] = 0;
                                    }
                                }                      

                                if($row[$i] === 'NULL' || $row[$i] === 'null') {
                                    $row[$i] = '';
                                }
                                $insert[$db_fields[$i]] = $row[$i];
                            }


                            if ($duplicate == true) {
                                continue;
                            }
                            if (count($insert) > 0) {
                                $total_imported++;
                                $insert['asset_created'] = date('Y-m-d');
                                // $insert['start_date'] = date('Y-m-d');
                                $insert['billing_type'] = '1';

                                if (!$this->input->post('simulate')) {

                                    $assetid                        = $this->assets_model->add($insert, true);
                                    
                                } else {

                                    foreach ($orgnization_fields as $orgnization_field) {
                                        if (array_key_exists($orgnization_field, $insert)) {
                                            if ($insert[$orgnization_field] != 0) {
                                                $c = get_asset($insert[$orgnization_field]);
                                                if ($c) {
                                                    $insert[$orgnization_field] = $c->userid;
                                                }
                                            } elseif ($insert[$orgnization_field] == 0) {
                                                $insert[$orgnization_field] = '';
                                            }
                                        }
                                    }

                                                                                                            
                                    $simulate_data[$_row_simulate] = $insert;
                                    $assetid                      = true;
                                }
                                if ($assetid) {
                                    $insert = array();
                                    foreach ($custom_fields as $field) {
                                        if (!$this->input->post('simulate')) {
                                            if ($row[$i] != '' && $row[$i] !== 'NULL' && $row[$i] !== 'null') {
                                                $this->db->insert('tblcustomfieldsvalues', array(
                                                    'relid' => $assetid,
                                                    'fieldid' => $field['id'],
                                                    'value' => $row[$i],
                                                    'fieldto' => 'assets',
                                                ));
                                            }
                                        } else {
                                            $simulate_data[$_row_simulate][$field['name']] = $row[$i];
                                        }
                                        $i++;
                                    }
                                }
                            }
                            $_row_simulate++;
                            if ($this->input->post('simulate') && $_row_simulate >= 100) {
                                break;
                            }
                        }
                        unlink($newFilePath);
                    }
                } else {
                    set_alert('warning', _l('import_upload_failed'));
                }
            }
        }
        if (count($simulate_data) > 0) {
            $data['simulate'] = $simulate_data;
        }
        if (isset($import_result)) {
            set_alert('success', _l('import_total_imported', $total_imported));
        }
        $data['not_importable'] = $this->not_importable_assets_fields;
        $data['title']          = _l('import');
        $data['bodyclass'] = 'dynamic-create-groups';
        $this->load->view('admin/assets/import', $data);
    }









    public function mark_as()
    {
        $success = false;
        $message = '';
        if ($this->input->is_ajax_request()) {
            if (has_permission('assets', '', 'create') || has_permission('assets', '', 'edit')) {
                $status = get_asset_status_by_id($this->input->post('status_id'));

                $message = _l('asset_marked_as_failed', $status['name']);
                $success = $this->assets_model->mark_as($this->input->post());

                if ($success) {
                    $message = _l('asset_marked_as_success', $status['name']);
                }
            }
        }
        echo json_encode(array(
            'success' => $success,
            'message' => $message,
        ));
    }

    public function file($id, $asset_id)
    {
        $data['discussion_user_profile_image_url'] = staff_profile_image_url(get_staff_user_id());
        $data['current_user_is_admin']             = is_admin();

        $data['file'] = $this->assets_model->get_file($id, $asset_id);
        if (!$data['file']) {
            header("HTTP/1.0 404 Not Found");
            die;
        }
        $this->load->view('admin/assets/_file', $data);
    }

    public function update_file_data()
    {
        if ($this->input->post()) {
            $this->assets_model->update_file_data($this->input->post());
        }
    }

    public function add_external_file()
    {
        if ($this->input->post()) {
            $data = array();
            $data['asset_id'] = $this->input->post('asset_id');
            $data['files'] = $this->input->post('files');
            $data['external'] = $this->input->post('external');
            $data['visible_to_organization'] =($this->input->post('visible_to_organization') == 'true' ? 1 : 0);
            $data['staffid'] = get_staff_user_id();
            $this->assets_model->add_external_file($data);
        }
    }

    public function download_all_files($id) {
        if ($this->assets_model->is_member($id) || has_permission('assets', '', 'view')) {
            $files = $this->assets_model->get_files($id);
            if(count($files) == 0){
                set_alert('warning',_l('no_files_found'));
                redirect(admin_url('assets/view/'.$id.'?group=asset_files'));
            }
            $path = get_upload_path_by_type('asset'). $id;
            $this->load->library('zip');
            foreach($files as $file) {
                $this->zip->read_file($path.'/'.$file['file_name']);
            }
            $this->zip->download(slug_it(get_asset_name_by_id($id)) . '-files.zip');
            $this->zip->clear_data();
        }
    }

    public function export_asset_data($id)
    {
        if (has_permission('assets', '', 'create')) {
            $asset = $this->assets_model->get($id);
            $this->load->library('pdf');
            $members                = $this->assets_model->get_asset_members($id);
            $asset->currency_data = $this->assets_model->get_currency($id);

            // Add <br /> tag and wrap over div element every image to prevent overlaping over text
            $asset->description = preg_replace('/(<img[^>]+>(?:<\/img>)?)/i', '<br><br><div>$1</div><br><br>', $asset->description);

            $data['asset']    = $asset;
            $data['milestones'] = $this->assets_model->get_milestones($id);
            $data['timesheets'] = $this->assets_model->get_timesheets($id);

            $data['tasks']             = $this->assets_model->get_tasks($id, array(), false);
            $data['total_logged_time'] = seconds_to_time_format($this->assets_model->total_logged_time($asset->id));
            if ($asset->date_finished) {
                $data['total_days'] = round((human_to_unix($asset->date_finished . ' 00:00') - human_to_unix($asset->start_date . ' 00:00')) / 3600 / 24);
            } else {
                $data['total_days'] = '/';
            }
            $data['total_members']  = count($members);
            $data['total_tickets']  = total_rows('tbltickets', array(
                'asset_id' => $id,
            ));
            $data['total_invoices'] = total_rows('tblinvoices', array(
                'asset_id' => $id,
            ));

            $this->load->model('invoices_model');

            $data['invoices_total_data'] = $this->invoices_model->get_invoices_total(array(
                'currency' => $asset->currency_data->id,
                'asset_id' => $asset->id,
            ));

            $data['total_milestones']     = count($data['milestones']);
            $data['total_files_attached'] = total_rows('tblassetfiles', array(
                'asset_id' => $asset->id,
            ));
            $data['total_discussion']     = total_rows('tblassetdiscussions', array(
                'asset_id' => $asset->id,
            ));
            $data['members']              = $members;
            $this->load->view('admin/assets/export_data_pdf', $data);
        }
    }

    public function update_task_milestone()
    {
        if ($this->input->post()) {
            $this->assets_model->update_task_milestone($this->input->post());
        }
    }

    public function update_milestones_order(){
        if ($post_data = $this->input->post()) {
            $this->assets_model->update_milestones_order($post_data);
        }
    }

    public function pin_action($asset_id)
    {
        $this->assets_model->pin_action($asset_id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function add_edit_members($asset_id)
    {
        if (has_permission('assets', '', 'edit') || has_permission('assets', '', 'create')) {
            $this->assets_model->add_edit_members($this->input->post(), $asset_id);
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function discussions($asset_id)
    {
        if ($this->assets_model->is_member($asset_id) || has_permission('assets', '', 'view')) {
            if ($this->input->is_ajax_request()) {
                $this->app->get_table_data('asset_discussions', array(
                    'asset_id' => $asset_id,
                ));
            }
        }
    }

    public function discussion($id = '')
    {
        if ($this->input->post()) {
            $message = '';
            $success = false;
            if (!$this->input->post('id')) {
                $id = $this->assets_model->add_discussion($this->input->post());
                if ($id) {
                    $success = true;
                    $message = _l('added_successfully', _l('asset_discussion'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message,
                ));
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->assets_model->edit_discussion($data, $id);
                if ($success) {
                    $message = _l('updated_successfully', _l('asset_discussion'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message,
                ));
            }
            die;
        }
    }

    public function get_discussion_comments($id, $type)
    {
        echo json_encode($this->assets_model->get_discussion_comments($id, $type));
    }

    public function add_discussion_comment($discussion_id, $type)
    {
        echo json_encode($this->assets_model->add_discussion_comment($this->input->post(), $discussion_id, $type));
    }

    public function update_discussion_comment()
    {
        echo json_encode($this->assets_model->update_discussion_comment($this->input->post()));
    }

    public function delete_discussion_comment($id)
    {
        echo json_encode($this->assets_model->delete_discussion_comment($id));
    }

    public function delete_discussion($id)
    {
        $success = false;
        if (has_permission('assets', '', 'delete')) {
            $success = $this->assets_model->delete_discussion($id);
        }
        $alert_type = 'warning';
        $message    = _l('asset_discussion_failed_to_delete');
        if ($success) {
            $alert_type = 'success';
            $message    = _l('asset_discussion_deleted');
        }
        echo json_encode(array(
            'alert_type' => $alert_type,
            'message' => $message,
        ));
    }

    public function change_milestone_color()
    {
        if ($this->input->post()) {
            $this->assets_model->update_milestone_color($this->input->post());
        }
    }

    public function upload_file($asset_id)
    {
        handle_asset_file_uploads($asset_id);
    }

    public function change_file_visibility($id, $visible)
    {
        if ($this->input->is_ajax_request()) {
            $this->assets_model->change_file_visibility($id, $visible);
        }
    }

    public function change_activity_visibility($id, $visible)
    {
        if (has_permission('assets', '', 'create')) {
            if ($this->input->is_ajax_request()) {
                $this->assets_model->change_activity_visibility($id, $visible);
            }
        }
    }

    public function remove_file($asset_id, $id)
    {
        $this->assets_model->remove_file($id);
        redirect(admin_url('assets/view/' . $asset_id . '?group=asset_files'));
    }

    public function milestones_kanban()
    {
        $data['milestones_exclude_completed_tasks'] = $this->input->get('exclude_completed_tasks') && $this->input->get('exclude_completed_tasks') == 'yes';

        $data['asset_id'] = $this->input->get('asset_id');
        $data['milestones'] = array();

        $data['milestones'][] = array(
          'name'=>_l('milestones_uncategorized'),
          'id'=>0,
          'total_logged_time'=>$this->assets_model->calc_milestone_logged_time($data['asset_id'], 0),
          'color'=>null,
          );

        $_milestones = $this->assets_model->get_milestones($data['asset_id']);

        foreach ($_milestones as $m) {
            $data['milestones'][] = $m;
        }

        echo $this->load->view('admin/assets/milestones_kan_ban', $data, true);
    }

    public function milestones_kanban_load_more()
    {
        $milestones_exclude_completed_tasks = $this->input->get('exclude_completed_tasks') && $this->input->get('exclude_completed_tasks') == 'yes';

        $status = $this->input->get('status');
        $page   = $this->input->get('page');
        $asset_id = $this->input->get('asset_id');
        $where = array();
        if ($milestones_exclude_completed_tasks) {
            $where['status !='] = 5;
        }
        $tasks = $this->assets_model->do_milestones_kanban_query($status, $asset_id, $page, $where);
        foreach ($tasks as $task) {
            $this->load->view('admin/assets/_milestone_kanban_card', array('task'=>$task, 'milestone'=>$status));
        }
    }

    public function milestones($asset_id)
    {
        if ($this->assets_model->is_member($asset_id) || has_permission('assets', '', 'view')) {
            if ($this->input->is_ajax_request()) {
                $this->app->get_table_data('milestones', array(
                    'asset_id' => $asset_id,
                ));
            }
        }
    }

    public function milestone($id = '')
    {
        if ($this->input->post()) {
            $message = '';
            $success = false;
            if (!$this->input->post('id')) {
                $id = $this->assets_model->add_milestone($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('asset_milestone')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->assets_model->update_milestone($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('asset_milestone')));
                }
            }
        }

        redirect(admin_url('assets/view/'.$this->input->post('asset_id').'?group=asset_milestones'));
    }

    public function delete_milestone($asset_id, $id)
    {
        if (has_permission('assets', '', 'delete')) {
            if ($this->assets_model->delete_milestone($id)) {
                set_alert('deleted', 'asset_milestone');
            }
        }
        redirect(admin_url('assets/view/' . $asset_id . '?group=asset_milestones'));
    }

    public function bulk_action_files()
    {
        do_action('before_do_bulk_action_for_asset_files');
        $total_deleted = 0;
        $hasPermissionDelete = has_permission('assets', '', 'delete');
        // bulk action for assets currently only have delete button
        if ($this->input->post()) {
            $fVisibility = $this->input->post('visible_to_organization') == 'true' ? 1 : 0;
            $ids    = $this->input->post('ids');
            if (is_array($ids)) {
                foreach ($ids as $id) {
                    if ($hasPermissionDelete && $this->input->post('mass_delete') && $this->assets_model->remove_file($id)) {
                        $total_deleted++;
                    } else {
                        $this->assets_model->change_file_visibility($id, $fVisibility);
                    }
                }
            }
        }
        if ($this->input->post('mass_delete')) {
            set_alert('success', _l('total_files_deleted', $total_deleted));
        }
    }

    public function timesheets($asset_id)
    {
        if ($this->assets_model->is_member($asset_id) || has_permission('assets', '', 'view')) {
            if ($this->input->is_ajax_request()) {
                $this->app->get_table_data('timesheets', array(
                    'asset_id' => $asset_id,
                ));
            }
        }
    }

    public function timesheet()
    {
        if ($this->input->post()) {
            $message = '';
            $success = false;
            $success = $this->tasks_model->timesheet($this->input->post());
            if ($success === true) {
                $message = _l('added_successfully', _l('asset_timesheet'));
            } elseif (is_array($success) && isset($success['end_time_smaller'])) {
                $message = _l('failed_to_add_asset_timesheet_end_time_smaller');
            } else {
                $message = _l('asset_timesheet_not_updated');
            }
            echo json_encode(array(
                'success' => $success,
                'message' => $message,
            ));
            die;
        }
    }

    public function timesheet_task_assignees($task_id, $asset_id, $staff_id = 'undefined')
    {
        $assignees             = $this->tasks_model->get_task_assignees($task_id);
        $data                  = '';
        $has_permission_edit   = has_permission('assets', '', 'edit');
        $has_permission_create = has_permission('assets', '', 'edit');
        // The second condition if staff member edit their own timesheet
        if ($staff_id == 'undefined' || $staff_id != 'undefined' && (!$has_permission_edit || !$has_permission_create)) {
            $staff_id     = get_staff_user_id();
            $current_user = true;
        }
        foreach ($assignees as $staff) {
            $selected = '';
            // maybe is admin and not asset member
            if ($staff['assigneeid'] == $staff_id && $this->assets_model->is_member($asset_id, $staff_id)) {
                $selected = ' selected';
            }
            if ((!$has_permission_edit || !$has_permission_create) && isset($current_user)) {
                if ($staff['assigneeid'] != $staff_id) {
                    continue;
                }
            }
            $data .= '<option value="' . $staff['assigneeid'] . '"' . $selected . '>' . get_staff_full_name($staff['assigneeid']) . '</option>';
        }
        echo $data;
    }

    public function remove_team_member($asset_id, $staff_id)
    {
        if (has_permission('assets', '', 'edit') || has_permission('assets', '', 'create')) {
            if ($this->assets_model->remove_team_member($asset_id, $staff_id)) {
                set_alert('success', _l('asset_member_removed'));
            }
        }
        redirect(admin_url('assets/view/' . $asset_id));
    }

    public function save_note($asset_id)
    {
        if ($this->input->post()) {
            $success = $this->assets_model->save_note($this->input->post(null, false), $asset_id);
            if ($success) {
                set_alert('success', _l('updated_successfully', _l('asset_note')));
            }
            redirect(admin_url('assets/view/' . $asset_id . '?group=asset_notes'));
        }
    }

    public function delete($asset_id)
    {
        if (has_permission('assets', '', 'delete')) {
            $asset = $this->assets_model->get($asset_id);
            $success = $this->assets_model->delete($asset_id);
            if ($success) {
                set_alert('success', _l('deleted', _l('asset')));
                redirect(admin_url('assets'));
            } else {
                set_alert('warning', _l('problem_deleting', _l('asset_lowercase')));
                redirect(admin_url('assets/view/' . $asset_id));
            }
        }
    }

    public function copy($asset_id)
    {
        if (has_permission('assets', '', 'create')) {
            $id = $this->assets_model->copy($asset_id, $this->input->post());
            if ($id) {
                set_alert('success', _l('asset_copied_successfully'));
                redirect(admin_url('assets/view/' . $id));
            } else {
                set_alert('danger', _l('failed_to_copy_asset'));
                redirect(admin_url('assets/view/' . $asset_id));
            }
        }
    }

    public function mass_stop_timers($asset_id, $billable = 'false')
    {
        if (has_permission('invoices', '', 'create')) {
            $where = array(
                'billed' => 0,
                'startdate <=' => date('Y-m-d'),
            );
            if ($billable == 'true') {
                $where['billable'] = true;
            }
            $tasks                = $this->assets_model->get_tasks($asset_id, $where);
            $total_timers_stopped = 0;
            foreach ($tasks as $task) {
                $this->db->where('task_id', $task['id']);
                $this->db->where('end_time IS NULL');
                $this->db->update('tbltaskstimers', array(
                    'end_time' => time(),
                ));
                $total_timers_stopped += $this->db->affected_rows();
            }
            $message = _l('asset_tasks_total_timers_stopped', $total_timers_stopped);
            $type    = 'success';
            if ($total_timers_stopped == 0) {
                $type = 'warning';
            }
            echo json_encode(array(
                'type' => $type,
                'message' => $message,
            ));
        }
    }

    public function get_pre_invoice_asset_info($asset_id)
    {
        if (has_permission('invoices', '', 'create')) {
            $data['billable_tasks']     = $this->assets_model->get_tasks($asset_id, array(
                'billable' => 1,
                'billed' => 0,
                'startdate <=' => date('Y-m-d'),
            ));

            $data['not_billable_tasks'] = $this->assets_model->get_tasks($asset_id, array(
                'billable' => 1,
                'billed' => 0,
                'startdate >' => date('Y-m-d'),
            ));

            $data['asset_id']         = $asset_id;
            $data['billing_type']       = get_asset_billing_type($asset_id);

            $this->load->model('expenses_model');
            $this->db->where('invoiceid IS NULL');
            $data['expenses'] = $this->expenses_model->get('', array(
                'asset_id' => $asset_id,
                'billable' => 1,
            ));

            $this->load->view('admin/assets/asset_pre_invoice_settings', $data);
        }
    }

    public function get_invoice_asset_data()
    {
        if (has_permission('invoices', '', 'create')) {
            $type       = $this->input->post('type');
            $asset_id = $this->input->post('asset_id');
            // Check for all cases
            if ($type == '') {
                $type == 'single_line';
            }
            $this->load->model('payment_modes_model');
            $data['payment_modes'] = $this->payment_modes_model->get('', array(
                'expenses_only !=' => 1,
            ));
            $this->load->model('taxes_model');
            $data['taxes']      = $this->taxes_model->get();
            $data['currencies'] = $this->currencies_model->get();
            $data['base_currency'] = $this->currencies_model->get_base_currency();
            $this->load->model('invoice_items_model');

            $data['ajaxItems'] = false;
            if (total_rows('tblitems') <= ajax_on_total_items()) {
                $data['items']        = $this->invoice_items_model->get_grouped();
            } else {
                $data['items'] = array();
                $data['ajaxItems'] = true;
            }

            $data['items_groups'] = $this->invoice_items_model->get_groups();
            $data['staff']    = $this->staff_model->get('', 1);
            $asset          = $this->assets_model->get($asset_id);
            $data['asset']  = $asset;
            $items            = array();

            $asset         = $this->assets_model->get($asset_id);
            $item['id']      = 0;

            $default_tax      = unserialize(get_option('default_tax'));
            $item['taxname'] = $default_tax;

            $tasks           = $this->input->post('tasks');
            if ($tasks) {
                $item['long_description'] = '';
                $item['qty']              = 0;
                $item['task_id']          = array();
                if ($type == 'single_line') {
                    $item['description'] = $asset->name;
                    foreach ($tasks as $task_id) {
                        $task = $this->tasks_model->get($task_id);
                        $sec = $this->tasks_model->calc_task_total_time($task_id);
                        $item['long_description'] .= $task->name . ' - ' . seconds_to_time_format($sec) . ' ' . _l('hours') . "\r\n";
                        $item['task_id'][] = $task_id;
                        if ($asset->billing_type == 2) {
                            if ($sec < 60) {
                                $sec = 0;
                            }
                            $item['qty'] += sec2qty($sec);
                        }
                    }
                    if ($asset->billing_type == 1) {
                        $item['qty']  = 1;
                        $item['rate'] = $asset->asset_cost;
                    } elseif ($asset->billing_type == 2) {
                        $item['rate'] = $asset->asset_rate_per_hour;
                    }
                    $item['unit'] = '';
                    $items[]      = $item;
                } elseif ($type == 'task_per_item') {
                    foreach ($tasks as $task_id) {
                        $task                     = $this->tasks_model->get($task_id);
                        $sec = $this->tasks_model->calc_task_total_time($task_id);
                        $item['description']      = $asset->name . ' - ' . $task->name;
                        $item['qty']              = floatVal(sec2qty($sec));
                        $item['long_description'] = seconds_to_time_format($sec) . ' ' . _l('hours');
                        if ($asset->billing_type == 2) {
                            $item['rate'] = $asset->asset_rate_per_hour;
                        } elseif ($asset->billing_type == 3) {
                            $item['rate'] = $task->hourly_rate;
                        }
                        $item['task_id'] = $task_id;
                        $item['unit']    = '';
                        $items[]         = $item;
                    }
                } elseif ($type == 'timesheets_individualy') {
                    $timesheets     = $this->assets_model->get_timesheets($asset_id, $tasks);
                    $added_task_ids = array();
                    foreach ($timesheets as $timesheet) {
                        if ($timesheet['task_data']->billed == 0 && $timesheet['task_data']->billable == 1) {
                            $item['description'] = $asset->name . ' - ' . $timesheet['task_data']->name;
                            if (!in_array($timesheet['task_id'], $added_task_ids)) {
                                $item['task_id'] = $timesheet['task_id'];
                            }

                            array_push($added_task_ids, $timesheet['task_id']);

                            $item['qty']              = floatVal(sec2qty($timesheet['total_spent']));
                            $item['long_description'] = _l('asset_invoice_timesheet_start_time', _dt($timesheet['start_time'], true)) . "\r\n" . _l('asset_invoice_timesheet_end_time', _dt($timesheet['end_time'], true)) . "\r\n" . _l('asset_invoice_timesheet_total_logged_time', seconds_to_time_format($timesheet['total_spent'])) . ' ' . _l('hours');

                            if ($this->input->post('timesheets_include_notes') && $timesheet['note']) {
                                $item['long_description'] .= "\r\n\r\n" . _l('note').': ' . $timesheet['note'];
                            }

                            if ($asset->billing_type == 2) {
                                $item['rate'] = $asset->asset_rate_per_hour;
                            } elseif ($asset->billing_type == 3) {
                                $item['rate'] = $timesheet['task_data']->hourly_rate;
                            }
                            $item['unit'] = '';
                            $items[]      = $item;
                        }
                    }
                }
            }
            if ($asset->billing_type != 1) {
                $data['hours_quantity'] = true;
            }
            if ($this->input->post('expenses')) {
                if (isset($data['hours_quantity'])) {
                    unset($data['hours_quantity']);
                }
                if (count($tasks) > 0) {
                    $data['qty_hrs_quantity'] = true;
                }
                $expenses = $this->input->post('expenses');
                $addExpenseNote = $this->input->post('expenses_add_note');
                $addExpenseName = $this->input->post('expenses_add_name');

                if (!$addExpenseNote) {
                    $addExpenseNote = array();
                }

                if (!$addExpenseName) {
                    $addExpenseName = array();
                }

                $this->load->model('expenses_model');
                foreach ($expenses as $expense_id) {
                    // reset item array
                    $item                     = array();
                    $item['id']               = 0;
                    $expense                  = $this->expenses_model->get($expense_id);
                    $item['expense_id']       = $expense->expenseid;
                    $item['description']      = _l('item_as_expense') . ' ' . $expense->name;
                    $item['long_description'] = $expense->description;

                    if (in_array($expense_id, $addExpenseNote) && !empty($expense->note)) {
                        $item['long_description'] .= PHP_EOL . $expense->note;
                    }

                    if (in_array($expense_id, $addExpenseName) && !empty($expense->expense_name)) {
                        $item['long_description'] .= PHP_EOL . $expense->expense_name;
                    }

                    $item['qty']              = 1;

                    $item['taxname'] = array();
                    if ($expense->tax != 0) {
                        array_push($item['taxname'], $expense->tax_name . '|' . $expense->taxrate);
                    }
                    if ($expense->tax2 != 0) {
                        array_push($item['taxname'], $expense->tax_name2 . '|' . $expense->taxrate2);
                    }
                    $item['rate']  = $expense->amount;
                    $item['order'] = 1;
                    $item['unit']  = '';
                    $items[]       = $item;
                }
            }
            $data['organization_id']          = $asset->organizationid;
            $data['invoice_from_asset'] = true;
            $data['add_items']            = $items;
            $this->load->view('admin/assets/invoice_asset', $data);
        }
    }

    public function get_rel_asset_data($id, $task_id = '')
    {
        if ($this->input->is_ajax_request()) {
            $selected_milestone = '';
            if ($task_id != '' && $task_id != 'undefined') {
                $task               = $this->tasks_model->get($task_id);
                $selected_milestone = $task->milestone;
            }

            $allow_to_view_tasks = 0;
            $this->db->where('asset_id', $id);
            $this->db->where('name', 'view_tasks');
            $asset_settings = $this->db->get('tblassetsettings')->row();
            if ($asset_settings) {
                $allow_to_view_tasks = $asset_settings->value;
            }

            echo json_encode(array(
                'allow_to_view_tasks' => $allow_to_view_tasks,
                'billing_type' => get_asset_billing_type($id),
                'milestones' => render_select('milestone', $this->assets_model->get_milestones($id), array(
                    'id',
                    'name',
                ), 'task_milestone', $selected_milestone),
            ));
        }
    }

    public function invoice_asset($asset_id)
    {
        if (has_permission('invoices', '', 'create')) {
            $this->load->model('invoices_model');
            $data               = $this->input->post();
            $data['asset_id'] = $asset_id;
            $invoice_id         = $this->invoices_model->add($data);
            if ($invoice_id) {
                $this->assets_model->log_activity($asset_id, 'asset_activity_invoiced_asset', format_invoice_number($invoice_id));
                set_alert('success', _l('asset_invoiced_successfully'));
            }
            redirect(admin_url('assets/view/' . $asset_id . '?group=asset_invoices'));
        }
    }

    public function view_asset_as_organization($id, $organizationid)
    {
        if (is_admin()) {
            login_as_organization($organizationid);
            redirect(site_url('organizations/asset/' . $id));
        }
    }


    // Classes
    /* Get all asset classes */
    public function asset_classes()
    {
        if (!is_admin()) {
            access_denied('Asset Classes');
        }
        $data['classes'] = $this->assets_model->get_asset_class();
        $data['title']      = _l('asset_classes');
        $this->load->view('admin/assets/classes/manage', $data);
    }

    /* Add new class od update existing*/
    public function asset_class()
    {
        if (!is_admin()) {
            access_denied('Asset Classes');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->assets_model->add_asset_class($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('asset_class')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->assets_model->update_asset_class($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('asset_class')));
                }
            }
            redirect(admin_url('assets/asset_classes'));
        }
    }

    /* Delete asset class */
    public function delete_asset_class($id)
    {
        if (!is_admin()) {
            access_denied('Asset Classes');
        }
        if (!$id) {
            redirect(admin_url('assets/asset_classes'));
        }
        $response = $this->assets_model->delete_asset_class($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('asset_class_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('asset_class')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('asset_class_lowercase')));
        }
        redirect(admin_url('assets/asset_classes'));
    }


    // Categories
    /* Get all asset categories */
    public function asset_categories()
    {
        if (!is_admin()) {
            access_denied('Asset Categories');
        }
        $data['categories'] = $this->assets_model->get_asset_category();
        $data['title']      = _l('asset_categories');
        $this->load->view('admin/assets/categories/manage', $data);
    }

    /* Add new category od update existing*/
    public function asset_category()
    {
        if (!is_admin()) {
            access_denied('Asset Categories');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->assets_model->add_asset_category($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('asset_category')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->assets_model->update_asset_category($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('asset_category')));
                }
            }
            redirect(admin_url('assets/asset_categories'));
        }
    }

    /* Delete asset category */
    public function delete_asset_category($id)
    {
        if (!is_admin()) {
            access_denied('Asset Categories');
        }
        if (!$id) {
            redirect(admin_url('assets/asset_categories'));
        }
        $response = $this->assets_model->delete_asset_category($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('asset_category_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('asset_category')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('asset_category_lowercase')));
        }
        redirect(admin_url('assets/asset_categories'));
    }


    /* Get asset by id / ajax */
    public function get_asset_by_id($id)
    {
        if ($this->input->is_ajax_request()) {
            $asset                   = $this->assets_model->get($id);
            $asset->description = nl2br($asset->description);

            $asset->custom_fields_html = render_custom_fields('assets',$id,array(),array('assets_pr'=>true));
            $asset->custom_fields = array();

            $cf = get_custom_fields('assets');

            foreach($cf as $custom_field) {
                $val = get_custom_field_value($id,$custom_field['id'],'assets_pr');
                if($custom_field['type'] == 'textarea') {
                    $val = clear_textarea_breaks($val);
                }
                $custom_field['value'] = $val;
                $asset->custom_fields[] = $custom_field;
            }

            echo json_encode($asset);
        }
    }

}