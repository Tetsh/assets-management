<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transfers extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transfers_model');
    }

    /* Get all transfers in case user go on index page */
    public function index($id = '')
    {
        $this->list_transfers($id);
    }

    /* List all transfers datatables */
    public function list_transfers($id = '')
    {
        if (!has_permission('transfers', '', 'view') && !has_permission('transfers', '', 'view_own')) {
            access_denied('transfers');
        }

        $isPipeline = $this->session->userdata('transfer_pipeline') == 'true';

        $data['transfer_statuses'] = $this->transfers_model->get_statuses();
        if ($isPipeline && !$this->input->get('status') && !$this->input->get('filter')) {
            $data['title']           = _l('transfers_pipeline');
            $data['bodyclass']       = 'transfers-pipeline transfers_total_manual';
            $data['switch_pipeline'] = false;

            if (is_numeric($id)) {
                $data['transferid'] = $id;
            } else {
                $data['transferid'] = $this->session->flashdata('transferid');
            }

            $this->load->view('admin/transfers/pipeline/manage', $data);
        } else {

            // Pipeline was initiated but user click from home page and need to show table only to filter
            if ($this->input->get('status') || $this->input->get('filter') && $isPipeline) {
                $this->pipeline(0, true);
            }

            $data['transferid'] = $id;
            $data['switch_pipeline']       = true;
            $data['title']                 = _l('transfers');
            $data['bodyclass']             = 'transfers_total_manual';
            $data['transfers_years']       = $this->transfers_model->get_transfers_years();
            $data['transfers_sale_agents'] = $this->transfers_model->get_sale_agents();
            $this->load->view('admin/transfers/manage', $data);
        }
    }

    public function table($organizationid = '')
    {
        if (!has_permission('transfers', '', 'view') && !has_permission('transfers', '', 'view_own')) {
            ajax_access_denied();
        }

        $this->app->get_table_data('transfers', array(
            'organizationid' => $organizationid,
        ));
    }

    /* Add new transfer or update existing */
    public function transfer($id = '')
    {
        if (!has_permission('transfers', '', 'view') && !has_permission('transfers', '', 'view_own')) {
            access_denied('transfers');
        }

        if ($this->input->post()) {
            $transfer_data = $this->input->post();
            if ($id == '') {
                if (!has_permission('transfers', '', 'create')) {
                    access_denied('transfers');
                }
                $id = $this->transfers_model->add($transfer_data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('transfer')));
                    if ($this->set_transfer_pipeline_autoload($id)) {
                        redirect(admin_url('transfers/list_transfers/'));
                    } else {
                        redirect(admin_url('transfers/list_transfers/' . $id));
                    }
                }
            } else {
                if (!has_permission('transfers', '', 'edit')) {
                    access_denied('transfers');
                }
                $success = $this->transfers_model->update($transfer_data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('transfer')));
                }
                if ($this->set_transfer_pipeline_autoload($id)) {
                    redirect(admin_url('transfers/list_transfers/'));
                } else {
                    redirect(admin_url('transfers/list_transfers/' . $id));
                }
            }
        }
        if ($id == '') {
            $title = _l('create_new_transfer');
        } else {
            $transfer = $this->transfers_model->get($id);

            if (!$transfer || (!has_permission('transfers', '', 'view') && $transfer->addedfrom != get_staff_user_id())) {
                blank_page(_l('transfer_not_found'));
            }
            $data['transfer'] = $transfer;
            $data['edit']     = true;
            $title            = _l('edit', _l('transfer_lowercase'));
        }
        if ($this->input->get('organization_id')) {
            $data['organization_id']        = $this->input->get('organization_id');
        }

        $data['ajaxAssets'] = false;
        if (total_rows('tblassets') <= ajax_on_total_items()) {
            $data['assets']        = $this->assets_model->get_asset_classes();
        } else {
            $data['assets'] = array();
            $data['ajaxAssets'] = true;
        }
        $data['assets_groups'] = $this->assets_model->get_asset_classes();


        $data['staff']             = $this->staff_model->get('', 1);
        $data['transfer_statuses'] = $this->transfers_model->get_statuses();
        $data['title']             = $title;
        $this->load->view('admin/transfers/transfer', $data);
    }

    public function update_number_settings($id)
    {
        $response = array(
            'success' => false,
            'message' => '',
        );
        if (has_permission('transfers', '', 'edit')) {
            $this->db->where('id', $id);
            $this->db->update('tbltransfers', array(
                'prefix' => $this->input->post('prefix'),
            ));
            if ($this->db->affected_rows() > 0) {
                $response['success'] = true;
                $response['message'] = _l('updated_successfully', _l('transfer'));
            }
        }

        echo json_encode($response);
        die;
    }

    public function validate_transfer_number()
    {
        $isedit          = $this->input->post('isedit');
        $number          = $this->input->post('number');
        $date            = $this->input->post('date');
        $original_number = $this->input->post('original_number');
        $number          = trim($number);
        $number          = ltrim($number, '0');

        if ($isedit == 'true') {
            if ($number == $original_number) {
                echo json_encode(true);
                die;
            }
        }

        if (total_rows('tbltransfers', array(
            'YEAR(date)' => date('Y', strtotime(to_sql_date($date))),
            'number' => $number,
        )) > 0) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    public function delete_attachment($id)
    {
        $file = $this->misc_model->get_file($id);
        if ($file->staffid == get_staff_user_id() || is_admin()) {
            echo $this->transfers_model->delete_attachment($id);
        } else {
            header('HTTP/1.0 400 Bad error');
            echo _l('access_denied');
            die;
        }
    }

    /* Get all transfer data used when user click on transfer number in a datatable left side*/
    public function get_transfer_data_ajax($id, $to_return = false)
    {
        if (!has_permission('transfers', '', 'view') && !has_permission('transfers', '', 'view_own')) {
            echo _l('access_denied');
            die;
        }
        if (!$id) {
            die('No transfer found');
        }
        $transfer = $this->transfers_model->get($id);
        if (!$transfer || (!has_permission('transfers', '', 'view') && $transfer->addedfrom != get_staff_user_id())) {
            echo _l('transfer_not_found');
            die;
        }

        $transfer->date       = _d($transfer->date);
        $transfer->expirydate = _d($transfer->expirydate);
        if ($transfer->invoiceid !== null) {
            $this->load->model('invoices_model');
            $transfer->invoice = $this->invoices_model->get($transfer->invoiceid);
        }

        if ($transfer->sent == 0) {
            $template_name = 'transfer-send-to-organization';
        } else {
            $template_name = 'transfer-already-send';
        }

        $contact = $this->organizations_model->get_contact(get_primary_contact_user_id($transfer->organizationid));
        $email   = '';
        if ($contact) {
            $email = $contact->email;
        }

        $data['template']      = get_email_template_for_sending($template_name, $email);
        $data['template_name'] = $template_name;

        $this->db->where('slug', $template_name);
        $this->db->where('language', 'english');
        $template_result = $this->db->get('tblemailtemplates')->row();

        $data['template_system_name'] = $template_result->name;
        $data['template_id'] = $template_result->emailtemplateid;

        $data['template_disabled'] = false;
        if (total_rows('tblemailtemplates', array('slug'=>$data['template_name'], 'active'=>0)) > 0) {
            $data['template_disabled'] = true;
        }

        $data['activity']          = $this->transfers_model->get_transfer_activity($id);
        $data['transfer']          = $transfer;
        $data['members']           = $this->staff_model->get('', 1);
        $data['transfer_statuses'] = $this->transfers_model->get_statuses();
        $data['totalNotes'] = total_rows('tblnotes', array('rel_id'=>$id, 'rel_type'=>'transfer'));
        if ($to_return == false) {
            $this->load->view('admin/transfers/transfer_preview_template', $data);
        } else {
            return $this->load->view('admin/transfers/transfer_preview_template', $data, true);
        }
    }

    public function get_transfers_total()
    {
        if ($this->input->post()) {
            $data['totals'] = $this->transfers_model->get_transfers_total($this->input->post());

            $this->load->model('currencies_model');

            if (!$this->input->post('organization_id')) {
                $multiple_currencies = call_user_func('is_using_multiple_currencies', 'tbltransfers');
            } else {
                $multiple_currencies = call_user_func('is_organization_using_multiple_currencies', $this->input->post('organization_id'), 'tbltransfers');
            }

            if ($multiple_currencies) {
                $data['currencies'] = $this->currencies_model->get();
            }

            $data['transfers_years']       = $this->transfers_model->get_transfers_years();

            if (count($data['transfers_years']) >= 1 && $data['transfers_years'][0]['year'] != date('Y')) {
                array_unshift($data['transfers_years'], array('year'=>date('Y')));
            }

            $data['_currency'] = $data['totals']['currencyid'];
            unset($data['totals']['currencyid']);
            $this->load->view('admin/transfers/transfers_total_template', $data);
        }
    }

    public function add_note($rel_id)
    {
        if ($this->input->post() && has_permission('transfers', '', 'view') || has_permission('transfers', '', 'view_own')) {
            $this->misc_model->add_note($this->input->post(), 'transfer', $rel_id);
            echo $rel_id;
        }
    }

    public function get_notes($id)
    {
        if (has_permission('transfers', '', 'view') || has_permission('transfers', '', 'view_own')) {
            $data['notes'] = $this->misc_model->get_notes($id, 'transfer');
            $this->load->view('admin/includes/sales_notes_template', $data);
        }
    }

    public function mark_action_status($status, $id)
    {
        if (!has_permission('transfers', '', 'edit')) {
            access_denied('transfers');
        }
        $success = $this->transfers_model->mark_action_status($status, $id);
        if ($success) {
            set_alert('success', _l('transfer_status_changed_success'));
        } else {
            set_alert('danger', _l('transfer_status_changed_fail'));
        }
        if ($this->set_transfer_pipeline_autoload($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(admin_url('transfers/list_transfers/' . $id));
        }
    }

    public function send_expiry_reminder($id)
    {
        if (!has_permission('transfers', '', 'view') && !has_permission('transfers', '', 'view_own')) {
            access_denied('transfers');
        }
        $success = $this->transfers_model->send_expiry_reminder($id);
        if ($success) {
            set_alert('success', _l('sent_expiry_reminder_success'));
        } else {
            set_alert('danger', _l('sent_expiry_reminder_fail'));
        }
        if ($this->set_transfer_pipeline_autoload($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(admin_url('transfers/list_transfers/' . $id));
        }
    }

    /* Send transfer to email */
    public function send_to_email($id)
    {
        if (!has_permission('transfers', '', 'view') && !has_permission('transfers', '', 'view_own')) {
            access_denied('transfers');
        }
        $success = $this->transfers_model->send_transfer_to_organization($id, '', $this->input->post('attach_pdf'), $this->input->post('cc'));
        // In case organization use another language
        load_admin_language();
        if ($success) {
            set_alert('success', _l('transfer_sent_to_organization_success'));
        } else {
            set_alert('danger', _l('transfer_sent_to_organization_fail'));
        }
        if ($this->set_transfer_pipeline_autoload($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(admin_url('transfers/list_transfers/' . $id));
        }
    }

    /* Convert transfer to invoice */
    public function convert_to_invoice($id)
    {
        if (!has_permission('invoices', '', 'create')) {
            access_denied('invoices');
        }
        if (!$id) {
            die('No transfer found');
        }
        $draft_invoice = false;
        if ($this->input->get('save_as_draft')) {
            $draft_invoice = true;
        }
        $invoiceid = $this->transfers_model->convert_to_invoice($id, false, $draft_invoice);
        if ($invoiceid) {
            set_alert('success', _l('transfer_convert_to_invoice_successfully'));
            redirect(admin_url('invoices/list_invoices/' . $invoiceid));
        } else {
            if ($this->session->has_userdata('transfer_pipeline') && $this->session->userdata('transfer_pipeline') == 'true') {
                $this->session->set_flashdata('transferid', $id);
            }
            if ($this->set_transfer_pipeline_autoload($id)) {
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect(admin_url('transfers/list_transfers/' . $id));
            }
        }
    }

    public function copy($id)
    {
        if (!has_permission('transfers', '', 'create')) {
            access_denied('transfers');
        }
        if (!$id) {
            die('No transfer found');
        }
        $new_id = $this->transfers_model->copy($id);
        if ($new_id) {
            set_alert('success', _l('transfer_copied_successfully'));
            if ($this->set_transfer_pipeline_autoload($new_id)) {
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect(admin_url('transfers/transfer/' . $new_id));
            }
        }
        set_alert('danger', _l('transfer_copied_fail'));
        if ($this->set_transfer_pipeline_autoload($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(admin_url('transfers/transfer/' . $id));
        }
    }

    /* Delete transfer */
    public function delete($id)
    {
        if (!has_permission('transfers', '', 'delete')) {
            access_denied('transfers');
        }
        if (!$id) {
            redirect(admin_url('transfers/list_transfers'));
        }
        $success = $this->transfers_model->delete($id);
        if (is_array($success)) {
            set_alert('warning', _l('is_invoiced_transfer_delete_error'));
        } elseif ($success == true) {
            set_alert('success', _l('deleted', _l('transfer')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('transfer_lowercase')));
        }
        redirect(admin_url('transfers/list_transfers'));
    }

    public function clear_acceptance_info($id)
    {
        if (is_admin()) {
            $this->db->where('id', $id);
            $this->db->update('tbltransfers', get_acceptance_info_array(true));
        }

        redirect(admin_url('transfers/list_transfers/'.$id));
    }

    /* Generates transfer PDF and senting to email  */
    public function pdf($id)
    {
        if (!has_permission('transfers', '', 'view') && !has_permission('transfers', '', 'view_own')) {
            access_denied('transfers');
        }
        if (!$id) {
            redirect(admin_url('transfers/list_transfers'));
        }
        $transfer        = $this->transfers_model->get($id);
        $transfer_number = format_transfer_number($transfer->id);

        try {
            $pdf             = transfer_pdf($transfer);
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            if (strpos($message, 'Unable to get the size of the image') !== false) {
                show_pdf_unable_to_get_image_size_error();
            }
            die;
        }

        $type            = 'D';
        if ($this->input->get('print')) {
            $type = 'I';
        }
        $pdf->Output(mb_strtoupper(slug_it($transfer_number)) . '.pdf', $type);
    }

    // Pipeline
    public function get_pipeline()
    {
        if (has_permission('transfers', '', 'view') || has_permission('transfers', '', 'view_own')) {
            $data['transfer_statuses'] = $this->transfers_model->get_statuses();
            $this->load->view('admin/transfers/pipeline/pipeline', $data);
        }
    }

    public function pipeline_open($id)
    {
        if (has_permission('transfers', '', 'view') || has_permission('transfers', '', 'view_own')) {
            $data['id']       = $id;
            $data['transfer'] = $this->get_transfer_data_ajax($id, true);
            $this->load->view('admin/transfers/pipeline/transfer', $data);
        }
    }

    public function update_pipeline()
    {
        if (has_permission('transfers', '', 'edit')) {
            $this->transfers_model->update_pipeline($this->input->post());
        }
    }

    public function pipeline($set = 0, $manual = false)
    {
        if ($set == 1) {
            $set = 'true';
        } else {
            $set = 'false';
        }
        $this->session->set_userdata(array(
            'transfer_pipeline' => $set,
        ));
        if ($manual == false) {
            redirect(admin_url('transfers/list_transfers'));
        }
    }

    public function pipeline_load_more()
    {
        $status = $this->input->get('status');
        $page   = $this->input->get('page');

        $transfers = $this->transfers_model->do_kanban_query($status, $this->input->get('search'), $page, array(
            'sort_by' => $this->input->get('sort_by'),
            'sort' => $this->input->get('sort'),
        ));

        foreach ($transfers as $transfer) {
            $this->load->view('admin/transfers/pipeline/_kanban_card', array(
                'transfer' => $transfer,
                'status' => $status,
            ));
        }
    }

    public function set_transfer_pipeline_autoload($id)
    {
        if ($id == '') {
            return false;
        }
        if ($this->session->has_userdata('transfer_pipeline') && $this->session->userdata('transfer_pipeline') == 'true') {
            $this->session->set_flashdata('transferid', $id);

            return true;
        }

        return false;
    }

    public function get_due_date()
    {
        if ($this->input->post()) {
            $date    = $this->input->post('date');
            $duedate = '';
            if (get_option('transfer_due_after') != 0) {
                $date    = to_sql_date($date);
                $d       = date('Y-m-d', strtotime('+' . get_option('transfer_due_after') . ' DAY', strtotime($date)));
                $duedate = _d($d);
                echo $duedate;
            }
        }
    }
}
