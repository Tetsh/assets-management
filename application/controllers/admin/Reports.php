<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Reports extends Admin_controller
{
    /**
     * Codeigniter Instance
     * Expenses detailed report filters use $ci
     * @var object
     */
    private $ci;

    public function __construct()
    {
        parent::__construct();
        if (!has_permission('reports','', 'view')) {
            access_denied('reports');
        }
        $this->ci = &get_instance();
        $this->load->model('reports_model');
    }

    /* No access on this url */
    public function index()
    {
        redirect(admin_url());
    }

    /* See knowledge base article reports*/
    public function knowledge_base_articles()
    {
        $this->load->model('knowledge_base_model');
        $data['groups'] = $this->knowledge_base_model->get_kbg();
        $data['title']  = _l('kb_reports');
        $this->load->view('admin/reports/knowledge_base_articles', $data);
    }


    /* Sales reportts */
    public function sales()
    {
        $data['mysqlVersion'] = $this->db->query('SELECT VERSION() as version')->row();
        $data['sqlMode'] = $this->db->query('SELECT @@sql_mode as mode')->row();

        $this->load->model('invoices_model');
        $this->load->model('transfers_model');
        $this->load->model('assets_model');

        $data['invoice_statuses']      = $this->invoices_model->get_statuses();
        $data['transfer_statuses']     = $this->transfers_model->get_statuses();
        $data['asset_statuses']        = $this->assets_model->get_asset_statuses();
        $data['asset_categories']        = $this->assets_model->get_asset_category();
        $data['asset_classes']        = $this->assets_model->get_asset_class();
        $data['payments_years']        = $this->reports_model->get_distinct_payments_years();

        $data['invoice_taxes'] = $this->distinct_taxes('invoice');
        $data['transfer_taxes'] = $this->distinct_taxes('transfer');


        $data['title']                 = _l('sales_reports');
        $this->load->view('admin/reports/sales', $data);
    }

    /* Customer report */
    public function organizations_report()
    {
        if ($this->input->is_ajax_request()) {
            $select = array(
                get_sql_select_organization_company(),
                '(SELECT COUNT(organizationid) FROM tblassets WHERE tblassets.organizationid = tblorganizations.userid)',
                '(SELECT SUM(asset_cost) FROM tblassets WHERE tblassets.organizationid = tblorganizations.userid)',
            );

            $custom_date_select = $this->get_where_report_period_start();
            if ($custom_date_select != '') {
                $i = 0;
                foreach ($select as $_select) {
                    if ($i !== 0) {
                        $_temp = substr($_select, 0, -1);
                        $_temp .= ' ' . $custom_date_select . ')';
                        $select[$i] = $_temp;
                    }
                    $i++;
                }
            }

            $aColumns     = $select;
            $sIndexColumn = "userid";
            $sTable       = 'tblorganizations';
            $where        = array();

            $result  = data_tables_init($aColumns, $sIndexColumn, $sTable, array(), $where, array(
                'userid'
            ));
            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
            foreach ($rResult as $aRow) {
                $row = array();
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($i == 0) {
                        $_data = '<a href="' . admin_url('organizations/organization/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                    } elseif ($aColumns[$i] == $select[2] || $aColumns[$i] == $select[2]) {
                        if ($_data == null) {
                            $_data = 0;
                        }
                        $_data = format_money($_data);
                    }
                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
                $x++;
            }
            echo json_encode($output);
            die();
        }
    }



    /* Customer report */
    public function classes_report()
    {
        if ($this->input->is_ajax_request()) {
            $select = array(
                'name',
                '(SELECT COUNT(class_id) FROM tblassets WHERE tblassets.class_id = tblassetclass.id)',
                '(SELECT SUM(asset_cost) FROM tblassets WHERE tblassets.class_id = tblassetclass.id)',
            );

            $custom_date_select = $this->get_where_report_period_start();
            if ($custom_date_select != '') {
                $i = 0;
                foreach ($select as $_select) {
                    if ($i !== 0) {
                        $_temp = substr($_select, 0, -1);
                        $_temp .= ' ' . $custom_date_select . ')';
                        $select[$i] = $_temp;
                    }
                    $i++;
                }
            }

            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tblassetclass';
            $where        = array();

            $result  = data_tables_init($aColumns, $sIndexColumn, $sTable, array(), $where, array(
                'id'
            ));
            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
            foreach ($rResult as $aRow) {
                $row = array();
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($i == 0) {
                        $_data = '<a href="' . admin_url('organizations/organization/' . $aRow['id']) . '" target="_blank">' . $aRow['name'] . '</a>';
                    } elseif ($aColumns[$i] == $select[2] || $aColumns[$i] == $select[2]) {
                        if ($_data == null) {
                            $_data = 0;
                        }
                        $_data = format_money($_data);
                    }
                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
                $x++;
            }
            echo json_encode($output);
            die();
        }
    }

 public function assets_analysis()
    {
        $this->load->helper('date_helper');

        if ($this->input->is_ajax_request()) {
            $select = array(
                'tblassetclass.name',
                'tblassetclass.id',
                'tblassetclass.code'
            );

            $dates              = array();
            $custom_date_select = $this->get_where_report_period_start();
            if ($custom_date_select != '') {
                array_push($dates, $custom_date_select);
            }
            if ($dates) {
                $from = substr($dates[0], 25, -19); 
                $last = substr($dates[0], 42, -2);
               }             

            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tblassetclass';
            $join         = array();
            $where        = array();
            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(

            ));

            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
          //  die(print_r($result['rResult']));
              foreach ($rResult as $aRow) {
                 $row = array();

                $assets = $this->assets_model->get_assets($aRow['tblassetclass.code']);
                    $curr_depratiaiton_cost=0;
                    $asset_cost=0;
                    $from_date           = new DateTime($from);
                    $to_date             = new DateTime($last);
                    $interval            = $to_date->diff($from_date);
                    $date_diff           =$interval->format('%a');
                    //die(print_r($date_diff));
                        foreach ($assets as $asset) {                    
                            $data['asset_finish_day']        = date('Y-m-d', strtotime(+$date_diff.'DAYS', strtotime($from)));
                           //die(print_r($data['asset_finish_day']));
                            $data['asset_total_days']        = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - human_to_unix($from . ' 00:00')) / 3600 / 24);

                            $data['asset_days_left']         = $data['asset_total_days'];
                            $data['asset_time_left_percent'] = 100;
                            if ($data['asset_finish_day']) {
                                if (human_to_unix($from. ' 00:00') < time() && human_to_unix($data['asset_finish_day'] . ' 00:00') > time()) {
                                    $data['asset_days_left']         = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - time()) / 3600 / 24);
                                    $data['asset_time_left_percent'] = $data['asset_days_left'] / $data['asset_total_days'] * 100;
                                    $data['asset_time_left_percent'] = round($data['asset_time_left_percent'], 2);
                                }
                                if (human_to_unix($data['asset_finish_day'] . ' 00:00') < time()) {
                                    $data['asset_days_left']         = 0;
                                    $data['asset_time_left_percent'] = 0;
                                }
                            }

                            $data['asset_cost_per_day']         = $asset['asset_cost']/ $data['asset_total_days'];
                            $data['asset_current_cost']         = $data['asset_cost_per_day'] * $data['asset_days_left'];
                            $data['asset_total_depratiation_cost']         =  $asset['asset_cost'] - $data['asset_current_cost'];
                            $asset_cost +=$asset['asset_cost'];
                            $curr_depratiaiton_cost += $data['asset_current_cost']; 

                         }    
                        

                $row[] = $aRow['tblassetclass.name'];
                
                $row[] = $asset_cost;
                $row[] = $curr_depratiaiton_cost;
                $row[] = $asset_cost-$curr_depratiaiton_cost;

                $row[] = 1;

                $output['aaData'][] = $row;
            }
            echo json_encode($output);
            die();
        }
    }

     public function assets_depreciation_analysis()
    {
        $this->load->model('assets_model');
        $this->load->helper('date_helper');
        if ($this->input->is_ajax_request()) {
           $select = array(
                'tblassetclass.id',
                'tblassetclass.name',
                 'tblassetclass.code',
            );

            $dates              = array();
            $custom_date_select = $this->get_where_report_period_start();
            if ($custom_date_select != '') {
                array_push($dates, $custom_date_select);
            }
            if ($dates) {
                $from = substr($dates[0], 25, -19); 
                $last = substr($dates[0], 42, -2);
               }             

            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tblassetclass';
            $join         = array();
            $where        = array();
            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(

            ));

            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
          //  die(print_r($result['rResult']));
              foreach ($rResult as $aRow) {
                 $row = array();

                $assets = $this->assets_model->get_assets($aRow['tblassetclass.code']);
                    $acc_depratiaiton=0;
                    foreach ($assets as $asset) {                    
                            $data['asset_finish_day']        = date('Y-m-d', strtotime('+ 10 YEAR', strtotime($asset['start_date'])));
                            $data['asset_total_days']        = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - human_to_unix($asset['start_date'] . ' 00:00')) / 3600 / 24);

                            $data['asset_days_left']         = $data['asset_total_days'];
                            $data['asset_time_left_percent'] = 100;
                            if ($data['asset_finish_day']) {
                                if (human_to_unix($asset['start_date']. ' 00:00') < time() && human_to_unix($data['asset_finish_day'] . ' 00:00') > time()) {
                                    $data['asset_days_left']         = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - time()) / 3600 / 24);
                                    $data['asset_time_left_percent'] = $data['asset_days_left'] / $data['asset_total_days'] * 100;
                                    $data['asset_time_left_percent'] = round($data['asset_time_left_percent'], 2);
                                }
                                if (human_to_unix($data['asset_finish_day'] . ' 00:00') < time()) {
                                    $data['asset_days_left']         = 0;
                                    $data['asset_time_left_percent'] = 0;
                                }
                            }

                            $data['asset_cost_per_day']         = $asset['asset_cost']/ $data['asset_total_days'];
                            $data['asset_current_cost']         = $data['asset_cost_per_day'] * $data['asset_days_left'];
                            $data['asset_total_depratiation_cost']         =  $asset['asset_cost'] - $data['asset_current_cost'];

                            // $data['asset_depratiation_percent'] = $data['asset_current_cost'] / $asset['asset_cost'] * 100;
                            // $data['asset_depratiation_percent'] = round($data['asset_depratiation_percent'], 2);

                            // $percent_depratiaiton        = $data['asset_depratiation_percent'] / 100;
                            // $data['asset_depratiation_percent'] = $percent_depratiaiton;
                            $acc_depratiaiton = $acc_depratiaiton+$data['asset_total_depratiation_cost'];
                         }
                 if ($dates) {
                     $curr_depratiaiton=0;
                    $from_date           = new DateTime($from);
                    $to_date             = new DateTime($last);
                    $interval            = $to_date->diff($from_date);
                    $date_diff           =$interval->format('%a');
                    //die(print_r($date_diff));
                        foreach ($assets as $asset) {                    
                            $data['asset_finish_day']        = date('Y-m-d', strtotime(+$date_diff.'DAYS', strtotime($from)));
                           //die(print_r($data['asset_finish_day']));
                            $data['asset_total_days']        = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - human_to_unix($from . ' 00:00')) / 3600 / 24);

                            $data['asset_days_left']         = $data['asset_total_days'];
                            $data['asset_time_left_percent'] = 100;
                            if ($data['asset_finish_day']) {
                                if (human_to_unix($from. ' 00:00') < time() && human_to_unix($data['asset_finish_day'] . ' 00:00') > time()) {
                                    $data['asset_days_left']         = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - time()) / 3600 / 24);
                                    $data['asset_time_left_percent'] = $data['asset_days_left'] / $data['asset_total_days'] * 100;
                                    $data['asset_time_left_percent'] = round($data['asset_time_left_percent'], 2);
                                }
                                if (human_to_unix($data['asset_finish_day'] . ' 00:00') < time()) {
                                    $data['asset_days_left']         = 0;
                                    $data['asset_time_left_percent'] = 0;
                                }
                            }

                            $data['asset_cost_per_day']         = $asset['asset_cost']/ $data['asset_total_days'];
                            $data['asset_current_cost']         = $data['asset_cost_per_day'] * $data['asset_days_left'];
                            $data['asset_total_depratiation_cost']         =  $asset['asset_cost'] - $data['asset_current_cost'];

                            // $data['asset_depratiation_percent'] = $data['asset_current_cost'] / $asset['asset_cost'] * 100;
                            // $data['asset_depratiation_percent'] = round($data['asset_depratiation_percent'], 2);

                            // $percent_depratiaiton        = $data['asset_depratiation_percent'] / 100;
                            // $data['asset_depratiation_percent'] = $percent_depratiaiton;
                            $curr_depratiaiton = $curr_depratiaiton+ $data['asset_total_depratiation_cost'];
                         }    
                       } 

                $row[] = $aRow['tblassetclass.name'];
                
                $row[] = $acc_depratiaiton;
          if ($dates) {
                $row[] = $curr_depratiaiton;
                $row[] = $acc_depratiaiton-$curr_depratiaiton;
             }else{
                 $row[] = $acc_depratiaiton;
                 $row[] = $acc_depratiaiton-$acc_depratiaiton;
             }

                $output['aaData'][] = $row;
            }
            echo json_encode($output);
            die();
        }
    }

     public function assets_transfer()
    {
         $this->load->helper('date_helper');

        if ($this->input->is_ajax_request()) {
            $select = array(
                'tblassets_in.class_id',
                'tblassets_in.code',
                'tblassets_in.name',
                'tblassets_in.reason',
                'tblassetclass.name',
                'tbltransfers.datesend',
                'tblassets_in.original_cost',
                'tblassets_in.original_start_date',
            );

            $where              = array();
            $custom_date_select = $this->get_where_report_period_start('datesend');
            if ($custom_date_select != '') {
                array_push($where, ' AND rel_type = "transfer" ');
                array_push($where, $custom_date_select);
            }         
//die(print_r($where));
            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tblassets_in';
            $join         = array('JOIN tblassetclass ON tblassetclass.code = tblassets_in.class_id',
                                  'JOIN tbltransfers ON tbltransfers.id = tblassets_in.rel_id');
            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(

            ));

            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
          // die(print_r($result['rResult']));
              foreach ($rResult as $aRow) {
                 $row = array();

                            $data['asset_finish_day']        = date('Y-m-d', strtotime('+ 10 YEAR', strtotime($aRow['tblassets_in.original_start_date'])));
                            $data['asset_total_days']        = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - human_to_unix($aRow['tblassets_in.original_start_date'] . ' 00:00')) / 3600 / 24);

                            $data['asset_days_left']         = $data['asset_total_days'];
                            $data['asset_time_left_percent'] = 100;
                            if ($data['asset_finish_day']) {
                                if (human_to_unix($aRow['tblassets_in.original_start_date']. ' 00:00') < time() && human_to_unix($data['asset_finish_day'] . ' 00:00') > time()) {
                                    $data['asset_days_left']         = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - time()) / 3600 / 24);
                                    $data['asset_time_left_percent'] = $data['asset_days_left'] / $data['asset_total_days'] * 100;
                                    $data['asset_time_left_percent'] = round($data['asset_time_left_percent'], 2);
                                }
                                if (human_to_unix($data['asset_finish_day'] . ' 00:00') < time()) {
                                    $data['asset_days_left']         = 0;
                                    $data['asset_time_left_percent'] = 0;
                                }
                            }

                            $data['asset_cost_per_day']         = $aRow['tblassets_in.original_cost']/ $data['asset_total_days'];
                            $data['asset_current_cost']         = $data['asset_cost_per_day'] * $data['asset_days_left'];
                            $data['asset_total_depratiation_cost']         =  $aRow['tblassets_in.original_cost'] - $data['asset_current_cost'];

                $row[] = $aRow['tblassets_in.code'];                
                $row[] = $aRow['tblassets_in.name'];
                $row[] = $aRow['tblassetclass.name'];
                $row[] = $aRow['tblassets_in.reason'];
                $row[] = $aRow['tbltransfers.datesend'];
                $row[] = $aRow['tblassets_in.original_cost'];
                $row[] = $data['asset_total_depratiation_cost'] ;
                $row[] =  $aRow['tblassets_in.original_cost']-$data['asset_total_depratiation_cost'];

                $output['aaData'][] = $row;
            }
            echo json_encode($output);
            die();
        }
    }

      public function assets_scrap()
    {
         $this->load->helper('date_helper');

        if ($this->input->is_ajax_request()) {
            $select = array(
                'tblassets_scrap.class_id',
                'tblassets_scrap.code',
                'tblassets_scrap.name',
                'tblassets_scrap.reason',
                'tblassetclass.name',
                'tbltransfers.datesend',
                'tblassets_scrap.original_cost',
                'tblassets_scrap.original_start_date',
            );

            $where              = array();
            $custom_date_select = $this->get_where_report_period_start('datesend');
            if ($custom_date_select != '') {
                array_push($where, ' AND rel_type = "transfer" ');
                array_push($where, $custom_date_select);
            }         
//die(print_r($where));
            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tblassets_scrap';
            $join         = array('JOIN tblassetclass ON tblassetclass.code = tblassets_scrap.class_id',
                                  'JOIN tbltransfers ON tbltransfers.id = tblassets_scrap.rel_id');
            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(

            ));

            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
          // die(print_r($result['rResult']));
              foreach ($rResult as $aRow) {
                 $row = array();

                            $data['asset_finish_day']        = date('Y-m-d', strtotime('+ 10 YEAR', strtotime($aRow['tblassets_scrap.original_start_date'])));
                            $data['asset_total_days']        = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - human_to_unix($aRow['tblassets_scrap.original_start_date'] . ' 00:00')) / 3600 / 24);

                            $data['asset_days_left']         = $data['asset_total_days'];
                            $data['asset_time_left_percent'] = 100;
                            if ($data['asset_finish_day']) {
                                if (human_to_unix($aRow['tblassets_scrap.original_start_date']. ' 00:00') < time() && human_to_unix($data['asset_finish_day'] . ' 00:00') > time()) {
                                    $data['asset_days_left']         = round((human_to_unix($data['asset_finish_day'] . ' 00:00') - time()) / 3600 / 24);
                                    $data['asset_time_left_percent'] = $data['asset_days_left'] / $data['asset_total_days'] * 100;
                                    $data['asset_time_left_percent'] = round($data['asset_time_left_percent'], 2);
                                }
                                if (human_to_unix($data['asset_finish_day'] . ' 00:00') < time()) {
                                    $data['asset_days_left']         = 0;
                                    $data['asset_time_left_percent'] = 0;
                                }
                            }

                            $data['asset_cost_per_day']         = $aRow['tblassets_scrap.original_cost']/ $data['asset_total_days'];
                            $data['asset_current_cost']         = $data['asset_cost_per_day'] * $data['asset_days_left'];
                            $data['asset_total_depratiation_cost']         =  $aRow['tblassets_scrap.original_cost'] - $data['asset_current_cost'];

                $row[] = $aRow['tblassets_scrap.code'];                
                $row[] = $aRow['tblassets_scrap.name'];
                $row[] = $aRow['tblassetclass.name'];
                $row[] = $aRow['tblassets_scrap.reason'];
                $row[] = $aRow['tbltransfers.datesend'];
                $row[] = $aRow['tblassets_scrap.original_cost'];
                $row[] = $data['asset_total_depratiation_cost'] ;
                $row[] =  $aRow['tblassets_scrap.original_cost']-$data['asset_total_depratiation_cost'];

                $output['aaData'][] = $row;
            }
            echo json_encode($output);
            die();
        }
    }



    public function payments_received()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('payment_modes_model');
            $online_modes = $this->payment_modes_model->get_online_payment_modes(true);
            $select       = array(
                'tblinvoicepaymentrecords.id',
                'tblinvoicepaymentrecords.date',
                'invoiceid',
                get_sql_select_organization_company(),
                'paymentmode',
                'transactionid',
                'note',
                'amount'
            );
            $where        = array(
                'AND status != 5'
            );

            $custom_date_select = $this->get_where_report_period('tblinvoicepaymentrecords.date');
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);

            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tblinvoicepaymentrecords';
            $join         = array(
                'JOIN tblinvoices ON tblinvoices.id = tblinvoicepaymentrecords.invoiceid',
                'LEFT JOIN tblorganizations ON tblorganizations.userid = tblinvoices.organizationid',
                'LEFT JOIN tblinvoicepaymentsmodes ON tblinvoicepaymentsmodes.id = tblinvoicepaymentrecords.paymentmode'
            );

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
                'number',
                'organizationid',
                'tblinvoicepaymentsmodes.name',
                'tblinvoicepaymentsmodes.id as paymentmodeid',
                'paymentmethod'
            ));

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data['total_amount'] = 0;
            foreach ($rResult as $aRow) {
                $row = array();
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($aColumns[$i] == 'paymentmode') {
                        $_data = $aRow['name'];
                        if (is_null($aRow['paymentmodeid'])) {
                            foreach ($online_modes as $online_mode) {
                                if ($aRow['paymentmode'] == $online_mode['id']) {
                                    $_data = $online_mode['name'];
                                }
                            }
                        }
                        if (!empty($aRow['paymentmethod'])) {
                            $_data .= ' - ' . $aRow['paymentmethod'];
                        }
                    } elseif ($aColumns[$i] == 'tblinvoicepaymentrecords.id') {
                        $_data = '<a href="' . admin_url('payments/payment/' . $_data) . '" target="_blank">' . $_data . '</a>';
                    } elseif ($aColumns[$i] == 'tblinvoicepaymentrecords.date') {
                        $_data = _d($_data);
                    } elseif ($aColumns[$i] == 'invoiceid') {
                        $_data = '<a href="' . admin_url('invoices/list_invoices/' . $aRow[$aColumns[$i]]) . '" target="_blank">' . format_invoice_number($aRow['invoiceid']) . '</a>';
                    } elseif ($i == 3) {
                        $_data = '<a href="' . admin_url('organizations/organization/' . $aRow['organizationid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                    } elseif ($aColumns[$i] == 'amount') {
                        $footer_data['total_amount'] += $_data;
                        $_data = format_money($_data, $currency_symbol);
                    }

                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
            }

            $footer_data['total_amount'] = format_money($footer_data['total_amount'], $currency_symbol);
            $output['sums']              = $footer_data;
            echo json_encode($output);
            die();
        }
    }


    public function transfers_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('transfers_model');

            $transferTaxes = $this->distinct_taxes('transfer');
            $totalTaxesColumns = count($transferTaxes);

            $select = array(
                'number',
                get_sql_select_organization_company(),
                'invoiceid',
                'YEAR(date) as year',
                'date',
                'expirydate',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                'reference_no',
                'status'
            );

            $transfersTaxesSelect = array_reverse($transferTaxes);

            foreach($transfersTaxesSelect as $key => $tax){
                 array_splice($select, 9, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),'.get_decimal_places().')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),'.get_decimal_places().')
                    ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),'.get_decimal_places().')
                    END
                    FROM tblitems_in
                    INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
                    WHERE tblitems_in.rel_type="transfer" AND taxname="'.$tax['taxname'].'" AND taxrate="'.$tax['taxrate'].'" AND tblitems_in.rel_id=tbltransfers.id) as total_tax_single_'.$key);
            }

            $where              = array();
            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('transfer_status')) {
                $statuses  = $this->input->post('transfer_status');
                $_statuses = array();
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $status);
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            if ($this->input->post('sale_agent_transfers')) {
                $agents  = $this->input->post('sale_agent_transfers');
                $_agents = array();
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $agent);
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }
            $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);

            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tbltransfers';
            $join         = array(
                'JOIN tblorganizations ON tblorganizations.userid = tbltransfers.organizationid'
            );

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
                'userid',
                'organizationid',
                'tbltransfers.id',
                'discount_percent'
            ));

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = array(
                'total' => 0,
                'subtotal' => 0,
                'total_tax' => 0,
                'discount_total' => 0,
                'adjustment' => 0
            );

            foreach($transferTaxes as $key => $tax){
                $footer_data['total_tax_single_'.$key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = array();

                $row[] = '<a href="' . admin_url('transfers/list_transfers/' . $aRow['id']) . '" target="_blank">' . format_transfer_number($aRow['id']) . '</a>';

                $row[] = '<a href="' . admin_url('organizations/organization/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';

                if($aRow['invoiceid'] === null){
                    $row[] = '';
                } else {
                    $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['invoiceid']) . '" target="_blank">' . format_invoice_number($aRow['invoiceid']) . '</a>';
                }

                $row[] = $aRow['year'];

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['expirydate']);

                $row[] = format_money($aRow['subtotal'],$currency_symbol);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = format_money($aRow['total'],$currency_symbol);
                $footer_data['total'] += $aRow['total'];

                $row[] = format_money($aRow['total_tax'],$currency_symbol);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach($transferTaxes as $tax){
                    $row[] = format_money(($aRow['total_tax_single_'.$t] == null ? 0 : $aRow['total_tax_single_'.$t]),$currency_symbol);
                    $footer_data['total_tax_single_'.$i] += ($aRow['total_tax_single_'.$t] == null ? 0 : $aRow['total_tax_single_'.$t]);
                    $t--;
                    $i++;
                }

                $row[] = format_money($aRow['discount_total'],$currency_symbol);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = format_money($aRow['adjustment'],$currency_symbol);
                $footer_data['adjustment'] += $aRow['adjustment'];


                $row[] = $aRow['reference_no'];

                $row[] = format_transfer_status($aRow['status']);

                $output['aaData'][] = $row;
            }
            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = format_money($total, $currency_symbol);
            }
            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }



    public function assets_register_report()
     {
        if ($this->input->is_ajax_request()) {
            $this->load->model('assets_model');

            $select = array(
                'tblassets.id as id',
                'tblassets.name',
                'tblassets.location',
                get_sql_select_organization_company(),
                'tblassetclass.name',
                'tblassetcategory.name',
                'start_date',
                'tbldepartments.name',
                'asset_cost',
                'status',
            );

            $where              = array();
            $custom_date_select = $this->get_where_report_period_start();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }


            if ($this->input->post('asset_status')) {
                $statuses  = $this->input->post('asset_status');
                $_statuses = array();
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $status);
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }



            if ($this->input->post('asset_class')) {
                $classes  = $this->input->post('asset_class');
                $_classes = array();
                if (is_array($classes)) {
                    foreach ($classes as $class) {
                        if ($class != '') {
                            array_push($_classes, $class);
                        }
                    }
                }
                if (count($_classes) > 0) {
                    array_push($where, 'AND class_id IN (' . implode(', ', $_classes) . ')');
                }
            }

            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tblassets';
            $join         = array(
                'JOIN tblorganizations ON tblorganizations.userid = tblassets.organizationid',
                'JOIN tblassetclass ON tblassetclass.id = tblassets.class_id',
                'JOIN tblassetcategory ON tblassetcategory.code = tblassets.category_id',
                'JOIN tbldepartments ON tbldepartments.departmentid = tblassets.department_id',
            );

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
                'organizationid'
            ));

            $output  = $result['output'];
            $rResult = $result['rResult'];


            foreach ($rResult as $aRow) {
                $row = array();

                $row[] = '<a href="' . admin_url('assets/view/' . $aRow['id']) . '">' . $aRow['id'] . '</a>';

                $row[] = '<a href="' . admin_url('assets/view/' . $aRow['id']) . '">' . $aRow['tblassets.name'] . '</a>';

                $row[] = '<a href="' . admin_url('assets/view/' . $aRow['id']) . '">' . $aRow['tblassets.location'] . '</a>';

                $row[] = '<a href="' . admin_url('organizations/organization/' . $aRow['organizationid']) . '">' . $aRow['company'] . '</a>';

                $row[] = $aRow['tblassetclass.name'];

                $row[] = $aRow['tblassetcategory.name'];
                
                $row[] = _d($aRow['start_date']);

                $row[] = $aRow['tbldepartments.name'];

                $row[] = $aRow['asset_cost'];

                $status = get_asset_status_by_id($aRow['status']);

                $row[] = '<span class="label label inline-block asset-status-' . $aRow['status'] . '" style="color:'.$status['color'].';border:1px solid '.$status['color'].'">' . $status['name'] . '</span>';

                $output['aaData'][] = $row;
            }

            echo json_encode($output);
            die();
        }
    }




    private function get_where_report_period($field = 'date')
    {
        $months_report      = $this->input->post('report_months');
        $custom_date_select = '';
        if ($months_report != '') {
            if (is_numeric($months_report)) {
                // Last month
               if($months_report == '1'){
                   $beginMonth = date('Y-m-01', strtotime("-$months_report MONTH"));
                   $endMonth   = date('Y-m-t', strtotime('-1 MONTH'));
               } else {
                   $months_report = (int) $months_report;
                   $months_report--;
                   $beginMonth = date('Y-m-01', strtotime("-$months_report MONTH"));
                   $endMonth   = date('Y-m-t');
               }
               $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $beginMonth . '" AND "' . $endMonth . '")';
            } elseif($months_report == 'this_month'){
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-t') . '")';
            } elseif($months_report == 'this_year'){
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d',strtotime(date('Y-01-01'))) .
                '" AND "' .
                date('Y-m-d',strtotime(date('Y-12-31'))) . '")';
            } elseif($months_report == 'last_year'){
             $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d',strtotime(date(date('Y',strtotime('last year')).'-01-01'))) .
                '" AND "' .
                date('Y-m-d',strtotime(date(date('Y',strtotime('last year')). '-12-31'))) . '")';
            } elseif ($months_report == 'custom') {
                $from_date = to_sql_date($this->input->post('report_from'));
                $to_date   = to_sql_date($this->input->post('report_to'));
                if ($from_date == $to_date) {
                    $custom_date_select = 'AND ' . $field . ' = "' . $from_date . '"';
                } else {
                    $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $from_date . '" AND "' . $to_date . '")';
                }
            }
        }

        return $custom_date_select;
    }

    private function get_where_report_period_start($field = 'start_date')
    {
        $months_report      = $this->input->post('report_months');
        $custom_date_select = '';
        if ($months_report != '') {
            if (is_numeric($months_report)) {
                // Last month
               if($months_report == '1'){
                   $beginMonth = date('Y-m-01', strtotime("-$months_report MONTH"));
                   $endMonth   = date('Y-m-t', strtotime('-1 MONTH'));
               } else {
                   $months_report = (int) $months_report;
                   $months_report--;
                   $beginMonth = date('Y-m-01', strtotime("-$months_report MONTH"));
                   $endMonth   = date('Y-m-t');
               }
               $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $beginMonth . '" AND "' . $endMonth . '")';
            } elseif($months_report == 'this_month'){
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-t') . '")';
            } elseif($months_report == 'this_year'){
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d',strtotime(date('Y-01-01'))) .
                '" AND "' .
                date('Y-m-d',strtotime(date('Y-12-31'))) . '")';
            } elseif($months_report == 'last_year'){
             $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d',strtotime(date(date('Y',strtotime('last year')).'-01-01'))) .
                '" AND "' .
                date('Y-m-d',strtotime(date(date('Y',strtotime('last year')). '-12-31'))) . '")';
            } elseif ($months_report == 'custom') {
                $from_date = to_sql_date($this->input->post('report_from'));
                $to_date   = to_sql_date($this->input->post('report_to'));
                if ($from_date == $to_date) {
                    $custom_date_select = 'AND ' . $field . ' = "' . $from_date . '"';
                } else {
                    $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $from_date . '" AND "' . $to_date . '")';
                }
            }
        }

        return $custom_date_select;
    }

    public function items(){
        if ($this->input->is_ajax_request()) {

            $this->load->model('currencies_model');
            $v = $this->db->query('SELECT VERSION() as version')->row();
            // 5.6 mysql version don't have the ANY_VALUE function implemented.

            if($v && strpos($v->version,'5.7') !== FALSE) {
                   $aColumns = array(
                        'ANY_VALUE(description) as description',
                        'ANY_VALUE((SUM(tblitems_in.qty))) as quantity_sold',
                        'ANY_VALUE(SUM(rate*qty)) as rate',
                        'ANY_VALUE(AVG(rate*qty)) as avg_price'
                    );
            } else {
                    $aColumns = array(
                        'description as description',
                        '(SUM(tblitems_in.qty)) as quantity_sold',
                        'SUM(rate*qty) as rate',
                        'AVG(rate*qty) as avg_price'
                    );
            }

            $sIndexColumn = "id";
            $sTable       = 'tblitems_in';
            $join         = array('JOIN tblinvoices ON tblinvoices.id = tblitems_in.rel_id');

            $where = array('AND rel_type="invoice"','AND status != 5','AND status=2');

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }
            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            if ($this->input->post('sale_agent_items')) {
                $agents  = $this->input->post('sale_agent_items');
                $_agents = array();
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $agent);
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);
            $result  = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(),"GROUP by description");

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = array(
                'total_amount' => 0,
                'total_qty' => 0,
            );

            foreach ($rResult as $aRow) {
                $row = array();

                $row[] = $aRow['description'];
                $row[] = $aRow['quantity_sold'];
                $row[] = format_money($aRow['rate'],$currency_symbol);
                $row[] = format_money($aRow['avg_price'],$currency_symbol);
                $footer_data['total_amount'] += $aRow['rate'];
                $footer_data['total_qty'] += $aRow['quantity_sold'];
                $output['aaData'][] = $row;

            }

            $footer_data['total_amount'] = format_money($footer_data['total_amount'],$currency_symbol);

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }


    public function invoices_report()
    {
        if ($this->input->is_ajax_request()) {
            $invoice_taxes = $this->distinct_taxes('invoice');
            $totalTaxesColumns = count($invoice_taxes);

            $this->load->model('currencies_model');
            $this->load->model('invoices_model');

            $select = array(
                'number',
                get_sql_select_organization_company(),
                'YEAR(date) as year',
                'date',
                'duedate',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                '(SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.invoice_id=tblinvoices.id) as credits_applied',
                '(SELECT total - (SELECT COALESCE(SUM(amount),0) FROM tblinvoicepaymentrecords WHERE invoiceid = tblinvoices.id) - (SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.invoice_id=tblinvoices.id))',
                'status'
            );

            $where  = array(
                'AND status != 5'
            );

            $invoiceTaxesSelect = array_reverse($invoice_taxes);

            foreach($invoiceTaxesSelect as $key => $tax){
                 array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),'.get_decimal_places().')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),'.get_decimal_places().')
                    ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),'.get_decimal_places().')
                    END
                    FROM tblitems_in
                    INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
                    WHERE tblitems_in.rel_type="invoice" AND taxname="'.$tax['taxname'].'" AND taxrate="'.$tax['taxrate'].'" AND tblitems_in.rel_id=tblinvoices.id) as total_tax_single_'.$key);
            }

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('sale_agent_invoices')) {
                $agents  = $this->input->post('sale_agent_invoices');
                $_agents = array();
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $agent);
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency = $this->input->post('report_currency');
            $totalPaymentsColumnIndex = (12+$totalTaxesColumns-1);

            if ($by_currency) {
                $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
                $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
                $select[$totalPaymentsColumnIndex] = $_temp;

                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
                $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
            }

            $currency_symbol = $currency->symbol;

            if ($this->input->post('invoice_status')) {
                $statuses  = $this->input->post('invoice_status');
                $_statuses = array();
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $status);
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            $aColumns     = $select;
            $sIndexColumn = "id";
            $sTable       = 'tblinvoices';
            $join         = array(
                'JOIN tblorganizations ON tblorganizations.userid = tblinvoices.organizationid'
            );

            $result  = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
                'userid',
                'organizationid',
                'tblinvoices.id',
                'discount_percent'
            ));

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = array(
                'total' => 0,
                'subtotal' => 0,
                'total_tax' => 0,
                'discount_total' => 0,
                'adjustment' => 0,
                'applied_credits' => 0,
                'amount_open' => 0
            );

            foreach($invoice_taxes as $key => $tax){
                $footer_data['total_tax_single_'.$key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = array();

                $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['id']) . '" target="_blank">' . format_invoice_number($aRow['id']) . '</a>';

                $row[] = '<a href="' . admin_url('organizations/organization/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';

                $row[] = $aRow['year'];

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['duedate']);

                $row[] = format_money($aRow['subtotal'],$currency_symbol);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = format_money($aRow['total'],$currency_symbol);
                $footer_data['total'] += $aRow['total'];

                $row[] = format_money($aRow['total_tax'],$currency_symbol);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach($invoice_taxes as $tax){
                    $row[] = format_money(($aRow['total_tax_single_'.$t] == null ? 0 : $aRow['total_tax_single_'.$t]),$currency_symbol);
                    $footer_data['total_tax_single_'.$i] += ($aRow['total_tax_single_'.$t] == null ? 0 : $aRow['total_tax_single_'.$t]);
                    $t--;
                    $i++;
                }

                $row[] = format_money($aRow['discount_total'],$currency_symbol);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = format_money($aRow['adjustment'],$currency_symbol);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[] = format_money($aRow['credits_applied'],$currency_symbol);
                $footer_data['applied_credits'] += $aRow['credits_applied'];

                $amountOpen = $aRow['amount_open'];
                $row[] = format_money($amountOpen,$currency_symbol);
                $footer_data['amount_open'] += $amountOpen;

                $row[] = format_invoice_status($aRow['status']);

                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = format_money($total, $currency_symbol);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function expenses($type = 'simple_report')
    {

        $this->load->model('currencies_model');
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['currencies']    = $this->currencies_model->get();


        $data['title'] = _l('expenses_report');
        if ($type != 'simple_report') {
            $this->load->model('assets_model');
            $data['categories'] = $this->assets_model->get_asset_class();
            $data['years']      = $this->assets_model->get_assets_years();

            if ($this->input->is_ajax_request()) {
                $aColumns = array(
                    'class_id',
                    'asset_cost',
                    'tblassets.name',
                    'start_date',
                    get_sql_select_organization_company()
                );
                $join     = array(
                    'LEFT JOIN tblorganizations ON tblorganizations.userid = tblassets.organizationid',
                    'LEFT JOIN tblassetclass ON tblassetclass.code = tblassets.class_id'
                );
                $where    = array();
                $filter   = array();
                include_once(APPPATH . 'views/admin/tables/includes/expenses_filter.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                $sIndexColumn = "id";
                $sTable       = 'tblassets';
                $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
                    'tblassetclass.name as category_name',
                    'tblassets.id',
                    'tblassets.organizationid'
                ));
                $output       = $result['output'];
                $rResult      = $result['rResult'];

                $footer_data = array(
                    'asset_cost' => 0,
                    'total_tax' => 0,
                );

                foreach ($rResult as $aRow) {
                    $row = array();
                    for ($i = 0; $i < count($aColumns); $i++) {
                        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                            $_data = $aRow[strafter($aColumns[$i], 'as ')];
                        } else {
                            $_data = $aRow[$aColumns[$i]];
                        }
                        if ($aColumns[$i] == 'class_id') {
                            $_data = '<a href="' . admin_url('expenses/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['category_name'] . '</a>';
                        } else if($aColumns[$i] == 'name') {
                            $_data = '<a href="' . admin_url('expenses/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['tblassets.name'] . '</a>';
                        } elseif ($aColumns[$i] == 'asset_cost' || $i == 6) {
                            $total = $_data;
                            if ($i != 6) {
                                $footer_data['asset_cost'] += $total;
                            } else {
                                $footer_data['asset_cost'] += $total;
                            }

                            $_data = format_money($total);
                        } elseif ($i == 9) {
                            $_data = '<a href="' . admin_url('organizations/organization/' . $aRow['organizationid']) . '">' . $aRow['company'] . '</a>';
                        } elseif ($aColumns[$i] == 'start_date') {
                            $_data = _d($_data);
                        } 

                        $row[] = $_data;
                    }
                    $output['aaData'][] = $row;
                }

                foreach ($footer_data as $key => $total) {
                    $footer_data[$key] = format_money($total);
                }

                $output['sums'] = $footer_data;
                echo json_encode($output);
                die;
            }
            $this->load->view('admin/reports/expenses_detailed', $data);
        } else {
            if (!$this->input->get('year')) {
                $data['current_year'] = date('Y');
            } else {
                $data['current_year'] = $this->input->get('year');
            }


            $data['export_not_supported'] = ($this->agent->browser() == 'Internet Explorer' || $this->agent->browser() == 'Spartan');

            $this->load->model('expenses_model');

            $data['chart_not_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('not_billable_expenses_by_categories'), array(
                'billable' => 0
            ), array(
                'backgroundColor' => 'rgba(252,45,66,0.4)',
                'borderColor' => '#fc2d42'
            ), $data['current_year']));

            $data['chart_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('billable_expenses_by_categories'), array(
                'billable' => 1
            ), array(
                'backgroundColor' => 'rgba(37,155,35,0.2)',
                'borderColor' => '#84c529'
            ), $data['current_year']));

            $data['expense_years']      = $this->assets_model->get_assets_years();

            if(count($data['expense_years']) > 0) {
                // Perhaps no expenses in new year?
                if(!in_array_multidimensional($data['expense_years'],'year',date('Y'))) {
                    array_unshift($data['expense_years'], array('year'=>date('Y')));
                }
            }

            $data['categories'] = $this->assets_model->get_asset_class();

            $this->load->view('admin/reports/expenses', $data);
        }
    }

public function assets_consuladated($type = 'simple_report'){

        $this->load->model('currencies_model');
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['currencies']    = $this->currencies_model->get();


        $data['title'] = _l('reports_sales_dt_assets_Consuladated_report');

            if (!$this->input->get('year')) {
                $data['current_year'] = date('Y');
            } else {
                $data['current_year'] = $this->input->get('year');
            }


            $data['export_not_supported'] = ($this->agent->browser() == 'Internet Explorer' || $this->agent->browser() == 'Spartan');

            $this->load->model('expenses_model');

            $data['chart_not_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('not_billable_expenses_by_categories'), array(
                'billable' => 0
            ), array(
                'backgroundColor' => 'rgba(252,45,66,0.4)',
                'borderColor' => '#fc2d42'
            ), $data['current_year']));

            $data['chart_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('billable_expenses_by_categories'), array(
                'billable' => 1
            ), array(
                'backgroundColor' => 'rgba(37,155,35,0.2)',
                'borderColor' => '#84c529'
            ), $data['current_year']));

            $data['expense_years']      = $this->assets_model->get_assets_years('conslated');
            $data['expense_years_count']      = $this->assets_model->count_assets_years();
            $data['categories'] = $this->assets_model->get_asset_class();
          //die(print_r($data['categories']));

            if(count($data['expense_years']) > 0) {
                // Perhaps no expenses in new year?
                if(!in_array_multidimensional($data['expense_years'],'year',date('Y'))) {
                  array_push($data['expense_years'], array('year'=>date('Y')));
                }
            $data['categories']['cost']=array();
            if ($data['categories']) {
                foreach ($data['categories'] as $category) {
                    foreach ($data['expense_years'] as $year) {
                        if ($category['code']) {
                            $data['categories']['costs'] = $this->assets_model->get_cost_by_year($year['year'],$category['code']); 
                        }
                            array_push($data['categories']['cost'],  $data['categories']['costs']);
                        }
                    }
              }
              // die(print_r($data['categories']['cost']));
            }


             $this->load->view('admin/reports/consuladated', $data);
        }

// public function assets_consuladated($type = 'simple_report'){
//      $this->load->model('currencies_model');
//          $data['base_currency'] = $this->currencies_model->get_base_currency();
//          $data['currencies']    = $this->currencies_model->get();

//      if (!$this->input->get('year')) {
//                 $data['current_year'] = date('Y');
//             } else {
//                 $data['current_year'] = $this->input->get('year');
//             }


//             $data['export_not_supported'] = ($this->agent->browser() == 'Internet Explorer' || $this->agent->browser() == 'Spartan');

//             $this->load->model('expenses_model');

//             $data['chart_not_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('not_billable_expenses_by_categories'), array(
//                 'billable' => 0
//             ), array(
//                 'backgroundColor' => 'rgba(252,45,66,0.4)',
//                 'borderColor' => '#fc2d42'
//             ), $data['current_year']));

//             $data['chart_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('billable_expenses_by_categories'), array(
//                 'billable' => 1
//             ), array(
//                 'backgroundColor' => 'rgba(37,155,35,0.2)',
//                 'borderColor' => '#84c529'
//             ), $data['current_year']));

//             $data['expense_years']      = $this->assets_model->get_assets_years('conslated');

//             $data['expense_years_count']      = $this->assets_model->count_assets_years();

//             if(count($data['expense_years']) > 0) {
//                 // Perhaps no expenses in new year?
//                 if(!in_array_multidimensional($data['expense_years'],'year',date('Y'))) {
//                      array_push($data['expense_years'], array('year'=>date('Y')));
//                 }
//             }

//             $data['categories'] = $this->assets_model->get_asset_class();
//             $this->load->view('admin/reports/consuladated', $data);
//        }
           


    public function assets($type = 'simple_report')
    {

        $this->load->model('currencies_model');
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['currencies']    = $this->currencies_model->get();


        $data['title'] = _l('assets_report');
        if ($type != 'simple_report') {
            $this->load->model('assets_model');
            $data['classes'] = $this->assets_model->get_asset_class();
            $data['years']      = $this->assets_model->get_assets_years();

            if ($this->input->is_ajax_request()) {
                $aColumns = array(
                    'class_id',
                    'asset_cost',
                    'tblassets.name',
                    'start_date',
                    get_sql_select_organization_company()
                );
                $join     = array(
                    'LEFT JOIN tblorganizations ON tblorganizations.userid = tblassets.organizationid',
                    'LEFT JOIN tblassetclass ON tblassetclass.id = tblassets.class_id'
                );
                $where    = array();
                $filter   = array();
                include_once(APPPATH . 'views/admin/reports/assets_filter.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                $sIndexColumn = "id";
                $sTable       = 'tblassets';
                $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, array(
                    'tblassetclass.name as category_name',
                    'tblassets.id',
                    'tblassets.organizationid'
                ));
                $output       = $result['output'];
                $rResult      = $result['rResult'];

                $footer_data = array(
                    'asset_cost' => 0,
                    'total_tax' => 0,
                );

                foreach ($rResult as $aRow) {
                    $row = array();
                    for ($i = 0; $i < count($aColumns); $i++) {
                        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                            $_data = $aRow[strafter($aColumns[$i], 'as ')];
                        } else {
                            $_data = $aRow[$aColumns[$i]];
                        }
                        if ($aColumns[$i] == 'class_id') {
                            $_data = '<a href="' . admin_url('assets/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['category_name'] . '</a>';
                        } else if($aColumns[$i] == 'name') {
                            $_data = '<a href="' . admin_url('assets/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['tblassets.name'] . '</a>';
                        } elseif ($aColumns[$i] == 'asset_cost' || $i == 6) {
                            $total = $_data;
                            if ($i != 6) {
                                $footer_data['asset_cost'] += $total;
                            } else {
                                $footer_data['asset_cost'] += $total;
                            }

                            $_data = format_money($total);
                        } elseif ($i == 9) {
                            $_data = '<a href="' . admin_url('organizations/organization/' . $aRow['organizationid']) . '">' . $aRow['company'] . '</a>';
                        } elseif ($aColumns[$i] == 'start_date') {
                            $_data = _d($_data);
                        } 

                        $row[] = $_data;
                    }
                    $output['aaData'][] = $row;
                }

                foreach ($footer_data as $key => $total) {
                    $footer_data[$key] = format_money($total);
                }

                $output['sums'] = $footer_data;
                echo json_encode($output);
                die;
            }
            $this->load->view('admin/reports/assets_detailed', $data);
        } else {
            if (!$this->input->get('year')) {
                $data['current_year'] = date('Y');
            } else {
                $data['current_year'] = $this->input->get('year');
            }


            $data['export_not_supported'] = ($this->agent->browser() == 'Internet Explorer' || $this->agent->browser() == 'Spartan');

            $this->load->model('expenses_model');

            $data['chart_not_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('not_billable_expenses_by_categories'), array(
                'billable' => 0
            ), array(
                'backgroundColor' => 'rgba(252,45,66,0.4)',
                'borderColor' => '#fc2d42'
            ), $data['current_year']));

            $data['chart_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('billable_expenses_by_categories'), array(
                'billable' => 1
            ), array(
                'backgroundColor' => 'rgba(37,155,35,0.2)',
                'borderColor' => '#84c529'
            ), $data['current_year']));

            $data['asset_years']      = $this->assets_model->get_assets_years();

            if(count($data['asset_years']) > 0) {
                // Perhaps no expenses in new year?
                if(!in_array_multidimensional($data['asset_years'],'year',date('Y'))) {
                    array_unshift($data['asset_years'], array('year'=>date('Y')));
                }
            }

            $data['classes'] = $this->assets_model->get_asset_class();
            $data['years']      = $this->assets_model->get_assets_years();
            $this->load->view('admin/reports/assets', $data);
        }
    }


    public function expenses_vs_income($year = '')
    {
        $_expenses_years = array();
        $_years          = array();
        $this->load->model('expenses_model');
        $expenses_years = $this->expenses_model->get_expenses_years();
        $payments_years = $this->reports_model->get_distinct_payments_years();

        foreach ($expenses_years as $y) {
            array_push($_years, $y['year']);
        }
        foreach ($payments_years as $y) {
            array_push($_years, $y['year']);
        }

        $_years                                  = array_map("unserialize", array_unique(array_map("serialize", $_years)));

        if(!in_array(date('Y'), $_years)) {
            $_years[] = date('Y');
        }

        rsort($_years, SORT_NUMERIC);
        $data['report_year'] = $year == '' ? date('Y') : $year;

        $data['years']                           = $_years;
        $data['chart_expenses_vs_income_values'] = json_encode($this->reports_model->get_expenses_vs_income_report($year));
        $data['title']                           = _l('als_expenses_vs_income');
        $this->load->view('admin/reports/expenses_vs_income', $data);
    }

    /* Total income report / ajax chart*/
    public function total_income_report()
    {
        echo json_encode($this->reports_model->total_income_report());
    }

    public function report_by_payment_modes()
    {
        echo json_encode($this->reports_model->report_by_payment_modes());
    }

    public function report_by_organization_groups()
    {
        echo json_encode($this->reports_model->report_by_organization_groups());
    }

    /* Leads conversion monthly report / ajax chart*/
    public function leads_monthly_report($month)
    {
        echo json_encode($this->reports_model->leads_monthly_report($month));
    }

    private function distinct_taxes($rel_type){
        return $this->db->query("SELECT DISTINCT taxname,taxrate FROM tblitemstax WHERE rel_type='".$rel_type."' ORDER BY taxname ASC")->result_array();
    }
}
